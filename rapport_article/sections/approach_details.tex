\addtocounter{tocdepth}{3}
\setcounter{secnumdepth}{3}
\newcommand{\probun}{$P(x \in C_1)$}
\newcommand{\probo}{$P(x \in C_0)$}
\newcommand{\car}{caractéristique}
\label{approach_details}
Here I describe how we approached the problem: static analysis, CFG analysis, dataflow\\
This section focuses only on describing the theory of our approach. For discussion on it interests and limitations, see section \ref{future_limitations}. We will first describe how to analysis a binary, then how we compare two pieces of code.\\
Generalities: \textbf{static analysis only}, \textbf{graph comparison},\textbf{ dataflow analysis}.\\
Analysis:
\begin{itemize}
    \item Binary loading: RADARE2 ; other use IDApro
    \item Disassembly \& CFG building: CAPSTONE to recursively disassemble the x86 instructions
    \item Function delimitation: RADARE2 heuristics \& NUCLEUS
    \item Sites chopping within functions
    \item \textbf{Dataflow Tensor (DFT)} computation, for each site (\textbf{Supersite} creation !)
\end{itemize}
Comparison:
\begin{itemize}
    \item Supersites comparison using \textbf{DFT} comparison
    \item Function comparison using Supersites comparisons
\end{itemize}

\subsection{Binary loading}
The first step is to \textit{load} the binary and its information into the engine. For now, we deal with ELF files and x86 assembly code.\\
% INSERT FIGURE machine code -> assembly code
This step is achieved using the RADARE2 framework~\cite{radare2}.
During this step, we also gather information regarding the functions of the binary. It will be useful for the function delimitation step.

\paragraph*{On function detection during binary loading (for an ELF file)}
Most of the information of a binary's functions are located in its \textit{Symbol Table} (function's entry address, length, name). This would make things easy, so usually binaries are \textit{stripped} (by using the linux command \textit{strip}, for example), and their symbols and sections are removed.\\
However, most of the information from the Symbol Table can be recovered from the \textit{eh\_frames} section~\cite{eh_frames}. Of course, this section can also be manually stripped from the binary. In that case, we rely on RADARE2's heuristics to identify functions entry points (option $aaaa$).
Here is an example of the RADARE2 interface in C++:\\
\textit{r2} is a pipe to RADARE2 opened before loading the binary. \\\vspace*{0.3mm}
\begin{lstlisting}[language=C++]
    // ---------- Fill Sections of Binary object
    string rabin2_cmd = "iSj";
    string answer = r2.sendCmd(rabin2_cmd); 
    json answer_parsed = nlohmann::json::parse(answer); 
\end{lstlisting}
We then look at \textit{answer\textunderscore parsed} and fill the \textit{binary} object. R2 commands include ("j" is for json output): 
\begin{itemize}
    \item "iIj" : binary info (arch, compiler, class...)
    \item "iSj" : sections
    \item "iej" : entry point
    \item  "isj" : symbols
    \item "iO d/S/.text" : dumps section ".text" (hex)
    \item "aaaa" : function analysis tools
\end{itemize}
\subsection{Disassembly}
After loading the binary, we disassemble it from \textit{machine code} to \textit{assembly code}. We use \textbf{CAPSTONE} to achieve this, and their disassembly tool. We use the skeleton of a basic recursive disassembly program from section 8.2.4, p.252 of \textit{Practical Binary Analysis}, from Denis Andriesse \cite{andriesse_pba}.
We face several limitations during this disassembly step, induced by the static analysis framework. We cannot obtain the target jump of a dynamic jump (ex: \textit{jmp rax}). We cannot be sure that a \textit{ret} instruction returns correctly (i.e. that there is no \textit{call stack tampering}).\\
Still, static analysis is of interest to us because it is far less computationally expensive than dynamic analysis, where a virtual environment needs to be set up, which is interesting for the IoT framework.\\

\subsubsection{Recursive and static disassembly}
A \textbf{recursive} disassembly follows the execution flow of the binary code to go from one instruction to the next one. This shows us most of the executed code, but we might miss part of the code.\\
A \textbf{static} disassembly doesn't, and just disassembles one instruction after another. This provides better code coverage, but...

Along the recursive disassembly process, we build the \textbf{Control Flow Graph} (CFG) of the binary, with \textbf{Basic Blocks} for its vertices. An example is given Figure~\ref{fig:CFG}.

\begin{figure}[h]
    \includegraphics[width=14cm]{ ../images/example_CFG.png}
    \caption{Exemple de CFG reconstruit réduit}
    \label{fig:CFG}
\end{figure}

A \textbf{Basic Block} is a set of sequential instructions that do not modify the control flow of the program (that do not "jump"), except maybe its last one. The CFG can be defined as such: 
 CFG $:= \{\{B_i\},\{E_i\}\}$, with $B_i$ a \textit{Basic Block} and $E_i$ an \textit{Edge}.
 
\subsection{Function delimitation}
In assembly language, a function is called by the instruction \textit{call start\_addr} where \textit{start\_addr} is its \textbf{entry point}. The instruction \textit{ret} marks the end of the function and sends the execution back to the instruction following the previous \textit{call}.\\
We use the information gathered in the binary loading step about the functions' entry points, and the technique detailed in this paper~\cite{nucleus}.\\
The idea is to modify the CFG by removing the edges of \textit{CALL} instructions to obtain separated graphs. We then define a function as a convex by arc part of this new set. The process is described in figure~\ref{fig::nucleus}, extracted from~\cite{nucleus}.

\begin{figure}[h]
    \includegraphics[width=15.5cm]{ ../images/NUCLEUS_fun_detection_algo.png}
    \caption{Function delimitation algorithm - Nucleus}
    \label{fig::nucleus}
\end{figure}

Have a figure (\ref{fig::colored_cfg}) of the previous CFG, colored by function:


\begin{figure}[h]
    \includegraphics[width=15.5cm]{ ../images/example_colored_CFG.png}
    \caption{Colored CFG. One color per function. Entry point blocks are different. The starting number of each line is the number of the function it belongs to.}
    \label{fig::colored_cfg}
\end{figure}

\subsection{Sites cutting}
We have a build and extracted functions from a reduced (basic blocks) CFG. We are interested in "cutting" the functions down to fixed-length \textbf{sub-graphs}. We call those a \textbf{Site}. A Site has its own \textbf{perimeter}, that is the function in which it belongs. By cutting Sites solely within one function, we hope to give them a better semantic interest.\\
Have another example, in figure~\ref{fig::site_cutting}.

\begin{figure}[h]
    \includegraphics[width=15.5cm]{ ../images/example_CFG_sites.png}
    \caption{In orange: CFG of a function. Right of it: all the different size-3 Sites extracted from it. An oval node is a node from outside the considered graph.}
    \label{fig::site_cutting}
\end{figure}

\subsection{Dataflow analysis}
We wish to enhance the semantic information carried by our Sites. We attach to a Site some of its dataflow information. We call this added vector its \textbf{Dataflow Tensor} (DFT). Then, we define a \textbf{Supersite}, which is an element of the binary's CFG composed of:
\begin{itemize}
    \item a site
    \item its dataflow tensor
    \item its perimeter
\end{itemize}
What's the motivation behind those supersites ? By reasoning on them rather than sites, we aim to lower the False Positive score during the comparison of two binaries. Adding dataflow information and the perimeter of a site increases its quality as an element of comparison.\\
We will detail how to extract the dataflow tensor from a given Site. First:

\subsubsection{Dataflow of a sequence of instruction}
 We define our dataflow as registers taint propagation accross a set of instructions:
The \textbf{dataflow} of a sequence of instructions is a bipartite graph where the vertices are x86 registers. An edge from $A$ to $B$ means that, at the end of the sequence of instructions, the value of $B$ depends on the value of $A$ ($A$ taints $B$).

\subsubsection{Tainting rules}
Given an instruction, the following tainting rules are applied to generate the corresponding dataflow:
\begin{itemize}
\item \textbf{if} regs are read and written: every \textit{read} reg taints every \textit{written} reg
\item \textbf{else if} regs are written: every \textit{written} reg is cleared
\item \textbf{else} do nothing     
\end{itemize}
We also have some special written rules for known instructions patterns (\textit{ex: xor eax eax} : we clear \textit{eax})\\
We can concatenate two following instructions' dataflow by combining their corresponding bipartite graphs, as described. 
Yet another figure, figure~\ref{fig::inst_dataflow}.

\begin{figure}
   % \includegraphics[width=15.5cm]{ ../images/bb.tex}
    \resizebox{15.5cm}{!}{
\begin{tikzpicture}[]
  \fill[color=blue!2,line width=1pt,rounded corners=3] (-0.25,5.5) rectangle (8.25,-0.5);
  \fill[color=red!5,line width=1pt,rounded corners=3] (9.75,5.5) rectangle (18.25,-0.5);

  % Premiere ligne
  \draw[color=blueoutline,rounded corners=1] (0,4) rectangle (2,5);
  \draw[text=bluetext] (1,4.5) node{EAX};

  \draw[color=blueoutline,rounded corners=1] (3,4) rectangle (5,5);
  \draw[text=bluetext] (4,4.5) node{EBX};

  \draw[color=blueoutline,rounded corners=1] (6,4) rectangle (8,5);
  \draw[text=bluetext] (7,4.5) node{EBP};


  % Deuxieme ligne
  \draw[color=blueoutline,rounded corners=1] (0,2) rectangle (2,3);
  \draw[text=bluetext] (1,2.5) node{EAX};

  \draw[color=blueoutline,rounded corners=1] (3,2) rectangle (5,3);
  \draw[text=bluetext] (4,2.5) node{EBX};

  \draw[color=blueoutline,rounded corners=1] (6,2) rectangle (8,3);
  \draw[text=bluetext] (7,2.5) node{EBP};


  % Troisieme ligne
  \draw[color=blueoutline,rounded corners=1] (0,0) rectangle (2,1);
  \draw[text=bluetext] (1,0.5) node{EAX};

  \draw[color=blueoutline,rounded corners=1] (3,0) rectangle (5,1);
  \draw[text=bluetext] (4,0.5) node{EBX};

  \draw[color=blueoutline,rounded corners=1] (6,0) rectangle (8,1);
  \draw[text=bluetext] (7,0.5) node{EBP};

  % Instructions
  \draw[] (-2,2.5) node{\texttt{add} $ebx,eax$};
  \draw[] (-2,0.5) node{\texttt{xor} $ebp,ebp$};

  % Equivalent
  \draw[] (9,2) node{\Huge{$\Rightarrow$}};
  
  % Basic block 1
  \draw[color=redoutline,rounded corners=1] (10,4) rectangle (12,5);
  \draw[text=redtext] (11,4.5) node{EAX};

  \draw[color=redoutline,rounded corners=1] (13,4) rectangle (15,5);
  \draw[text=redtext] (14,4.5) node{EBX};

  \draw[color=redoutline,rounded corners=1] (16,4) rectangle (18,5);
  \draw[text=redtext] (17,4.5) node{EBP};

  % Basic block 2
  \draw[color=redoutline,rounded corners=1] (10,0) rectangle (12,1);
  \draw[text=redtext] (11,0.5) node{EAX};

  \draw[color=redoutline,rounded corners=1] (13,0) rectangle (15,1);
  \draw[text=redtext] (14,0.5) node{EBX};

  \draw[color=redoutline,rounded corners=1] (16,0) rectangle (18,1);
  \draw[text=redtext] (17,0.5) node{EBP};

  % Fleches a gauche
  \draw[line width=1pt, ->] (1,4) -- (1,3); %EAX -> EAX
  \draw[line width=1pt, ->] (1,4) -- (3.5,3); %EAX -> EBX
  \draw[line width=1pt, ->] (1,2) -- (1,1); %EAX -> EAX en dessous
  \draw[line width=1pt, ->] (4,2) -- (4,1); %EBX -> EBX milieu
  \draw[line width=1pt, ->] (4,4) -- (4,3); %EBX -> EBX haut milieu
  \draw[line width=1pt, ->] (7,4) -- (7,3); %EBP -> EBP


  % Fleches a droite
  \draw[line width=1pt, ->] (11,4) -- (11,1); %EAX -> EAX
  \draw[line width=1pt, ->] (11,4) -- (13.5,1); %EAX -> EBX
  \draw[line width=1pt, ->] (14,4) -- (14,1); %EAX -> EBX
\end{tikzpicture}
}
   % \scalebox{.55}{\input{../images/bb.tikz}}
    \caption{How to propagate the tainting of register through x86 instructions}
    \label{fig::inst_dataflow}
\end{figure}

\subsubsection{Register classes}
We define \textit{register classes} to soften the rules of our dataflow. We believe that:
\begin{lstlisting}
mov ebx, 0x100
cmp eab, ebx
\end{lstlisting}
Is similar to:
\begin{lstlisting}
mov ecx, 0x100
cmp eab, ecx
\end{lstlisting}
We regroup registers in the following classes: 
\begin{itemize}
\item \textbf{general purpose}
\item \textbf{general purpose 64 bit}
\item \textbf{segment}
\item \textbf{flags}
\item \textbf{EIP \& RIP}
\item \textbf{control}
\item \textbf{debug}
\item \textbf{others} (FPX, MMX, KX, STX \& EIZ)
\end{itemize}

\subsubsection{DFT of a graph}
Consider a binary $B$ and a subgraph $\mathcal{G}$ of its CFG, made of Basic Blocks $bb_i$ and edges between them $e_j$.\\
We define the dataflow of this graph as follows:\\
\begin{definition}
The \textit{Dataflow Tensor} of a graph $\mathcal{G}$ is a set of \textit{dataflows} \textit{(i.e. bipartite graphs)}, each one associated with one possible path of $\mathcal{G}$, from its entry point to any exit.\\ In other words, the dataflow tensor is the \textbf{set of the dataflows associated to all the different execution paths of $\mathcal{G}$ between its entry point to any exit}.
\end{definition}
We wish to compute the \textbf{Dataflow Tensor} of $\mathcal{G}$. The idea is to gather all the possible execution paths within $\mathcal{G}$ and then compute the dataflow of these sequences of instructions. The set of all these dataflow is the DFT of $\mathcal{G}$.\\
However, a CFG may not be acyclic, so how do we compute exhaustively its execution paths if it has loops? We extract its \textbf{strongly connected components}, build the \textbf{strongly connected component graph}, that is acyclic, and compute this graph's DFT. These steps are detailed in the slides 29, 30, 31 of the slide presentation.

\subsection{Binary comparison}
We want to compare two binaries. After the analysis, we have extracted their functions, and their functions' SuperSites. We can compare the binaries' functions by comparing their SuperSites:\\
Let's consider two SuperSites $S_1$ and $S_2$.\\
A SuperSite is a directed graph $ \left( \{BB_i, e_j\}_{i,j}, \mathcal{D} \right) \in G$, with $BB_{i}$ a Basic Block, $e_{j}$ an edge and $\mathcal{D}$ the dataflow of the SuperSite.\\
The problem is the following:
Can we build an isomophism $\Phi: G \mapsto G$,      $$ \left\{
\begin{array}{ll}
     BB &\mapsto \Phi(BB)\\
     e &\mapsto \Phi(e) \\
     \mathcal{D} &\mapsto \Phi(\mathcal{D})
\end{array}
\right.
$$  such that $S_1 = \Phi(S_2)$ ?
\begin{definition}
    Two sites are \textbf{similar} if we can build an isomorphism such as the previous one between them if we can build an isomorphism such as the previous one between them
\end{definition}
For two given functions, we can compute the \textit{inclusion score} of one into another:
\begin{definition}
    \label{def::inclusion_score}
    Let $\mathcal{P}$ be the set of the pairs of similar SuperSites that can be build between $f_1$ and $f_2$, where each SuperSite in $f_2$ can only be used once.\\
    The \textbf{inclusion score} $s_{incl}(f_1, f_2)$ of $f_1$ in $f_2$ is:
    \begin{equation}
        s_{incl}(f_1, f_2) = \frac{\#\mathcal{P}}{\#\{Sites \in  f_1\}}
    \end{equation}
\end{definition}
This score is not symmetrical.
In the next section, we will describe the different evaluation method we made to check Tangue, our implementation of this method.