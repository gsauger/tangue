\contentsline {section}{\numberline {1}Introduction}{2}{}%
\contentsline {section}{\numberline {2}Related Works}{2}{}%
\contentsline {subsection}{\numberline {2.1}My position and objectives}{3}{}%
\contentsline {section}{\numberline {3}Approach details}{3}{}%
\contentsline {subsection}{\numberline {3.1}Binary loading}{4}{}%
\contentsline {subsection}{\numberline {3.2}Disassembly}{5}{}%
\contentsline {subsubsection}{\numberline {3.2.1}Recursive and static disassembly}{5}{}%
\contentsline {subsection}{\numberline {3.3}Function delimitation}{5}{}%
\contentsline {subsection}{\numberline {3.4}Sites cutting}{6}{}%
\contentsline {subsection}{\numberline {3.5}Dataflow analysis}{6}{}%
\contentsline {subsubsection}{\numberline {3.5.1}Dataflow of a sequence of instruction}{7}{}%
\contentsline {subsubsection}{\numberline {3.5.2}Tainting rules}{7}{}%
\contentsline {subsubsection}{\numberline {3.5.3}Register classes}{8}{}%
\contentsline {subsubsection}{\numberline {3.5.4}DFT of a graph}{8}{}%
\contentsline {subsection}{\numberline {3.6}Binary comparison}{9}{}%
\contentsline {section}{\numberline {4}Evaluation}{9}{}%
\contentsline {subsection}{\numberline {4.1}Dataset}{9}{}%
\contentsline {subsection}{\numberline {4.2}Version / Optimization performance test}{10}{}%
\contentsline {subsection}{\numberline {4.3}Performance evaluation}{10}{}%
\contentsline {subsection}{\numberline {4.4}Results}{10}{}%
\contentsline {section}{\numberline {5}Future work and limitations}{11}{}%
\contentsline {section}{Appendices}{14}{}%
