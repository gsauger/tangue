#include "basic_block.hpp"

#include <sstream>
namespace boa
{

// MARK:- Constructors and destructors
BasicBlock::BasicBlock(const waddr_t& entry_point) :  m_color(0), m_is_entry_block(false)
{
  m_entry_point.push_back(entry_point);
}
BasicBlock::BasicBlock(const addr_t& entry_point) :  m_color(0), m_is_entry_block(false)
{
	waddr_t ep = {0,entry_point};
  m_entry_point.push_back(ep);
}

// MARK:- Operators overloading
bool operator==(const BasicBlock& lhs, const BasicBlock& rhs)
{
  // Compare instrs size
  if(lhs.m_instrs.size() != rhs.m_instrs.size())
  {
    return false;
  }

  // Compare each instr
  for(size_t i = 0; i < rhs.m_instrs.size(); i++)
  {
    if(lhs.m_instrs.at(i) != rhs.m_instrs.at(i))
    {
      return false;
    }
  }
  return true;
}

bool operator!=(const BasicBlock& lhs, const BasicBlock& rhs)
{
  return !(lhs == rhs);
}

// MARK:- Getters and setters


std::vector<addr_t> BasicBlock::getChildren() const{
  std::vector<addr_t> output;
  for(auto const& inst : getOutInstrs()){
    for(auto const& addr : inst->getChildren()){
      if(!isContainsInstr(addr)){
        output.push_back(addr);
      }
    }
  }
  return output;
}



// Instrs
const std::vector<uptrConstInstr>& BasicBlock::getInstrs() const
{
  return m_instrs;
}

color_t BasicBlock::getColor() const{
  return m_color;
}

void BasicBlock::setColor(color_t color) const
{
  m_color = color;
}

const bool BasicBlock::isEntryBlock() const
{
  return m_is_entry_block;
}

void BasicBlock::setEntryBlockBool(bool var) const
{
  m_is_entry_block = var;
}

bool sortbyAddr(waddr_t ep1, waddr_t ep2){
  return ep1.second < ep2.second;
}

void BasicBlock::addEntryPoint(waddr_t ep_waddr) const{
  m_entry_point.push_back(ep_waddr);
  std::sort(m_entry_point.begin(), m_entry_point.end(), sortbyAddr);
}

void BasicBlock::changeEntryPoint(waddr_t ep_waddr) const{
  m_entry_point.clear();
  m_entry_point.push_back(ep_waddr);
}


void BasicBlock::setDataflow(std::vector<BVERTEX> reg_dataflow_vector){
  m_dataflow_vector = reg_dataflow_vector;
}

std::vector<BVERTEX> BasicBlock::getDataflowVector() const{
  return m_dataflow_vector;
}


bool BasicBlock::isContainsInstr(const addr_t instr_addr) const
{
  for(const auto& instr : m_instrs)
  {
    if(instr_addr == instr->getAddr())
    {
      return true;
    }
  }
  return false;
}
 

/*
const Instr* BasicBlock::getInstr(const addr_t &instr_addr) const
{
  for(const auto& instr : m_instrs)
  {
    if(instr_addr == instr->getAddr())
    {
      return instr.get();
    }
  }
  throw std::out_of_range("Instruction at address " + addr2s(instr_addr) + " not found in basic block " + toString());
}
*/

std::unordered_set<uptrConstInstr> BasicBlock::getOutInstrs() const{
  return m_out_instr;
}

uptrConstInstr BasicBlock::getLastInstr() const
{
  if(m_instrs.empty()){
    return NULL;
  }
  return m_instrs.back();
}

const InstrType BasicBlock::getLastInstrType() const
{
  return m_instrs.back().get()->getType();
}


size_t BasicBlock::getSize() const
{
  return m_instrs.size();
}

bool sortFunction (uptrConstInstr instr1, uptrConstInstr instr2){
  return(instr1->getAddr() < instr2->getAddr());
   }

void BasicBlock::pushBackInstr(uptrConstInstr instr) const
{
  m_instrs.push_back(instr);
  //std::sort(m_instrs.begin(), m_instrs.end(), sortFunction);

  // If we push an instr, we need to clear tainting infos
  m_tainting_info = std::nullopt;
}

void BasicBlock::pushFrontInstr(uptrConstInstr instr) const
{
  m_instrs.insert(m_instrs.begin(),instr);

  // If we push an instr, we need to clear tainting infos
  m_tainting_info = std::nullopt;
}

uptrConstInstr BasicBlock::popBackInstr() const
{
  m_tainting_info = std::nullopt;
  uptrConstInstr back_instr = m_instrs.back();
  m_instrs.pop_back();
  return back_instr;
}

// Edges
/**
 * @arg waddr_t of the destination address
 * @return Pointer to the Edge object with src from the BB outgoing instructions and dst like the input
 * */
const sptrConstEdge BasicBlock::getEdge(const waddr_t& dst) const
{ 
  if(isContainsInstr(dst.second)){
    std::cerr << "Warning: BB seraching for an edeg to himself?\n";
  }
  sptrConstEdge output;
  for(auto const& inst : getOutInstrs()){
    output = inst->getEdge(dst);
    if(output != NULL ){
      return output;
    }
  }
  return NULL;
}

const std::map<waddr_t, sptrConstEdge> BasicBlock::getEdges() const
{ 
  std::map<waddr_t, sptrConstEdge> output;
  for(auto const& inst : getOutInstrs()){
    for(auto const& elt : inst->getEdges()){
      output.insert(elt);
    }
  }
  return output;
}

void BasicBlock::addOutgoingInstruction(uptrConstInstr inst){
  if(inst == NULL){
    return;
  }
  m_out_instr.insert(inst);
}


// Other
/**
 * This constructs a new bb containing the fusion of two bbs
 * @return A shared ptr to the newly formed bb
 * */
sptrBasicBlock BasicBlock::mergeBasicBlock(sptrBasicBlock bb2merge) const{
  sptrBasicBlock new_bb = std::make_shared<BasicBlock>(*this);

  for(auto const& inst : bb2merge->getInstrs()){
    new_bb->pushBackInstr(inst);
  }
  new_bb->changeEntryPoint({0,std::min(getEntryPointAddr(), bb2merge->getEntryPointAddr())});
  new_bb->ArrangeInstructionsByAddress();
  return new_bb;
}


const bb::tainting_info_t& BasicBlock::getTaintingInfo() const
{
  if(!m_tainting_info.has_value())
  {
    std::shared_ptr<spdlog::logger> log = spdlog::get(utils::tainting_logger);

    m_tainting_info = std::make_optional<bb::tainting_info_t>();

    for(const auto& instr : m_instrs)
    {
      const auto& instr_tainting_info = instr->getTaintingInfo();

      m_tainting_info->regs_read.insert(instr_tainting_info.regs_read.begin(), instr_tainting_info.regs_read.end());
      m_tainting_info->regs_write.insert(instr_tainting_info.regs_write.begin(), instr_tainting_info.regs_write.end());
      m_tainting_info->mems_read.insert(instr_tainting_info.mems_read.begin(), instr_tainting_info.mems_read.end());
      m_tainting_info->mems_write.insert(instr_tainting_info.mems_write.begin(), instr_tainting_info.mems_write.end());
    }
  }
  return *m_tainting_info;
}

const waddr_t& BasicBlock::getEntryPoint() const
{
  return m_entry_point.front();
}

const addr_t& BasicBlock::getEntryPointAddr() const
{ 
  return m_entry_point.front().second;
}

std::string BasicBlock::toString() const
{
  return waddr2s(m_entry_point.front());
}

std::string BasicBlock::printFullBB() const{
  std::stringstream f;
  for(auto const& inst : m_instrs){
    f << inst->toStringLight() << "\n";
  }
  return f.str();
}

std::string BasicBlock::printEdgesOut() const{
  std::stringstream f;
  f << "Edges out ->" << "\n";
  for(auto const& [waddr, edge] : getEdges()){
    f << waddr2s(edge->getDst()) << "\n";
  }
return f.str();
}

bool BasicBlock::isAllMicroInstrsAlreadyKnown() const
{
  for(const auto& instr : m_instrs)
  {
    if(!instr->getMicroInstrs().has_value())
    {
      return false;
    }
  }
  return true;
}

bool BasicBlock::isAddrInBB(addr_t addr) const{
  for(auto const& inst : getInstrs()){
  	if(inst->getAddr() == addr){return true;}
  }
  return false;
}

bool BasicBlock::containsReg(x86_reg reg) const{
  for(uptrConstInstr inst : getInstrs()){
    std::vector<x86_reg> inst_regs = inst->getRegisters();
    // x in v
    if(std::find(inst_regs.begin(), inst_regs.end(), reg) != inst_regs.end()){
      return true;
    }
  }
  return false;
}

size_t BasicBlock::getHash() const
{
  // Hash of a basic block = hash of all instrs
  size_t hash{};
  for(auto const& instr : this->m_instrs)
  {
    hash = hash ^ instr->getHash();
  }
  return hash;
}

// MARK:- Methods

  void BasicBlock::ArrangeInstructionsByAddress() const{
      std::sort(m_instrs.begin(), m_instrs.end(), sortFunction);
  }

  std::string BasicBlock::printDataflow() const{
    std::stringstream f;
    f << "[";
    for(BVERTEX bvertex : m_dataflow_vector){
        f << reg2str(bvertex.reg_class_name) << ": " << regVect2str(bvertex.source_regs) << " ";
    }
    f << "]";
    return f.str();
  }
/*
  void BasicBlock::removeInstDuplicate(){
    uint inst_size = m_instrs.size();
    inst1 = m_instrs[0];
    for(uint i =0; i < inst_size; i++){
      if(m_instrs[i] == inst1){
        continue;
      }
    }
  }
*/

} // namespace boa
