#include "instr.hpp"

#include <iomanip>
#include <iostream>
#include <sstream>

namespace boa
{

// MARK:- Constructors and destructors
Instr::Instr(const std::string& opcode, const std::vector<uint8_t>& bytes, const InstrType& type,
             const InstrSubType& sub_type, const addr_t& addr, unsigned int wave, const unsigned int& size,
             const addr_t& next_addr, const std::optional<addr_t>& jmp_addr, const std::optional<addr_t>& ptr_addr, const std::string& sect_name)
    : m_opcode(opcode), m_bytes(bytes), m_type(type), m_sub_type(sub_type), m_addr(addr), m_wave(wave), m_size(size),
      m_next_addr(next_addr), m_jmp_addr(jmp_addr), m_ptr_addr(ptr_addr), m_sect_name(sect_name)
{
}

Instr::Instr(const addr_t& addr, unsigned int wave) : m_addr(addr), m_wave(wave)
{
}

// MARK:- Operators overloading
std::ostream& operator<<(std::ostream& os, const Instr& instr)
{
  os << std::left << std::setfill(' ') << std::setw(15);
  os << waddr2s({instr.m_wave, instr.m_addr});

  os << std::left << std::setfill(' ') << std::setw(20);
  os << Instr::bytesVectorToString(instr.m_bytes);

  os << std::left << std::setfill(' ') << std::setw(45);
  os << instr.m_opcode;

  os << Instr::typeToString(instr.m_type);
  return os;
}

// Two instructions are different just if addresses are different
bool operator<(const Instr& lhs, const Instr& rhs)
{
  return lhs.m_addr < rhs.m_addr;
}

// Two instructions are different just if addresses are different
bool operator==(const Instr& lhs, const Instr& rhs)
{
  return lhs.m_addr == rhs.m_addr;
}

bool operator!=(const Instr& lhs, const Instr& rhs)
{
  return !(lhs == rhs);
}

// MARK:- Getters and setters
const std::string& Instr::getOpcode() const
{
  return m_opcode;
}

const std::vector<uint8_t>& Instr::getBytes() const
{
  return m_bytes;
}

addr_t Instr::getAddr() const
{
  return m_addr;
}

waddr_t Instr::getWaddr() const
{
  return {m_wave, m_addr};
}

addr_t Instr::getNextAddr() const
{
  return m_next_addr;
}

unsigned int Instr::getSize() const
{
  return m_size;
}

const addr_t Instr::getJmpAddr() const
{
	return m_edges.begin()->second->getDst().second;
	/*
  if(m_jmp_addr){
    return m_jmp_addr.value();
  }
  else{
    return 0;
  }
  */
}

void Instr::SetJmpAddr(addr_t jmp_addr) const
{
  m_jmp_addr = jmp_addr;
}


const std::optional<addr_t>& Instr::getPtrAddr() const
{
  return m_ptr_addr;
}

const InstrSubType& Instr::getSubType() const
{
  return m_sub_type;
}

const InstrType& Instr::getType() const
{
  return m_type;
}

const std::map<waddr_t, sptrConstEdge>& Instr::getEdges() const
{
  return m_edges;
}

const sptrConstEdge Instr::getEdge(const waddr_t& dst) const
{
  if(m_edges.find(dst) == m_edges.end())
  {
    //throw std::runtime_error("Edge with dst " + waddr2s(dst) + " does not exist");
    return NULL;
  }
  return m_edges.at(dst);
}

const std::vector<addr_t> Instr::getChildren() const
{
  std::vector<addr_t> neighbours;
  for(auto const& [waddr, edge] : m_edges){
      neighbours.push_back((edge->getDst()).second);
  }
    return neighbours;
}

std::vector<addr_t> Instr::getEdgesSet() const
{
  std::vector<addr_t> edges{};
  for(auto const& edge_pair : m_edges)
  {
    edges.push_back(edge_pair.first.second);
  }
  return edges;
}

const std::optional<std::vector<uptrConstMicroInstr>>& Instr::getMicroInstrs() const
{
  return m_micro_instrs;
}

std::string Instr::getBytesString() const
{
  return bytesVectorToString(m_bytes);
}

const instr::tainting_info_t& Instr::getTaintingInfo() const
{

  if(!m_tainting_info.has_value())
  {
    if(!m_micro_instrs.has_value())
    {
      throw std::runtime_error("getTaintingInfo() is only avaible with micro instrs");
    }

    m_tainting_info = std::make_optional<instr::tainting_info_t>();

    for(const auto& microinstr : *m_micro_instrs)
    {
      // Ecriture en mémoire
      if(microinstr->getMemWrite().has_value())
      {
        m_tainting_info->mems_read.insert(&(*microinstr->getMemWrite()));
      }

      // Lecture en mémoire
      if(microinstr->getMemRead().has_value())
      {
        m_tainting_info->mems_read.insert(&(*microinstr->getMemRead()));
      }

      // Written reg or memory
      std::string left_var_without_suffix = MicroInstr::removeVarSuffix(microinstr->getLeftVar());
      if(left_var_without_suffix != "memory" && isAValidReg(left_var_without_suffix))
      {
        m_tainting_info->regs_write.insert(s2reg(left_var_without_suffix));
      }

      // Read reg and mem
      for(const auto& right_var : microinstr->getRightVars())
      {
        std::string right_var_without_suffix = MicroInstr::removeVarSuffix(right_var);
        if(right_var_without_suffix != "memory" && isAValidReg(right_var_without_suffix))
        {
          m_tainting_info->regs_read.insert(s2reg(right_var_without_suffix));
        }
      }
    }
  }

  return *m_tainting_info;
}

uint8_t Instr::getOperandCount(){
  return m_op_count;  
}

void Instr::setOperandCount(uint8_t nbr_op){
  m_op_count = nbr_op;
}

std::map<x86_reg, std::string> Instr::getListOpRegAccess(){
  return m_list_op_reg_access;
}

void Instr::addOperandRegAccess(x86_reg reg, std::string access){
  m_list_op_reg_access[reg] = access;
}

// MARK:- Other functions
void Instr::addEdge(const waddr_t& dst, const EdgeFoundMethod& found_method, std::shared_ptr<spdlog::logger> log) const
{
  waddr_t src = {m_wave, m_addr};

  // If edge doesn't already exists
  
  if(m_edges.count(dst) == 0)
  {
    /*SPDLOG_LOGGER_DEBUG(log, "Create edge {} --> {} ({})\n", waddr2s(src), waddr2s(dst),
                        Edge::foundMethodToString(found_method)); */
    sptrConstEdge edge = std::make_shared<const Edge>(src, dst, found_method);
    auto out_pair = m_edges.insert({dst, edge});
    if(out_pair.second){
    SPDLOG_LOGGER_DEBUG(log, "Added edge: {:x} -> {:x}\n",src.second,  dst.second);
    }
    else{
          SPDLOG_LOGGER_DEBUG(log, "Edge: {:x} -> {:x} was not inserted by .insert\n",src.second,  dst.second);
    }
  }
  else{
    SPDLOG_LOGGER_DEBUG(log, "Edge already existed : {:x} -> {:x}\n", src.second,  dst.second);

  }

}

void Instr::addEdge(const waddr_t& dst, std::shared_ptr<spdlog::logger> log) const{
  waddr_t src = {m_wave, m_addr};

  // If edge doesn't already exists

  if(m_edges.count(dst) ==  0)
  {
    /*SPDLOG_LOGGER_DEBUG(log, "Create edge {} --> {} ({})\n", waddr2s(src), waddr2s(dst),
                        Edge::foundMethodToString(found_method)); */
    sptrConstEdge edge = std::make_shared<const Edge>(src.second, dst.second);
    auto out_pair = m_edges.insert({dst, edge});
    if(out_pair.second){
    SPDLOG_LOGGER_TRACE(log, "Added edge: {:x} -> {:x}\n",src.second,  dst.second);
    }
    else{
          SPDLOG_LOGGER_TRACE(log, "Edge: {:x} -> {:x} was not inserted by .insert\n",src.second,  dst.second);
    }
  }
	else{
   SPDLOG_LOGGER_TRACE(log, "Edge already existed : {:x} -> {:x}\n", src.second,  dst.second);
	}
}


void Instr::removeEdge(waddr_t dst, std::shared_ptr<spdlog::logger> log) const
{ 
  SPDLOG_LOGGER_DEBUG(log, "Deleted {:x} -> {:x}\n", m_addr, dst.second);
  m_edges.erase(dst);

}

std::string Instr::toString() const
{
  std::stringstream ss;
  ss << *this;
  return ss.str();
}

std::string Instr::toStringLight() const
{
  std::stringstream ss;
  //ss << "#op/#reg: " << std::to_string(m_op_count) << "/" << std::to_string(m_list_op_reg_access.size()) << " ";
  //ss << m_wave << "_0x" << std::hex << m_addr << ": ";
  ss << std::hex << m_addr << ": ";
  ss << m_opcode << " ";
  
  return ss.str();
}

void Instr::pushBackMicroInstr(uptrConstMicroInstr micro_instr) const
{
  if(!m_micro_instrs.has_value())
  {
    throw std::runtime_error("Call initMicroInstrs before addMicroInstrs");
  }
  m_micro_instrs->push_back(std::move(micro_instr));
}

void Instr::initMicroInstrs() const
{
  m_micro_instrs = std::make_optional<std::vector<uptrConstMicroInstr>>();
}

const char* Instr::typeToString(const InstrType& instr_type)
{
  switch(instr_type)
  {
  case InstrType::CALL:
    return "CALL";
  case InstrType::FAKE_LIB_FUNC:
    return "FAKE_LIB_FUNC";
  case InstrType::HLT:
    return "HLT";
  case InstrType::INT:
    return "INT";
  case InstrType::JCC:
    return "JCC";
  case InstrType::JMP:
    return "JMP";
  case InstrType::LIB_MOCK:
    return "LIB_MOCK";
  case InstrType::RET:
    return "RET";
  case InstrType::SEQ:
    return "SEQ";
  case InstrType::TRAP:
    return "TRAP";
  default:
  return "UNKNOWN";
  }
}

std::string Instr::type2String() const
{
  switch(m_type)
  {
  case InstrType::CALL:
    return "CALL";
  case InstrType::FAKE_LIB_FUNC:
    return "FAKE_LIB_FUNC";
  case InstrType::HLT:
    return "HLT";
  case InstrType::INT:
    return "INT";
  case InstrType::JCC:
    return "JCC";
  case InstrType::JMP:
    return "JMP";
  case InstrType::LIB_MOCK:
    return "LIB_MOCK";
  case InstrType::RET:
    return "RET";
  case InstrType::SEQ:
    return "SEQ";
  case InstrType::TRAP:
    return "TRAP";
  case InstrType::UNKNOWN:
    return "UNKNOWN";
  default: return "MISSINGno";
  }
}

InstrType Instr::instrSubTypeToInstrType(const InstrSubType& instr_sub_type)
{
  switch(instr_sub_type)
  {
  case InstrSubType::ADD:
  case InstrSubType::AND:
  case InstrSubType::CMP:
  case InstrSubType::DIV:
  case InstrSubType::IO:
  case InstrSubType::LEA:
  case InstrSubType::LEAVE:
  case InstrSubType::LOAD:
  case InstrSubType::MOV:
  case InstrSubType::MUL:
  case InstrSubType::NEG:
  case InstrSubType::NOP:
  case InstrSubType::NOT:
  case InstrSubType::NULL_:
  case InstrSubType::OR:
  case InstrSubType::POP:
  case InstrSubType::PUSH:
  case InstrSubType::REP:
  case InstrSubType::SHIFT:
  case InstrSubType::STORE:
  case InstrSubType::SUB:
  case InstrSubType::XOR:
  case InstrSubType::UNKNOWN:
    return InstrType::SEQ;

  case InstrSubType::JCC:
    return InstrType::JCC;

  case InstrSubType::RET:
    return InstrType::RET;

  case InstrSubType::DYN_CALL:
  case InstrSubType::STATIC_CALL:
    return InstrType::CALL;

  case InstrSubType::DYN_JMP:
  case InstrSubType::STATIC_JMP:
    return InstrType::JMP;

  case InstrSubType::TRAP:
    return InstrType::TRAP;

  case InstrSubType::FAKE_LIB_FUNC:
    return InstrType::FAKE_LIB_FUNC;
  default:
  return InstrType::UNKNOWN;
  }
}

std::string Instr::bytesVectorToString(const std::vector<uint8_t>& bytes)
{
  std::stringstream ss;

  for(uint8_t byte : bytes)
  {
    ss << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(byte);
  }

  return ss.str();
}

size_t Instr::getHash() const
{
  std::hash<addr_t> hash_fn;
  return hash_fn(m_addr) + m_wave;
}

bool Instr::is_cflow_ins(){
  if(getType() == InstrType::CALL ||
  getType() == InstrType::JCC ||
  getType() == InstrType::JMP ||
  getType() == InstrType::RET){
    return true;
  }
  return false;
}
// Registers (Dataflow)

std::vector<x86_reg> Instr::getRegisters(){
  std::vector<x86_reg> output;
  for(auto const& [reg, access] : getListOpRegAccess()){
    output.push_back(reg);
  }
  return output;
}


std::vector<x86_reg> Instr::getWrittenRegs(){
  std::vector<x86_reg> output;
  for(auto const& [reg, access] : getListOpRegAccess()){
      if(access == "CS_AC_WRITE" || access == "CS_AC_READ_WRITE"){
          output.push_back(reg);
      }
  }
  return output;
}

/**
 * @return Vector with all registers in the instr that 1) accessed and 2) WRITE or READ//WRITE
 * */
std::vector<x86_reg_class> Instr::getWrittenRegsClasses(){
  std::vector<x86_reg_class> output;
  for(auto const& reg : getWrittenRegs()){
      output.push_back(getRegClass(reg));
  }
  return output;
}

std::vector<x86_reg> Instr::getReadRegs(){
  std::vector<x86_reg> output;
  for(auto const& [reg, access] : getListOpRegAccess()){
      if(access == "CS_AC_READ"){
          output.push_back(reg);
      }
  }
  return output;
}

/**
 * @return Vector with all registers in the instr that 1) accessed and 2) READ
 * */
std::vector<x86_reg_class> Instr::getReadRegsClasses(){
  std::vector<x86_reg_class> output;
  for(auto const& reg : getReadRegs()){
      output.push_back(getRegClass(reg));
  }
  return output;
}
/*
std::vector<x86_reg> Instr::getTaintingRegs(x86_reg mod_reg){
  // temporary TODO: WHAT IS THIS ?
  std::vector<x86_reg> output;
  for(auto const& [reg, access] : getListOpRegAccess()){
      if(access == "CS_AC_READ"){
          output.push_back(reg);
      }
  }
  return output;

  // the spice; how to link a register that is modified by the instruction
  // and the registers that are read by the instruction
  // and the instruction itself 

  //TAKE INSPIRATION FROM LIBDFT PAPER
  // http://nsl.cs.columbia.edu/papers/2012/libdft.vee12.pdf
  // They have 5 classes of instructions
  // case of ALU operations: union of the two regs
  // case of r2r, i2r or m2r (reg to reg, imm to reg, mem to reg): modified is replaced by what's read
  // case of specific instructions that clear the registers: xor/sub eax eax, CPUID
  // case of special instructions... hand made, I guess...
  // and some ignored options...
}

*/

// VERSIONS with classes



} // namespace boa
