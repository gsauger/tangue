
#include "cfg_manager.hpp"

#include <sstream>

namespace boa
{

// MARK:- Getters and setters
CFG* CFGManager::createEmptyCfg(const std::vector<addr_t>& entry_points)
{
  std::unique_ptr<CFG> cfg = std::make_unique<CFG>(entry_points, m_cfgs.size());
  CFG* cfg_ptr = cfg.get();
  m_cfgs.insert({m_cfgs.size(), std::move(cfg)});
  return cfg_ptr;
}

const std::map<unsigned int, std::unique_ptr<CFG>>& CFGManager::getCFGs() const
{
  return m_cfgs;
}

std::set<const Instr*> CFGManager::getInstrsWithSubType(const InstrSubType& sub_type) const
{
  std::set<const Instr*> cfgs_instrs;
  for(const auto& cfg_p : m_cfgs)
  {
    auto cfg_instrs = cfg_p.second->getInstrsWithSubType(sub_type);
    cfgs_instrs.insert(cfg_instrs.begin(), cfg_instrs.end());
  }
  return cfgs_instrs;
}

void CFGManager::createInstrsAndEdgesListTextFile(const std::string& instrs_list_filepath,
                                                  const std::string& edges_list_filepath) const
{
  std::string instrs_list;
  std::string edges_list;
  for(const auto& cfg_p : m_cfgs)
  {
    for(auto const& instr_p : cfg_p.second->getSortedInstrs())
    {
      instrs_list += instr_p.second->toString() + "\n";
      for(const auto& edge : instr_p.second->getEdges())
      {
        edges_list += waddr2s({cfg_p.first, instr_p.first}) + " --> " + waddr2s(edge.first) + "\n";
      }
    }
  }
  utils::create_text_file(instrs_list, instrs_list_filepath);
  utils::create_text_file(edges_list, edges_list_filepath);
}

void CFGManager::createBBsListTextFile(const std::string& filepath) const
{
  std::string bbs_list;
  for(const auto& cfg_p : m_cfgs)
  {
    for(auto const& bb : cfg_p.second->getSortedBasicBlocks())
    {
      bbs_list += bb.second->toString();
      bbs_list += " (size: " + std::to_string(bb.second->getSize()) + ")";
      bbs_list += " (first instr: " + bb.second->getInstrs().at(0)->toStringLight() + ")\n";
    }
  }
  utils::create_text_file(bbs_list, filepath);
}

std::string CFGManager::createDot() const
{
  std::string dot_s = "Digraph G {\n";
  for(const auto& cfg_p : m_cfgs)
  {
    dot_s += cfg_p.second->createDotWithoutDigraph();
  }
  return dot_s + "}";
}

unsigned int CFGManager::getDisassembledInstrsSize() const
{
  unsigned int total = 0;
  for(const auto& cfg_p : m_cfgs)
  {
    total += cfg_p.second->getDisassembledInstrs().size();
  }

  return total;
}

} // namespace boa
