#include "edge.hpp"
#include <iostream>

namespace boa
{

// MARK:- Constructors and destructors
Edge::Edge(waddr_t src, waddr_t dst, EdgeFoundMethod found_method)
    : m_src(src), m_dst(dst), m_found_method(found_method)
{
}

  Edge::Edge(addr_t src, addr_t dst){
    m_src = std::make_pair(0,src);
    m_dst = std::make_pair(0,dst);
    m_found_method = EdgeFoundMethod::STATIC_DISAS;
  }


// MARK:- Getters and setters
const char* Edge::getFoundMethodString() const
{
  return Edge::foundMethodToString(m_found_method);
}

EdgeFoundMethod Edge::getFoundMethod() const
{
  return m_found_method;
}

waddr_t Edge::getSrc() const
{
  return m_src;
}

waddr_t Edge::getDst() const
{
  return m_dst;
}

// MARK:- Static functions

const char* Edge::foundMethodToString(const EdgeFoundMethod& found_method)
{
  switch(found_method)
  {
  case EdgeFoundMethod::STATIC_DISAS:
    return "STATIC_DISAS";
  case EdgeFoundMethod::BOA_DISAS:
    return "BOA_DISAS";
  }
  return "unknown mode";
}

// String

std::string Edge::toString() const{
  std::stringstream f;

  f << addr2s(m_src.second) << "->" << addr2s(m_dst.second);

  return f.str();
}


} // namespace boa
