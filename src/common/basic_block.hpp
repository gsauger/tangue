#ifndef BASIC_BLOCK_H
#define BASIC_BLOCK_H

#include <unordered_map>
#include <vector>

#include "instr.hpp"
#include "utils/boa_types.hpp"
#include "utils/utils.hpp"

namespace boa
{



namespace bb
{

struct tainting_info_t
{
  std::set<Register> regs_read;
  std::set<Register> regs_write;
  std::set<const microinstr::mem_op_t*> mems_read;
  std::set<const microinstr::mem_op_t*> mems_write;
};


} // namespace bb


class BasicBlock
{
public:
  // MARK:- Constructors and destructors
  BasicBlock(const waddr_t& entry_point);
  BasicBlock(const addr_t& entry_point);
  ~BasicBlock() = default;

  // MARK:- Operators overloading
  friend bool operator==(const BasicBlock& lhs, const BasicBlock& rhs);
  friend bool operator!=(const BasicBlock& lhs, const BasicBlock& rhs);

  // MARK:- Getters and setters

  std::vector<addr_t> getChildren() const;

  // Instrs
  const std::vector<uptrConstInstr>& getInstrs() const;
  bool isContainsInstr(const addr_t instr_addr) const;
  // const Instr* getInstr(const addr_t &instr_addr) const;
  uptrConstInstr getLastInstr() const;
  const InstrType getLastInstrType() const;
  const bool isEntryBlock() const;
  void setEntryBlockBool(bool var) const;
  bool isLastInstrAJump();
  void addEntryPoint(waddr_t ep_waddr) const;
  void changeEntryPoint(waddr_t ep_waddr) const;

  size_t getSize() const;
  void pushBackInstr(uptrConstInstr instr) const;
  void pushFrontInstr(uptrConstInstr instr) const;
  uptrConstInstr popBackInstr() const;

  void setDataflow(std::vector<BVERTEX> reg_dataflow_vector);
  std::vector<BVERTEX> getDataflowVector() const;


  // Edges
  const sptrConstEdge getEdge(const waddr_t& dst) const;
  const std::map<waddr_t, sptrConstEdge> getEdges() const;
  void addOutgoingInstruction(uptrConstInstr inst);

  // Others

  std::shared_ptr<BasicBlock> mergeBasicBlock(std::shared_ptr<BasicBlock> bb2merge) const;

  const bb::tainting_info_t& getTaintingInfo() const;
  const waddr_t& getEntryPoint() const;
  const addr_t& getEntryPointAddr() const;
  std::string toString() const;
  std::string printEdgesOut() const;
  std::string printFullBB() const;
  bool isAllMicroInstrsAlreadyKnown() const;
  size_t getHash() const;
  color_t getColor() const;
  void setColor(color_t color) const;
  bool isAddrInBB(addr_t addr) const;
  bool containsReg(x86_reg reg) const;

  void setFunName(std::string name) {m_fun_name = name;}
  std::string& getFunName() { return m_fun_name;}

  // MARK:- Methods
  void ArrangeInstructionsByAddress() const;

  std::string printDataflow() const;

  void removeInstDuplicate();

  // Outgoing instructions
  std::unordered_set<uptrConstInstr> getOutInstrs() const;


private:
  // MARK:- Private member variables
  std::vector<BVERTEX> m_dataflow_vector;

  mutable std::vector<waddr_t> m_entry_point;
  mutable uint32_t m_color;
  std::string m_fun_name;
  mutable bool m_is_entry_block;
  mutable std::vector<uptrConstInstr> m_instrs;
  mutable std::optional<bb::tainting_info_t> m_tainting_info;
  std::unordered_set<uptrConstInstr> m_out_instr; // list of the outgoing instrs of the bb
};

using sptrBasicBlock = std::shared_ptr<BasicBlock>;


} // namespace boa

#endif
