#include "micro_instr.hpp"

#include <map>

namespace boa

{

// MARK:- Constructors and destructors

// Full constructor
MicroInstr::MicroInstr(MicroInstrType type, const std::string& mnemonic, const std::string& smtlib_expr,
                       const std::string& left_var, const unSetString& right_vars, const optMemOp& mem_read,
                       const optMemOp& mem_write)
    : m_type(type), m_mnemonic(mnemonic), m_smtlib_expr(smtlib_expr), m_left_var(left_var), m_right_vars(right_vars),
      m_mem_read(mem_read), m_mem_write(mem_write)
{
  for(const auto& right_var : m_right_vars)
  {
    m_right_vars_wihtout_suffix.insert(removeVarSuffix(right_var));
  }
  m_left_var_without_suffix = removeVarSuffix(m_left_var);
}

// Var reset constructor
MicroInstr::MicroInstr(MicroInstrType type, const std::string& mnemonic, const std::string& smtlib_expr,
                       const std::string& left_var)
    : m_type(type), m_mnemonic(mnemonic), m_smtlib_expr(smtlib_expr), m_left_var(left_var)
{
  m_left_var_without_suffix = removeVarSuffix(m_left_var);
}

// MARK:- Getters and setters
// type
MicroInstrType MicroInstr::getType() const
{
  return m_type;
}

std::string const& MicroInstr::getMnemonic() const
{
  return m_mnemonic;
}

const std::string& MicroInstr::getSmtlibExpr() const
{
  return m_smtlib_expr;
}

const std::string& MicroInstr::getLeftVar() const
{
  return m_left_var;
}

const std::string& MicroInstr::getLeftVarWithoutSuffix() const
{
  return m_left_var_without_suffix;
}

const unSetString& MicroInstr::getRightVars() const
{
  return m_right_vars;
}

const unSetString& MicroInstr::getRightVarsWithoutSuffix() const
{
  return m_right_vars_wihtout_suffix;
}

const optMemOp& MicroInstr::getMemRead() const
{
  return m_mem_read;
}

const optMemOp& MicroInstr::getMemWrite() const
{
  return m_mem_write;
}

// MARK:- Other functions
std::string MicroInstr::toString() const
{
  return m_mnemonic;
}

// MARK:- Static functions

std::string MicroInstr::removeVarSuffix(const std::string& var_with_r_l_suffix)
{
  std::string last_2_char = var_with_r_l_suffix.substr(var_with_r_l_suffix.size() - 2);
  if(last_2_char == "_l" || last_2_char == "_r")
  {
    return var_with_r_l_suffix.substr(0, var_with_r_l_suffix.size() - 2);
  }
  return var_with_r_l_suffix;
}

} // namespace boa
