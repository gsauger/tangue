#include "cfg.hpp"


#include <sstream>

namespace boa
{

// MARK:- Constructor
CFG::CFG(const std::vector<addr_t>& entry_points, unsigned int wave)
{
  m_entry_points.insert(entry_points.begin(), entry_points.end());
  m_wave = wave;
}

CFG::CFG(addr_t entry_point){
  m_entry_points.insert(entry_point);
  m_wave = 0;
  m_id_addr = entry_point;
}


// MARK:- Getters and setters

std::string CFG::getAddrFunSymbol(addr_t addr){
  if(m_fun_symbols_map.count(addr)>0){
    return m_fun_symbols_map.at(addr);
  }
  else{
    return "None";
  }
}

std::set<sptrBasicBlock> CFG::getParents(sptrBasicBlock bb) const{
  std::set<sptrBasicBlock> output;
  for(auto const& bb2 : getBasicBlocks()){
    for(auto const& addr : bb2.second->getChildren()){
      if(addr == bb->getEntryPointAddr()){
        output.insert(bb2.second);
      }
    }
  }
  return output;
}

void CFG::addExitPoint(addr_t ep){
  m_exit_points.insert(ep);
}


// Basic blocks

sptrBasicBlock CFG::getBasicBlockWithEntryPoint(addr_t addr) const
{
  if(!isBasicBlockExists(addr))
  {     
    throw std::out_of_range("Basic block with entry address " + addr2s(addr) + " not found");
  }
  return m_basic_blocks.at(addr);
}

/*
sptrBasicBlock CFG::getBasicBlock(const addr_t& addr) const
{
Note:   //std::cout << "Searching for a BB at addr : " << addr2s(addr) << std::endl;

  if(!isBasicBlockExists(addr))
  {
    //std::cout << "Trying to find a BB containing the address\n" ;
    if(!isInstrExists(addr)){
      throw std::out_of_range("Basic block containing address " + addr2s(addr) + " not found");
    }
    sptrBasicBlock bb = m_instrs_bb.at(addr);
    if(bb->isAddrInBB(addr)){   
      return bb;
    }
    else{
      std::cout << "ERROR, Managed to find a bb :\n" + bb->printFullBB() + "...Containing addr (in cfg->m_instrs_bb) " + addr2s(addr) + " but the address was not in it (not in bb->getInstrs()) ..." << std::endl ;
      throw std::runtime_error("ERROR, Managed to find a bb with addr that did not contained it...");
      //throw std::runtime_error("Managed to find a bb for addr " + addr2s(addr) + " that did not contained it...");
    }
    // return m_instrs_bb.at(addr);
  }

  return m_basic_blocks.at(addr).get();
}
*/

sptrBasicBlock CFG::getBasicBlock(const addr_t& addr) const
{
  //std::cout << "Searching for a BB at addr : " << addr2s(addr) << std::endl;

  if(isBasicBlockExists(addr))
  {
    return m_basic_blocks.at(addr);
  }
  else{
    //std::cout << "Trying to find a BB containing the address\n" ;

    for(auto const& [bb_ep, bb] : m_basic_blocks){
      if(bb->isAddrInBB(addr)){
        return bb;
      }
    }

    return NULL;
    //throw std::runtime_error("ERROR, could not find a BB containing " + addr2s(addr) + "...");
  }

}

sptrBasicBlock CFG::getBasicBlockWithoutCheck(const addr_t& addr) const
{
  return m_basic_blocks.at(addr);
}

sptrBasicBlock CFG::getBasicBlock(const Instr* instr) const
{
  return getBasicBlock(instr->getAddr());
}

void CFG::removeBasicBlock(const addr_t& addr, std::shared_ptr<spdlog::logger> log)
{
  SPDLOG_LOGGER_TRACE(log, "Removed BB {:x}\n", addr);
  m_basic_blocks.erase(addr);
}

void CFG::setBasicBlockColor(const addr_t& addr, color_t color){
  if(!isBasicBlockExists(addr))
  {
    throw std::out_of_range("Basic block at address 0x" + addr2s(addr) + " not found");
  }
  sptrBasicBlock bb = m_basic_blocks.at(addr);
  bb->setColor(color);
}

std::map<addr_t, sptrBasicBlock> CFG::getSortedBasicBlocks() const
{
  std::map<addr_t, sptrBasicBlock> bbs_map;
  for(const auto& [bb_ep, bb] : m_basic_blocks)
  {
    bbs_map.insert({bb->getEntryPointAddr(), bb});
  }
  return bbs_map;
}

std::vector<sptrBasicBlock> CFG::getVectorBasicBlocks() const{
  std::vector<sptrBasicBlock> bbs_vect;
  for(const auto& [bb_ep, bb] : m_basic_blocks)
  {
    bbs_vect.push_back(bb);
  }
  return bbs_vect;
}


const std::unordered_map<addr_t, sptrBasicBlock>& CFG::getBasicBlocks() const
{
  return m_basic_blocks;
}

bool CFG::isBasicBlockExists(const addr_t& addr) const
{
  return (m_basic_blocks.count(addr) > 0);
}

bool CFG::isBasicBlockExists(const Instr* instr) const
{
  return isBasicBlockExists(instr->getAddr());
}

bool CFG::isInstrExists(const addr_t& addr) const
{
  for(auto const& [bb_ep, bb] : m_basic_blocks){
    if(bb->isAddrInBB(addr)){
      return true;
    }
  }
  return false;
}

/*
const std::map<addr_t, sptrBasicBlock> CFG::getDisassembledInstrs() const
{
  return m_instrs_bb;
}*/

size_t CFG::getNumberofBBofColor(color_t color) const{
  size_t number_of_BB = 0;
  for(auto const& BB : getBasicBlocks()){
    if(BB.second->getColor() == color){ number_of_BB++; }
  }
  return number_of_BB;
}

void CFG::insertBasicBlock(sptrBasicBlock basic_block){
  std::pair<addr_t, sptrBasicBlock> newBB (basic_block->getEntryPointAddr(), basic_block);
  m_basic_blocks.insert(newBB);
  m_id_addr = std::min(m_id_addr, newBB.first);
}

void CFG::addBasicBlock(sptrBasicBlock bb_ptr){
  // check if it already exists
  if(isBasicBlockExists(bb_ptr->getEntryPointAddr()))
  {
    return;
  }
	m_basic_blocks.emplace(bb_ptr->getEntryPointAddr(), bb_ptr);
	m_id_addr = std::min(m_id_addr, bb_ptr->getEntryPointAddr());
	return;
}

sptrBasicBlock CFG::createEmptyBB(addr_t ep_addr)
{
  waddr_t bb_ep = {0, ep_addr};
  // Check if BB already exists

  if(isBasicBlockExists(bb_ep.second))
  {
    //std::cout << "Basic block " + addr2s(bb_ep.second) + " already exists \n";
    return getBasicBlock(bb_ep.second);
  }

  // Create new BB
  sptrBasicBlock bb_ptr = std::make_shared<BasicBlock>(bb_ep);

  // Add BB in CFG

  m_basic_blocks.emplace(bb_ep.second, bb_ptr);
  m_id_addr = std::min(m_id_addr, ep_addr);

  return bb_ptr;
}


sptrBasicBlock CFG::createBB(uptrConstInstr bb_ep_instr)
{
  waddr_t bb_ep = bb_ep_instr->getWaddr();
  // Check if BB already exists

  if(isBasicBlockExists(bb_ep.second))
  {
    //std::cout << "Basic block " + addr2s(bb_ep.second) + " already exists \n";
    return getBasicBlock(bb_ep.second);
  }

  // Create new BB
  sptrBasicBlock bb_ptr = std::make_shared<BasicBlock>(bb_ep);

  // Move instr in BB
  //m_instrs_bb.emplace(bb_ep.second, bb_ptr);
  bb_ptr->pushBackInstr(bb_ep_instr);

  // Add BB in CFG

  m_basic_blocks.emplace(bb_ep.second, bb_ptr);
  m_id_addr = std::min(m_id_addr, bb_ep.second);

  return bb_ptr;
}

void CFG::pushBackInstrInBB(sptrBasicBlock bb, uptrConstInstr instr)
{
  // check first if inst is not already in BB
  if(bb->isAddrInBB(instr->getAddr())){
    return;
  }
  bb->pushBackInstr( (instr));
}

/*
void CFG::shrinkBB(std::shared_ptr<spdlog::logger> log, sptrBasicBlock bb, waddr_t first_instr_to_remove)
{
  log->info("Shrink existing BasicBlock {:#x} to remove all instruction after address {:#x} (including this one)",
            bb->getEntryPointAddr(), first_instr_to_remove.second);
  bool again = true;
  while(again)
  {
    uptrConstInstr removed_instr = bb->popBackInstr();

    // Remove instr from BB/instr map
   //m_instrs_bb.erase(removed_instr->getAddr());

    if(removed_instr->getAddr() == first_instr_to_remove.second)
    {
      again = false;
    }
  }

  // Remove edges of last BB instr
  bb->getLastInstr()->removeEdge(first_instr_to_remove);

  log->info("Modified BasicBlock at address {:#x}", bb->getEntryPointAddr());
  for(auto const& instr : bb->getInstrs())
  {
    log->info("\t{}", instr->toString());
  }
}
*/

/*
sptrBasicBlock CFG::splitBB(std::shared_ptr<spdlog::logger> log, waddr_t new_bb_ep)
{
  sptrBasicBlock existing_bb = getCorrespondingBasicBlock(new_bb_ep.second);
  log->info("Split existing BasicBlock {:#x} at address {:#x} (the new BB entry point)",
            existing_bb->getEntryPointAddr(), new_bb_ep.second);

  // Create new BB with new_bb_ep
  std::shared_ptr<const BasicBlock> new_bb_ptr = std::make_shared<const BasicBlock>(new_bb_ep);

  // Transfer instrs from existing_bb to new_bb
  bool move_instrs_to_new_bb = true;
  while(move_instrs_to_new_bb)
  {
    // Pop back instr from exiting BB
    uptrConstInstr moving_instr = existing_bb->popBackInstr();

    // Check if we are at the split point
    if(moving_instr->getAddr() == new_bb_ep.second)
    {
      move_instrs_to_new_bb = false;
    }

    // Set correct BB/instr map
    //m_instrs_bb[moving_instr->getAddr()] = new_bb_ptr;

    // Move instr in new_bb
    new_bb->pushFrontInstr( (moving_instr));
  }

  // Add new BB in CFG
  if(m_basic_blocks.find(new_bb_ep.second) != m_basic_blocks.end())
  {
    throw std::runtime_error("Basic block " + addr2s(new_bb_ep.second) + " already exists");
  }
  m_basic_blocks.insert({new_bb_ep.second,  (new_bb)});

  // Print old and new BB after split operation
  log->info("Modified BasicBlock at address {:#x}:", existing_bb->getEntryPointAddr());
  for(auto const& instr : existing_bb->getInstrs())
  {
    log->info("\t{}", instr->toString());
  }
  log->info("New BasicBlock at address {:#x}:", new_bb_ep.second);
  for(auto const& instr : new_bb_ptr->getInstrs())
  {
    log->info("\t{}", instr->toString());
  }
  return new_bb_ptr;
}
*/

sptrBasicBlock CFG::getCorrespondingBasicBlock(const addr_t& instr_addr) const
{
  return getBasicBlock(instr_addr);

}

std::map<addr_t, const Instr*> CFG::getSortedInstrs() const
{
  std::map<addr_t, const boa::Instr*> sorted_instrs = {};
  for(auto const& bb_pair : m_basic_blocks)
  {
    for(auto const& instr : bb_pair.second->getInstrs())
    {
      if(instr->getType() != InstrType::FAKE_LIB_FUNC)
      {
        sorted_instrs.insert({instr->getAddr(), instr.get()});
      }
    }
  }

  return sorted_instrs;
}


unsigned int CFG::getNumberInstrs() const
{
  unsigned int n = 0;
  for(auto const& bb_pair : m_basic_blocks)
  {
    for(auto const& instr : bb_pair.second->getInstrs())
    {
      if(instr->getType() != InstrType::FAKE_LIB_FUNC)
      {
        n++;
      }
    }
  }
  return n;
}

unsigned int CFG::getNumberOfInstrWithSubType(const InstrSubType& sub_type) const
{
  unsigned int n = 0;
  for(auto const& bb_pair : m_basic_blocks)
  {
    for(auto const& instr : bb_pair.second->getInstrs())
    {
      if(instr->getSubType() == sub_type)
      {
        n++;
      }
    }
  }
  return n;
}

std::set<const Instr*> CFG::getInstrsWithSubType(const InstrSubType& sub_type) const
{
  std::set<const Instr*> instrs{};
  for(auto const& bb_pair : m_basic_blocks)
  {
    for(auto const& instr : bb_pair.second->getInstrs())
    {
      if(instr->getSubType() == sub_type)
      {
        instrs.insert(instr.get());
      }
    }
  }
  return instrs;
}

std::unordered_set<sptrBasicBlock> CFG::getBBNeighbours(sptrBasicBlock BB){
  std::unordered_set<sptrBasicBlock> neighbours_list;
  for(auto const& edge: getEdges()){
    if(edge->getSrc().second == BB->getEntryPointAddr()){
      neighbours_list.insert(getBasicBlock(edge->getSrc().second));
    }
    else if(edge->getDst().second == BB->getEntryPointAddr()){
      neighbours_list.insert(getBasicBlock(edge->getDst().second));
    }
  }
  return neighbours_list;
}

/**
 * Merges two bb together (second one in the first one). Updates the CFG edges and bb list too.

*/
void CFG::mergeBasicBlocksAndUpdateGraph(sptrBasicBlock first_BB, sptrBasicBlock second_BB, std::shared_ptr<spdlog::logger> log){

  // case where a jump happened in the middle of a BB
  // and we disas the start of the BB afterwards
  // we attach the current BB to the previously built BB
  // WARNING: "first_BB" could be the same as current_BB, raising issues

  // WE MERGE THE SECOND INTO THE FIRST ONE

  for(auto const& inst : second_BB->getInstrs()){
    first_BB->pushBackInstr(inst);
  }

  // edges

  // manipulations regarding the entry point:
  // we have to change the new entry point
  addr_t old_ep_addr = first_BB->getEntryPointAddr();
  addr_t new_ep_addr = second_BB->getEntryPointAddr();
  first_BB->changeEntryPoint({0, new_ep_addr});

  removeBasicBlock(second_BB->getEntryPointAddr(), log);
  m_basic_blocks.emplace(new_ep_addr, first_BB);
  removeBasicBlock(old_ep_addr, log);


  // and redirect the edges that targeted the old BB
  
  sptrBasicBlock BB_to_change;
  for(auto const& edge : getEdges()){
    // if we find a BB pointing to the second_BB (that got merge with first_BB)$

    if(second_BB->isAddrInBB(edge->getDst().second)){
      /*
      BB_to_change = getBasicBlock(edge->getSrc().second);
      BB_to_change->getLastInstr()->removeEdge({0, new_ep_addr});
      BB_to_change->getLastInstr()->addEdge({0, new_ep_addr}, EdgeFoundMethod::STATIC_DISAS);
      */
    }
    // or pointing to the old ep of first_BB !!

    if(edge->getDst().second==old_ep_addr){
      BB_to_change = getBasicBlock(edge->getSrc().second);
      SPDLOG_LOGGER_TRACE(log, "BB whose edge had to change: {:x}\n", BB_to_change->getEntryPointAddr());
      BB_to_change->getLastInstr()->removeEdge({0, old_ep_addr}, log);
      BB_to_change->getLastInstr()->addEdge({0, new_ep_addr},log);
      SPDLOG_LOGGER_TRACE(log, "Done\n");

    }

  }

  // this also sorts the m_entry_point vect of "this" basic block
  // remove the old BB


  //std::shared_ptr<const BasicBlock> first_BB_sptr(first_BB);
  //cfg->insertBasicBlock(first_BB_sptr);

  first_BB->ArrangeInstructionsByAddress();
}

// Functions

void CFG::setEntryBlock(std::unordered_set<addr_t>& entry_points){
  for(auto const& ep: entry_points){
    m_entry_points.insert(ep);
    //getBasicBlock(ep)->setEntryBlockBool(true);    
  }
}

void CFG::insertEntryPoint(addr_t ep){
  m_entry_points.insert(ep);
}

uptrConstInstr CFG::getInstr(addr_t addr){
  for(auto const& [bbep,BB] : getBasicBlocks()){
    if(BB->isAddrInBB(addr)){
      for(uptrConstInstr inst : BB->getInstrs()){
        if(inst->getAddr() == addr){
          return inst;
        }
      }
    }
  }
  throw std::runtime_error("Could not find instuction in WHOLE CFG.");
}


// Edges

/*
void CFG::insertEdge(sptrConstEdge edge){
  m_edges.insert(edge);
  getInstr(edge->getSrc().second)->addEdge(edge->getDst());
}
*/

std::unordered_set<sptrConstEdge> CFG::getEdges(){
  std::unordered_set<sptrConstEdge> edges;
  for( auto const& [bbep,BB] : getBasicBlocks()){
    for(auto const& inst : BB->getInstrs()){
      for(auto const& [waddr, edge_ptr] : inst->getEdges()){
        edges.insert(edge_ptr);
      }
    }
  }
  return edges;
}

bool CFG::isEdgeInCFG(addr_t src, addr_t dst){
  for(auto const& edge : getEdges()){
    if(edge->getSrc().second == src && edge->getDst().second == dst){
      return true;
    }
  }
  return false;
}
 

std::string CFG::edges2string(){
  std::stringstream f;
  for(auto const& edge : getEdges()){
  f << " src: " << addr2s(edge.get()->getSrc().second);
  f << " --> " <<addr2s(edge.get()->getDst().second);
  f << "\n";
  }
  return f.str();
}

// symbols

void CFG::addFunSymbol(addr_t addr, std::string name){
  m_fun_symbols_map.insert(std::pair{addr, name});
}

std::map<addr_t, std::string> CFG::getFunSymbolsMap(){
  return m_fun_symbols_map;
}

bool CFG::isMainInSymbols(){
  for(auto const& [addr, name] : m_fun_symbols_map){
    if(!name.compare("main")){
      return true;
    }
  }
  return false;
}


// Entry point

const std::unordered_set<addr_t>& CFG::getEntryPoints() const
{
  return m_entry_points;
}


// Dataflow

  // -- misc functions for dataflow inquiries
dataflow_index CFG::getDataflowIndex(sptrBasicBlock bb) const{

  if(m_entry_points.size() > 1){
    throw std::runtime_error("Error, graph has > 1 entry point. Not supported. Aborting. \n");
  }  
  // if we are the entry point of the Site, 0
  if(m_entry_points.count(bb->getEntryPointAddr()) > 0){
    return dataflow_index::DF_ENTRY;
  }

  // if we are an exit point of the Site, 1
  if(bb->getChildren().size() == 0){ return dataflow_index::DF_EXIT;} // leaf
  for(auto const& neighbour : bb->getChildren()){
    if(!isBasicBlockExists(neighbour)){ return dataflow_index::DF_EXIT;} // jumps outside the Graph
  }

  // else 2
  return dataflow_index::DF_ELSE;
}

static sptrBasicBlock getPathEntryBB(std::map<sptrBasicBlock, dataflow_index> graph){
  for(auto const& elt : graph){
    if(elt.second == dataflow_index::DF_ENTRY){
      return elt.first;
    }
  }
  std::cout << "ERROR, could not find entry point for the current Site (dataflow computing)\n";
  return NULL;
}

static std::string printPath(std::vector<sptrBasicBlock> path, std::map<sptrBasicBlock, uint> graph){
  std::stringstream f;
  f << "[";
  for(auto const & elt : path){
    f << addr2s(elt->getEntryPointAddr()) << ";" << graph.at(elt) << "->";
  }
  f << "]";
  return f.str();
}


std::set<sptrBasicBlock> CFG::getChildren(sptrBasicBlock bb){
  std::set<sptrBasicBlock> output;
  std::vector<addr_t> addr_vector = bb->getChildren();
  for(auto const& addr : addr_vector){
    if(isBasicBlockExists(addr)){
      output.insert(getBasicBlock(addr));
    }
  }
  return output;
}

static std::vector<sptrInstr> concatenatePathInstrs(std::vector<sptrBasicBlock> path){
  std::vector<sptrInstr> output;
  for(auto const& bb : path){
    for(auto const& instr : bb->getInstrs()){
      output.push_back(instr);
    }
  }
  return output;
}

    // -- "Stupid" method (depreciated)

/**
 * This extracts all the different paths found on the site with the 
 * "stupid dataflow method"
 * */
void CFG::extractDataflowStupid(std::shared_ptr<spdlog::logger> log){
  std::map<sptrBasicBlock, dataflow_index> graph_map;
  sptrBasicBlock bb;

  for(auto const& elt : getBasicBlocks()){
    bb = elt.second;
    graph_map.insert(std::pair{bb, getDataflowIndex(bb)});
  }

  dataflowExtractionStupidCore(graph_map, log);
}

void CFG::dataflowExtractionStupidCore(std::map<sptrBasicBlock, dataflow_index> graph, std::shared_ptr<spdlog::logger> log){
  // we take a graph and try to find its paths
  // from entry point to every exit point (given by the uint in the map: 0 = entry, 1 = exit, 2 = else)
  // faire un bfs
  // on garde en memoire tous les chemins qui se construisent en parallele
  // on s'en fiche de boucler
  // chaque noeud enfant cree un nouveau chemin
  // on a une limite max pour la longueur d'un . Si elle est atteinte le chemin est abandonne
  uint path_size_limit = 24; // MAGIC NUMBER
  // on calcule le dataflow de chaque chemin qui se termine
  // si on l'a pas deja precedemment calcule, on ajoute le df a la liste, sinon on supprime le chemin

  using node_path_t = std::vector<sptrBasicBlock>;

  sptrBasicBlock current_BB;
  std::set<sptrBasicBlock> neighbours_list;
  node_path_t current_path;
  node_path_t tmp_path;
  std::pair<sptrBasicBlock, node_path_t> current_pair;
  sptrBipartite current_path_dataflow;
  std::vector<sptrInstr> current_path_inst_vect;

  std::queue<std::pair<sptrBasicBlock, node_path_t>> queue;
  // each element of the queue is: an address (node) and its path

  // ----- init
  current_path.push_back(getPathEntryBB(graph));
  queue.push({getPathEntryBB(graph), current_path});
  

  while(queue.size()>0){
    current_path.clear();
    current_pair = queue.front();
    queue.pop();
    current_BB = current_pair.first;
    current_path = current_pair.second;

    if(current_path.size() > path_size_limit){
      continue;
    }
    
    neighbours_list = getChildren(current_BB);

    for(auto const& neighbour : neighbours_list){
      tmp_path.clear();
      tmp_path = current_path;
      tmp_path.push_back(neighbour);
      queue.push({neighbour, tmp_path}); // add every neighbour to the list
    }

    if(graph.at(current_BB) == dataflow_index::DF_EXIT){
      // reached an exit
      //SPDLOG_LOGGER_DEBUG(log, printPath(current_path, graph));
      // we add the path's dataflow to our site's dataflow list
      current_path_inst_vect = concatenatePathInstrs(current_path);
      current_path_dataflow = std::make_shared<BipartiteGraph>(current_path.front()->getEntryPointAddr(), current_path_inst_vect);
      //current_path_dataflow->createBipartiteGraphFromInstrs();
      m_dataflow_graphs_v2.insert(current_path_dataflow);
      continue;
    }

  } // end of while
  SPDLOG_LOGGER_TRACE(log, "We found {} different paths for site {:x}\n", m_dataflow_graphs_v2.size(), getEntryPointAddr());
}


  // -- tensor method


/**
 * @input: Entry point of the graph from wich to start the tainting process
 * Extracts dataflow tensor (and stores it in m_dataflow_tensor) 
 * Propagates dataflow info from the given entry point of the graph through the regs
 * and create a new df whenever an exit is met.
 * This function create one dataflow bipartite graph (matrix) for every exit node of the graph, and adds it to the tensor
 **/
/*
void CFG::extractDataflowTensorFromSCCG(addr_t entry_point, std::shared_ptr<spdlog::logger> log){
  // NOTE to future self: this function works
  // BFS
  SPDLOG_LOGGER_DEBUG(log,"\t\tStarting dataflow tensor computing from {:x}.\n", getEntryPointAddr());

  std::queue<std::pair<sptrBasicBlock, x86_reg_class>> bfs_queue;
  std::set<std::pair<sptrBasicBlock, x86_reg_class>> seen_node_set;

  // Maps
  std::map<sptrBasicBlock, dataflow_index> bb_type_map;
  std::unordered_map<sptrBipartite> bb_dataflow_map;

  // Tensor
  std::unordered_set<sptrBipartite> dataflow_tensor; // {exit id block, final reachability graph}

  // Tmp var
  sptrBasicBlock bb_tmp, current_bb, entry_block;
  sptrBipartite df_bb, init_bipartite;
  //std::pair<sptrBasicBlock, sptrBipartite> tmp_queue_elt;
  std::pair<sptrBasicBlock, x86_reg_class> current_node;
  x86_reg_class current_reg_class;

  std::vector<addr_t> neighbours_list;
  std::set<x86_reg_class> bb_tainted_regs;

  // --- Init

// bb type map; bb datflow map;
  for(auto const& elt : getBasicBlocks()){
    bb_tmp = elt.second;

    //get bb index (exit, else)
    if(getExitPoints().count(elt.first)>0){
      SPDLOG_LOGGER_DEBUG(log, "\t\tAdded an exit point: {:x}\n", elt.first);
      bb_type_map.insert({bb_tmp, dataflow_index::DF_EXIT});
    }
    else{
      bb_type_map.insert({bb_tmp, dataflow_index::DF_ELSE});
    }

    //get each bb's dataflow graph
    df_bb = std::make_shared<BipartiteGraph>(elt.first, bb_tmp->getInstrs());
    //df_bb->createBipartiteGraphFromInstrs();

    bb_dataflow_map.insert({bb_tmp, df_bb});

  }


// entry block
  entry_block = getBasicBlock(entry_point);

// dataflow tensor init

  bool is_no_exit = true;
  for(auto const& [bb, bb_type] : bb_type_map){
    if(bb_type == dataflow_index::DF_EXIT){
      dataflow_tensor.insert(std::make_shared<BipartiteGraph>(entry_block->getEntryPointAddr()));
      is_no_exit = false;
    }
  }
  if(is_no_exit){
    SPDLOG_LOGGER_DEBUG(log,"Current graph has no exit. Aborting Dataflow computing.\n");
    return;
  }


  // --- Register dataflow computing

  for(x86_reg_class current_reg_class_init = X86_CLASS_BEGIN; current_reg_class_init < x86_reg_class::X86_CLASS_ENDING; current_reg_class_init = (x86_reg_class)(current_reg_class_init+1)){

  
    //SPDLOG_LOGGER_INFO(log,"\tCurrent reg elt {}.\n", current_reg_class_init);


    // do a BFS
    // clean the queue
    while (!bfs_queue.empty()){bfs_queue.pop();}
    
    bfs_queue.push({entry_block, current_reg_class_init});
    //current_reg_class = current_reg_class_init;
    

    while(bfs_queue.size()>0){

      current_node = bfs_queue.front();
      bfs_queue.pop();

      current_bb = current_node.first;
      current_reg_class = current_node.second;

      // I need to get the df output's info in some way
      if(seen_node_set.count(current_node)){continue;}

      seen_node_set.insert(current_node);

      SPDLOG_LOGGER_TRACE(log,"\tCurrent bb: {:x} reg: {}\n", current_bb->getEntryPointAddr(), current_reg_class);

  // manage children

      // info about how many regs are tainted in the current bb (starting from current_reg_class)
      bb_tainted_regs = bb_dataflow_map.at(current_bb)->getRegsTaintedBy(current_reg_class);

      // info about how many children the current bb has
      neighbours_list = current_bb->getChildren();
      
      SPDLOG_LOGGER_TRACE(log,"\t\tNeighbours - Regs : {} - {} elts\n", neighbours_list.size(),bb_tainted_regs.size());


      // combined, we get the number of new pathes for the current_reg_class data

      for(auto const& bb : neighbours_list){
        if(isBasicBlockExists(bb)){
          for(auto const& reg : bb_tainted_regs){
            bfs_queue.push({getBasicBlock(bb), reg});
            SPDLOG_LOGGER_DEBUG(log,"\tAdded a child to bfs queue: {:x},{}\n", bb, reg);
          }
        }
      }

  // if exit

      if(bb_type_map.at(current_bb) == dataflow_index::DF_EXIT){
        // First apply the bb's dataflow transformation
        SPDLOG_LOGGER_TRACE(log,"\t\tFound an exit\n");

        // then update the tensor
        for(auto const& reached_reg : bb_tainted_regs){
          dataflow_tensor.at(current_bb)->addTaintingRegsTo(reached_reg, current_reg_class_init);
          // dataflow_tensor.at(current_bb).at(reached_reg).source_regs.insert(current_reg_class_init);
          SPDLOG_LOGGER_TRACE(log,"\t\tAdded {} here {}\n", current_reg_class_init, reached_reg);

        }
      }
    }
  }
  //SPDLOG_LOGGER_DEBUG(log,"\t\tExceptional entry block print print : {}\n", dataflow_tensor.at(entry_block)->toString());
  SPDLOG_LOGGER_DEBUG(log,"\t\tFinished dataflow tensor computing\n\n");


  m_dataflow_tensor = dataflow_tensor;
  return;
}
*/

/**
 * len_limit: limit length for a random walk. Here we have 100 basic blocks.
 * Could be determined dynamically compared to the size of the sites, like n * S ?
 **/
std::vector<addr_t> CFG::getRandomWalk(std::shared_ptr<spdlog::logger> log){
	std::vector<addr_t> output;
	uint len_limit = 100;
	addr_t current_addr;
	sptrBasicBlock current_bb;
	std::vector<addr_t> successors;
// random
	uint rd_index = 0;
	std::random_device dev;
  std::mt19937 rng(dev());

	successors.push_back(this->getRandomEntryPointAddr());

	while(output.size() < len_limit){
  	std::uniform_int_distribution<std::mt19937::result_type> dist6(0,successors.size()-1); // distribution in range [0, successors.size()]
		rd_index = dist6(rng);
		current_addr = successors.at(rd_index);
		current_bb = getBasicBlock(current_addr);
		if(!this->isBasicBlockExists(current_addr)){break;} // got out of the graph
		output.push_back(current_addr);
		successors.clear();
		if(!current_bb->getChildren().size()){break;} // reached a leaf
		for(addr_t const& child : current_bb->getChildren()){
			successors.push_back(child);
		}
	}
	return output;
}

/**
 * Core fuction that extracts the whole dataflow paths from the graph
 * Uses RANDOM WALKS
 * ver 3
 * */
void CFG::extractDataflowTensor(std::shared_ptr<spdlog::logger> log){
  std::unordered_set<sptrBipartite> dataflow_tensor; // exit_bb, bipartite_graph
  uint nb_walks = 1000;
  bool already_computed_df=false;
	std::vector<addr_t> c_walk;
	std::set<std::vector<addr_t>> c_walk_seen;
	std::set<sptrBipartite> b_df_seen;
	std::vector<sptrInstr> walk_insts;
	sptrBipartite b_df;

	for(uint i=0; i < nb_walks; i++){
		c_walk = this->getRandomWalk(log);
		if(c_walk_seen.count(c_walk)){ continue; }
		c_walk_seen.insert(c_walk);
		walk_insts.clear();
		for(auto const& addr : c_walk){
			for(auto const& inst : getBasicBlock(addr)->getInstrs()){
				walk_insts.push_back(inst);
			}
		}
		// computes the dataflow of every walk (sequence of instructions)
		b_df = std::make_shared<BipartiteGraph>(c_walk.front(), walk_insts);
		log->trace("\t df -> {}\n", b_df->toString());

		already_computed_df = false;
		for(auto const& elt: b_df_seen){
			if(b_df->isEqual(elt, log)){
				log->trace("{} and {} are equal dataflow\n", b_df->toString(), elt->toString());
				already_computed_df = true;
				break;
			}
		}
		if(already_computed_df){continue;}

		b_df_seen.insert(b_df);
		dataflow_tensor.insert(b_df);
	}

  m_dataflow_tensor = dataflow_tensor;
  return;
}

/**
 * Core fuction that extracts the whole dataflow paths (using SCC) from the graph
 * ver 2
 * not used

void CFG::extractDataflowTensor(std::shared_ptr<spdlog::logger> log){

  struct queue_elt{
    sptrCFG current_sccg;
    addr_t current_ep;
    sptrBipartite dataflow_path;
  };
  std::unordered_set<sptrBipartite> dataflow_tensor;

  std::unordered_set<sptrCFG> seen_sccg; // theorically useless
  //sptrCFG tmp_sccg;
  //addr_t tmp_entry=0;
  queue_elt tmp_elt, new_elt;
  sptrBipartite current_exit_bp_graph;
  //std::unordered_set<addr_t> tmp_exit_list;

  // tmp stuff
  sptrBasicBlock sccg_exit_bb;
  sptrBipartite sccg_path_df;

  std::queue<queue_elt> sccg_queue; // ssc and the concerned entry point, and the dataflow

  // queue init
  queue_elt init_elt;
  addr_t entry_addr = getRandomEntryPointAddr(); // TODO: better than this
  sptrCFG init_sccg = getSCCGraphContaining(entry_addr);
  init_elt.current_ep = entry_addr;
  init_elt.current_sccg = init_sccg;
  init_elt.dataflow_path = std::make_shared<BipartiteGraph>(entry_addr);
  init_elt.dataflow_path->initToIdentity();


  sccg_queue.push(init_elt);

  while(sccg_queue.size()>0){
    tmp_elt = sccg_queue.front();
    sccg_queue.pop();
    if(seen_sccg.count(tmp_elt.current_sccg)>0){
      //this should not happen
      continue;
    }

    // updates current dataflow path with current sccg data
    tmp_elt.current_sccg->extractDataflowTensorFromSCCG(tmp_elt.current_ep, log);

    // This part is to check if we create new paths (different children) / stop here (leaf) / update the dataflow tensor (exit node)
    for(auto const& sccg_path : tmp_elt.current_sccg->getDataflowTensor()){ // gather all df from this sccg
      // each exit of this sccg creates a new path.
      sccg_exit_bb = sccg_path.first;
      sccg_path_df = sccg_path.second;

      if(sccg_exit_bb->getChildren().size() == 0){
        // We found a true exit (leaf) of the graph, export the df
        dataflow_tensor.insert({sccg_exit_bb, tmp_elt.dataflow_path->concatenateAndCreateNewGraphWith(sccg_path_df, log)});
        continue;
      }

      for(auto const& child : sccg_exit_bb->getChildren()){
        if(!isBasicBlockExists(child)){
        SPDLOG_LOGGER_DEBUG(log, "\t\t1\n");

          // child is not in the current graph (site or whatever), then we skip it
          // and the current node is actually an exit node (but can have other children within the graph)
          // so we update the dataflow_tensor.
          dataflow_tensor.insert({sccg_exit_bb, tmp_elt.dataflow_path->concatenateAndCreateNewGraphWith(sccg_path_df, log)});
          SPDLOG_LOGGER_DEBUG(log, "\t\tAdded this dataflow to the tensor : {}\n", tmp_elt.dataflow_path->concatenateAndCreateNewGraphWith(sccg_path_df, log)->toString());
        }
        else if(!tmp_elt.current_sccg->isBasicBlockExists(child)){
                  SPDLOG_LOGGER_DEBUG(log, "\t\t2\n");

          // we found a child that's not in the current sccg, but is in the graph
          if(getSCCGraphContaining(child) == NULL){
            log->info("ERROR: could not find a SCC graph containing {:x}. Should not be the case. Aborting dataflow tensor computation\n", child);
            return;
          }
          new_elt.current_ep = child;
          new_elt.current_sccg = getSCCGraphContaining(child);
          new_elt.dataflow_path = tmp_elt.dataflow_path->concatenateAndCreateNewGraphWith(sccg_path_df, log);
          sccg_queue.push(new_elt);
        }
        else{
          // looped back in the SCCG
          // NOTE: can this be possible? yes it's actually okay;
          // we are just in an exit block of the sccg, and one of its children is another block the sccg
          // just ingnore it
          //log->info("WARNING: Found an EXIT df child {:x} looping back into the sccg. Shoud not be the case\n", child);

          continue;
        }
      }
    }
  }

  m_dataflow_tensor = dataflow_tensor;
  return;

}
*/

// Graph Reduction

std::shared_ptr<CFG> CFG::getSCCGraphContaining(addr_t addr){
  for(auto const& sccg : m_scc_nodes){
    if(sccg->isBasicBlockExists(addr)){
      return sccg;
    }
  }
  return NULL;
  //throw std::runtime_error("Could not find a scc graph contaning the addr. Aborting.");
}

std::shared_ptr<CFG> CFG::getSCCGraphContaining(sptrBasicBlock bb){
  return getSCCGraphContaining(bb->getEntryPointAddr());
}

/**
 * 
 * Reduces the graph.
 * NOTE: not used for now. Broken. Don't use it.
 * */
void CFG::reduceGraph(std::shared_ptr<spdlog::logger> log){

  for(auto const& bb : getBasicBlocks()){
    if(bb.second->getChildren().size()==1){ // has one child
      mergeBasicBlocksAndUpdateGraph(bb.second, getBasicBlock(bb.second->getChildren().front()), log);
    }
  }
}

/**
 * Subroutine for visiting graph - Kosaraju's algorithm
 * */
void CFG::sub_visit(sptrBasicBlock bb, std::unordered_set<sptrBasicBlock>* visited_set, std::stack<sptrBasicBlock>* L){
  if(visited_set->count(bb)>0){return;}
  visited_set->insert(bb);
  for(auto const& child_addr : bb->getChildren()){
    if(getBasicBlock(child_addr)==NULL){
      continue;
    }
    sub_visit(getBasicBlock(child_addr), visited_set, L);
  }
  L->push(bb);
  return;
}

/**
 * Subroutine for assigning graph - Kosaraju's algorithm
 * */
void CFG::sub_assign(sptrBasicBlock bb,
  sptrBasicBlock root,
  std::unordered_set<sptrBasicBlock>* assigned_set,
  std::unordered_map<addr_t, std::unordered_set<sptrBasicBlock>>* scc_tmp_map){
  if(assigned_set->count(bb)>0){return;}
  assigned_set->insert(bb);
  if(scc_tmp_map->count(root->getEntryPointAddr()) == 0){
    std::unordered_set<sptrBasicBlock> tmp_bb_set;
    tmp_bb_set.insert(bb);
    scc_tmp_map->insert(std::pair{root->getEntryPointAddr(), tmp_bb_set});
  }
  else{
    scc_tmp_map->at(root->getEntryPointAddr()).insert(bb);
  }

  for(auto const& parent : getParents(bb)){
    sub_assign(parent, root, assigned_set, scc_tmp_map);
  }
  return;
}

/**
 * Fills the m_scc_nodes and m_scc_edges members of the graph
 * This identifies and links the different scc of the graph
 * We use Kosaraju's algorithm (Tarjan is faster but apparently less easy to understand -> see if optimization is needed)
 * */
void CFG::getStronglyConnectedComponentsNodes(std::shared_ptr<spdlog::logger> log){
  // Kojaru's algorithm
  std::unordered_set<sptrBasicBlock> visited_bb_set;
  std::stack<sptrBasicBlock> reversed_graph;
  std::unordered_set<sptrBasicBlock> assigned_bb_set;
  std::unordered_map<addr_t, std::unordered_set<sptrBasicBlock>> scc_tmp_map;
  std::shared_ptr<CFG> tmp_cfg_ptr;

  sptrBasicBlock tmp_bb;
  SPDLOG_LOGGER_TRACE(log, "\t\tStarting first traversal\n");
  for(auto const& bb: getBasicBlocks()){
    sub_visit(bb.second, &visited_bb_set, &reversed_graph);
  }
  SPDLOG_LOGGER_TRACE(log, "\t\tStarting second traversal\n");

  while(!reversed_graph.empty()){
    tmp_bb = reversed_graph.top();
    reversed_graph.pop();
    sub_assign(tmp_bb, tmp_bb, &assigned_bb_set, &scc_tmp_map);
  }

  // we fill the CFG's member with data from scc_tmp_map
  SPDLOG_LOGGER_TRACE(log, "\t\tFilling scc member\n");

  for(auto const& subgraph : scc_tmp_map){
    tmp_cfg_ptr.reset();
    tmp_cfg_ptr = std::make_shared<CFG>(subgraph.first);
    for(auto const& bb : subgraph.second){
      tmp_cfg_ptr->insertBasicBlock(bb);/*
      for(auto const& child : bb->getChildren()){
        if(subgraph.second.count(getBasicBlock(child))<1){
          tmp_cfg_ptr->addExitPoint(child);
        }
      }*/
    }

    m_scc_nodes.insert(tmp_cfg_ptr);
  }

  return;

}

/**
 * Updates the m_scc_edges member AND the m_exit_points and m_input_points of
 * the scc graphs of the main graph. 
 * 
 **/
void CFG::getStronglyConnectedComponentsEdges(std::shared_ptr<spdlog::logger> log){
   //std::map<addr_t, std::unordered_set<addr_t>> sccg_children_map;
   //std::unordered_set<addr_t> tmp_children_set;
   addr_t current_sccg_id;
   std::shared_ptr<CFG> tmp_sccg_ptr;
   bool is_current_sccg_a_leaf=true;
   addr_t current_max_addr_value=0;

   for(auto const& current_sccg: m_scc_nodes){
      //init
      current_sccg_id = current_sccg->getSCCGId();
      is_current_sccg_a_leaf = true;
      current_max_addr_value = 0;
      //tmp_children_set.clear();
      //sccg_children_map.insert({current_sccg_id, tmp_children_set});
      SPDLOG_LOGGER_TRACE(log, "\t\tLooking at sccg {:x}\n", current_sccg_id);

      //gather, per sccg, all the children addr
      for(auto const& node : current_sccg->getBasicBlocks()){
        current_max_addr_value = std::max(current_max_addr_value, node.first);
        for(auto const& child : node.second->getChildren()){
          SPDLOG_LOGGER_TRACE(log, "\t\tFound a child to {:x} : {:x}\n", node.first, child);
          if(!current_sccg->isBasicBlockExists(child)){
            is_current_sccg_a_leaf = false;
            //sccg_children_map.at(current_sccg_id).insert(child);  
            //tmp_children_set.insert(child);

            // found an exit point for the current sccg
            current_sccg->addExitPoint(node.first);

            // modify the concerned sccg
            tmp_sccg_ptr = getSCCGraphContaining(child);
            if(getSCCGraphContaining(child) != NULL){
              tmp_sccg_ptr->addInputNode(child);
              addSCCGEdge(current_sccg_id, getSCCGraphContaining(child)->getSCCGId());
            }
          }
        }
      }
      if(is_current_sccg_a_leaf){
        current_sccg->addExitPoint(current_max_addr_value);
      }
   }
   return;
}

// Other functions (CFG)

addr_t CFG::getRandomEntryPointAddr() const{
  for(auto const& ep : this->getEntryPoints()){
    return ep;
  }
  throw std::runtime_error("CFG has no entry points !");
}

/**
 * Did not change its name yet because of sites using this funciton to acces their ep
 * Because they only have one
 * Ther eis another version of this functinon that describes it ebtter, see getRandomEntryPoint
 * 
 **/
addr_t CFG::getEntryPointAddr() const{
  if(this->getEntryPoints().size()>1){
      throw std::runtime_error("CFG has several entry points");
  }
  for(auto const& ep : this->getEntryPoints()){
    return ep;
  }
  throw std::runtime_error("CFG has no entry points !");
}

std::string CFG::createDotWithoutDigraph(bool is_reduced) const
{
  std::stringstream f;

  // First, we add each BB without edges
  auto bbs = getSortedBasicBlocks();
  for(auto const& bb_pair : bbs)
  {
    waddr_t bb_ep = {m_wave, bb_pair.first};
    sptrBasicBlock bb = bb_pair.second;

    // Key: EP of the BB
    f << '"' << waddr2s(bb_ep) << "\"[label=\"";
    std::string bb_instrs = "";

    if(!is_reduced){

      for(auto const& instr : bb->getInstrs())
      {
        bb_instrs +=  instr->getSectName() + " ; " + bb->getFunName() + " ; " + instr->toStringLight() + " ; " + instr->getBytesString() + "\\l";
      }
      f << bb_instrs;

    }
    else{
      uptrConstInstr instr = bb->getLastInstr();
      bb_instrs +=  instr->getSectName() + " ; " + bb->getFunName() + " ; " + addr2s(bb->getEntryPointAddr()) + ": " + instr->getOpcode() + " " + instr->getBytesString() + "\\l";
      f << bb_instrs;
    }


    f << "\",shape=";
    if(bb->isEntryBlock()){f << "component,style=\"filled\"";}
    else{f << "box,style=\"filled\"";}
    f << ", fontweight=\"bold\",fillcolor=\"";
    /*if(m_entry_points.find(bb_ep.second) != m_entry_points.end())
    {
      f << "/spectral11/1";
      //f << "orange";
    }
    else if(bb->getLastInstr()->getSubType() == InstrSubType::FAKE_LIB_FUNC)
    {
      f << "cyan";
    }
    else
    { }*/
       // f << "white";
       //f << "/spectral11/" + std::to_string(bb->getColor() + 1);
    if(bb->getColor() == 0) { f << "white"; }
    else if(bb->getColor() <= 8){ f << "/greens9/" + std::to_string(bb->getColor()); }
    else if(bb->getColor() <= 16){ f << "/oranges9/" + std::to_string(bb->getColor()-8); }
    else if(bb->getColor() <= 24){ f << "/pubu9/" + std::to_string(bb->getColor()-16); }
    else {f << "white";}

    f << "\"];" << std::endl;
  }

  // Second, we add edges
  for(auto const& bb_pair : bbs)
  {
    std::string src = waddr2s({m_wave, bb_pair.first});
    for(auto const& child_addr : bb_pair.second->getChildren())
    {
      sptrBasicBlock dst_bb = getBasicBlock(child_addr);
      if(dst_bb==NULL){continue;}
      std::string dst = waddr2s({0,dst_bb->getEntryPointAddr()});

      // If STATIC DISAS --> black
      f << '"' << src << "\" -> \"" << dst << "\";" << std::endl;

    }
  }

  // We add construction links between CALL and returnsite
  /*
  for(auto const& bb_pair : bbs)
  {
    auto bb_last_instr = bb_pair.second->getLastInstr();
    if(bb_last_instr->getType() == InstrType::CALL)
    {
      if(isBasicBlockExists(bb_last_instr->getNextAddr()))
      {
        std::string src = waddr2s({m_wave, bb_pair.first});
        std::string dst = waddr2s({m_wave, bb_last_instr->getNextAddr()});
        f << '"' << src << "\" -> \"" << dst << "\"[style=dotted];" << std::endl;
      }
    }
  } */
  return f.str();
}

std::string CFG::createDot(bool is_reduced) const
{
  return "Digraph G {\n" + createDotWithoutDigraph(is_reduced) + "}";
}

void CFG::createInstrsAndEdgesListTextFile(const std::string& instrs_list_filepath,
                                           const std::string& edges_list_filepath) const
{
  std::string instrs_list;
  std::string edges_list;
  for(auto const& instr_p : getSortedInstrs())
  {
    instrs_list += instr_p.second->toString() + "\n";
    for(const auto& edge : instr_p.second->getEdges())
    {
      edges_list += waddr2s({m_wave, instr_p.first}) + " --> " + waddr2s(edge.first) + "\n";
    }
  }
  utils::create_text_file(instrs_list, instrs_list_filepath);
  utils::create_text_file(edges_list, edges_list_filepath);
}

void CFG::createBBsListTextFile(const std::string& filepath) const
{
  std::string bbs_list;
  for(auto const& bb : getSortedBasicBlocks())
  {
    bbs_list += bb.second->toString();
    bbs_list += " (size: " + std::to_string(bb.second->getSize()) + ")";
    bbs_list += " (first instr: " + bb.second->getInstrs().at(0)->toStringLight() + ")\n";
  }
  utils::create_text_file(bbs_list, filepath);
}

// Other functions (stats)

unsigned int CFG::getNumberBasicBlocks() const
{
  return m_basic_blocks.size();
}

unsigned int CFG::getNumberEdges() const
{
  size_t n = 0;
  for(auto const& bb_pair : m_basic_blocks)
  {
    n += bb_pair.second->getEdges().size();
  }
  return n;
}

// Printing

std::string CFG::printGraphDataflowTensor(const std::string& tab_depth){
  std::stringstream f;
  for(auto const& graph : getDataflowTensor()){
    f << "\n";
    f << tab_depth;
    f << "\t" << addr2s(graph->getLastInstrAddr()) << ": ";
    f << graph->toString();
  }
  f << "\n";
  return f.str();
}

std::string CFG::printGraphDataflowTensor(const std::string& tab_depth, addr_t current_fun_ep){
  std::stringstream f;
  for(auto const& graph : getDataflowTensor()){
    f << "\n";
    f << tab_depth;
    f << "\t" << addr2s(graph->getLastInstrAddr()) << "(" << addr2s(graph->getLastInstrAddr() - current_fun_ep) << ")";
    f << " - " << instType2str(graph->getLastInstrType()) << " : ";
    f << graph->toString();
  }
  f << "\n";
  return f.str();
}

std::string CFG::printBasicBlocksList() const{

  std::stringstream f;
  f << "[";
  for(auto const& bb : getSortedBasicBlocks()){
    f << addr2s(bb.first) << ",";
  }
  f << "]";
  return f.str();
}

std::string CFG::printGraphSCC() const{
  std::stringstream f;
  f << "{\nNodes:\n";
  for(auto const& subgraph : m_scc_nodes){
    f << "\t" << subgraph->printBasicBlocksList() << "\n";
    f << "\t |- Input points: " << set2strAddr(subgraph->m_input_points) << "\n";
    f << "\t |- Exit points: " << set2strAddr(subgraph->m_exit_points) << "\n";
  }
  f << "\nEdges: \n";
  f << "\t";
  for(auto const& edge : m_scc_edges){
    f << edge->toString() << "; ";
  }
  f << "\n\n}\n";
  return f.str();
}


std::string CFG::createDotWithoutDigraph(uint index, bool is_reduced)
{
  std::stringstream f;

  // First, we add each BB without edges
  auto bbs = getSortedBasicBlocks();
  for(auto const& bb_pair : bbs)
  {
    waddr_t bb_ep = {index, bb_pair.first};
    sptrBasicBlock BB = bb_pair.second;

    // Key: EP of the BB
    f << '"' << waddr2s(bb_ep) << "\"[label=\"";
    std::string bb_instrs = "";

    if(!is_reduced){
      for(auto const& instr : BB->getInstrs())
      {
        bb_instrs += BB->getFunName() + " ; " + instr->toStringLight() + "\\l";
      }
      f << bb_instrs;
    }
    else{
      uptrConstInstr instr = BB->getLastInstr();
      bb_instrs += BB->getFunName() + " ; " + addr2s(BB->getEntryPointAddr()) + ": " + instr->getOpcode() + "\\l";
      f << bb_instrs;
    }

    f << "\",shape=";
    if(BB->isEntryBlock()){f << "component,style=\"filled\"";}
    else{f << "box,style=\"filled\"";}
    f << ", fontweight=\"bold\",fillcolor=\"";

    if(BB->getColor() == 0) { f << "white"; }
    else if(BB->getColor() <= 8){ f << "/greens9/" + std::to_string(BB->getColor()); }
    else if(BB->getColor() <= 16){ f << "/oranges9/" + std::to_string(BB->getColor()-8); }
    else if(BB->getColor() <= 24){ f << "/pubu9/" + std::to_string(BB->getColor()-16); }
    else {f << "white";}    
    f << "\"];" << std::endl;
  }

  // Second, we add edges
  for(auto const& bb_pair : bbs)
  {
    std::string src = waddr2s({index, bb_pair.first});
    for(auto const& child_addr : bb_pair.second->getChildren())
    {
      sptrBasicBlock dst_bb = getBasicBlock(child_addr);
      if(dst_bb==NULL){continue;}
      std::string dst = waddr2s({index,dst_bb->getEntryPointAddr()});
      f << '"' << src << "\" -> \"" << dst << "\";" << std::endl;
    }
  }
  return f.str();
}

/**
 * This creates, for a graph and its dataflow,
 * the graph of every path linked to the bipartite graph corresponding to it
 * for all pathes of the dataflow, alongside the site
 * */
std::string CFG::createDataflowDotWithoutDigraph(uint index){
  // Print the dataflow tensor of the graph
  // For example, we want to print the df of a Site


  std::stringstream f;

  addr_t last_bb_ep_addr=0;
  uint index_dataflow=0;
  std::string dataflow_id, path_id;

  // link every path with a bipartite graph (its dataflow)
  for(auto bipartite_ptr : getDataflowTensor()){

    // ------------ instruction graphs

    // a cluster with the current path's graph


    /*

    dataflow_inst_id = addr2s(bipartite_ptr->getFirstInstrAddr()) + std::to_string(index_dataflow);
    f << '"' << dataflow_inst_id << "\"[label=\"";
    f << bipartite_ptr->instrsToString();
    f << "\",shape=";
    f << "box,style=\"filled\"";
    f << ", fontweight=\"bold\",fillcolor=\"";
    f << "lightgreen";
    f << "\"];" << std::endl;
    */

    path_id = addr2s(bipartite_ptr->getFirstInstrAddr()) + "_" + std::to_string(index_dataflow) + "_" + std::to_string(index); // identifier for the current graph

    f << "subgraph cluster_" + path_id << "{\n";
      f << "color=\"green\"\n";

      f << "node [shape=box,style=\"filled\", fontweight=\"bold\",fillcolor=\"white\"];\n";
      f << "\"" + path_id + "\""; // label of the node
      f  << "[label=\"" + bipartite_ptr->instrsToString() + "\"]\n"; // content of the cluster (do a instToGraph ?); now a node
      f << "label = \"Path #" << std::to_string(index_dataflow) << "\";";

    f << "}\n";

    // ------------ bipartite graph block
    // subgraph with the corresponding dataflow bipartite graph
    last_bb_ep_addr = bipartite_ptr->getLastInstrAddr();
    dataflow_id = addr2s(last_bb_ep_addr) + "_" + std::to_string(index_dataflow) + "_" + std::to_string(index);
    // we add the bipartite graphs
/*
      f << '"' << dataflow_id << "\"[label=\"";
      f << bipartite_ptr->toString();
      f << "\",shape=";
      f << "box,style=\"filled\"";
      f << ", fontweight=\"bold\",fillcolor=\"";
      f << "white";
      f << "\"];" << std::endl;
  */ 

    f << "subgraph cluster_" + dataflow_id << "{\n";
      f << "color=\"blue\"\n";

      f << "node [shape=ellipse,style=\"filled\", fontweight=\"bold\",fillcolor=\"white\"];\n";
      //include here the df graph

      f << bipartite_ptr->toGraphString(dataflow_id);
      
      /*
      f << "\"" + dataflow_id + "\""; // label of the node
      f  << "[label=\"" + bipartite_ptr->toString() + "\"]\n"; // content of the cluster (do a instToGraph ?); now a node
      */
      f << "label = \"Dataflow graph #" << std::to_string(index_dataflow) << "\";";

    f << "}\n";

    //add edge from bgraph to its BB (doted)

    f << '"' << path_id  << "\" -> \"" << dataflow_id << "\"";
    f << "[style=dashed ltail=cluster_" << path_id << " lhead=cluster_" << dataflow_id+ "];" << std::endl;

    index_dataflow++;
  }
  return f.str();
}

std::string CFG::createDotWithDataflow(bool is_reduced){
  std::stringstream f;
  f << "Digraph G {\n " ;
  f << "graph [fontsize=10 fontname=\"Verdana\" compound=true];\n";
  f << createDotWithoutDigraph(0,  is_reduced);
  f << createDataflowDotWithoutDigraph(0);
  f<< "}";
  return f.str();
}


} // namespace boa
