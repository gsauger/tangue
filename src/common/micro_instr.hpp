#ifndef MICRO_INSTR_H
#define MICRO_INSTR_H

#include "utils/utils.hpp"
#include <optional>
#include <string>
#include <unordered_set>

namespace boa
{

namespace microinstr
{

struct mem_op_t
{
  unsigned int size;                                   // in bytes
  std::string loc_expr;                                // Smtlib address location expr
  std::unordered_set<std::string> loc_expr_right_vars; // Right vars used in location expr
  std::unordered_set<std::string> loc_expr_right_vars_without_suffix;
};

} // namespace microinstr

using optString = std::optional<std::string>;
using optUnSetString = std::optional<std::unordered_set<std::string>>;
using unSetString = std::unordered_set<std::string>;
using optMemOp = std::optional<microinstr::mem_op_t>;

enum class MicroInstrType
{
  DYN_JMP,    // RET, CALL EAX, JMP EAX, ... case
  COND,       // Jcc case
  VAR_UPDATE, // define-fun case
  VAR_RESET,  // declare-fun case
  ASSERT      // div case (check for overflow or devide by zero)
};

class MicroInstr
{
public:
  // MARK:- Constructors and destructors
  MicroInstr(MicroInstrType type, const std::string& mnemonic, const std::string& smtlib_expr,
             const std::string& left_var, const unSetString& right_vars, const optMemOp& mem_read,
             const optMemOp& mem_write);

  MicroInstr(MicroInstrType type, const std::string& mnemonic, const std::string& smtlib_expr,
             const std::string& left_var);

  MicroInstr() = delete;

  MicroInstr(MicroInstr const&) = delete;
  MicroInstr& operator=(MicroInstr const&) = delete;

  MicroInstr(MicroInstr&&) = delete;
  MicroInstr& operator=(MicroInstr&&) = delete;

  ~MicroInstr() = default;

  // MARK:- Getters and setters
  MicroInstrType getType() const;
  const std::string& getMnemonic() const;
  const std::string& getSmtlibExpr() const;
  const std::string& getLeftVar() const;
  const std::string& getLeftVarWithoutSuffix() const;

  const unSetString& getRightVars() const;
  const unSetString& getRightVarsWithoutSuffix() const;
  const optMemOp& getMemRead() const;
  const optMemOp& getMemWrite() const;

  // MARK:- Other functions
  std::string toString() const;

  // MARK:- Static functions
  static std::string removeVarSuffix(const std::string& var_with_r_l_suffix);

private:
  // MARK:- Private member variables

  // Common for all MicroInstrs
  MicroInstrType m_type;
  std::string m_mnemonic;
  std::string m_smtlib_expr;
  std::string m_left_var;                // Assigned var
  std::string m_left_var_without_suffix; // Assigned var

  // Optinal fields
  unSetString m_right_vars;                // Read vars
  unSetString m_right_vars_wihtout_suffix; // Read vars
  optMemOp m_mem_read;                     // Only when memory_r in right var
  optMemOp m_mem_write;                    // Only when left var is memory_l
};

} // namespace boa

#endif /* MICRO_INSTR_H */
