#ifndef INSTR_H
#define INSTR_H

#include <iostream>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <unordered_map>
#include <vector>

#include "utils/boa_types.hpp"

#include "edge.hpp"
#include "micro_instr.hpp"

namespace boa
{




enum class InstrSubType
{
  ADD,
  AND,
  CMP,
  DIV,
  DYN_CALL,
  DYN_JMP,
  FAKE_LIB_FUNC,
  IO,
  JCC,
  LEA,
  LEAVE,
  LOAD,
  MOV,
  MUL,
  NEG,
  NOP,
  NOT,
  NULL_,
  OR,
  POP,
  PUSH,
  REP,
  RET,
  SHIFT,
  STATIC_CALL,
  STATIC_JMP,
  STORE,
  SUB,
  TRAP,
  UNKNOWN,
  XOR
};

namespace instr
{

struct tainting_info_t
{
  std::set<Register> regs_write;
  std::set<Register> regs_read;

  std::set<const microinstr::mem_op_t*> mems_write;
  std::set<const microinstr::mem_op_t*> mems_read;
};

} // namespace instr

using uptrConstMicroInstr = std::unique_ptr<const MicroInstr>;

class Instr
{
  friend class DisassemblerCapstone;
  friend class DisassemblerRadare2;

public:
  // MARK:- Constructors and destructors
  Instr(const std::string& opcode, const std::vector<uint8_t>& bytes, const InstrType& type,
        const InstrSubType& sub_type, const addr_t& addr, unsigned int wave, const unsigned int& size,
        const addr_t& next_addr, const std::optional<addr_t>& jmp_addr, const std::optional<addr_t>& ptr_addr, const std::string& sect_name);
  Instr(const addr_t& addr, unsigned int wave);

  ~Instr() = default;

  // MARK:- Operators overloading
  friend std::ostream& operator<<(std::ostream& stream, const Instr& instr);
  friend bool operator<(const Instr& lhs, const Instr& rhs);
  friend bool operator==(const Instr& lhs, const Instr& rhs);
  friend bool operator!=(const Instr& lhs, const Instr& rhs);

  // MARK:- Getters and setters
  const std::string& getOpcode() const;
  const std::vector<uint8_t>& getBytes() const;
  addr_t getAddr() const;
  waddr_t getWaddr() const;
  addr_t getNextAddr() const;
  unsigned int getSize() const;
  const addr_t getJmpAddr() const;
  void SetJmpAddr(addr_t jmp_addr) const;
  const std::optional<addr_t>& getPtrAddr() const;
  const InstrSubType& getSubType() const;
  const InstrType& getType() const;
  const std::map<waddr_t, sptrConstEdge>& getEdges() const;
  const sptrConstEdge getEdge(const waddr_t& dst) const;
  std::vector<addr_t> getEdgesSet() const;
  const std::optional<std::vector<uptrConstMicroInstr>>& getMicroInstrs() const;
  std::string getBytesString() const;
  const instr::tainting_info_t& getTaintingInfo() const;
  const std::vector<addr_t> getChildren() const;
  std::string& getSectName() { return m_sect_name; }

  std::map<x86_reg, std::string> getListOpRegAccess();
  void addOperandRegAccess(x86_reg reg, std::string access);
  uint8_t getOperandCount();
  void setOperandCount(uint8_t nbr_op);

  // MARK:- Other functions
  void addEdge(const waddr_t& dst, const EdgeFoundMethod& found_method, std::shared_ptr<spdlog::logger> log) const;
  void addEdge(const waddr_t& dst, std::shared_ptr<spdlog::logger> log) const;
  void removeEdge(waddr_t dst, std::shared_ptr<spdlog::logger> log) const;
  std::string toString() const;
  std::string toStringLight() const;
  void pushBackMicroInstr(uptrConstMicroInstr micro_instr) const;
  void initMicroInstrs() const;
  std::string type2String() const;

	bool is_cflow_ins();
  // MARK:- Static functions
  static const char* typeToString(const InstrType& instr_type);
  static InstrType instrSubTypeToInstrType(const InstrSubType& instr_sub_type);
  static std::string bytesVectorToString(const std::vector<uint8_t>& bytes);

  // Registers (Dataflow)

  std::vector<x86_reg> getRegisters();

  std::vector<x86_reg> getWrittenRegs();
  std::vector<x86_reg_class> getWrittenRegsClasses();

  std::vector<x86_reg> getReadRegs();
  std::vector<x86_reg_class> getReadRegsClasses();


  size_t getHash() const;

private:
  // MARK:- Private member variables
  // (obtained from disassembly engine)
  std::string m_opcode;             // jne 0x80480a8
  std::vector<uint8_t> m_bytes;     // 7505
  InstrType m_type;                 // JCC
  InstrSubType m_sub_type;          // JCC
  addr_t m_addr;                    // 0x12345678
  unsigned int m_wave;              // 0
  unsigned int m_size;              // 2
  addr_t m_next_addr;               // 0x1234567a
  mutable std::optional<addr_t> m_jmp_addr; // 0x80480a8
  std::optional<addr_t> m_ptr_addr;
  std::string m_sect_name;          // .text

  // operands info

  uint8_t m_op_count; // number of operands
  std::map<x86_reg, std::string> m_list_op_reg_access; // <register, access (read, write, both)>

  // (obtained later depending on the disassembly mode)
  mutable std::map<waddr_t, sptrConstEdge> m_edges;

  // In symb exec mode
  mutable std::optional<std::vector<uptrConstMicroInstr>> m_micro_instrs;
  mutable std::optional<instr::tainting_info_t> m_tainting_info;
};

using sptrInstr = std::shared_ptr<Instr>;
using uptrConstInstr = std::shared_ptr<Instr>;

} // namespace boa

#endif
