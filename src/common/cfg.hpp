#ifndef CFG_H
#define CFG_H

#include <map>
#include <set>
#include <stdio.h>
#include <unordered_map>
#include <queue>
#include <sstream>
#include <stack>


#include "basic_block.hpp"
//#include "boa_disassembling/exec_formula.hpp"
#include "utils/boa_types.hpp"
#include "../thirdparty/md5/md5.h"
#include "analysis/bipartite_graph.hpp"



namespace boa
{

  enum dataflow_index{
  DF_ENTRY = 0,
  DF_EXIT = 1,
  DF_ELSE = 2
  };

//using UPtrExecFormula = std::unique_ptr<ExecFormula>;

//using uptrConstExecFormula = std::unique_ptr<const ExecFormula>;

// MARK:- Pathes and registers hex values
// I could differenciate paths to jmp target
// and paths to next addresses (ret of a call)
const uint16_t PATH = 0xffff;
const uint16_t PATH_DOTTED = 0xfffe;
const uint16_t PATH_UNDEF = 0xfffd;
const uint8_t PATH_8 = 0xff;
const uint8_t PATH_DOTTED_8 = 0xfe;
const uint8_t PATH_UNDEF_8 = 0xfd;
const uint MAX_SUBGRAPH_SIZE = 0xffff;

class CFG
{
public:
  // MARK:- Constructor
  CFG(const std::vector<addr_t>& entry_points, unsigned int wave);
  CFG(addr_t entry_point);


  // MARK:- Getters and setters

  // Basic blocks
  sptrBasicBlock getBasicBlockWithEntryPoint(addr_t addr) const;
  sptrBasicBlock getBasicBlock(const addr_t& addr) const;
  sptrBasicBlock getBasicBlockWithoutCheck(const addr_t& addr) const;

  sptrBasicBlock getBasicBlock(const Instr* instr) const;
  void removeBasicBlock(const addr_t& addr, std::shared_ptr<spdlog::logger> log);
  std::map<addr_t, sptrBasicBlock> getSortedBasicBlocks() const;
  std::vector<sptrBasicBlock> getVectorBasicBlocks() const;
  const std::unordered_map<addr_t, sptrBasicBlock>& getBasicBlocks() const;
  void insertBasicBlock(sptrBasicBlock basic_block);
  bool isBasicBlockExists(const addr_t& addr) const;
  bool isBasicBlockExists(const Instr* instr) const;
  sptrBasicBlock getCorrespondingBasicBlock(const addr_t& instr_addr) const;
  size_t getNumberofBBofColor(color_t i) const; 
  void setBasicBlockColor(const addr_t& addr, color_t color);
  std::unordered_set<sptrBasicBlock> getBBNeighbours(sptrBasicBlock BB);

  void mergeBasicBlocksAndUpdateGraph(sptrBasicBlock bb1, sptrBasicBlock bb2, std::shared_ptr<spdlog::logger>);

  // Neighbours
  std::set<sptrBasicBlock> getChildren(sptrBasicBlock bb);
  std::set<sptrBasicBlock> getParents(sptrBasicBlock bb) const;

  // Symbols

  void addFunSymbol(addr_t addr, std::string name);
  std::map<addr_t, std::string> getFunSymbolsMap();
  std::string getAddrFunSymbol(addr_t addr);
  bool isMainInSymbols();

  // Instrs
  // const Instr* getInstr(const addr_t &instr_addr) const;
  std::map<addr_t, const Instr*> getSortedInstrs() const;
  unsigned int getNumberInstrs() const;
  uptrConstInstr getInstr(addr_t addr);
  unsigned int getNumberOfInstrWithSubType(const InstrSubType& sub_type) const;
  std::set<const Instr*> getInstrsWithSubType(const InstrSubType& sub_type) const;
  bool isInstrExists(const addr_t& addr) const;
  //const std::map<addr_t, sptrBasicBlock> getDisassembledInstrs() const;
  void addBasicBlock(sptrBasicBlock bb_ptr);
  sptrBasicBlock createEmptyBB(addr_t addr);
  sptrBasicBlock createBB(uptrConstInstr bb_ep_instr);
  void pushBackInstrInBB(sptrBasicBlock bb, uptrConstInstr instr);
  void shrinkBB(std::shared_ptr<spdlog::logger> log, sptrBasicBlock bb, waddr_t first_instr_to_remove);
  sptrBasicBlock splitBB(std::shared_ptr<spdlog::logger> log, waddr_t new_bb_ep);
  
  // Entry point
  const std::unordered_set<addr_t>& getEntryPoints() const;
  addr_t getEntryPointAddr() const;
  addr_t getRandomEntryPointAddr() const;
  void insertEntryPoint(addr_t ep);
  void addEntryPoint(addr_t ep);
  void addExitPoint(addr_t ep);
  std::unordered_set<addr_t> getExitPoints() const{return m_exit_points;}

  // Edges  
  // std::set<const Edge*> getEdgesWithDst(const addr_t& dst) const;
  void insertEdge(sptrConstEdge edge);
  bool isEdgeInCFG(addr_t src, addr_t dst);
  std::unordered_set<sptrConstEdge> getEdges();
  std::string edges2string();


  // Dataflow

	std::vector<addr_t> getRandomWalk(std::shared_ptr<spdlog::logger> log);
  dataflow_index getDataflowIndex(sptrBasicBlock bb) const;
  std::set<sptrBipartite> getDataflowV2() const {return m_dataflow_graphs_v2;}

    // Stupid

  void extractDataflowStupid(std::shared_ptr<spdlog::logger> log);
  void dataflowExtractionStupidCore(std::map<sptrBasicBlock, dataflow_index> graph, std::shared_ptr<spdlog::logger> log);

    // Tensor
  void extractDataflowTensorFromSCCG(addr_t addr,std::shared_ptr<spdlog::logger> log);
  void extractDataflowTensor(std::shared_ptr<spdlog::logger> log);


 //void extractSCCDataflowGraphs(std::shared_ptr<spdlog::logger> log);


  const std::unordered_set<sptrBipartite>& getDataflowTensor() const{ return m_dataflow_tensor;}

  // Graph Reduction
  addr_t getSCCGId(){return m_id_addr;}
  void reduceGraph(std::shared_ptr<spdlog::logger> log);
  void addSCCGEdge(addr_t src, addr_t dst){m_scc_edges.insert(std::make_shared<Edge>(src, dst));}

  void getStronglyConnectedComponentsNodes(std::shared_ptr<spdlog::logger> log);
  void getStronglyConnectedComponentsEdges(std::shared_ptr<spdlog::logger> log);

  void sub_visit(sptrBasicBlock bb, std::unordered_set<sptrBasicBlock>* visited_set, std::stack<sptrBasicBlock>* L);

  void sub_assign(sptrBasicBlock bb,
  sptrBasicBlock root,
  std::unordered_set<sptrBasicBlock>* assigned_set,
  std::unordered_map<addr_t, std::unordered_set<sptrBasicBlock>>* scc_tmp_map);

  void addInputNode(addr_t addr){m_input_points.insert(addr);}
  void addInputNode(sptrBasicBlock node){m_input_points.insert(node->getEntryPointAddr());}
  std::unordered_set<addr_t> getInputNodes(){return m_input_points;}

  std::shared_ptr<CFG> getSCCGraphContaining(addr_t addr);
  std::shared_ptr<CFG> getSCCGraphContaining(sptrBasicBlock bb);

  // Other functions
  std::string createDotWithoutDigraph(bool is_reduced) const;
  std::string createDot(bool is_reduced) const;
  void createInstrsAndEdgesListTextFile(const std::string& instrs_list_filepath,
                                        const std::string& edges_list_filepath) const;
  void setEntryBlock(std::unordered_set<addr_t>& entry_points);
  void createBBsListTextFile(const std::string& filepath) const;

  // Print

  std::string printGraphDataflowTensor(const std::string& tab_depth);
  std::string printGraphDataflowTensor(const std::string& tab_depth, addr_t current_fun_ep);
  std::string printGraphSCC() const;


  // Other functions (stats)
  std::string printBasicBlocksList() const;
  unsigned int getNumberBasicBlocks() const;
  unsigned int getNumberEdges() const;
  unsigned int getApproximateNumberInstrs() const;

  std::string createDotWithoutDigraph(uint site_index, bool is_reduced);
  std::string createDataflowDotWithoutDigraph(uint site_index);
  virtual std::string createDotWithDataflow(bool is_reduced);

protected:

  std::unordered_set<addr_t> m_entry_points;
  std::unordered_set<addr_t> m_exit_points;
  std::unordered_set<addr_t> m_input_points; // not to be confused with entry points. these points are where other graphs enter this graph (useful for SCC graphs)
  unsigned int m_wave;

  // Disassembled BasicBlocks live here
  // Edges are here too
  std::unordered_map<addr_t, sptrBasicBlock> m_basic_blocks;

  std::map<addr_t, std::string> m_fun_symbols_map;

  // Strongly connected Component Subgraph
  std::set<std::shared_ptr<CFG>> m_scc_nodes; // scc := strongly connected components
  std::set<sptrConstEdge> m_scc_edges;
  addr_t m_id_addr; // lowest addr of the graph (useful for scc graphs identification)

  // Dataflow

  std::set<sptrBipartite> m_dataflow_graphs_v2; // each dataflow has the last inst type included in it
  //std::map<sptrBasicBlock, sptrBipartite> m_dataflow_tensor; // exit bb, corresponding bgraph
	std::unordered_set<sptrBipartite> m_dataflow_tensor;

private:
};

using sptrCFG = std::shared_ptr<CFG>;

} // namespace boa

#endif /* CFG_H */
