// Bridge with Radare2 lib

#ifndef RADARE2_UTILS_H
#define RADARE2_UTILS_H

#include "utils.hpp"
#include <r_socket.h>
#include <string>

namespace boa
{

class R2
{
public:
  R2(const std::string& bin_filepath, const std::string& parameters);

  R2() = delete;

  R2(R2 const&) = delete;
  R2& operator=(R2 const&) = delete;

  R2(R2&&) = delete;
  R2& operator=(R2&&) = delete;

  ~R2();

  std::string sendCmd(const std::string& cmd) const;

private:
  R2Pipe* m_r2;
  static bool s_r2_found_in_path;
};

} // namespace boa

#endif
