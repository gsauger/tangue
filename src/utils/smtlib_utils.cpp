#include "smtlib_utils.hpp"
#include <assert.h>
#include <stdexcept>

namespace boa
{

namespace smtlib_utils
{

/*!
 * Bitvector arithmetic
 */

std::string smt_bv_sub(const std::string& a, const std::string& b)
{
  return "(bvsub " + a + " " + b + ")";
}

std::string smt_bv_add(const std::string& a, const std::string& b)
{
  return "(bvadd " + a + " " + b + ")";
}

std::string smt_bv_mul(const std::string& a, const std::string& b)
{
  return "(bvmul " + a + " " + b + ")";
}

/*!
 * Predicates over Bitvectors
 */

std::string smt_bv_ule(const std::string& a, const std::string& b)
{
  return "(bvule " + a + " " + b + ")";
}

std::string smt_bv_ult(const std::string& a, const std::string& b)
{
  return "(bvult " + a + " " + b + ")";
}

std::string smt_bv_uge(const std::string& a, const std::string& b)
{
  return "(bvuge " + a + " " + b + ")";
}

std::string smt_bv_ugt(const std::string& a, const std::string& b)
{
  return "(bvugt " + a + " " + b + ")";
}

std::string smt_bv_sle(const std::string& a, const std::string& b)
{
  return "(bvsle " + a + " " + b + ")";
}

std::string smt_bv_slt(const std::string& a, const std::string& b)
{
  return "(bvslt " + a + " " + b + ")";
}

std::string smt_bv_sge(const std::string& a, const std::string& b)
{
  return "(bvsge " + a + " " + b + ")";
}

std::string smt_bv_sgt(const std::string& a, const std::string& b)
{
  return "(bvsgt " + a + " " + b + ")";
}

/*!
 * Autre SMT
 */

std::string smt_equal(const std::string& a, const std::string& b)
{
  return "(= " + a + " " + b + ")";
}

std::string smt_not(const std::string& symbol)
{
  return "(not " + symbol + ")";
}

std::string smt_comment(const std::string& comment)
{
  return "\n; ------[ " + comment + " ]------\n";
}

std::string smt_or(const std::vector<std::string>& terms)
{
  if(terms.size() == 1)
  {
    return terms.at(0);
  }
  std::string s = "(or";
  for(const auto& term : terms)
  {
    s += " " + term;
  }
  s += ")";
  return s;
}

std::string smt_or(const std::string& a, const std::string& b)
{
  return "(or " + a + " " + b + ")";
}

std::string smt_and(const std::vector<std::string>& terms)
{
  if(terms.size() == 1)
  {
    return terms.at(0);
  }
  std::string s = "(and";
  for(const auto& term : terms)
  {
    s += " " + term;
  }
  s += ")";
  return s;
}

std::string smt_and(const std::string& a, const std::string& b)
{
  return "(and " + a + " " + b + ")";
}

std::string smt_bv_val(const uint32_t& value, unsigned int size)
{
  return "(_ bv" + std::to_string(value) + " " + std::to_string(size) + ")";
}

std::string smt_concat(const std::string& a, const std::string& b)
{
  return "(concat " + a + " " + b + ")";
}

// Fonction select
std::string smt_select(const std::string& array, const std::string& idx)
{
  return "(select " + array + " " + idx + ")";
}

// Pour récupérer 2 octets en mémoire l'@ donnée.
std::string smt_select_2_array_cases(const std::string& array, const std::string& first_idx)
{
  std::string addr1 = first_idx;
  std::string addr2 = smt_bv_add(first_idx, smt_bv_val((uint32_t)1, 32));

  std::string select1 = smt_select(array, addr1);
  std::string select2 = smt_select(array, addr2);

  std::string mem_case = select1;
  mem_case = smt_concat(select2, mem_case);

  return mem_case;
}

// Pour récupérer 4 octets en mémoire l'@ donnée.
std::string smt_select_4_array_cases(const std::string& array, const std::string& first_idx)
{
  std::string addr1 = first_idx;
  std::string addr2 = smt_bv_add(first_idx, smt_bv_val((uint32_t)1, 32));
  std::string addr3 = smt_bv_add(first_idx, smt_bv_val((uint32_t)2, 32));
  std::string addr4 = smt_bv_add(first_idx, smt_bv_val((uint32_t)3, 32));

  std::string select1 = smt_select(array, addr1);
  std::string select2 = smt_select(array, addr2);
  std::string select3 = smt_select(array, addr3);
  std::string select4 = smt_select(array, addr4);

  std::string mem_case = select1;
  mem_case = smt_concat(select2, mem_case);
  mem_case = smt_concat(select3, mem_case);
  mem_case = smt_concat(select4, mem_case);

  return mem_case;
}

std::string smt_select_n_array_cases(unsigned int n, std::string array, std::string first_idx)
{
  if(n == 4)
  {
    return smtlib_utils::smt_select_4_array_cases(array, first_idx);
  }
  else if(n == 2)
  {
    return smtlib_utils::smt_select_2_array_cases(array, first_idx);
  }
  else if(n == 1)
  {
    return smtlib_utils::smt_select(array, first_idx);
  }
  else
  {
    throw std::runtime_error("Memory operation with a size different from 1, 2 or 4 bytes 0_o?");
  }
}

// If then else
std::string smt_ite(const std::string& cond, const std::string& term_if_true, const std::string& term_if_false)
{
  return "(ite " + cond + " " + term_if_true + " " + term_if_false + ")";
}

/*!
 * Les sorts SMT
 */

std::string smt_sort_bv(const int& size)
{
  return "(_ BitVec " + std::to_string(size) + ")";
}

std::string smt_sort_array(const std::string& idx_sort, const std::string& elt_sort)
{
  return "(Array " + idx_sort + " " + elt_sort + ")";
}

/*!
 * Les commandes SMT
 */

std::string smt_assert(const std::string& term)
{
  return "(assert " + term + ")\n";
}

std::string smt_check_sat()
{
  return "(check-sat)\n";
}

std::string smt_decl_const(const std::string& symbol, const std::string& sort)
{
  return "(declare-const " + symbol + " " + sort + ")\n";
}

std::string smt_decl_fun(const std::string& symbol, const std::string& sort)
{
  return "(declare-fun " + symbol + " () " + sort + ")\n";
}

std::string smt_def_fun(const std::string& symbol, const std::string& sort, const std::string& term)
{
  return "(define-fun " + symbol + " () " + sort + " " + term + ")\n";
}

std::string smt_exit()
{
  return "(exit)\n";
}

std::string smt_get_model()
{
  return "(get-model)\n";
}

std::string smt_get_value(const std::string& term)
{
  return "(get-value (" + term + "))\n";
}

std::string smt_get_value(const std::unordered_set<std::string>& terms)
{
  assert(!terms.empty());
  std::string s = "(get-value (";
  for(const auto& term : terms)
  {
    s += term + " ";
  }
  s.pop_back();
  s += "))\n";
  return s;
}

std::string smt_get_value(const std::unordered_map<std::string, uint32_t>& terms)
{
  assert(!terms.empty());
  std::string s = "(get-value (";
  for(const auto& term_p : terms)
  {
    s += term_p.first + " ";
  }
  s.pop_back();
  s += "))\n";
  return s;
}

std::string smt_get_value(const std::vector<std::string>& terms)
{
  assert(!terms.empty());
  std::string s = "(get-value (";
  for(const auto& term : terms)
  {
    s += term + " ";
  }
  s.pop_back();
  s += "))\n";
  return s;
}

std::string smt_pop()
{
  return "(pop 1)\n";
}

std::string smt_push()
{
  return "(push 1)\n";
}

std::string smt_reset()
{
  return "(reset)\n";
}

std::string smt_set_info(const std::string& attribute)
{
  return "(set-info " + attribute + ")\n";
}

std::string smt_set_logic(const std::string& symbol)
{
  return "(set-logic " + symbol + ")\n";
}

std::string smt_set_option(const std::string& option)
{
  return "(set-option " + option + ")\n";
}

} // namespace smtlib_utils

} // namespace boa
