#include "boa_types.hpp"

#include <algorithm>
#include <iomanip>
#include <sstream>
#include <string>

//#include "utils/smtlib_utils.hpp"

namespace boa
{

// MARK:- Permissions
std::string perms2s(const std::vector<Permission>& perms)
{
  static const std::map<std::vector<Permission>, std::string> perms2s_map = {
      {{}, "----"},
      {{Permission::READ}, "-r--"},
      {{Permission::READ, Permission::WRITE}, "-rw-"},
      {{Permission::READ, Permission::EXECUTE}, "-r-x"},
      {{Permission::READ, Permission::WRITE, Permission::EXECUTE}, "-rwx"}};

  if(perms2s_map.find(perms) != perms2s_map.end())
  {
    return perms2s_map.at(perms);
  }
  return "????";
}

// MARK:- Memory addresses
std::string addr2s(addr_t addr)
{
  std::stringstream s;
  s << "0x" << std::hex << addr;
  return s.str();
}

std::string waddr2s(waddr_t waddr)
{
  std::stringstream s;
  s << waddr.first << "_0x" << std::hex << waddr.second;
  return s.str();
}

std::string mem_case2s(addr_t addr)
{
  return "@[" + addr2s(addr) + "]";
}

std::string addrs2s(const std::unordered_set<addr_t>& values, const std::string& sep)
{
  if(values.empty())
    return "";
  std::string s = "";
  for(auto v : values)
    s = s + addr2s(v) + sep;
  return s.substr(0, s.length() - sep.length());
}

std::string addrs2s(const std::set<addr_t>& values, const std::string& sep)
{
  if(values.empty())
    return "";
  std::string s = "";
  for(auto v : values)
    s = s + addr2s(v) + sep;
  return s.substr(0, s.length() - sep.length());
}

std::string addrs2s(const std::deque<addr_t>& values, const std::string& sep)
{
  if(values.empty())
    return "";
  std::string s = "";
  for(auto v : values)
    s = s + addr2s(v) + sep;
  return s.substr(0, s.length() - sep.length());
}

std::string addrs2s(const std::vector<addr_t>& values, const std::string& sep)
{
  if(values.empty())
    return "";
  std::string s = "";
  for(auto v : values)
    s = s + addr2s(v) + sep;
  return s.substr(0, s.length() - sep.length());
}

std::string waddrs2s(const std::set<waddr_t>& values, const std::string& sep)
{
  if(values.empty())
    return "";
  std::string s = "";
  for(auto v : values)
    s = s + waddr2s(v) + sep;
  return s.substr(0, s.length() - sep.length());
}

std::string waddrs2s(const std::deque<waddr_t>& values, const std::string& sep)
{
  if(values.empty())
    return "";
  std::string s = "";
  for(auto const& v : values)
    s = s + waddr2s(v) + sep;
  return s.substr(0, s.length() - sep.length());
}

// MARK:- Byte
std::string byte2s(byte_t v, bool with_0x_prefix)
{
  std::stringstream stream;
  if(with_0x_prefix)
  {
    stream << "0x";
  }
  stream << std::setfill('0') << std::setw(2) << std::hex << static_cast<int>(v);
  return stream.str();
}

std::string bytes2s(const std::unordered_set<byte_t>& values)
{
  if(values.empty())
    return "";
  std::string s = "";
  for(auto const& v : values)
    s = s + byte2s(v, true) + "; ";
  return s.substr(0, s.length() - 2);
}

std::string bytes2s(const std::set<byte_t>& values)
{
  if(values.empty())
    return "";
  std::string s = "";
  for(auto const& v : values)
    s = s + byte2s(v, true) + "; ";
  return s.substr(0, s.length() - 2);
}

// MARK:- Optimisaiton levels

OptLevel str2opt(const std::string& opt_level_str){
	if(opt_level_str == "O0"){ return OptLevel::Opt_0;}
	if(opt_level_str == "O1"){ return OptLevel::Opt_1;}
	if(opt_level_str == "O2"){ return OptLevel::Opt_2;}
	if(opt_level_str == "O3"){ return OptLevel::Opt_3;}
	if(opt_level_str == "Of"){ return OptLevel::Opt_f;}
	if(opt_level_str == "Ou"){ return OptLevel::Opt_unk;}
	else{
		return OptLevel::Opt_unk;
    //throw std::runtime_error("Unknown optimization type (" + opt_level_str + "), not 0,1,2,3 or f\n");
  }
}

std::string opt2str(OptLevel opt){
	if(OptLevel::Opt_0 == opt){return "O0";}
	if(OptLevel::Opt_1 == opt){return "O1";}
	if(OptLevel::Opt_2 == opt){return "O2";}
	if(OptLevel::Opt_3 == opt){return "O3";}
	if(OptLevel::Opt_f == opt){return "Of";}
	if(OptLevel::Opt_unk == opt){return "Ou";}
	else{
    throw std::runtime_error("Unknown optimization type (str), not 0,1,2,3 or f\n");
  }
}

BinaryFormat str2format(const std::string& format_str){
	if(format_str == "elf"){ return BinaryFormat::ELF;}
	if(format_str == "pe"){ return BinaryFormat::PE;}
	if(format_str == "macho"){ return BinaryFormat::MACHO;}
	if(format_str == "raw"){ return BinaryFormat::RAW;}
	if(format_str == "auto"){ return BinaryFormat::AUTO;}
	else{
		return BinaryFormat::AUTO;
    //throw std::runtime_error("Unknown binary format\n");
  }
}

std::string format2str(BinaryFormat opt){
	if(BinaryFormat::ELF == opt){return "ELF";}
	if(BinaryFormat::AUTO == opt){return "AUTO";}
	if(BinaryFormat::PE == opt){return "PE";}
	if(BinaryFormat::MACHO == opt){return "MACHO";}
	if(BinaryFormat::RAW == opt){return "RAW";}
	else{
    throw std::runtime_error("Unknown format (str)\n");
  }
}

BinaryArch str2arch(const std::string& format_str){
	if(format_str == "X86" || format_str == "x86"){ return BinaryArch::x86;}
	if(format_str == "X86_64" || format_str == "x86_64" || format_str == "x64" || format_str == "X64"){ return BinaryArch::x86_64;}
	if(format_str == "NONE"){ return BinaryArch::NONE;}
	else{
		return BinaryArch::NONE;
    //throw std::runtime_error("Unknown binary architecture\n");
  }
}

std::string arch2str(BinaryArch opt){
	if(BinaryArch::x86 == opt){return "X86";}
	if(BinaryArch::x86_64 == opt){return "X86_64";}
	if(BinaryArch::NONE == opt){return "NONE";}
	else{
    throw std::runtime_error("Unknown architecture (str)\n");
  }
}

BinaryCompiler str2compiler(const std::string& format_str){
	if(format_str.find("GCC") != std::string::npos){ return BinaryCompiler::GCC;}
	if(format_str.find("CLANG") != std::string::npos){ return BinaryCompiler::CLANG;}
	if(format_str.find("NONE") != std::string::npos){ return BinaryCompiler::NONE;}
	else{
		return BinaryCompiler::NONE;
    //throw std::runtime_error("Unknown binary architecture\n");
  }
}

std::string compiler2str(BinaryCompiler opt){
	if(BinaryCompiler::GCC == opt){return "GCC";}
	if(BinaryCompiler::CLANG == opt){return "CLANG";}
	if(BinaryCompiler::NONE == opt){return "NONE";}
	else{
    throw std::runtime_error("Unknown compiler (str)\n");
  }
}

// MARK:- Registers
std::unordered_map<std::string, Register> s2reg_map = {
    {"eax", Register::EAX},    {"ebx", Register::EBX}, {"ecx", Register::ECX}, {"edx", Register::EDX},
    {"edi", Register::EDI},    {"esi", Register::ESI}, {"esp", Register::ESP}, {"ebp", Register::EBP},
    {"OF", Register::OF},      {"SF", Register::SF},   {"ZF", Register::ZF},   {"PF", Register::PF},
    {"CF", Register::CF},      {"AF", Register::AF},   {"DF", Register::DF},   {"TF", Register::TF},
    {"fs_base", Register::FS}, {"dr0", Register::DR0}, {"dr1", Register::DR1}, {"dr2", Register::DR2},
    {"dr3", Register::DR3},    {"dr6", Register::DR6}, {"dr7", Register::DR7}};

std::unordered_map<Register, std::string> reg2s_map = {
    {Register::EAX, "eax"},    {Register::EBX, "ebx"}, {Register::ECX, "ecx"}, {Register::EDX, "edx"},
    {Register::EDI, "edi"},    {Register::ESI, "esi"}, {Register::ESP, "esp"}, {Register::EBP, "ebp"},
    {Register::OF, "OF"},      {Register::SF, "SF"},   {Register::ZF, "ZF"},   {Register::PF, "PF"},
    {Register::CF, "CF"},      {Register::AF, "AF"},   {Register::DF, "DF"},   {Register::TF, "TF"},
    {Register::FS, "fs_base"}, {Register::DR0, "dr0"}, {Register::DR1, "dr1"}, {Register::DR2, "dr2"},
    {Register::DR3, "dr3"},    {Register::DR6, "dr6"}, {Register::DR7, "dr7"}};

std::unordered_map<Register, addr_t> reg2size_map = {
    {Register::EAX, 32},    {Register::EBX, 32}, {Register::ECX, 32}, {Register::EDX, 32},
    {Register::EDI, 32},    {Register::ESI, 32}, {Register::ESP, 32}, {Register::EBP, 32},
    {Register::OF, 1},      {Register::SF, 1},   {Register::ZF, 1},   {Register::PF, 1},
    {Register::CF, 1},      {Register::AF, 1},   {Register::DF, 1},   {Register::TF, 1},
    {Register::FS, 32}, {Register::DR0, 32}, {Register::DR1, 32}, {Register::DR2, 32},
    {Register::DR3, 32},    {Register::DR6, 32}, {Register::DR7, 32}};
/*
std::unordered_map<Register, std::pair<std::string, std::string>> reg2smt_map = {
  {Register::EAX, {"eax", smtlib_utils::smt_sort_bv(32)}},
  {Register::EBX, {"ebx", smtlib_utils::smt_sort_bv(32)}},
  {Register::ECX, {"ecx", smtlib_utils::smt_sort_bv(32)}},
  {Register::EDX, {"edx", smtlib_utils::smt_sort_bv(32)}},
  {Register::EDI, {"edi", smtlib_utils::smt_sort_bv(32)}},
  {Register::ESI, {"esi", smtlib_utils::smt_sort_bv(32)}},
  {Register::ESP, {"esp", smtlib_utils::smt_sort_bv(32)}},
  {Register::EBP, {"ebp", smtlib_utils::smt_sort_bv(32)}},
  {Register::OF, {"OF", smtlib_utils::smt_sort_bv(1)}},
  {Register::SF, {"SF", smtlib_utils::smt_sort_bv(1)}},
  {Register::ZF, {"ZF", smtlib_utils::smt_sort_bv(1)}},
  {Register::PF, {"PF", smtlib_utils::smt_sort_bv(1)}},
  {Register::CF, {"CF", smtlib_utils::smt_sort_bv(1)}},
  {Register::AF, {"AF", smtlib_utils::smt_sort_bv(1)}},
  {Register::DF, {"DF", smtlib_utils::smt_sort_bv(1)}},
  {Register::TF, {"TF", smtlib_utils::smt_sort_bv(1)}},
  {Register::FS, {"fs_base", smtlib_utils::smt_sort_bv(32)}},
  {Register::DR0, {"dr0", smtlib_utils::smt_sort_bv(32)}},
  {Register::DR1, {"dr1", smtlib_utils::smt_sort_bv(32)}},
  {Register::DR2, {"dr2", smtlib_utils::smt_sort_bv(32)}},
  {Register::DR3, {"dr3", smtlib_utils::smt_sort_bv(32)}},
  {Register::DR6, {"dr6", smtlib_utils::smt_sort_bv(32)}},
  {Register::DR7, {"dr7", smtlib_utils::smt_sort_bv(32)}}};
*/
const std::string& reg2s(Register name)
{
  auto r = reg2s_map.find(name);
  if(r == reg2s_map.end())
  {
    throw std::runtime_error("Unknown register given to 'reg2s'");
  }
  return r->second;
}

addr_t reg2size(Register name)
{
  auto r = reg2size_map.find(name);
  if(r == reg2size_map.end())
  {
    throw std::runtime_error("Unknown register given to 'reg2size_map'");
  }
  return r->second;
}
/*
const std::pair<std::string, std::string>& reg2smt(Register name)
{
  auto r = reg2smt_map.find(name);
  if(r == reg2smt_map.end())
  {
    throw std::runtime_error("Unknown register given to 'reg2smt_map'");
  }
  return r->second;
}
*/
bool isAValidReg(const std::string& s)
{
  if(s2reg_map.find(s) == s2reg_map.end())
  {
    return false;
  }
  return true;
}

Register s2reg(const std::string& s)
{
  if(!isAValidReg(s))
  {
    throw std::runtime_error("Unknown register: " + s);
  }
  return s2reg_map.at(s);
}

const std::unordered_map<Register, std::string>& getAllCpuRegs()
{
  return reg2s_map;
}

x86_reg_class getRegClass(x86_reg reg){
  x86_reg_class reg_class;
  switch (reg){

    case X86_REG_INVALID:
      reg_class = X86_CLASS_INVALID;
      break;

    case X86_REG_AH:
    case X86_REG_AL:
    case X86_REG_AX:
    case X86_REG_EAX:
    case X86_REG_RAX:
      reg_class = X86_CLASS_A;
      break;

    case X86_REG_CH:
    case X86_REG_CL:
    case X86_REG_CX:
    case X86_REG_ECX:
    case X86_REG_RCX:
      reg_class = X86_CLASS_C;
      break;

    case X86_REG_DH:
    case X86_REG_DL:
    case X86_REG_DX:
    case X86_REG_EDX:
    case X86_REG_RDX:
      reg_class = X86_CLASS_D;
      break;

    case X86_REG_BL:
    case X86_REG_BH:
    case X86_REG_BX:
    case X86_REG_EBX:
    case X86_REG_RBX:
      reg_class = X86_CLASS_B;
      break;

    case X86_REG_RSP:
    case X86_REG_ESP:
    case X86_REG_SP:
    case X86_REG_SPL:
      reg_class = X86_CLASS_SP;
      break;

    case X86_REG_RBP:
    case X86_REG_EBP:
    case X86_REG_BP:
    case X86_REG_BPL:
      reg_class = X86_CLASS_BP;
      break;

    case X86_REG_RSI:
    case X86_REG_ESI:
    case X86_REG_SI:
    case X86_REG_SIL:
      reg_class = X86_CLASS_SI;
      break;

    case X86_REG_DIL:
    case X86_REG_DI:
    case X86_REG_EDI:
    case X86_REG_RDI:
      reg_class = X86_CLASS_DI;
      break;

    case X86_REG_DS:
    case X86_REG_CS:
    case X86_REG_ES:
    case X86_REG_FS:
    case X86_REG_GS:
    case X86_REG_SS:
      reg_class = X86_CLASS_SEGMENT;
      break;
      
    case X86_REG_EFLAGS:
      reg_class = X86_CLASS_EFLAGS;
      break;

    case X86_REG_IP:
    case X86_REG_EIP:
    case X86_REG_RIP:
      reg_class = X86_CLASS_INST;
      break;

    case X86_REG_CR0:
    case X86_REG_CR1:
    case X86_REG_CR2:
    case X86_REG_CR3:
    case X86_REG_CR4:
    case X86_REG_CR5:
    case X86_REG_CR6:
    case X86_REG_CR7:
    case X86_REG_CR8:
    case X86_REG_CR9:
    case X86_REG_CR11:
    case X86_REG_CR12:
    case X86_REG_CR13:
    case X86_REG_CR14:
    case X86_REG_CR15:
      reg_class = X86_CLASS_CR;
      break;

    case X86_REG_DR0:
    case X86_REG_DR1:
    case X86_REG_DR2:
    case X86_REG_DR3:
    case X86_REG_DR4:
    case X86_REG_DR5:
    case X86_REG_DR6:
    case X86_REG_DR7:
    case X86_REG_DR8:
    case X86_REG_DR9:
    case X86_REG_DR10:
    case X86_REG_DR11:
    case X86_REG_DR12:
    case X86_REG_DR13:
    case X86_REG_DR14:
    case X86_REG_DR15:
      reg_class = X86_CLASS_DR;
      break;

    case X86_REG_FPSW:
    case X86_REG_EIZ:
    case X86_REG_RIZ:
    case X86_REG_FP0:
    case X86_REG_FP1:
    case X86_REG_FP2:
    case X86_REG_FP3:
    case X86_REG_FP4:
    case X86_REG_FP5:
    case X86_REG_FP6:
    case X86_REG_FP7:
    case X86_REG_K0:
    case X86_REG_K1:
    case X86_REG_K2:
    case X86_REG_K3:
    case X86_REG_K4:
    case X86_REG_K5:
    case X86_REG_K6:
    case X86_REG_K7:
    case X86_REG_MM0:
    case X86_REG_MM1:
    case X86_REG_MM2:
    case X86_REG_MM3:
    case X86_REG_MM4:
    case X86_REG_MM5:
    case X86_REG_MM6:
    case X86_REG_MM7:
    case X86_REG_ST0:
    case X86_REG_ST1:
    case X86_REG_ST2:
    case X86_REG_ST3:
    case X86_REG_ST4:
    case X86_REG_ST5:
    case X86_REG_ST6:
    case X86_REG_ST7:
      reg_class = X86_CLASS_MYSTERY;
      break;

    case X86_REG_R8:
    case X86_REG_R9:
    case X86_REG_R10:
    case X86_REG_R11:
    case X86_REG_R12:
    case X86_REG_R13:
    case X86_REG_R14:
    case X86_REG_R15:
    case X86_REG_XMM0:
    case X86_REG_XMM1:
    case X86_REG_XMM2:
    case X86_REG_XMM3:
    case X86_REG_XMM4:
    case X86_REG_XMM5:
    case X86_REG_XMM6:
    case X86_REG_XMM7:
    case X86_REG_XMM8:
    case X86_REG_XMM9:
    case X86_REG_XMM10:
    case X86_REG_XMM11:
    case X86_REG_XMM12:
    case X86_REG_XMM13:
    case X86_REG_XMM14:
    case X86_REG_XMM15:
    case X86_REG_XMM16:
    case X86_REG_XMM17:
    case X86_REG_XMM18:
    case X86_REG_XMM19:
    case X86_REG_XMM20:
    case X86_REG_XMM21:
    case X86_REG_XMM22:
    case X86_REG_XMM23:
    case X86_REG_XMM24:
    case X86_REG_XMM25:
    case X86_REG_XMM26:
    case X86_REG_XMM27:
    case X86_REG_XMM28:
    case X86_REG_XMM29:
    case X86_REG_XMM30:
    case X86_REG_XMM31:
    case X86_REG_YMM0:
    case X86_REG_YMM1:
    case X86_REG_YMM2:
    case X86_REG_YMM3:
    case X86_REG_YMM4:
    case X86_REG_YMM5:
    case X86_REG_YMM6:
    case X86_REG_YMM7:
    case X86_REG_YMM8:
    case X86_REG_YMM9:
    case X86_REG_YMM10:
    case X86_REG_YMM11:
    case X86_REG_YMM12:
    case X86_REG_YMM13:
    case X86_REG_YMM14:
    case X86_REG_YMM15:
    case X86_REG_YMM16:
    case X86_REG_YMM17:
    case X86_REG_YMM18:
    case X86_REG_YMM19:
    case X86_REG_YMM20:
    case X86_REG_YMM21:
    case X86_REG_YMM22:
    case X86_REG_YMM23:
    case X86_REG_YMM24:
    case X86_REG_YMM25:
    case X86_REG_YMM26:
    case X86_REG_YMM27:
    case X86_REG_YMM28:
    case X86_REG_YMM29:
    case X86_REG_YMM30:
    case X86_REG_YMM31:
    case X86_REG_ZMM0:
    case X86_REG_ZMM1:
    case X86_REG_ZMM2:
    case X86_REG_ZMM3:
    case X86_REG_ZMM4:
    case X86_REG_ZMM5:
    case X86_REG_ZMM6:
    case X86_REG_ZMM7:
    case X86_REG_ZMM8:
    case X86_REG_ZMM9:
    case X86_REG_ZMM10:
    case X86_REG_ZMM11:
    case X86_REG_ZMM12:
    case X86_REG_ZMM13:
    case X86_REG_ZMM14:
    case X86_REG_ZMM15:
    case X86_REG_ZMM16:
    case X86_REG_ZMM17:
    case X86_REG_ZMM18:
    case X86_REG_ZMM19:
    case X86_REG_ZMM20:
    case X86_REG_ZMM21:
    case X86_REG_ZMM22:
    case X86_REG_ZMM23:
    case X86_REG_ZMM24:
    case X86_REG_ZMM25:
    case X86_REG_ZMM26:
    case X86_REG_ZMM27:
    case X86_REG_ZMM28:
    case X86_REG_ZMM29:
    case X86_REG_ZMM30:
    case X86_REG_ZMM31:
    case X86_REG_R8B:
    case X86_REG_R9B:
    case X86_REG_R10B:
    case X86_REG_R11B:
    case X86_REG_R12B:
    case X86_REG_R13B:
    case X86_REG_R14B:
    case X86_REG_R15B:
    case X86_REG_R8D:
    case X86_REG_R9D:
    case X86_REG_R10D:
    case X86_REG_R11D:
    case X86_REG_R12D:
    case X86_REG_R13D:
    case X86_REG_R14D:
    case X86_REG_R15D:
    case X86_REG_R8W:
    case X86_REG_R9W:
    case X86_REG_R10W:
    case X86_REG_R11W:
    case X86_REG_R12W:
    case X86_REG_R13W:
    case X86_REG_R14W:
    case X86_REG_R15W:
      reg_class = X86_CLASS_R;
      break;

    case X86_REG_ENDING:
      reg_class = X86_CLASS_ENDING;
        break;
    default:
      reg_class = X86_CLASS_INVALID;
      break;

  }
  return reg_class;

}

// MARK:- Exceptions
uint32_t exc2code(Exception name)
{
  switch(name)
  {
  case Exception::ACCESS_VIOLATION:
    return 0xc0000005U;
  case Exception::INTEGER_DIVIDE_BY_ZERO:
    return 0xc0000094U;
  case Exception::BREAKPOINT:
    return 0x80000003U;
  case Exception::SINGLE_STEP:
    return 0x80000004U;
  case Exception::ILLEGAL_INSTRUCTION:
    return 0xc000001dU;
  }
  return 0x00000000U;
}

std::string exc2s(Exception name)
{
  switch(name)
  {
  case Exception::ACCESS_VIOLATION:
    return "ACCESS_VIOLATION";
  case Exception::INTEGER_DIVIDE_BY_ZERO:
    return "INTEGER_DIVIDE_BY_ZERO";
  case Exception::BREAKPOINT:
    return "BREAKPOINT";
  case Exception::SINGLE_STEP:
    return "SINGLE_STEP";
  case Exception::ILLEGAL_INSTRUCTION:
    return "ILLEGAL_INSTRUCTION";
  }
  return "unknown exception";
}

std::string reg2str(uint16_t reg){
  std::string reg_name;
  switch(reg){
    case X86_REG_INVALID:
      reg_name = "INVALID";
      break;
    case X86_REG_AH:
      reg_name = "AH";
      break;
    case X86_REG_AL:
      reg_name = "AL";
      break;
    case X86_REG_AX:
      reg_name = "AX";
      break;
    case X86_REG_BH:
      reg_name = "BH";
      break;
    case X86_REG_BL:
      reg_name = "BL";
      break;
    case X86_REG_BP:
      reg_name = "BP";
      break;
    case X86_REG_BPL:
      reg_name = "BPL";
      break;
    case X86_REG_BX:
      reg_name = "BX";
      break;
    case X86_REG_CH:
      reg_name = "CH";
      break;
    case X86_REG_CL:
      reg_name = "CL";
      break;
    case X86_REG_CS:
      reg_name = "CS";
      break;
    case X86_REG_CX:
      reg_name = "CX";
      break;
    case X86_REG_DH:
      reg_name = "DH";
      break;
    case X86_REG_DI:
      reg_name = "DI";
      break;
    case X86_REG_DIL:
      reg_name = "DIL";
      break;
    case X86_REG_DL:
      reg_name = "DL";
      break;
    case X86_REG_DS:
      reg_name = "DS";
      break;
    case X86_REG_DX:
      reg_name = "DX";
      break;
    case X86_REG_EAX:
      reg_name = "EAX";
      break;
    case X86_REG_EBP:
      reg_name = "EBP";
      break;
    case X86_REG_EBX:
      reg_name = "EBX";
      break;
    case X86_REG_ECX:
      reg_name = "ECX";
      break;
    case X86_REG_EDI:
      reg_name = "EDI";
      break;
    case X86_REG_EDX:
      reg_name = "EDX";
      break;
    case X86_REG_EFLAGS:
      reg_name = "EFLAGS";
      break;
    case X86_REG_EIP:
      reg_name = "EIP";
      break;
    case X86_REG_EIZ:
      reg_name = "EIZ";
      break;
    case X86_REG_ES:
      reg_name = "ES";
      break;
    case X86_REG_ESI:
      reg_name = "ESI";
      break;
    case X86_REG_ESP:
      reg_name = "ESP";
      break;
    case X86_REG_FPSW:
      reg_name = "FPSW";
      break;
    case X86_REG_FS:
      reg_name = "FS";
      break;
    case X86_REG_GS:
      reg_name = "GS";
      break;
    case X86_REG_IP:
      reg_name = "IP";
      break;
    case X86_REG_RAX:
      reg_name = "RAX";
      break;
    case X86_REG_RBP:
      reg_name = "RBP";
      break;
    case X86_REG_RBX:
      reg_name = "RBX";
      break;
    case X86_REG_RCX:
      reg_name = "RCX";
      break;
    case X86_REG_RDI:
      reg_name = "RDI";
      break;
    case X86_REG_RDX:
      reg_name = "RDX";
      break;
    case X86_REG_RIP:
      reg_name = "RIP";
      break;
    case X86_REG_RIZ:
      reg_name = "RIZ";
      break;
    case X86_REG_RSI:
      reg_name = "RSI";
      break;
    case X86_REG_RSP:
      reg_name = "RSP";
      break;
    case X86_REG_SI:
      reg_name = "SI";
      break;
    case X86_REG_SIL:
      reg_name = "SIL";
      break;
    case X86_REG_SP:
      reg_name = "SP";
      break;
    case X86_REG_SPL:
      reg_name = "SPL";
      break;
    case X86_REG_SS:
      reg_name = "SS";
      break;
    case X86_REG_CR0:
      reg_name = "CR0";
      break;
    case X86_REG_CR1:
      reg_name = "CR1";
      break;
    case X86_REG_CR2:
      reg_name = "CR2";
      break;
    case X86_REG_CR3:
      reg_name = "CR3";
      break;
    case X86_REG_CR4:
      reg_name = "CR4";
      break;
    case X86_REG_CR5:
      reg_name = "CR5";
      break;
    case X86_REG_CR6:
      reg_name = "CR6";
      break;
    case X86_REG_CR7:
      reg_name = "CR7";
      break;
    case X86_REG_CR8:
      reg_name = "CR8";
      break;
    case X86_REG_CR9:
      reg_name = "CR9";
      break;
    case X86_REG_CR10:
      reg_name = "CR10";
      break;
    case X86_REG_CR11:
      reg_name = "CR11";
      break;
    case X86_REG_CR12:
      reg_name = "CR12";
      break;
    case X86_REG_CR13:
      reg_name = "CR13";
      break;
    case X86_REG_CR14:
      reg_name = "CR14";
      break;
    case X86_REG_CR15:
      reg_name = "CR15";
      break;
    case X86_REG_DR0:
      reg_name = "DR0";
      break;
    case X86_REG_DR1:
      reg_name = "DR1";
      break;
    case X86_REG_DR2:
      reg_name = "DR2";
      break;
    case X86_REG_DR3:
      reg_name = "DR3";
      break;
    case X86_REG_DR4:
      reg_name = "DR4";
      break;
    case X86_REG_DR5:
      reg_name = "DR5";
      break;
    case X86_REG_DR6:
      reg_name = "DR6";
      break;
    case X86_REG_DR7:
      reg_name = "DR7";
      break;
    case X86_REG_DR8:
      reg_name = "DR8";
      break;
    case X86_REG_DR9:
      reg_name = "DR9";
      break;
    case X86_REG_DR10:
      reg_name = "DR10";
      break;
    case X86_REG_DR11:
      reg_name = "DR11";
      break;
    case X86_REG_DR12:
      reg_name = "DR12";
      break;
    case X86_REG_DR13:
      reg_name = "DR13";
      break;
    case X86_REG_DR14:
      reg_name = "DR14";
      break;
    case X86_REG_DR15:
      reg_name = "DR15";
      break;
    case X86_REG_FP0:
      reg_name = "FP0";
      break;
    case X86_REG_FP1:
      reg_name = "FP1";
      break;
    case X86_REG_FP2:
      reg_name = "FP2";
      break;
    case X86_REG_FP3:
      reg_name = "FP3";
      break;
    case X86_REG_FP4:
      reg_name = "FP4";
      break;
    case X86_REG_FP5:
      reg_name = "FP5";
      break;
    case X86_REG_FP6:
      reg_name = "FP6";
      break;
    case X86_REG_FP7:
      reg_name = "FP7";
      break;
    case X86_REG_K0:
      reg_name = "K0";
      break;
    case X86_REG_K1:
      reg_name = "K1";
      break;
    case X86_REG_K2:
      reg_name = "K2";
      break;
    case X86_REG_K3:
      reg_name = "K3";
      break;
    case X86_REG_K4:
      reg_name = "K4";
      break;
    case X86_REG_K5:
      reg_name = "K5";
      break;
    case X86_REG_K6:
      reg_name = "K6";
      break;
    case X86_REG_K7:
      reg_name = "K7";
      break;
    case X86_REG_MM0:
      reg_name = "MM0";
      break;
    case X86_REG_MM1:
      reg_name = "MM1";
      break;
    case X86_REG_MM2:
      reg_name = "MM2";
      break;
    case X86_REG_MM3:
      reg_name = "MM3";
      break;
    case X86_REG_MM4:
      reg_name = "MM4";
      break;
    case X86_REG_MM5:
      reg_name = "MM5";
      break;
    case X86_REG_MM6:
      reg_name = "MM6";
      break;
    case X86_REG_MM7:
      reg_name = "MM7";
      break;
    case X86_REG_R8:
      reg_name = "R8";
      break;
    case X86_REG_R9:
      reg_name = "R9";
      break;
    case X86_REG_R10:
      reg_name = "R10";
      break;
    case X86_REG_R11:
      reg_name = "R11";
      break;
    case X86_REG_R12:
      reg_name = "R12";
      break;
    case X86_REG_R13:
      reg_name = "R13";
      break;
    case X86_REG_R14:
      reg_name = "R14";
      break;
    case X86_REG_R15:
      reg_name = "R15";
      break;
    case X86_REG_ST0:
      reg_name = "ST0";
      break;
    case X86_REG_ST1:
      reg_name = "ST1";
      break;
    case X86_REG_ST2:
      reg_name = "ST2";
      break;
    case X86_REG_ST3:
      reg_name = "ST3";
      break;
    case X86_REG_ST4:
      reg_name = "ST4";
      break;
    case X86_REG_ST5:
      reg_name = "ST5";
      break;
    case X86_REG_ST6:
      reg_name = "ST6";
      break;
    case X86_REG_ST7:
      reg_name = "ST7";
      break;
    case X86_REG_XMM0:
      reg_name = "XMM0";
      break;
    case X86_REG_XMM1:
      reg_name = "XMM1";
      break;
    case X86_REG_XMM2:
      reg_name = "XMM2";
      break;
    case X86_REG_XMM3:
      reg_name = "XMM3";
      break;
    case X86_REG_XMM4:
      reg_name = "XMM4";
      break;
    case X86_REG_XMM5:
      reg_name = "XMM5";
      break;
    case X86_REG_XMM6:
      reg_name = "XMM6";
      break;
    case X86_REG_XMM7:
      reg_name = "XMM7";
      break;
    case X86_REG_XMM8:
      reg_name = "XMM8";
      break;
    case X86_REG_XMM9:
      reg_name = "XMM9";
      break;
    case X86_REG_XMM10:
      reg_name = "XMM10";
      break;
    case X86_REG_XMM11:
      reg_name = "XMM11";
      break;
    case X86_REG_XMM12:
      reg_name = "XMM12";
      break;
    case X86_REG_XMM13:
      reg_name = "XMM13";
      break;
    case X86_REG_XMM14:
      reg_name = "XMM14";
      break;
    case X86_REG_XMM15:
      reg_name = "XMM15";
      break;
    case X86_REG_XMM16:
      reg_name = "XMM16";
      break;
    case X86_REG_XMM17:
      reg_name = "XMM17";
      break;
    case X86_REG_XMM18:
      reg_name = "XMM18";
      break;
    case X86_REG_XMM19:
      reg_name = "XMM19";
      break;
    case X86_REG_XMM20:
      reg_name = "XMM20";
      break;
    case X86_REG_XMM21:
      reg_name = "XMM21";
      break;
    case X86_REG_XMM22:
      reg_name = "XMM22";
      break;
    case X86_REG_XMM23:
      reg_name = "XMM23";
      break;
    case X86_REG_XMM24:
      reg_name = "XMM24";
      break;
    case X86_REG_XMM25:
      reg_name = "XMM25";
      break;
    case X86_REG_XMM26:
      reg_name = "XMM26";
      break;
    case X86_REG_XMM27:
      reg_name = "XMM27";
      break;
    case X86_REG_XMM28:
      reg_name = "XMM28";
      break;
    case X86_REG_XMM29:
      reg_name = "XMM29";
      break;
    case X86_REG_XMM30:
      reg_name = "XMM30";
      break;
    case X86_REG_XMM31:
      reg_name = "XMM31";
      break;
    case X86_REG_YMM0:
      reg_name = "YMM0";
      break;
    case X86_REG_YMM1:
      reg_name = "YMM1";
      break;
    case X86_REG_YMM2:
      reg_name = "YMM2";
      break;
    case X86_REG_YMM3:
      reg_name = "YMM3";
      break;
    case X86_REG_YMM4:
      reg_name = "YMM4";
      break;
    case X86_REG_YMM5:
      reg_name = "YMM5";
      break;
    case X86_REG_YMM6:
      reg_name = "YMM6";
      break;
    case X86_REG_YMM7:
      reg_name = "YMM7";
      break;
    case X86_REG_YMM8:
      reg_name = "YMM8";
      break;
    case X86_REG_YMM9:
      reg_name = "YMM9";
      break;
    case X86_REG_YMM10:
      reg_name = "YMM10";
      break;
    case X86_REG_YMM11:
      reg_name = "YMM11";
      break;
    case X86_REG_YMM12:
      reg_name = "YMM12";
      break;
    case X86_REG_YMM13:
      reg_name = "YMM13";
      break;
    case X86_REG_YMM14:
      reg_name = "YMM14";
      break;
    case X86_REG_YMM15:
      reg_name = "YMM15";
      break;
    case X86_REG_YMM16:
      reg_name = "YMM16";
      break;
    case X86_REG_YMM17:
      reg_name = "YMM17";
      break;
    case X86_REG_YMM18:
      reg_name = "YMM18";
      break;
    case X86_REG_YMM19:
      reg_name = "YMM19";
      break;
    case X86_REG_YMM20:
      reg_name = "YMM20";
      break;
    case X86_REG_YMM21:
      reg_name = "YMM21";
      break;
    case X86_REG_YMM22:
      reg_name = "YMM22";
      break;
    case X86_REG_YMM23:
      reg_name = "YMM23";
      break;
    case X86_REG_YMM24:
      reg_name = "YMM24";
      break;
    case X86_REG_YMM25:
      reg_name = "YMM25";
      break;
    case X86_REG_YMM26:
      reg_name = "YMM26";
      break;
    case X86_REG_YMM27:
      reg_name = "YMM27";
      break;
    case X86_REG_YMM28:
      reg_name = "YMM28";
      break;
    case X86_REG_YMM29:
      reg_name = "YMM29";
      break;
    case X86_REG_YMM30:
      reg_name = "YMM30";
      break;
    case X86_REG_YMM31:
      reg_name = "YMM31";
      break;
    case X86_REG_ZMM0:
      reg_name = "ZMM0";
      break;
    case X86_REG_ZMM1:
      reg_name = "ZMM1";
      break;
    case X86_REG_ZMM2:
      reg_name = "ZMM2";
      break;
    case X86_REG_ZMM3:
      reg_name = "ZMM3";
      break;
    case X86_REG_ZMM4:
      reg_name = "ZMM4";
      break;
    case X86_REG_ZMM5:
      reg_name = "ZMM5";
      break;
    case X86_REG_ZMM6:
      reg_name = "ZMM6";
      break;
    case X86_REG_ZMM7:
      reg_name = "ZMM7";
      break;
    case X86_REG_ZMM8:
      reg_name = "ZMM8";
      break;
    case X86_REG_ZMM9:
      reg_name = "ZMM9";
      break;
    case X86_REG_ZMM10:
      reg_name = "ZMM10";
      break;
    case X86_REG_ZMM11:
      reg_name = "ZMM11";
      break;
    case X86_REG_ZMM12:
      reg_name = "ZMM12";
      break;
    case X86_REG_ZMM13:
      reg_name = "ZMM13";
      break;
    case X86_REG_ZMM14:
      reg_name = "ZMM14";
      break;
    case X86_REG_ZMM15:
      reg_name = "ZMM15";
      break;
    case X86_REG_ZMM16:
      reg_name = "ZMM16";
      break;
    case X86_REG_ZMM17:
      reg_name = "ZMM17";
      break;
    case X86_REG_ZMM18:
      reg_name = "ZMM18";
      break;
    case X86_REG_ZMM19:
      reg_name = "ZMM19";
      break;
    case X86_REG_ZMM20:
      reg_name = "ZMM20";
      break;
    case X86_REG_ZMM21:
      reg_name = "ZMM21";
      break;
    case X86_REG_ZMM22:
      reg_name = "ZMM22";
      break;
    case X86_REG_ZMM23:
      reg_name = "ZMM23";
      break;
    case X86_REG_ZMM24:
      reg_name = "ZMM24";
      break;
    case X86_REG_ZMM25:
      reg_name = "ZMM25";
      break;
    case X86_REG_ZMM26:
      reg_name = "ZMM26";
      break;
    case X86_REG_ZMM27:
      reg_name = "ZMM27";
      break;
    case X86_REG_ZMM28:
      reg_name = "ZMM28";
      break;
    case X86_REG_ZMM29:
      reg_name = "ZMM29";
      break;
    case X86_REG_ZMM30:
      reg_name = "ZMM30";
      break;
    case X86_REG_ZMM31:
      reg_name = "ZMM31";
      break;
    case X86_REG_R8B:
      reg_name = "R8B";
      break;
    case X86_REG_R9B:
      reg_name = "R9B";
      break;
    case X86_REG_R10B:
      reg_name = "R10B";
      break;
    case X86_REG_R11B:
      reg_name = "R11B";
      break;
    case X86_REG_R12B:
      reg_name = "R12B";
      break;
    case X86_REG_R13B:
      reg_name = "R13B";
      break;
    case X86_REG_R14B:
      reg_name = "R14B";
      break;
    case X86_REG_R15B:
      reg_name = "R15B";
      break;
    case X86_REG_R8D:
      reg_name = "R8D";
      break;
    case X86_REG_R9D:
      reg_name = "R9D";
      break;
    case X86_REG_R10D:
      reg_name = "R10D";
      break;
    case X86_REG_R11D:
      reg_name = "R11D";
      break;
    case X86_REG_R12D:
      reg_name = "R12D";
      break;
    case X86_REG_R13D:
      reg_name = "R13D";
      break;
    case X86_REG_R14D:
      reg_name = "R14D";
      break;
    case X86_REG_R15D:
      reg_name = "R15D";
      break;
    case X86_REG_R8W:
      reg_name = "R8W";
      break;
    case X86_REG_R9W:
      reg_name = "R9W";
      break;
    case X86_REG_R10W:
      reg_name = "R10W";
      break;
    case X86_REG_R11W:
      reg_name = "R11W";
      break;
    case X86_REG_R12W:
      reg_name = "R12W";
      break;
    case X86_REG_R13W:
      reg_name = "R13W";
      break;
    case X86_REG_R14W:
      reg_name = "R14W";
      break;
    case X86_REG_R15W:
      reg_name = "R15W";
      break;
    case X86_REG_ENDING:
      reg_name = "ENDING";
      break;
  }
  return reg_name;
}

std::string reg2str(x86_reg reg){
  std::string reg_name;
  switch(reg){
    case X86_REG_INVALID:
      reg_name = "INVALID";
      break;
    case X86_REG_AH:
      reg_name = "AH";
      break;
    case X86_REG_AL:
      reg_name = "AL";
      break;
    case X86_REG_AX:
      reg_name = "AX";
      break;
    case X86_REG_BH:
      reg_name = "BH";
      break;
    case X86_REG_BL:
      reg_name = "BL";
      break;
    case X86_REG_BP:
      reg_name = "BP";
      break;
    case X86_REG_BPL:
      reg_name = "BPL";
      break;
    case X86_REG_BX:
      reg_name = "BX";
      break;
    case X86_REG_CH:
      reg_name = "CH";
      break;
    case X86_REG_CL:
      reg_name = "CL";
      break;
    case X86_REG_CS:
      reg_name = "CS";
      break;
    case X86_REG_CX:
      reg_name = "CX";
      break;
    case X86_REG_DH:
      reg_name = "DH";
      break;
    case X86_REG_DI:
      reg_name = "DI";
      break;
    case X86_REG_DIL:
      reg_name = "DIL";
      break;
    case X86_REG_DL:
      reg_name = "DL";
      break;
    case X86_REG_DS:
      reg_name = "DS";
      break;
    case X86_REG_DX:
      reg_name = "DX";
      break;
    case X86_REG_EAX:
      reg_name = "EAX";
      break;
    case X86_REG_EBP:
      reg_name = "EBP";
      break;
    case X86_REG_EBX:
      reg_name = "EBX";
      break;
    case X86_REG_ECX:
      reg_name = "ECX";
      break;
    case X86_REG_EDI:
      reg_name = "EDI";
      break;
    case X86_REG_EDX:
      reg_name = "EDX";
      break;
    case X86_REG_EFLAGS:
      reg_name = "EFLAGS";
      break;
    case X86_REG_EIP:
      reg_name = "EIP";
      break;
    case X86_REG_EIZ:
      reg_name = "EIZ";
      break;
    case X86_REG_ES:
      reg_name = "ES";
      break;
    case X86_REG_ESI:
      reg_name = "ESI";
      break;
    case X86_REG_ESP:
      reg_name = "ESP";
      break;
    case X86_REG_FPSW:
      reg_name = "FPSW";
      break;
    case X86_REG_FS:
      reg_name = "FS";
      break;
    case X86_REG_GS:
      reg_name = "GS";
      break;
    case X86_REG_IP:
      reg_name = "IP";
      break;
    case X86_REG_RAX:
      reg_name = "RAX";
      break;
    case X86_REG_RBP:
      reg_name = "RBP";
      break;
    case X86_REG_RBX:
      reg_name = "RBX";
      break;
    case X86_REG_RCX:
      reg_name = "RCX";
      break;
    case X86_REG_RDI:
      reg_name = "RDI";
      break;
    case X86_REG_RDX:
      reg_name = "RDX";
      break;
    case X86_REG_RIP:
      reg_name = "RIP";
      break;
    case X86_REG_RIZ:
      reg_name = "RIZ";
      break;
    case X86_REG_RSI:
      reg_name = "RSI";
      break;
    case X86_REG_RSP:
      reg_name = "RSP";
      break;
    case X86_REG_SI:
      reg_name = "SI";
      break;
    case X86_REG_SIL:
      reg_name = "SIL";
      break;
    case X86_REG_SP:
      reg_name = "SP";
      break;
    case X86_REG_SPL:
      reg_name = "SPL";
      break;
    case X86_REG_SS:
      reg_name = "SS";
      break;
    case X86_REG_CR0:
      reg_name = "CR0";
      break;
    case X86_REG_CR1:
      reg_name = "CR1";
      break;
    case X86_REG_CR2:
      reg_name = "CR2";
      break;
    case X86_REG_CR3:
      reg_name = "CR3";
      break;
    case X86_REG_CR4:
      reg_name = "CR4";
      break;
    case X86_REG_CR5:
      reg_name = "CR5";
      break;
    case X86_REG_CR6:
      reg_name = "CR6";
      break;
    case X86_REG_CR7:
      reg_name = "CR7";
      break;
    case X86_REG_CR8:
      reg_name = "CR8";
      break;
    case X86_REG_CR9:
      reg_name = "CR9";
      break;
    case X86_REG_CR10:
      reg_name = "CR10";
      break;
    case X86_REG_CR11:
      reg_name = "CR11";
      break;
    case X86_REG_CR12:
      reg_name = "CR12";
      break;
    case X86_REG_CR13:
      reg_name = "CR13";
      break;
    case X86_REG_CR14:
      reg_name = "CR14";
      break;
    case X86_REG_CR15:
      reg_name = "CR15";
      break;
    case X86_REG_DR0:
      reg_name = "DR0";
      break;
    case X86_REG_DR1:
      reg_name = "DR1";
      break;
    case X86_REG_DR2:
      reg_name = "DR2";
      break;
    case X86_REG_DR3:
      reg_name = "DR3";
      break;
    case X86_REG_DR4:
      reg_name = "DR4";
      break;
    case X86_REG_DR5:
      reg_name = "DR5";
      break;
    case X86_REG_DR6:
      reg_name = "DR6";
      break;
    case X86_REG_DR7:
      reg_name = "DR7";
      break;
    case X86_REG_DR8:
      reg_name = "DR8";
      break;
    case X86_REG_DR9:
      reg_name = "DR9";
      break;
    case X86_REG_DR10:
      reg_name = "DR10";
      break;
    case X86_REG_DR11:
      reg_name = "DR11";
      break;
    case X86_REG_DR12:
      reg_name = "DR12";
      break;
    case X86_REG_DR13:
      reg_name = "DR13";
      break;
    case X86_REG_DR14:
      reg_name = "DR14";
      break;
    case X86_REG_DR15:
      reg_name = "DR15";
      break;
    case X86_REG_FP0:
      reg_name = "FP0";
      break;
    case X86_REG_FP1:
      reg_name = "FP1";
      break;
    case X86_REG_FP2:
      reg_name = "FP2";
      break;
    case X86_REG_FP3:
      reg_name = "FP3";
      break;
    case X86_REG_FP4:
      reg_name = "FP4";
      break;
    case X86_REG_FP5:
      reg_name = "FP5";
      break;
    case X86_REG_FP6:
      reg_name = "FP6";
      break;
    case X86_REG_FP7:
      reg_name = "FP7";
      break;
    case X86_REG_K0:
      reg_name = "K0";
      break;
    case X86_REG_K1:
      reg_name = "K1";
      break;
    case X86_REG_K2:
      reg_name = "K2";
      break;
    case X86_REG_K3:
      reg_name = "K3";
      break;
    case X86_REG_K4:
      reg_name = "K4";
      break;
    case X86_REG_K5:
      reg_name = "K5";
      break;
    case X86_REG_K6:
      reg_name = "K6";
      break;
    case X86_REG_K7:
      reg_name = "K7";
      break;
    case X86_REG_MM0:
      reg_name = "MM0";
      break;
    case X86_REG_MM1:
      reg_name = "MM1";
      break;
    case X86_REG_MM2:
      reg_name = "MM2";
      break;
    case X86_REG_MM3:
      reg_name = "MM3";
      break;
    case X86_REG_MM4:
      reg_name = "MM4";
      break;
    case X86_REG_MM5:
      reg_name = "MM5";
      break;
    case X86_REG_MM6:
      reg_name = "MM6";
      break;
    case X86_REG_MM7:
      reg_name = "MM7";
      break;
    case X86_REG_R8:
      reg_name = "R8";
      break;
    case X86_REG_R9:
      reg_name = "R9";
      break;
    case X86_REG_R10:
      reg_name = "R10";
      break;
    case X86_REG_R11:
      reg_name = "R11";
      break;
    case X86_REG_R12:
      reg_name = "R12";
      break;
    case X86_REG_R13:
      reg_name = "R13";
      break;
    case X86_REG_R14:
      reg_name = "R14";
      break;
    case X86_REG_R15:
      reg_name = "R15";
      break;
    case X86_REG_ST0:
      reg_name = "ST0";
      break;
    case X86_REG_ST1:
      reg_name = "ST1";
      break;
    case X86_REG_ST2:
      reg_name = "ST2";
      break;
    case X86_REG_ST3:
      reg_name = "ST3";
      break;
    case X86_REG_ST4:
      reg_name = "ST4";
      break;
    case X86_REG_ST5:
      reg_name = "ST5";
      break;
    case X86_REG_ST6:
      reg_name = "ST6";
      break;
    case X86_REG_ST7:
      reg_name = "ST7";
      break;
    case X86_REG_XMM0:
      reg_name = "XMM0";
      break;
    case X86_REG_XMM1:
      reg_name = "XMM1";
      break;
    case X86_REG_XMM2:
      reg_name = "XMM2";
      break;
    case X86_REG_XMM3:
      reg_name = "XMM3";
      break;
    case X86_REG_XMM4:
      reg_name = "XMM4";
      break;
    case X86_REG_XMM5:
      reg_name = "XMM5";
      break;
    case X86_REG_XMM6:
      reg_name = "XMM6";
      break;
    case X86_REG_XMM7:
      reg_name = "XMM7";
      break;
    case X86_REG_XMM8:
      reg_name = "XMM8";
      break;
    case X86_REG_XMM9:
      reg_name = "XMM9";
      break;
    case X86_REG_XMM10:
      reg_name = "XMM10";
      break;
    case X86_REG_XMM11:
      reg_name = "XMM11";
      break;
    case X86_REG_XMM12:
      reg_name = "XMM12";
      break;
    case X86_REG_XMM13:
      reg_name = "XMM13";
      break;
    case X86_REG_XMM14:
      reg_name = "XMM14";
      break;
    case X86_REG_XMM15:
      reg_name = "XMM15";
      break;
    case X86_REG_XMM16:
      reg_name = "XMM16";
      break;
    case X86_REG_XMM17:
      reg_name = "XMM17";
      break;
    case X86_REG_XMM18:
      reg_name = "XMM18";
      break;
    case X86_REG_XMM19:
      reg_name = "XMM19";
      break;
    case X86_REG_XMM20:
      reg_name = "XMM20";
      break;
    case X86_REG_XMM21:
      reg_name = "XMM21";
      break;
    case X86_REG_XMM22:
      reg_name = "XMM22";
      break;
    case X86_REG_XMM23:
      reg_name = "XMM23";
      break;
    case X86_REG_XMM24:
      reg_name = "XMM24";
      break;
    case X86_REG_XMM25:
      reg_name = "XMM25";
      break;
    case X86_REG_XMM26:
      reg_name = "XMM26";
      break;
    case X86_REG_XMM27:
      reg_name = "XMM27";
      break;
    case X86_REG_XMM28:
      reg_name = "XMM28";
      break;
    case X86_REG_XMM29:
      reg_name = "XMM29";
      break;
    case X86_REG_XMM30:
      reg_name = "XMM30";
      break;
    case X86_REG_XMM31:
      reg_name = "XMM31";
      break;
    case X86_REG_YMM0:
      reg_name = "YMM0";
      break;
    case X86_REG_YMM1:
      reg_name = "YMM1";
      break;
    case X86_REG_YMM2:
      reg_name = "YMM2";
      break;
    case X86_REG_YMM3:
      reg_name = "YMM3";
      break;
    case X86_REG_YMM4:
      reg_name = "YMM4";
      break;
    case X86_REG_YMM5:
      reg_name = "YMM5";
      break;
    case X86_REG_YMM6:
      reg_name = "YMM6";
      break;
    case X86_REG_YMM7:
      reg_name = "YMM7";
      break;
    case X86_REG_YMM8:
      reg_name = "YMM8";
      break;
    case X86_REG_YMM9:
      reg_name = "YMM9";
      break;
    case X86_REG_YMM10:
      reg_name = "YMM10";
      break;
    case X86_REG_YMM11:
      reg_name = "YMM11";
      break;
    case X86_REG_YMM12:
      reg_name = "YMM12";
      break;
    case X86_REG_YMM13:
      reg_name = "YMM13";
      break;
    case X86_REG_YMM14:
      reg_name = "YMM14";
      break;
    case X86_REG_YMM15:
      reg_name = "YMM15";
      break;
    case X86_REG_YMM16:
      reg_name = "YMM16";
      break;
    case X86_REG_YMM17:
      reg_name = "YMM17";
      break;
    case X86_REG_YMM18:
      reg_name = "YMM18";
      break;
    case X86_REG_YMM19:
      reg_name = "YMM19";
      break;
    case X86_REG_YMM20:
      reg_name = "YMM20";
      break;
    case X86_REG_YMM21:
      reg_name = "YMM21";
      break;
    case X86_REG_YMM22:
      reg_name = "YMM22";
      break;
    case X86_REG_YMM23:
      reg_name = "YMM23";
      break;
    case X86_REG_YMM24:
      reg_name = "YMM24";
      break;
    case X86_REG_YMM25:
      reg_name = "YMM25";
      break;
    case X86_REG_YMM26:
      reg_name = "YMM26";
      break;
    case X86_REG_YMM27:
      reg_name = "YMM27";
      break;
    case X86_REG_YMM28:
      reg_name = "YMM28";
      break;
    case X86_REG_YMM29:
      reg_name = "YMM29";
      break;
    case X86_REG_YMM30:
      reg_name = "YMM30";
      break;
    case X86_REG_YMM31:
      reg_name = "YMM31";
      break;
    case X86_REG_ZMM0:
      reg_name = "ZMM0";
      break;
    case X86_REG_ZMM1:
      reg_name = "ZMM1";
      break;
    case X86_REG_ZMM2:
      reg_name = "ZMM2";
      break;
    case X86_REG_ZMM3:
      reg_name = "ZMM3";
      break;
    case X86_REG_ZMM4:
      reg_name = "ZMM4";
      break;
    case X86_REG_ZMM5:
      reg_name = "ZMM5";
      break;
    case X86_REG_ZMM6:
      reg_name = "ZMM6";
      break;
    case X86_REG_ZMM7:
      reg_name = "ZMM7";
      break;
    case X86_REG_ZMM8:
      reg_name = "ZMM8";
      break;
    case X86_REG_ZMM9:
      reg_name = "ZMM9";
      break;
    case X86_REG_ZMM10:
      reg_name = "ZMM10";
      break;
    case X86_REG_ZMM11:
      reg_name = "ZMM11";
      break;
    case X86_REG_ZMM12:
      reg_name = "ZMM12";
      break;
    case X86_REG_ZMM13:
      reg_name = "ZMM13";
      break;
    case X86_REG_ZMM14:
      reg_name = "ZMM14";
      break;
    case X86_REG_ZMM15:
      reg_name = "ZMM15";
      break;
    case X86_REG_ZMM16:
      reg_name = "ZMM16";
      break;
    case X86_REG_ZMM17:
      reg_name = "ZMM17";
      break;
    case X86_REG_ZMM18:
      reg_name = "ZMM18";
      break;
    case X86_REG_ZMM19:
      reg_name = "ZMM19";
      break;
    case X86_REG_ZMM20:
      reg_name = "ZMM20";
      break;
    case X86_REG_ZMM21:
      reg_name = "ZMM21";
      break;
    case X86_REG_ZMM22:
      reg_name = "ZMM22";
      break;
    case X86_REG_ZMM23:
      reg_name = "ZMM23";
      break;
    case X86_REG_ZMM24:
      reg_name = "ZMM24";
      break;
    case X86_REG_ZMM25:
      reg_name = "ZMM25";
      break;
    case X86_REG_ZMM26:
      reg_name = "ZMM26";
      break;
    case X86_REG_ZMM27:
      reg_name = "ZMM27";
      break;
    case X86_REG_ZMM28:
      reg_name = "ZMM28";
      break;
    case X86_REG_ZMM29:
      reg_name = "ZMM29";
      break;
    case X86_REG_ZMM30:
      reg_name = "ZMM30";
      break;
    case X86_REG_ZMM31:
      reg_name = "ZMM31";
      break;
    case X86_REG_R8B:
      reg_name = "R8B";
      break;
    case X86_REG_R9B:
      reg_name = "R9B";
      break;
    case X86_REG_R10B:
      reg_name = "R10B";
      break;
    case X86_REG_R11B:
      reg_name = "R11B";
      break;
    case X86_REG_R12B:
      reg_name = "R12B";
      break;
    case X86_REG_R13B:
      reg_name = "R13B";
      break;
    case X86_REG_R14B:
      reg_name = "R14B";
      break;
    case X86_REG_R15B:
      reg_name = "R15B";
      break;
    case X86_REG_R8D:
      reg_name = "R8D";
      break;
    case X86_REG_R9D:
      reg_name = "R9D";
      break;
    case X86_REG_R10D:
      reg_name = "R10D";
      break;
    case X86_REG_R11D:
      reg_name = "R11D";
      break;
    case X86_REG_R12D:
      reg_name = "R12D";
      break;
    case X86_REG_R13D:
      reg_name = "R13D";
      break;
    case X86_REG_R14D:
      reg_name = "R14D";
      break;
    case X86_REG_R15D:
      reg_name = "R15D";
      break;
    case X86_REG_R8W:
      reg_name = "R8W";
      break;
    case X86_REG_R9W:
      reg_name = "R9W";
      break;
    case X86_REG_R10W:
      reg_name = "R10W";
      break;
    case X86_REG_R11W:
      reg_name = "R11W";
      break;
    case X86_REG_R12W:
      reg_name = "R12W";
      break;
    case X86_REG_R13W:
      reg_name = "R13W";
      break;
    case X86_REG_R14W:
      reg_name = "R14W";
      break;
    case X86_REG_R15W:
      reg_name = "R15W";
      break;
    case X86_REG_ENDING:
      reg_name = "ENDING";
      break;
  }
  return reg_name;
}

std::string reg2str(x86_reg_class reg){
  std::string reg_name;
  switch(reg){
    case X86_CLASS_INVALID:
      reg_name = "INVALID";
      break;
    case X86_CLASS_A:
      reg_name = "A";
      break;
    case X86_CLASS_C:
      reg_name = "C";
      break;
    case X86_CLASS_D:
      reg_name = "D";
      break;
    case X86_CLASS_B:
      reg_name = "B";
      break;
    case X86_CLASS_SP:
      reg_name = "SP";
      break;
    case X86_CLASS_BP:
      reg_name = "BP";
      break;
    case X86_CLASS_SI:
      reg_name = "SI";
      break;
    case X86_CLASS_DI:
      reg_name = "DI";
      break;
    case X86_CLASS_R:
      reg_name = "R";
      break;
    case X86_CLASS_SEGMENT:
      reg_name = "SEGMENT";
      break;
    case X86_CLASS_EFLAGS:
      reg_name = "EFLAGS";
      break;
    case X86_CLASS_INST:
      reg_name = "INST";
      break;
    case X86_CLASS_CR:
      reg_name = "CR";
      break;
    case X86_CLASS_DR:
      reg_name = "DR";
      break;
    case X86_CLASS_MYSTERY:
      reg_name = "MYSTERY";
      break;
    case X86_CLASS_GEN_REG:
      reg_name = "GEN_REG";
      break;
    default:
    	reg_name = "INVALID";
			break;
  }
  return reg_name;

}

std::string regVect2str(std::vector<x86_reg_class> source_reg ){
  std::stringstream ss;
  ss << "{";
  for(auto const & reg : source_reg){
    ss << reg2str(reg) << " ";
  }
  ss << "}";
  return ss.str();
}

std::string regVect2str(std::set<x86_reg_class> source_reg ){
  std::stringstream ss;
  ss << "{";
  for(auto const & reg : source_reg){
    ss << reg2str(reg) << ",";
  }
  ss << "}";
  return ss.str();
}

std::string regVect2str(std::vector<x86_reg> source_reg ){
  std::stringstream ss;
  ss << "{";
  for(auto const & reg : source_reg){
    ss << reg2str(reg) << " ";
  }
  ss << "}";
  return ss.str();
}

std::string vec2str32(std::vector<uint32_t> vect){
  std::stringstream ss;
  ss << "[";
  for(auto const & reg : vect){
    ss << std::hex << reg << " ";
  }
  ss << "]";
  return ss.str();
}

std::string vec2strAddr(std::vector<addr_t> vect){
  std::stringstream ss;
  ss << "[";
  std::vector<addr_t> cpy;
  for(auto const& elt : vect){
    cpy.push_back(elt);
  }
  std::sort(cpy.begin(), cpy.end());
  for(auto const & reg : cpy){
    ss << addr2s(reg) << " ";
  }
  ss << "]";
  return ss.str();
}

std::string set2strAddr(std::unordered_set<addr_t> vect){
  std::stringstream ss;
  ss << "[";
  std::vector<addr_t> cpy;
  for(auto const& elt : vect){
    cpy.push_back(elt);
  }
  std::sort(cpy.begin(), cpy.end());
  for(auto const & reg : cpy){
    ss << addr2s(reg) << " ";
  }
  ss << "]";
  return ss.str();
}

std::string regVect2str(std::vector<BVERTEX> bigraph ){
    std::stringstream ss;
  ss << "[";
  for(auto const & vertex : bigraph){
    ss << regVect2str(vertex.source_regs) << " ";
  }
  ss << "]";
  return ss.str();
}

std::string instType2str(InstrType inst){
  switch(inst){
    case InstrType::CALL:
      return "CALL";
    case InstrType::FAKE_LIB_FUNC:
      return "FAKE_LIB_FUNC";
    case InstrType::HLT:
      return "HLT";
    case InstrType::INT:
      return "INT";
    case InstrType::JCC:
      return "JCC";
    case InstrType::JMP:
      return "JMP";
    case InstrType::LIB_MOCK:
      return "LIB_MOCK";
    case InstrType::RET:
      return "RET";
    case InstrType::SEQ:
      return "SEQ";
    case InstrType::TRAP:
      return "TRAP";
    case InstrType::UNKNOWN:
      return "UNKNOWN";
    default:
      return "UNKNOWN";
  }
}

bool isRegGeneralPurpose(const x86_reg_class reg){
    if(reg == X86_CLASS_A || reg == X86_CLASS_C || reg == X86_CLASS_D || reg == X86_CLASS_B || reg == X86_CLASS_SP || reg == X86_CLASS_BP || reg == X86_CLASS_SI || reg == X86_CLASS_DI || reg == X86_CLASS_R){return true;}
    return false;
  }


// String to bytes
/**
 * Copies a string of bytes to a byte array in memory
 */
void copystring2byte(byte_t* const byte_ptr, const std::string& byte_str){
  std::vector<std::string> substr_vect;
  std::string tmp_two_chars;
  uint str_size = byte_str.size();
  byte_t hex_value;


  for(uint i = 0; i < (str_size/2); i++){
    tmp_two_chars = byte_str.substr(2 * i, 2);
    substr_vect.push_back(tmp_two_chars);
  }

  uint j = 0;
  for(auto const & byte_str : substr_vect){
    hex_value = std::strtoul(byte_str.data(), NULL, 16);
    byte_ptr[j] = hex_value;
    j++;
  }
  return;
}

// Capstone various

InstrType csToInstrType(cs_insn *cs_insn){
  for(std::size_t i = 0; i <= cs_insn->detail->groups_count; i++){
    byte_t ins_group = cs_insn->detail->groups[i];
    switch(ins_group){
      case CS_GRP_CALL:
        return InstrType::CALL;
      case CS_GRP_RET:
        return InstrType::RET;
      case CS_GRP_INT:
        return InstrType::INT;
      case CS_GRP_JUMP:
        if(cs_insn->id == X86_INS_JMP || cs_insn->id == X86_INS_LJMP){return InstrType::JMP;}
        else{return InstrType::JCC;}
      default:
        continue;
      }
  }
  return InstrType::UNKNOWN;
}

std::string getCapstoneInstStr(cs_insn *cs_insn) {

  std::string opcode_str(cs_insn->mnemonic);
  opcode_str += " ";
  std::string second_half(cs_insn->op_str);

  //std::string third_half(std::to_string(cs_insn->detail->regs_read_count));
  //std::string fourth_half(std::to_string(cs_insn->detail->regs_write_count));
  // std::string suffix = getImplicitRegReadStr(cs_insn);
  //std::string suffix = printOperandsInfoX86(cs_insn);

  return opcode_str + second_half ;
}

std::string opAccess2str(uint8_t access){
  std::string str_out;
  switch(access){
    case CS_AC_INVALID:
    str_out = "CS_AC_INVALID";
    break;
    case CS_AC_READ:
    str_out = "CS_AC_READ";
    break;
    case CS_AC_WRITE:
    str_out = "CS_AC_WRITE";
    break;
    case 3:
    str_out = "CS_AC_READ_WRITE";
    break;
    default:
    str_out = "CS_AC_ERROR";
    break;
  }
  return str_out;
}
bool is_cs_cflow_ins(cs_insn *cs_insn){
  if(csToInstrType(cs_insn) == InstrType::CALL ||
  csToInstrType(cs_insn) == InstrType::JCC ||
  csToInstrType(cs_insn) == InstrType::JMP ||
  csToInstrType(cs_insn) == InstrType::RET){
    return true;
  }
  return false;
}


  /**
    Gets, if possible, the immediate target of a CAPSTONE instruction (not the sequential instruction).
    Computes the target of a dynamic jump involving RIP, EIP or IP
    @param ins CAPSTONE instruction (cs_insn).
    @return The address of the target.
  */
addr_t get_cs_ins_immediate_target(cs_insn *ins) {
  // WARNING MALWARE WEAK
  cs_x86_op *cs_op;
  addr_t target=0;

  for (size_t i = 0; i < ins->detail->groups_count; i++){
    if (is_cs_cflow_group(ins->detail->groups[i])){
      for (size_t j = 0; j < ins->detail->x86.op_count; j++) {
        cs_op = &ins->detail->x86.operands[j];
        if (cs_op->type == X86_OP_IMM) {
          target = (uint)cs_op->imm;
          return target;
        }
        else if(cs_op->type == X86_OP_MEM){

          if(cs_op->mem.base == X86_REG_EIP || cs_op->mem.base == X86_REG_RIP || cs_op->mem.base == X86_REG_IP){
            // the reg is an inst reg (RIP, EIP, IP)
            // target = base (current addr instr) + (scale * index (reg value)) + disp
            if(cs_op->mem.index != X86_REG_INVALID){
              // not sure what to do here in that case. We skip it
              break;
            }
            else{
              // We assume we have no index reg
              target = (ins->address + ins->size) + (addr_t)cs_op->mem.disp;
              return target;
            }
            // compute the value of operand
          }
        }
      }
    }
  }
  return target;
}
  /**
    Checks if a CAPSTONE group is a control flow group.
    @param g CAPSTONE byte about the instruction's group (cs_group_type).
    @return Boolean.
  */
bool is_cs_cflow_group(byte_t g) {
  return (g == CS_GRP_JUMP) || (g == CS_GRP_CALL) || (g == CS_GRP_RET) || (g == CS_GRP_IRET);
}
  /**
    Checks if an instruction is an unconditionnal control flow instruction.
    @param ins CAPSTONE instruction (cs_insn).
    @return Boolean.
  */
bool is_cs_unconditional_cflow_ins(cs_insn *ins) {
  switch (ins->id) {
  case X86_INS_JMP:
  case X86_INS_LJMP:
  case X86_INS_RET:
  case X86_INS_RETF:
  case X86_INS_RETFQ:
    return true;
  default:
    return false;
  }
}
bool is_cs_call_ins(cs_insn *ins) {
  switch (ins->id) {
  case X86_INS_CALL:
  case X86_INS_LCALL:
  case X86_INS_VMCALL:
  case X86_INS_SYSCALL:
  case X86_INS_VMMCALL:
    return true;
  default:
    return false;
  }
}
} // namespace boa
