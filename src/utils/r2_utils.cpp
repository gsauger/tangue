#include "r2_utils.hpp"

namespace boa
{

bool R2::s_r2_found_in_path = false;

R2::R2(const std::string& bin_filepath, const std::string& parameters = {})
{
  if(!s_r2_found_in_path)
  {
    // Check if radare2 is in the PATH
    auto r = utils::exec_shell_command("which radare2 > /dev/null 2>&1");
    if(r.first != 0)
    {
      throw std::runtime_error("Missing radare2? Check if 'radare2' program is available from the PATH");
    }
    s_r2_found_in_path = true;
  }

  m_r2 = r2pipe_open(("radare2 -q0 " + parameters + " " + bin_filepath).c_str());
  if(!m_r2)
  {
    throw std::runtime_error("r2pipe_open failed for binary " + bin_filepath);
  }
}

R2::~R2()
{
  r2pipe_close(m_r2);
}

std::string R2::sendCmd(const std::string& cmd) const
{
  char* msg = r2pipe_cmd(m_r2, cmd.c_str());
  std::string answer;
  if(msg)
  {
    answer.assign(msg);
    free(msg);
  }
  else
  {
    throw std::runtime_error("Failed to send command " + cmd + " to radare2");
  }
  return answer;
}

} // namespace boa
