#ifndef BOA_TYPES_H
#define BOA_TYPES_H

#include <cstdint>
#include <deque>
#include <map>
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <memory>
#include <iostream>
#include <capstone/capstone.h>
#include <functional>
namespace boa
{

	// Binary stuff
	
enum class BinaryFormat
{
  ELF,
  PE,
  MACHO,
  RAW,
  AUTO
};

  enum class BinaryArch
{
  x86,
  x86_64,
  NONE
};

// MARk:- Optimisation levels
enum class OptLevel
{
  Opt_0,
  Opt_1,
  Opt_2,
  Opt_3,
  Opt_f,
  Opt_unk
};

enum class BinaryCompiler
{
	GCC,
	CLANG,
	NONE
};

// MARK:- Permissions
enum class Permission
{
  READ,
  WRITE,
  EXECUTE,
  NONE
};

// MARK:- Registers
enum class Register
{
  EAX,
  EBX,
  ECX,
  EDX,
  EDI,
  ESI,
  ESP,
  EBP,
  OF,
  SF,
  ZF,
  PF,
  CF,
  AF,
  DF,
  TF,
  FS,
  DR0,
  DR1,
  DR2,
  DR3,
  DR6,
  DR7
};
/*
typedef enum x86_reg
{
    X86_REG_INVALID = 0,
    X86_REG_AH, X86_REG_AL, X86_REG_AX, X86_REG_BH, X86_REG_BL,
    X86_REG_BP, X86_REG_BPL, X86_REG_BX, X86_REG_CH, X86_REG_CL,
    X86_REG_CS, X86_REG_CX, X86_REG_DH, X86_REG_DI, X86_REG_DIL,
    X86_REG_DL, X86_REG_DS, X86_REG_DX, X86_REG_EAX, X86_REG_EBP,
    X86_REG_EBX, X86_REG_ECX, X86_REG_EDI, X86_REG_EDX, X86_REG_EFLAGS,
    X86_REG_EIP, X86_REG_EIZ, X86_REG_ES, X86_REG_ESI, X86_REG_ESP,
    X86_REG_FPSW, X86_REG_FS, X86_REG_GS, X86_REG_IP, X86_REG_RAX,
    X86_REG_RBP, X86_REG_RBX, X86_REG_RCX, X86_REG_RDI, X86_REG_RDX,
    X86_REG_RIP, X86_REG_RIZ, X86_REG_RSI, X86_REG_RSP, X86_REG_SI,
    X86_REG_SIL, X86_REG_SP, X86_REG_SPL, X86_REG_SS, X86_REG_CR0,
    X86_REG_CR1, X86_REG_CR2, X86_REG_CR3, X86_REG_CR4, X86_REG_CR5,
    X86_REG_CR6, X86_REG_CR7, X86_REG_CR8, X86_REG_CR9, X86_REG_CR10,
    X86_REG_CR11, X86_REG_CR12, X86_REG_CR13, X86_REG_CR14, X86_REG_CR15,
    X86_REG_DR0, X86_REG_DR1, X86_REG_DR2, X86_REG_DR3, X86_REG_DR4,
    X86_REG_DR5, X86_REG_DR6, X86_REG_DR7, X86_REG_DR8, X86_REG_DR9,
    X86_REG_DR10, X86_REG_DR11, X86_REG_DR12, X86_REG_DR13, X86_REG_DR14,
    X86_REG_DR15, X86_REG_FP0, X86_REG_FP1, X86_REG_FP2, X86_REG_FP3,
    X86_REG_FP4, X86_REG_FP5, X86_REG_FP6, X86_REG_FP7,
    X86_REG_K0, X86_REG_K1, X86_REG_K2, X86_REG_K3, X86_REG_K4,
    X86_REG_K5, X86_REG_K6, X86_REG_K7, X86_REG_MM0, X86_REG_MM1,
    X86_REG_MM2, X86_REG_MM3, X86_REG_MM4, X86_REG_MM5, X86_REG_MM6,
    X86_REG_MM7, X86_REG_R8, X86_REG_R9, X86_REG_R10, X86_REG_R11,
    X86_REG_R12, X86_REG_R13, X86_REG_R14, X86_REG_R15,
    X86_REG_ST0, X86_REG_ST1, X86_REG_ST2, X86_REG_ST3,
    X86_REG_ST4, X86_REG_ST5, X86_REG_ST6, X86_REG_ST7,
    X86_REG_XMM0, X86_REG_XMM1, X86_REG_XMM2, X86_REG_XMM3, X86_REG_XMM4,
    X86_REG_XMM5, X86_REG_XMM6, X86_REG_XMM7, X86_REG_XMM8, X86_REG_XMM9,
    X86_REG_XMM10, X86_REG_XMM11, X86_REG_XMM12, X86_REG_XMM13, X86_REG_XMM14,
    X86_REG_XMM15, X86_REG_XMM16, X86_REG_XMM17, X86_REG_XMM18, X86_REG_XMM19,
    X86_REG_XMM20, X86_REG_XMM21, X86_REG_XMM22, X86_REG_XMM23, X86_REG_XMM24,
    X86_REG_XMM25, X86_REG_XMM26, X86_REG_XMM27, X86_REG_XMM28, X86_REG_XMM29,
    X86_REG_XMM30, X86_REG_XMM31, X86_REG_YMM0, X86_REG_YMM1, X86_REG_YMM2,
    X86_REG_YMM3, X86_REG_YMM4, X86_REG_YMM5, X86_REG_YMM6, X86_REG_YMM7,
    X86_REG_YMM8, X86_REG_YMM9, X86_REG_YMM10, X86_REG_YMM11, X86_REG_YMM12,
    X86_REG_YMM13, X86_REG_YMM14, X86_REG_YMM15, X86_REG_YMM16, X86_REG_YMM17,
    X86_REG_YMM18, X86_REG_YMM19, X86_REG_YMM20, X86_REG_YMM21, X86_REG_YMM22,
    X86_REG_YMM23, X86_REG_YMM24, X86_REG_YMM25, X86_REG_YMM26, X86_REG_YMM27,
    X86_REG_YMM28, X86_REG_YMM29, X86_REG_YMM30, X86_REG_YMM31, X86_REG_ZMM0,
    X86_REG_ZMM1, X86_REG_ZMM2, X86_REG_ZMM3, X86_REG_ZMM4, X86_REG_ZMM5,
    X86_REG_ZMM6, X86_REG_ZMM7, X86_REG_ZMM8, X86_REG_ZMM9, X86_REG_ZMM10,
    X86_REG_ZMM11, X86_REG_ZMM12, X86_REG_ZMM13, X86_REG_ZMM14, X86_REG_ZMM15,
    X86_REG_ZMM16, X86_REG_ZMM17, X86_REG_ZMM18, X86_REG_ZMM19, X86_REG_ZMM20,
    X86_REG_ZMM21, X86_REG_ZMM22, X86_REG_ZMM23, X86_REG_ZMM24, X86_REG_ZMM25,
    X86_REG_ZMM26, X86_REG_ZMM27, X86_REG_ZMM28, X86_REG_ZMM29, X86_REG_ZMM30,
    X86_REG_ZMM31, X86_REG_R8B, X86_REG_R9B, X86_REG_R10B, X86_REG_R11B,
    X86_REG_R12B, X86_REG_R13B, X86_REG_R14B, X86_REG_R15B, X86_REG_R8D,
    X86_REG_R9D, X86_REG_R10D, X86_REG_R11D, X86_REG_R12D, X86_REG_R13D,
    X86_REG_R14D, X86_REG_R15D, X86_REG_R8W, X86_REG_R9W, X86_REG_R10W,
    X86_REG_R11W, X86_REG_R12W, X86_REG_R13W, X86_REG_R14W, X86_REG_R15W,

    X86_REG_ENDING      // <-- mark the end of the list of registers
} x86_reg;
*/
typedef enum x86_reg_class{
  X86_CLASS_INVALID=0,
  X86_CLASS_A, X86_CLASS_C, X86_CLASS_D, X86_CLASS_B, X86_CLASS_SP, X86_CLASS_BP, X86_CLASS_SI, X86_CLASS_DI, // general purpose
  X86_CLASS_R, // general purpose 64 bit
  X86_CLASS_SEGMENT, // segment
  X86_CLASS_EFLAGS, // flags
  X86_CLASS_INST, // EIP & RIP % IP
  X86_CLASS_CR, // control
  X86_CLASS_DR, // debug
  X86_CLASS_MYSTERY, // FPX, MMX, KX, STX and EIZ
	X86_CLASS_GEN_REG
} x86_reg_class;

const x86_reg_class X86_CLASS_BEGIN = x86_reg_class::X86_CLASS_INVALID;
const x86_reg_class X86_CLASS_ENDING = x86_reg_class::X86_CLASS_MYSTERY;
const x86_reg_class X86_CLASS_BEGIN_ONE_GEN_REG = x86_reg_class::X86_CLASS_R;
const x86_reg_class X86_CLASS_ENDING_ONE_GEN_REG = x86_reg_class::X86_CLASS_GEN_REG;

#define NUMBER_REG_CLASSES 16

enum class InstrType
{
  CALL,
  FAKE_LIB_FUNC,
  HLT,
  INT,
  JCC,
  JMP,
  LIB_MOCK,
  RET,
  SEQ,
  TRAP,
  UNKNOWN
};

struct BVERTEX{
    x86_reg_class reg_class_name;
    std::set<x86_reg_class> source_regs;
};

using sptrBvertex = std::shared_ptr<BVERTEX>;


std::string perms2s(const std::vector<Permission>& perms);

// MARK:- Memory address
using addr_t = std::uint64_t;
using waddr_t = std::pair<unsigned int, addr_t>;


std::string addr2s(addr_t addr);
std::string waddr2s(waddr_t waddr);
std::string mem_case2s(addr_t addr);
std::string addrs2s(const std::unordered_set<addr_t>& values, const std::string& sep = "; ");
std::string addrs2s(const std::set<addr_t>& values, const std::string& sep = "; ");
std::string addrs2s(const std::deque<addr_t>& values, const std::string& sep = "; ");
std::string addrs2s(const std::vector<addr_t>& values, const std::string& sep = "; ");
std::string waddrs2s(const std::set<waddr_t>& values, const std::string& sep = "; ");
std::string waddrs2s(const std::deque<waddr_t>& values, const std::string& sep = "; ");

// MARK:- Byte
using byte_t = std::uint8_t;
std::string byte2s(byte_t v, bool with_0x_prefix = true);
std::string bytes2s(const std::unordered_set<byte_t>& values);
std::string bytes2s(const std::set<byte_t>& values);

// MARK:- Color
using color_t = std::uint32_t;


const std::string& reg2s(Register name);
addr_t reg2size(Register name);
const std::pair<std::string, std::string>& reg2smt(Register name);
bool isAValidReg(const std::string& s);
bool isRegGeneralPurpose(const x86_reg_class reg);
Register s2reg(const std::string& s);
const std::unordered_map<Register, std::string>& getAllCpuRegs();
x86_reg_class getRegClass(x86_reg reg);
OptLevel str2opt(const std::string& opt_level_str);
BinaryFormat str2format(const std::string& format_str);
BinaryArch str2arch(const std::string& format_str);
BinaryCompiler str2compiler(const std::string& format_str);

std::string compiler2str(BinaryCompiler opt);
std::string arch2str(BinaryArch opt);
std::string format2str(BinaryFormat opt);
std::string opt2str(OptLevel opt);

std::string reg2str(uint16_t reg);
std::string reg2str(x86_reg reg);
std::string reg2str(x86_reg_class reg);

std::string regVect2str(std::vector<x86_reg> vect );
std::string regVect2str(std::vector<x86_reg_class> vect);
std::string regVect2str(std::set<x86_reg_class> source_reg );
std::string regVect2str(std::vector<BVERTEX> bigraph );


std::string vec2str32(std::vector<uint32_t>);
std::string vec2strAddr(std::vector<addr_t> vect);
std::string set2strAddr(std::unordered_set<addr_t> vect);

std::string instType2str(InstrType inst);

void copystring2byte(byte_t* byte_ptr, const std::string& byte_str);

//std::string reg2str(x86_reg reg);

// MARK:- Bipartite Graph Vector


using biGraphVect = std::vector<sptrBvertex>;


// MARK:- Binary's sites data (used for comparison)

// MARK:- Exceptions
enum class Exception
{
  ACCESS_VIOLATION,
  INTEGER_DIVIDE_BY_ZERO,
  BREAKPOINT,
  SINGLE_STEP,
  ILLEGAL_INSTRUCTION
};
uint32_t exc2code(Exception name);
std::string exc2s(Exception name);

	// Capstone stuff
	InstrType csToInstrType(cs_insn *cs_insn);
	std::string getCapstoneInstStr(cs_insn *cs_insn);
	std::string opAccess2str(uint8_t access);
	bool is_cs_cflow_ins(cs_insn *cs_insn);
	addr_t get_cs_ins_immediate_target(cs_insn *ins);
	bool is_cs_cflow_group(byte_t g);
	bool is_cs_unconditional_cflow_ins(cs_insn *ins);
	bool is_cs_call_ins(cs_insn *ins);
} // namespace boa

#endif
