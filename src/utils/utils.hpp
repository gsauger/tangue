#ifndef UTILS_H
#define UTILS_H

#define SPDLOG_SHORT_LEVEL_NAMES                                                                                       \
  {                                                                                                                    \
    "A", "B", "C", "D", "E", "F", "G"                                                                        \
  }
#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_TRACE
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/spdlog.h"
#include "capstone/capstone.h"

#include <experimental/filesystem>
#include <cstring>
#include <deque>
#include <memory>
#include <mutex>
#include <set>
#include <string>
#include <thread>
#include <unordered_set>
#include <vector>
#include <tuple>
#include <random>
#include <iterator>
#include <fstream>
namespace boa
{

using addr_t = uint64_t;
using UPtrString = std::unique_ptr<std::string>;
using SetString = std::set<std::string>;
using UPtrSetString = std::unique_ptr<SetString>;

using UnorderedSetString = std::unordered_set<std::string>;
using UPtrUnorderedSetString = std::unique_ptr<UnorderedSetString>;

using sptrLog = std::shared_ptr<spdlog::logger>;

enum class DisassemblyMode
{
  STATIC,
  BOA
};
enum class DisassemblyType
{
  RECURSIVE,
};

enum class DisassemblerEngine
{
  CAPSTONE,
 	 IDA
};

struct func_pair_data_t
{
	std::string bin1;
	std::string bin2;
	addr_t fva1;
	addr_t fva2;
	std::string fname1;
	std::string fname2;
};

struct config_global_t
{
  // From CLI or config file
  addr_t current_start_addr=0;
  std::vector<addr_t> start_addrs;
  std::unordered_set<addr_t> addrs_not_to_disas;
  DisassemblyMode disas_mode;
  DisassemblyType disas_type = DisassemblyType::RECURSIVE;
  DisassemblerEngine disassembler;
  bool print_csv_result;
  bool no_disassemble_call_targets;
	uint multithreading;
  // general

	std::string path_to_ida_script;
	std::string output_folder;
	std::string site_sim_method = "bijection";
	std::string bgraph_sim_method;
  std::unordered_set<std::string> bin_folder;
  std::unordered_set<std::string> bin_folder_test_pairs;

  // learn mode
  bool is_learn_mode;
  bool print_functions;
  bool print_sites;
  bool print_sites_dataflow;
  bool dotfile;
  bool output_function_map;
  bool saveDataRaw;
  bool quiet;
  bool condensed_output;
  bool is_reduced;

  uint site_entry_point=0;
  uint fun_entry_point=0;
  std::string fun_name;


  // correlation mode
  bool is_correlation_mode = false;
  int min_nb_bb=0;
  bool corr_verbose = false;
	std::string func_similarity_score;
	std::string pairs_test_file;
	std::vector<func_pair_data_t> test_funcs_pairs;

  std::vector<std::string> bin_str_vect;

  // distance mode

  bool is_distance_mode = false;
  std::vector<std::string> distance_str_vect;
  //std::string bin2_filepath;
  std::vector<std::string> bin_folder1;
  std::vector<std::string> bin_folder2;

  // database
  std::string database_output;

  // dbdistance
  bool is_dbdistance_mode = false;
  std::string dbdistance_database;
  std::string bin_db_name;

  // dbcompare
  std::string database_filepath;
  std::vector<std::string> dbcompare_binaries_vect;
  std::string dbcompare_database;
  bool is_dbcompare_mode;
  std::string bin1_name;
  std::string bin2_name;

  // Computed later
  std::string bin_folderpath;
  std::string bin_filename;

  // Debug
  bool test_mode = false;
  bool all_bbs_to_smtlib;
  bool dump_bbs_to_smtlib;
  bool verbose;
  bool print;
  bool distance_print;

  // Analysis
  bool opaque_predicate_analysis;
  unsigned int op_analysis_nb_thread;

  unsigned int sites_size = 24;
  bool recover_common_sites;

  // Classification

  bool sample_distance = false; // control sample for distance (without dataflow)
  float site_similarity_threshold=0.5;


};

struct my_time_format_t
{
  long long hr;
  long long min;
  long long sec;
  long long milli;
};

namespace utils
{
extern const char* core_logger;
extern const char* bin_parser_logger;
extern const char* disassembler_logger;
extern const char* rec_disas_logger;
extern const char* boa_disas_logger;
extern const char* x86_to_smtlib_logger;
extern const char* solver_logger;
extern const char* tainting_logger;
extern const char* analysis_logger;
extern const char* classification_logger;
extern const char* database_logger;




void pushMessage(const std::string& m);
const std::vector<std::string>& getMessagesStack();

std::string DisassemblyMode_to_string(DisassemblyMode disas_mode);
std::string uint32_to_string(const uint32_t& v);
std::string uint322s(const std::vector<uint32_t>& values, const std::string& sep = "; ");
std::string get_folder_path(const std::string& s);
std::string get_filename(const std::string& s);
void create_text_file(const std::string& content, const std::string& filepath);
std::pair<int, std::string> exec_shell_command(std::string cmd);
void dot_to_pdf(const std::string& dot_filepath, const std::string& pdf_filepath);
void find_and_replace(std::string* p_source, std::string const& find, std::string const& replace);
FILE* string_to_file_pointer(char* s);
std::vector<std::string> string_splitter(std::string const& s, std::string const& delim);
my_time_format_t milli2hr_min_sec_milli(long long milli);
std::tuple<uint8_t, uint8_t, uint8_t, uint8_t> dword2bytes(uint32_t v);
std::tuple<uint8_t, uint8_t> word2bytes(uint16_t v);
std::vector<uint8_t> bytes_to_vect(uint8_t bytes[16], uint8_t size);
std::string base_name(std::string filepath);
std::string bin_simple_name(std::string filepath);
std::string mangled_name(std::string filepath);
bool isFileExists(std::string name);

std::vector<std::string> getParsedStringVect(std::string filename);

// read csv
//
std::vector<std::string> getNextLineAndSplitIntoTokens(std::istream& str);
// print


std::string disassemblerToString(DisassemblerEngine disas);
} // namespace utils

} // namespace boa

#endif
