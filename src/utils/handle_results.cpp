#include "handle_results.hpp"
namespace boa{
void handleResultsLearn(Config *config, std::unordered_set<sptrAnalysis> analysis_engine_set,
                   std::shared_ptr<spdlog::logger> log) {

	// config info
	std::string config_out_path = config->getConfigGlobal().output_folder + "/_config_used.txt";
  auto config_log = spdlog::basic_logger_mt("config_log", config_out_path, true);
  config_log->set_pattern("%v");
  config->printOptions(config_log);

  log->info("Saved config in {}\n", config_out_path);
  // infos on the database
	std::string out_db_path = config->getConfigGlobal().output_folder + "/" + "_learn_db.json";
  auto output_db = spdlog::basic_logger_mt("db_log", out_db_path, true);
  output_db->set_pattern("%v");
  // - nb_sites/nb_func
  // - nb_func/nb_bin
  // - nb_bb/nb_func
  output_db->info("{"); // open (close at the end)

	uint ctr = 0;
	for(auto const& analysis_engine : analysis_engine_set){

		// gather data for the db log
		ctr++;
		output_db->info("\"{}\":{{",analysis_engine->getBinName());
		uint ctr2 = 0;
		for(auto const& fun_ptr : analysis_engine->getFunToAnalyze()){
			ctr2++;
			output_db->info("\"{}\":\"{},{},{}\"", fun_ptr->getName(), fun_ptr->getBasicBlocks().size(),
																			fun_ptr->getSites().size(), fun_ptr->getDataSites().size());
			if(ctr2<analysis_engine->getFunToAnalyze().size()){output_db->info(",");}
		}
		output_db->info("}");
		if(ctr < analysis_engine_set.size()){output_db->info(",");}

		log->info("- Binary {}\n", analysis_engine->getBinName());

  	std::shared_ptr<CFG> cfg = analysis_engine->getCFG();
  	std::string bin_name = analysis_engine->getBinName();
  	bool is_reduced = config->getConfigGlobal().is_reduced;
		std::string output_log_name = "learn" + bin_name;

  	log->info("\t-> Sites of size {}: {}\n",config->getConfigGlobal().sites_size, analysis_engine->getNumberOfSites());
  	log->info("\t-> Functions: {}\n", analysis_engine->getNumberofFunctions());
  	log->info("\t-> Basic blocks: {}\n", cfg->getNumberBasicBlocks());
  	log->info("\t-> Instructions: {}\n", cfg->getNumberInstrs());
  	log->info("\t-> Edges: {}\n", cfg->getNumberEdges());

  	// output log creation
  	if(!config->getConfigGlobal().output_folder.empty()){

			// condensed output log
			std::string condensed_output_log_path = config->getConfigGlobal().output_folder+ "/" +  bin_name + "_condensed_learn_log.txt";
			std::ofstream condensed_output_log(condensed_output_log_path);
    	for(auto const& fun : analysis_engine->getSortedFun()){
    		condensed_output_log << fun->getName() << "," << fun->getEntryPointAddr() << "," << fun->getSignature() << "," << fun->getSize();
    	}
    	log->info("Saved condensed log at {}\n", condensed_output_log_path);

			// regular output log
			std::string out_log_path = config->getConfigGlobal().output_folder + "/" +  bin_name+ "_learn_log.txt";
    	auto output_log = spdlog::basic_logger_mt(output_log_name, out_log_path, true);
    	output_log->set_pattern("%v");
			analysis_engine->printOutputLog(output_log);
    	log->info("Saved learn log at {}\n", out_log_path);
  	}

  	// pdf creation
  	if(config->getConfigGlobal().dotfile){
    	std::string file_save_path;
    	if(config->getConfigGlobal().output_folder == "."){
      	file_save_path = utils::disassemblerToString(config->getConfigGlobal().disassembler) + "_" + analysis_engine->getBinName();
    	}
    	else{
      	file_save_path = config->getConfigGlobal().output_folder + "/" + utils::disassemblerToString(config->getConfigGlobal().disassembler) + "_" + analysis_engine->getBinName();
    	}

    	if(config->getConfigGlobal().site_entry_point){
      	uint site_ep = config->getConfigGlobal().site_entry_point;
      	// Case where we only output a site's CFG
      	file_save_path += "_site_" + addr2s(site_ep);
      	log->info("Creating pdf for site starting at addr {:x} :...\n", site_ep);

      	sptrSite site_pointer = analysis_engine->getSiteCheckingAlsoEnd(site_ep);

      	if(config->getConfigGlobal().print_sites_dataflow){
        	log->info("With dataflow ! Site has {} paths\n", site_pointer->getDataflowTensor().size());
        	utils::create_text_file(site_pointer->createDotWithDataflow( is_reduced), file_save_path + ".dot");
        	log->info("Printing Site dataflow info: {}", site_pointer->printGraphDataflowTensor(""));
      	}
      	else{
        	utils::create_text_file(site_pointer->createDot(is_reduced), file_save_path + ".dot");
      	}

      	utils::dot_to_pdf(file_save_path + ".dot", file_save_path + ".pdf");
      	log->info("Created CFG at: {}\n", file_save_path + ".pdf");

      	return;
    	}
    	else if(config->getConfigGlobal().fun_entry_point){
      	uint fun_ep = config->getConfigGlobal().fun_entry_point;
      	// Case where we only output a site's CFG
      	file_save_path += "_fun_" + addr2s(fun_ep);
      	log->info("Creating pdf for fun starting at addr {:x}:...\n", fun_ep);

      	std::shared_ptr<Function> fun_ptr = analysis_engine->getFunction(fun_ep);

      	if(config->getConfigGlobal().print_sites_dataflow){
        	log->info("With dataflow ! Fun has {} sites and {} datasites\n",fun_ptr->getSites().size(), fun_ptr->getDataSites().size());
        	utils::create_text_file(fun_ptr->createDotWithDataflow( is_reduced), file_save_path + ".dot");
        	log->info("Printing Site dataflow info: {}", fun_ptr->printGraphDataflowTensor(""));
      	}
      	else{
        	utils::create_text_file(fun_ptr->createDot(is_reduced), file_save_path + ".dot");
      	}

      	utils::dot_to_pdf(file_save_path + ".dot", file_save_path + ".pdf");
      	log->info("Created CFG at: {}\n", file_save_path + ".pdf");
      	return;
    	}
    	else if(!config->getConfigGlobal().fun_name.empty()){
      	uint fun_ep;
      	std::string func_name = config->getConfigGlobal().fun_name;
      	std::shared_ptr<Function> fun_ptr = analysis_engine->getFunction(func_name);
      	if(fun_ptr == NULL){
        	SPDLOG_LOGGER_INFO(log, "Warning: could not find a function with name {} to print\n", func_name);
        	return;
      	}

      	fun_ep = fun_ptr->getEntryPointAddr();
      	// Case where we only output a site's CFG
      	file_save_path += "_fun_" + func_name + "_" + addr2s(fun_ep);
      	log->info("Creating pdf for fun {} starting at addr {:x}:...\n",func_name, fun_ep);

      	if(config->getConfigGlobal().print_sites_dataflow){
        	log->info("With dataflow ! Fun has {} sites and {} datasites\n",fun_ptr->getSites().size(), fun_ptr->getDataSites().size());
        	utils::create_text_file(fun_ptr->createDotWithDataflow( is_reduced), file_save_path + ".dot");
        	log->info("Printing Site dataflow info: {}", fun_ptr->printGraphDataflowTensor(""));
      	}
      	else{
        	utils::create_text_file(fun_ptr->createDot(is_reduced), file_save_path + ".dot");
      	}

      	utils::dot_to_pdf(file_save_path + ".dot", file_save_path + ".pdf");
      	log->info("Created CFG at: {}\n", file_save_path + ".pdf");
      	return;    }

    	utils::create_text_file(cfg->createDot(is_reduced), file_save_path + ".dot");

    	if (cfg->getNumberBasicBlocks() < 3000) {
      	log->info("Creating CFG's pdf ...\n");
      	utils::dot_to_pdf(file_save_path + ".dot", file_save_path + ".pdf");
      	log->info("Created CFG at: {}\n", file_save_path + ".pdf");
    	}
    	else{
      	log->info("Skipped saving the full CFG pdf, too many basic blocks (> 3000)\n");
    	}

    	if(config->getConfigGlobal().output_function_map){
      	log->info("Creating interprocedural map's pdf ...\n");
      	std::string map_save_path = file_save_path + "_interprocedural_map";
      	utils::create_text_file(analysis_engine->createFunMapDot(), map_save_path + ".dot");
      	utils::dot_to_pdf(map_save_path + ".dot", map_save_path + ".pdf");
      	log->info("Created interprocedural map at: {}\n", map_save_path + ".pdf");
    	}
    	if(config->getConfigGlobal().print_functions){
      	log->info("Creating some functions CFG examples...\n");

      	for(auto const& function : analysis_engine->getFunToAnalyze()){
        	if(function->getSites().size() > 125 || function->getSites().size() == 0){continue;}
        	std::string fun_filepath = file_save_path + "_funCFG_" + function->getName();

       	 if(config->getConfigGlobal().print_sites){
        	if(config->getConfigGlobal().print_sites_dataflow ){
          	log->info("Creating CFG with sites and dataflow at: {} ...\n", fun_filepath + ".pdf");
          	utils::create_text_file(function->createDotWithSitesWithDataflow(), fun_filepath + ".dot");
        	}
        	else{
          	log->info("Creating CFG with sites at: {} ...\n", fun_filepath + ".pdf");
        	utils::create_text_file(function->createDotWithSites(), fun_filepath + ".dot");
        	}
       	 }
       	 else{
          	log->info("Creating CFG at: {} ...\n", fun_filepath + ".pdf");
          	utils::create_text_file(function->createDot(is_reduced), fun_filepath + ".dot");
       	 }
        utils::dot_to_pdf(fun_filepath + ".dot", fun_filepath + ".pdf");
      	}
    	}
  	}

  	// database creation
  	if(!config->getConfigGlobal().database_output.empty()){
    	std::string db_name = config->getConfigGlobal().database_output;
    	log->info("Creating database {} ... \n", db_name);
    	std::shared_ptr<DBManager> db = std::make_shared<DBManager>(db_name);
    	db->addBinaryData(analysis_engine);
  	}
  }
  output_db->info("}"); // close at the end
  log->info("Saved database log at {}\n", out_db_path);
}

void handleResultsCorrelation(Config *config, std::shared_ptr<ClassificationEngine> classification_engine,std::shared_ptr<spdlog::logger> log){
  classification_engine->sortCorrelFunctions();

  auto time_tick = std::chrono::system_clock::now();

	// --------- Score Matrix

	std::string inc_mat_path = config->getConfigGlobal().output_folder + "/_inclusion_matrix.csv";
	std::ofstream output_str(inc_mat_path);
  log->info("Saving csv detailed results in {}...\n", inc_mat_path);

  // columns headers
  output_str << "fun1; fun2; score\n";

	uint index=0, nb_fun;
	nb_fun = classification_engine->getCorrelFunctions().size();
	std::string enter="\n", comma=";";

  auto sim_time_tick = std::chrono::system_clock::now();
  std::chrono::duration<double> sim_elapsed_seconds;
  for(auto const& fun : classification_engine->getCorrelFunctions()){
    // for each function
    sim_elapsed_seconds = std::chrono::system_clock::now() - sim_time_tick;
    log->info("Saved {}/{} ({:.2f}l/s)\r", index, nb_fun, (float)index/((float)sim_elapsed_seconds.count() + 10e+9));
    index++;

    for(auto const& [fun2, score] : fun.score_map){ // line of inclusion score
        // fill a new line with scores with other functions
        // order is a bit fragile here but hopefully is preserved
      output_str << fun.fun_ptr->getBinName() + "_" + fun.fun_ptr->getName() << ";"\
      						<< fun2->getBinName() + "_" +  fun2->getName() << ";"\
      						<< std::to_string(score) << "\n";
    }
    output_str << "\n";
  }
  log->info("Done\n");
  std::chrono::duration<double> elapsed_seconds = std::chrono::system_clock::now() - time_tick;
  log->info("Writing score matrix csv elapsed time : {} sec\n", elapsed_seconds.count());

	// --------- Marcelli et al. - like csv output (if a test functions pairs csv was provided)

	if(!config->getConfigGlobal().test_funcs_pairs.empty()){
		std::string pairs_filename = utils::base_name(config->getConfigGlobal().pairs_test_file);

  	std::size_t found = pairs_filename.find_first_of("_");
  	std::string test_type_id = pairs_filename.substr(0, found); // neg or pos ?
  	std::string test_type;
		if(test_type_id == "pos"){ test_type = "pos_rank_testing";}
		else if(test_type_id == "neg"){ test_type = "neg_rank_testing";}
		else{log->info("Error, input function pairs name is neither 'neg' nor 'pos' ({}).\n", test_type_id);}

  	found = pairs_filename.find_last_of("_");
  	std::string dataset = pairs_filename.substr(found + 1); // neg or pos ?
  	found = dataset.find_last_of(".");
  	dataset = dataset.substr(0,found); // neg or pos ?

		std::string model = "TANGUE";
		std::string options = config->printOptionsCondensed();
		std::string file_name = test_type + "_" + dataset + "_" + model + "_" + options + ".csv";
		std::string test_func_pair_path = config->getConfigGlobal().output_folder + "/" + file_name;
		std::ofstream output_func_pairs_str(test_func_pair_path);
  	log->info("Saving selected function pairs results in {}...\n", test_func_pair_path);

  	// headers
  	output_func_pairs_str << "idb_path_1;fva_1;idb_path_2;fva_2;sum\n";
  	// rows
		for(auto const & fun : classification_engine->getCorrelFunctions()){
			for(auto const& fun2 : fun.score_map){
					// matching pair
					output_func_pairs_str << fun.fun_ptr->getBinName() << ";" << addr2s(fun.fun_ptr->getEntryPointAddr()) \
																<< ";" << fun2.first->getBinName() << ";" << addr2s(fun2.first->getEntryPointAddr()) \
																<< ";" << std::to_string(fun2.second) << "\n"; } }
	}

	// --------- Options

	std::string config_out_path = config->getConfigGlobal().output_folder + "/_config_used.txt";

  auto config_log = spdlog::basic_logger_mt("config_log", config_out_path, true);
  config_log->set_pattern("%v");
  config->printOptions(config_log);

  log->info("Saved config in {}\n", config_out_path);
  return;
	}

}
