#include "utils.hpp"

#include <fstream>
#include <iomanip>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>

namespace boa
{
namespace utils
{

const char* core_logger = "core";
const char* bin_parser_logger = "bin_parser";
const char* disassembler_logger = "disassembler";
const char* rec_disas_logger = "rec_disas";
const char* boa_disas_logger = "boa_disas";
const char* x86_to_smtlib_logger = "x86_to_smtlib";
const char* solver_logger = "solver";
const char* tainting_logger = "tainting";
const char* analysis_logger = "analysis";
const char* classification_logger = "classification";
const char* database_logger = "database";



std::vector<std::string> messages_stack;

void pushMessage(const std::string& m)
{
  messages_stack.push_back(m);
}

const std::vector<std::string>& getMessagesStack()
{
  return messages_stack;
}

std::string DisassemblyMode_to_string(DisassemblyMode disas_mode)
{
  switch(disas_mode)
  {
  case DisassemblyMode::STATIC:
    return "STATIC";
  case DisassemblyMode::BOA:
    return "BOA";
  default:
    return "UNKNOWN";
  }
}

std::string uint32_to_string(const uint32_t& v)
{
  std::stringstream s;
  s << "0x" << std::hex << v;
  return s.str();
}

std::string uint322s(const std::vector<uint32_t>& values, const std::string& sep)
{
  if(values.empty())
    return "";
  std::string s = "";
  for(auto v : values)
    s = s + uint32_to_string(v) + sep;
  return s.substr(0, s.length() - sep.length());
}

std::string get_folder_path(const std::string& s)
{
  char sep = '/';

#ifdef _WIN32
  sep = '\\';
#endif

  size_t i = s.rfind(sep, s.length());
  if(i != std::string::npos)
  {
    return (s.substr(0, i) + sep);
  }

  return "";
}

std::string get_filename(const std::string& s)
{

  char sep = '/';

#ifdef _WIN32
  sep = '\\';
#endif

  size_t i = s.rfind(sep, s.length());
  if(i != std::string::npos)
  {
    return (s.substr(i + 1, s.length() - i));
  }

  return s;
}

void create_text_file(const std::string& content, const std::string& filepath)
{
  std::ofstream out(filepath);
  out << content;
  out.close();
}

std::pair<int, std::string> exec_shell_command(std::string cmd)
{
  int exit_code = -1;
  std::string data;
  FILE* stream;
  const int max_buffer = 256;
  char buffer[max_buffer];
#ifdef __APPLE__
  cmd = "set -o pipefail && " + cmd;
#endif
  cmd.append(" 2>&1");
  stream = popen(cmd.c_str(), "r");
  if(stream)
  {
    while(!feof(stream))
      if(fgets(buffer, max_buffer, stream) != nullptr)
        data.append(buffer);
    exit_code = pclose(stream);
  }
  return {exit_code, data};
}

void dot_to_pdf(const std::string& dot_filepath, const std::string& pdf_filepath)
{
  std::string cmd = "dot -Tpdf " + dot_filepath + " -o " + pdf_filepath;
  auto r = exec_shell_command(cmd);
  if(r.first == -1)
  {
    throw std::runtime_error("Failed to generate PDF from dot file, missing GraphViz? Check if 'dot' program is "
                             "available from the PATH");
  }
}

void find_and_replace(std::string* p_source, std::string const& find, std::string const& replace)
{
  for(std::string::size_type i = 0; (i = p_source->find(find, i)) != std::string::npos;)
  {
    p_source->replace(i, find.length(), replace);
    i += replace.length();
  }
}

FILE* string_to_file_pointer(char* s)
{
  FILE* f = fmemopen(s, strlen(s), "r");
  if(f == NULL)
  {
    throw std::runtime_error("fmemopen failed");
  }
  return f;
}

std::vector<std::string> string_splitter(std::string const& s, std::string const& delim)
{
  std::vector<std::string> results_stack{};
  std::size_t current, previous = 0;
  current = s.find(delim);
  while(current != std::string::npos)
  {
    results_stack.push_back(s.substr(previous, current - previous));
    previous = current + delim.size();
    current = s.find(delim, previous);
  }
  results_stack.push_back(s.substr(previous, current - previous));
  return results_stack;
}

my_time_format_t milli2hr_min_sec_milli(long long milli)
{
  my_time_format_t r;
  r.hr = milli / 3600000;
  milli = milli - 3600000 * r.hr;
  r.min = milli / 60000;
  milli = milli - 60000 * r.min;
  r.sec = milli / 1000;
  r.milli = milli - 1000 * r.sec;
  return r;
}

std::tuple<uint8_t, uint8_t, uint8_t, uint8_t> dword2bytes(uint32_t v)
{
  return std::make_tuple(v & 0x000000FF, (v & 0x0000FF00) >> 8, (v & 0x00FF0000) >> 16, (v & 0xFF000000) >> 24);
}

std::tuple<uint8_t, uint8_t> word2bytes(uint16_t v)
{
  return std::make_tuple(v & 0x00FF, (v & 0xFF00) >> 8);
}

std::vector<uint8_t> bytes_to_vect(uint8_t bytes[16], uint8_t size){

  std::vector<uint8_t> output_vect;
  for(uint i=0; i<size; i++){
    output_vect.push_back(bytes[i]);
  }
  return output_vect;
  }

/**
 * /path/to/bin -> bin
 **/
std::string base_name(std::string filepath){
  return filepath.substr(filepath.find_last_of("/\\") + 1);
  // TODO: maybe find a better alternative ?
}//return filepath;

std::string bin_simple_name(std::string filepath){
  return filepath.substr(filepath.find_last_of("-") + 1);
  // TODO: maybe find a better alternative ?
}//return filepath;

std::string mangled_name(std::string filepath){
  //fuses the bin name and its parent folder
  std::size_t found = filepath.find_last_of("/\\");
  std::string file = filepath.substr(found + 1);
  std::string path = filepath.substr(0, found);
  std::string folder = path.substr(path.find_last_of("/\\") + 1);
  return folder + "_" + file;
}

bool isFileExists (const std::string& name) {
    if (FILE *file = fopen(name.c_str(), "r")) {
        fclose(file);
        return true;
    } else {
        return false;
    }
}

std::vector<std::string> getParsedStringVect(std::string filename){
  std::string s = filename;
  std::vector<std::string> output;
  std::string delimiter = "-";
  size_t pos = 0;
  std::string token;
  while ((pos = s.find(delimiter)) != std::string::npos) {
    token = s.substr(0, pos);
    output.push_back(token);
    s = s.substr(pos + delimiter.length());
  }
  return output;
}

// CSV Reading

std::vector<std::string> getNextLineAndSplitIntoTokens(std::istream& str)
{
    std::vector<std::string>   result;
    std::string                line;
    std::getline(str,line);

    std::stringstream          lineStream(line);
    std::string                cell;

    while(std::getline(lineStream,cell, ','))
    {
        result.push_back(cell);
    }
    // This checks for a trailing comma with no data after it.
    if (!lineStream && cell.empty())
    {
        // If there was a trailing comma then add an empty element.
        result.push_back("");
    }
    return result;
}


// PRINT

std::string disassemblerToString(DisassemblerEngine disas){
	switch (disas){
		case DisassemblerEngine::CAPSTONE:
			return "CAPSTONE";
		case DisassemblerEngine::IDA:
			return "IDA";
		default:
			return "UNKNOWN?";
	}
}

} // namespace utils
} // namespace boa
