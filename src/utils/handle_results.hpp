#ifndef HANDLE_H
#define HANDLE_H
#include "config.hpp"
#include "utils/utils.hpp"

#include "binary/binary_parser_r2.hpp"

#include "disassembler/disassembler_capstone.hpp"

#include "analysis/analysis.hpp"
#include "classification/classification.hpp"
#include "db_manager/db_manager.hpp"

#include "common/cfg_manager.hpp"

namespace boa{

void handleResultsLearn(Config *config, std::unordered_set<sptrAnalysis> analysis_engine,
                   std::shared_ptr<spdlog::logger> log);

void handleResultsCorrelation(Config *config, std::shared_ptr<ClassificationEngine> classification_engine,std::shared_ptr<spdlog::logger> log);

void handleResultsDistance(Config *config, std::shared_ptr<spdlog::logger> log, std::shared_ptr<ClassificationEngine> classification_engine,const std::string& bin1_name, const std::string& bin2_name);

void handleResultsDBCompare(Config *config, std::shared_ptr<spdlog::logger> log, std::shared_ptr<ClassificationEngine> classification_engine);
}
#endif
