#ifndef SMTLIB_UTILS_H
#define SMTLIB_UTILS_H

#include <deque>
#include <stdio.h>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace boa
{

namespace smtlib_utils
{

/*!
 * Bitvector arithmetic
 */

std::string smt_bv_sub(const std::string& a, const std::string& b);
std::string smt_bv_add(const std::string& a, const std::string& b);
std::string smt_bv_mul(const std::string& a, const std::string& b);

/*!
 * Predicates over Bitvectors
 */

std::string smt_bv_ule(const std::string& a, const std::string& b);
std::string smt_bv_ult(const std::string& a, const std::string& b);
std::string smt_bv_uge(const std::string& a, const std::string& b);
std::string smt_bv_ugt(const std::string& a, const std::string& b);
std::string smt_bv_sle(const std::string& a, const std::string& b);
std::string smt_bv_slt(const std::string& a, const std::string& b);
std::string smt_bv_sge(const std::string& a, const std::string& b);
std::string smt_bv_sgt(const std::string& a, const std::string& b);

/*!
 * Autre SMT
 */

std::string smt_equal(const std::string& a, const std::string& b);

std::string smt_not(const std::string& symbol);

std::string smt_comment(const std::string& comment);

std::string smt_or(const std::vector<std::string>& terms);
std::string smt_or(const std::string& a, const std::string& b);

std::string smt_and(const std::vector<std::string>& terms);
std::string smt_and(const std::string& a, const std::string& b);

std::string smt_bv_val(const uint32_t& value, unsigned int size);

std::string smt_concat(const std::string& a, const std::string& b);

std::string smt_select(const std::string& array, const std::string& idx);

std::string smt_select_2_array_cases(const std::string& array, const std::string& first_idx);

std::string smt_select_4_array_cases(const std::string& array, const std::string& first_idx);

std::string smt_select_n_array_cases(unsigned int n, std::string array, std::string first_idx);

std::string smt_ite(const std::string& cond, const std::string& term_if_true, const std::string& term_if_false);

/*!
 * Les sorts SMT
 */

std::string smt_sort_bv(const int& size);

std::string smt_sort_array(const std::string& idx_sort, const std::string& elt_sort);

/*!
 * Les commandes SMT
 */

std::string smt_assert(const std::string& term);

std::string smt_check_sat();

std::string smt_decl_const(const std::string& symbol, const std::string& sort);

std::string smt_decl_fun(const std::string& symbol, const std::string& sort);

std::string smt_def_fun(const std::string& symbol, const std::string& sort, const std::string& term);

std::string smt_exit();

std::string smt_get_model();

std::string smt_get_value(const std::string& term);

std::string smt_get_value(const std::unordered_set<std::string>& terms);

std::string smt_get_value(const std::unordered_map<std::string, uint32_t>& terms);

std::string smt_get_value(const std::vector<std::string>& terms);

std::string smt_pop();

std::string smt_push();

std::string smt_reset();

std::string smt_set_info(const std::string& attribute);

std::string smt_set_logic(const std::string& symbol);

std::string smt_set_option(const std::string& option);

} // namespace smtlib_utils

} // namespace boa

#endif /* SMTLIB_UTILS_H */
