#ifndef CONFIG_H
#define CONFIG_H

#include <optional>
#include <string>

#include "binary/binary.hpp"
#include "utils/utils.hpp"

namespace boa
{

extern const char* core_logger;
extern const char* bin_parser_logger;
extern const char* disassembler_logger;
extern const char* rec_disas_logger;
extern const char* boa_disas_logger;
extern const char* x86_to_smtlib_logger;
extern const char* solver_logger;
extern const char* tainting_logger;
extern const char* analysis_logger;
extern const char* database_logger;


class Config
{
public:
  Config(int argc, char* argv[]);

  const config_global_t& getConfigGlobal() const;
  void setCurrentStartAddr(addr_t addr);
  void addStartAddr(addr_t addr);

	std::string disassemblerToString(DisassemblerEngine disas);
	void printOptions(std::shared_ptr<spdlog::logger> log);
	std::string printOptionsCondensed() const;
	void checkForCsvInput(sptrLog log);
	const std::unordered_set<std::string>& getBinFolder() const;

private:
  config_global_t m_config_global;
};

} // namespace boa

#endif
