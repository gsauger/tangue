#ifndef CLASSIFICATION_H
#define CLASSIFICATION_H

#include <iostream>
#include "utils/boa_types.hpp"
#include "analysis/analysis.hpp"
#include "db_manager/db_manager.hpp"

namespace boa{

struct BINARYDATA{
  // elt i of each vector of the struct represent the i_th site's data of a binary
  std::string bin_name;
  std::vector<sptrSite> sites_ptr_vect;
  std::vector<addr_t> sites_addr_data;
};

struct FUNCTIONDATA{
  // a function name, and the its sites, within a binary
  std::string fun_name;
  std::vector<sptrSite> sites_list;
};

struct FUNC_CORREL_DATA{
  sptrFunc fun_ptr; // pointer to the Function object associated with the function
  std::string fun_correl_name; // name of the function, in the engine ( usage_basename_O3_5.93)
  std::string fun_label; // true name of the function (ground truth: ex usage)
  std::unordered_map<sptrFunc, float> score_map; // first: other fun's ptr; second: correl score with the other function
  bool operator < (const FUNC_CORREL_DATA &other) const { return fun_correl_name < other.fun_correl_name; } // override the > operator for building sets
  bool operator == (const FUNC_CORREL_DATA &other) const { return fun_ptr == other.fun_ptr;} // overrides for equality
};

class ClassificationEngine{

public:

    ClassificationEngine(const std::string& site_sim_method, const std::string& bgraph_sim_method);

    ~ClassificationEngine();


    // Setters and Getters

		const std::string& getSiteSimMethod() const{return m_site_sim_method;}

	uint getNbFuncNotInBin( const std::string& bin_name);
    const std::map<std::string, std::vector<FUNCTIONDATA>>& getMapFunSites()const{return m_map_fun_sites;}
		const std::string& getBgraphSimMethod()const {return m_bgraph_sim_method;}
    float getDistance() const;
    BINARYDATA getBinData(const std::string& bin_filename);
    unsigned int getBinNbSites(const std::string& bin_filename);

    uint getBin1NbUniqueSites();
    uint getBin2NbUniqueSites();

    unsigned int getCommonSitesNbrBin(const std::string& name);
    //unsigned int getCommonSitesNbrBin1();
    //unsigned int getCommonSitesNbrBin2();

    std::string printBinFilenames();

    std::map<sptrSite, std::vector<sptrSite>> getCommonSitesPairs();
    std::string printCommonDetailedInfo(const std::string& bin1, const std::string& bin2);

    std::vector<sptrSite> getUniqueSitesLikeSigtoolBin1();
    std::vector<sptrSite> getUniqueSitesLikeSigtoolBin2();

    float getElapsedTime() const ;

    std::vector<sptrSite> getSitesOfFun(const std::string& bin_name, const std::string& fun_name);

    std::vector<sptrSite> getSitesMatchingSiteFromBin2(sptrSite site);

    FUNCTIONDATA getFunData(const std::string& bin1, const std::string& fun_str);

    std::set<std::string> getUniqueFunctionsLabels() const;

    std::string getMeanNbOfSitesInFun(const std::string& label) const;

    // Checks

    void checkIfBinInEngine(const std::string& bin_name) const;
    bool isBinInEngine(const std::string& bin_name) const;

    // Comparison algos
    void compareBinToDb(bool sample_distance, std::shared_ptr<DBManager> db, const std::string& bin1, const std::string& bin_db);

    bool areDataflowEqual( const std::vector<sptrBipartite>& dataflow_graph1, const std::vector<sptrBipartite>& dataflow_graph2);
    bool areSitesSimilarArchitecture(const sptrSite&  site1 ,const sptrSite&  site2);
    bool areSuperSitesSimilar( sptrSite  site1,  sptrSite  site2, const Config* config);
    bool areSitesSimilarUsingHash(const sptrSite&  site1, const sptrSite&  site2, bool sample_distance);

    void computeDistanceLikeSigtool(const std::string& bin1, const std::string& bin2);
    void computeDistance(const std::string& bin1, const std::string& bin2);

    // ------ Correlation mode

    FUNC_CORREL_DATA getFuncCorrelDataOf(sptrFunc fun_ptr) const;
		std::shared_ptr<FUNC_CORREL_DATA> getFuncCorrelDataOf(std::string fbin, std::string fname) const;
    void addBinaryFunctionsForCorrelation(
      std::shared_ptr<AnalysisEngine> analysis_engine, const Config* config);

    void computeFunctionsCorrelationScores(const Config* config);

    float computeInclusionScore(sptrFunc f1, sptrFunc f2, const Config* config);

    const std::vector<FUNC_CORREL_DATA>& getCorrelFunctions() const{return m_function_vect_correl;}

   // score computation

    float computeLabelScore(const std::string& label);
    void sortCorrelFunctions();

    // print
    std::string getBestMatchStr(FUNC_CORREL_DATA fun_correl_data) const;
		std::tuple<int,int, float> getRank(FUNC_CORREL_DATA fun_correl_data) const;


    // Stats

    uint getNbFunWithKSites(const std::string& name, uint k);

    std::string printTopMatchedFun(const std::string& bin1, const std::string& bin2);

    std::vector<std::pair<std::string, uint>> getFunBestMatchesInBin2(const FUNCTIONDATA& fun_data);
    std::vector<std::pair<std::string, uint>> getFunBestMatchesInBin1(const FUNCTIONDATA& fun_data);

    // Printing

    std::string printCommonSitesEp();
    std::string createCommonDotWithoutDigraph(bool is_reduced);
    std::string createCommonSitesDot(bool is_reduced);
    std::string printCommonSitesSigtoolCompare() const;
    std::string printBinDetailedInfos(const std::string& bin_name);

		std::string printFunsSummary();
    std::string printNbrFunctionWithMoreThanKSites(const std::string& bin_name);

private:

    std::vector<std::string> m_bin_filenames;

    std::map<std::string, uint> m_bin_index_map; // key: bin filename

    std::vector<BINARYDATA> m_binary_data;
    std::vector<BINARYDATA> m_binary_data_unique_sites;

    // Function data
    std::map<std::string, std::vector<FUNCTIONDATA>> m_map_fun_sites; // maps a binary to its functionS data

    //BINARY_DATA : get the i_th elt of the vects of this struct to access the i_th site's data of the binary

    // Common sites data - these values are always changing according to the last ComputeBinSimilarity method call
		std::string m_site_sim_method; // directly from the config file
		std::string m_bgraph_sim_method; // same as previous
    float m_distance;
    //unsigned int m_common_bin1;
    //unsigned int m_common_bin2;
    float m_elapsed_time;
    std::map<sptrSite, std::vector<sptrSite>> m_map_common_sites; // bin1 - bin2 !! order
    std::map<std::string, uint> m_common_sites_bin_list;

    // sigtool like
    std::vector<sptrSite> m_unique_sites_bin1;
    std::vector<sptrSite> m_unique_sites_bin2;
    //std::map<addr_t, std::vector<addr_t>> m_map_common_sites_addr;

    // ------ Correlation mode

    std::vector<FUNC_CORREL_DATA> m_function_vect_correl;
    // vect of fun and their names (defined as in slides)
    // fun number k of a bin1, optimization level i, version j
    // := bin1_i_j_k



    // ------------------------

    // Logging

    std::shared_ptr<spdlog::logger> m_log;

};

} // namespace boa

#endif
