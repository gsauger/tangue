#include "classification.hpp"

namespace boa{

// MARK:- Constructor
ClassificationEngine::ClassificationEngine(const std::string& site_sim_method, const std::string& bgraph_sim_method)
{
    m_log = spdlog::get(utils::classification_logger);
    SPDLOG_LOGGER_TRACE(m_log, "ClassificationEngine::ClassificationEngine()\n");
    m_site_sim_method = site_sim_method;
    m_bgraph_sim_method = bgraph_sim_method;
}

ClassificationEngine::~ClassificationEngine(){
    SPDLOG_LOGGER_TRACE(m_log, "ClassificationEngine::~ClassificationEngine()\n");
}

// checks

bool ClassificationEngine::isBinInEngine(const std::string& bin_name) const{
    return(m_bin_index_map.count(bin_name) > 0);
}


void ClassificationEngine::checkIfBinInEngine(const std::string&bin_name) const{
    if(!m_bin_index_map.count(bin_name)){
        // key doesn't exist
        // maybe try mangled name ?
        if(m_bin_index_map.count(utils::mangled_name(bin_name))>0){
            SPDLOG_LOGGER_DEBUG(m_log, "WARNING: Mangled name of {} data was found classification engine, but not the name. Aborting\n", bin_name);
        }
        SPDLOG_LOGGER_DEBUG(m_log, "Error: trying to access {} data that is not in this classification engine\n", bin_name);
        throw std::runtime_error("");
    }
}


// Getters and setters

 float ClassificationEngine::getDistance() const{
     return m_distance;
 }
uint ClassificationEngine::getNbFuncNotInBin( const std::string& bin_name){
	uint nb=0;
	for(auto const& fun : getCorrelFunctions()){
		if(fun.fun_ptr->getBinName()!=bin_name){
			nb++;
		}
	}
	return nb;
}
BINARYDATA  ClassificationEngine::getBinData(const std::string&bin_filename ){
    checkIfBinInEngine(bin_filename);
    return m_binary_data.at(m_bin_index_map.at(bin_filename));
}

uint ClassificationEngine::getCommonSitesNbrBin(const std::string&bin_name){
    checkIfBinInEngine(bin_name);
    return m_common_sites_bin_list.at(bin_name);
}

std::string ClassificationEngine::printBinFilenames(){
    std::stringstream f;
    for(const std::string& name : m_bin_filenames){
        f << name << ",";
    }
    return f.str();
}

uint ClassificationEngine::getBin1NbUniqueSites(){
    return m_unique_sites_bin1.size();
}

uint ClassificationEngine::getBin2NbUniqueSites(){
    return m_unique_sites_bin2.size();
}

unsigned int ClassificationEngine::getBinNbSites(const std::string& bin_filename){
    checkIfBinInEngine(bin_filename);
    if(getBinData(bin_filename).sites_ptr_vect.empty()){
        // case of using db info
        return getBinData(bin_filename).sites_addr_data.size();
    }
    return getBinData(bin_filename).sites_ptr_vect.size();
}

std::map<sptrSite, std::vector<sptrSite>> ClassificationEngine::getCommonSitesPairs(){
    return m_map_common_sites;
}

std::vector<sptrSite> ClassificationEngine::getSitesOfFun(const std::string& bin_name, const std::string& fun_name){
    std::vector<sptrSite> output;
    for(const sptrSite& site_ptr : getBinData(bin_name).sites_ptr_vect){
        if (site_ptr->getFunctionName() == fun_name){
            output.push_back(site_ptr);
        }
    }
    return output;
}

std::vector<sptrSite> ClassificationEngine::getUniqueSitesLikeSigtoolBin1(){
    return m_unique_sites_bin1;
}
std::vector<sptrSite> ClassificationEngine::getUniqueSitesLikeSigtoolBin2(){
    return m_unique_sites_bin2;
}

float ClassificationEngine::getElapsedTime() const{
    return m_elapsed_time;
}


std::vector<sptrSite> ClassificationEngine::getSitesMatchingSiteFromBin2(sptrSite site){
    std::vector<sptrSite> output;
    output.reserve(m_map_common_sites.size());
    for(auto const& [bin1_site, bin2_vect] : m_map_common_sites){
        if(std::find(bin2_vect.begin(), bin2_vect.end(), site) != bin2_vect.end()){
            // site is in vect
            output.push_back(bin1_site);
        }
    }
    return output;
}

FUNCTIONDATA ClassificationEngine::getFunData(const std::string& bin1,const std::string& fun_str){
    for(FUNCTIONDATA& fun_data : m_map_fun_sites.at(bin1)){
        if(fun_data.fun_name == fun_str){
            return fun_data;
        }
    }
    throw std::runtime_error("Error, could not locate the requested function in bin " + bin1);
}

std::set<std::string> ClassificationEngine::getUniqueFunctionsLabels() const{
    std::set<std::string> output;
    for(auto const& elt :  m_function_vect_correl){
        output.insert(elt.fun_label);
    }
    return output;
}

std::string ClassificationEngine::getMeanNbOfSitesInFun(const std::string& label) const{
    std::stringstream f;
    f << " ";
    for(auto const& fun_data : m_function_vect_correl){
        if(fun_data.fun_label == label){
            f << fun_data.fun_ptr->getSites().size() << "-";
        }
    }
    f.seekp(-1, std::ios_base::end);
    return f.str();
}


// Comparison
/*
bool ClassificationEngine::areDataflowEqual(const std::vector<sptrBipartite>& dataflow_graph1, const std::vector<sptrBipartite>& dataflow_graph2){
    if(dataflow_graph1.size() != dataflow_graph2.size()){ return false; }
    for(uint i = 0; i < dataflow_graph1.size(); i++){
        if(!dataflow_graph1.at(i)->isEqualTo(dataflow_graph2.at(i))){
            SPDLOG_LOGGER_DEBUG(m_log, "Dataflow of BBs {:x} and {:x} are different: \n{} and \n{}\n", dataflow_graph1.at(i)->getEntryPointAddr(), dataflow_graph2.at(i)->getEntryPointAddr(), dataflow_graph1.at(i)->toString(), dataflow_graph2.at(i)->toString());

            return false;
        }
    }
    return true;
}
*/
bool ClassificationEngine::areSitesSimilarArchitecture(const sptrSite& site1 ,const sptrSite& site2){

    if(site1->getEncoding() != site2->getEncoding()){
        return false;
    }
    return true;
}

bool ClassificationEngine::areSitesSimilarUsingHash(const sptrSite& site1 ,const sptrSite& site2, bool sample_distance){

    if(memcmp(site1->getEncodingHash(), site2->getEncodingHash(), MD5_HASH_SIZE)!=0){
        return false;
    }
    if(!sample_distance){
        if(memcmp(site1->getDataflowHash(), site2->getDataflowHash(), MD5_HASH_SIZE)!=0){
            return false;
        }
    }
    return true;
}

/**
 *  NOTE: This method updates the list of common sites, m_unique_sites_bin1 and m_unique_sites_bin2
 *
 * */
/*
void ClassificationEngine::computeBinSimilarity(const std::string& bin1, const std::string& bin2,const Config* config){

    //
    // We reset those values
    m_map_common_sites.clear();
    uint bin1_common=0, bin2_common=0;
    std::map<addr_t, bool> seen_bin1_ep;
    std::map<addr_t, bool> seen_bin2_ep;

    std::pair<sptrSite, sptrSite> common_site_pair_tmp;    

    BINARYDATA bin1_data = getBinData(bin1);
    BINARYDATA bin2_data = getBinData(bin2);

    uint total_nb_it = (getBinNbSites(bin1) * getBinNbSites(bin2));
    auto time_tick = std::chrono::system_clock::now();

    for(uint index1 = 0; index1 < getBinNbSites(bin1); index1 ++){

        std::vector<sptrSite> bin2_tmp_common_sites_vect;

        for(uint index2 = 0; index2 < getBinNbSites(bin2); index2 ++){
            // progress bar
            std::cout << "\t " << int(float((index1 * getBinNbSites(bin2) + index2)) / total_nb_it * 100) << " %\r";
            std::cout.flush();

            if(areSuperSitesSimilar(bin1_data.sites_ptr_vect.at(index1), bin2_data.sites_ptr_vect.at(index2), config)){

                if(!seen_bin1_ep[bin1_data.sites_addr_data.at(index1)]){
                    bin1_common++;
                    seen_bin1_ep[bin1_data.sites_addr_data.at(index1)]=true;
                }
                if(!seen_bin2_ep[bin2_data.sites_addr_data.at(index2)]){
                    bin2_common++;
                    seen_bin2_ep[bin2_data.sites_addr_data.at(index2)]=true;
                }

                bin2_tmp_common_sites_vect.push_back(bin2_data.sites_ptr_vect.at(index2));
                //common_site_pair_tmp = std::make_pair(bin1_data.sites_ptr_vect.at(index1), bin2_data.sites_ptr_vect.at(index2));
                //m_list_common_sites.push_back(common_site_pair_tmp);
            }
        }
        if(!bin2_tmp_common_sites_vect.empty()){
            m_map_common_sites.insert(std::pair{bin1_data.sites_ptr_vect.at(index1), bin2_tmp_common_sites_vect});
        }
    }

    std::cout << std::endl;
    std::chrono::duration<double> elapsed_seconds = std::chrono::system_clock::now() - time_tick;
    m_log->info("Distance elapsed time: {} sec\n", elapsed_seconds.count());

    m_elapsed_time = elapsed_seconds.count();
    m_common_sites_bin_list[bin1]=bin1_common;
    m_common_sites_bin_list[bin2]=bin2_common;
}
*/
/*
void ClassificationEngine::computeBinSimilarityUsingHash(bool sample_distance, const std::string& bin1, const std::string& bin2){

    // NOTE: This method updates the list of common sites, m_unique_sites_bin1 and m_unique_sites_bin2
    // We reset those values
    m_map_common_sites.clear();
    uint bin1_common=0, bin2_common=0;
    std::map<addr_t, bool> seen_bin1_ep;
    std::map<addr_t, bool> seen_bin2_ep;

    std::pair<sptrSite, sptrSite> common_site_pair_tmp;    

    BINARYDATA bin1_data = getBinData(bin1);
    BINARYDATA bin2_data = getBinData(bin2);

    uint total_nb_it = (getBinNbSites(bin1) * getBinNbSites(bin2));
    auto time_tick = std::chrono::system_clock::now();

    for(uint index1 = 0; index1 < getBinNbSites(bin1); index1 ++){
        std::vector<sptrSite> bin2_tmp_common_sites_vect;
        for(uint index2 = 0; index2 < getBinNbSites(bin2); index2 ++){
            // progress bar
            std::cout << "\t " << int(float((index1 * getBinNbSites(bin2) + index2)) / total_nb_it * 100) << " %\r";
            std::cout.flush();


            if(areSitesSimilarUsingHash(bin1_data.sites_ptr_vect.at(index1), bin2_data.sites_ptr_vect.at(index2), sample_distance)){
                
                if(!seen_bin1_ep[bin1_data.sites_addr_data.at(index1)]){
                    bin1_common++;
                    seen_bin1_ep[bin1_data.sites_addr_data.at(index1)]=true;
                }
                if(!seen_bin2_ep[bin2_data.sites_addr_data.at(index2)]){
                    bin2_common++;
                    seen_bin2_ep[bin2_data.sites_addr_data.at(index2)]=true;
                }

                bin2_tmp_common_sites_vect.push_back(bin2_data.sites_ptr_vect.at(index2));
                //common_site_pair_tmp = std::make_pair(bin1_data.sites_ptr_vect.at(index1), bin2_data.sites_ptr_vect.at(index2));
                //m_list_common_sites.push_back(common_site_pair_tmp);
            }
        }
        m_map_common_sites.insert(std::pair{bin1_data.sites_ptr_vect.at(index1), bin2_tmp_common_sites_vect});
    }

    std::cout << std::endl;
    std::chrono::duration<double> elapsed_seconds = std::chrono::system_clock::now() - time_tick;
    m_log->info("Distance elapsed time: {} sec\n", elapsed_seconds.count());

    m_elapsed_time = elapsed_seconds.count();
    m_common_sites_bin_list[bin1]=bin1_common;
    m_common_sites_bin_list[bin2]=bin2_common;
}
*/
/*
void ClassificationEngine::computeBinSimilarityLikeSigtool(bool sample_distance, const std::string& bin1, const std::string& bin2, const float threshold){
    

    // NOTE: This method updates the list of common sites AND the unique common sites numbers for each bin

    // SIGTOOL style: we only take the unique sites.

    std::pair<sptrSite, sptrSite> common_site_pair_tmp;    

    BINARYDATA bin1_data = getBinData(bin1);
    BINARYDATA bin2_data = getBinData(bin2);

    // process bin1 data beforehand, to keep only unique sites
    // updates m_unique_sites_1
    std::vector<sptrSite> unique_sites_bin1;
    bool is_unique = true;
    for(const sptrSite& current_site : bin1_data.sites_ptr_vect){
        for(const sptrSite& seen_site : unique_sites_bin1 ){
            if(areSitesSimilarArchitecture(seen_site, current_site)){
                is_unique = false;
                break;
            }
        }
        if(is_unique){unique_sites_bin1.push_back(current_site);}
        is_unique = true;
    }
    m_unique_sites_bin1 = unique_sites_bin1;

    // process bin2 data beforehand, to keep only unique sites
    // updates m_unique_sites_2

    std::vector<sptrSite> unique_sites_bin2;
    for(const sptrSite& current_site : bin2_data.sites_ptr_vect){
        for(const sptrSite& seen_site : unique_sites_bin2 ){
            if(areSitesSimilarArchitecture(seen_site, current_site)){
                is_unique = false;
                break;
            }
        }
        if(is_unique){unique_sites_bin2.push_back(current_site);}
        is_unique = true;
    }
    m_unique_sites_bin2 = unique_sites_bin2;

    // Now find common sites between those two
    // updates m_list_common_sites

    for(uint index1 = 0; index1 < unique_sites_bin1.size(); index1 ++){
        std::vector<sptrSite> bin2_tmp_common_sites_vect;
        for(uint index2 = 0; index2 < unique_sites_bin2.size(); index2 ++){
            if(areSuperSitesSimilar(unique_sites_bin1.at(index1), unique_sites_bin2.at(index2), sample_distance, threshold)){
                //common_site_pair_tmp = std::make_pair(unique_sites_bin1.at(index1), unique_sites_bin2.at(index2));
                //m_list_common_sites.push_back(common_site_pair_tmp);
                bin2_tmp_common_sites_vect.push_back(bin2_data.sites_ptr_vect.at(index2));
                break; // we can break because we know that in that case, there cannot be any more matches between any bin1 sites and this particular bin2 site (unicity)
            }
        }
        m_map_common_sites.insert(std::pair{bin1_data.sites_ptr_vect.at(index1), bin2_tmp_common_sites_vect});
    }
    m_common_sites_bin_list.insert(std::pair{bin1, m_map_common_sites.size()});
    m_common_sites_bin_list.insert(std::pair{bin2, m_map_common_sites.size()});

}
*/

/*
void ClassificationEngine::computeBinSimilarityFromDb(bool sample_distance, std::shared_ptr<DBManager> db, const std::string& bin1, const std::string& bin2){
    
    float nb_common_bin1 = 0, nb_common_bin2 = 0;

    if(m_bin_index_map.count(bin1) || m_bin_index_map.count(bin2)){
        SPDLOG_LOGGER_DEBUG(m_log, "Error: binaries already in the classification engine ! Why use a database ?\n");
        throw std::runtime_error("");
    }

    // let's load the db bin data in the classification engine
    BINARYDATA bin1_data, bin2_data;
    bin1_data.sites_addr_data = db->getBinSitesAddr(bin1);
    bin1_data.bin_name = bin1;
    bin2_data.sites_addr_data = db->getBinSitesAddr(bin2);
    bin2_data.bin_name = bin2;
    addBinarySiteData(bin1_data);
    addBinarySiteData(bin2_data);

     // update the unique site member values
    
    //std::pair<uint, uint> ucommon_sites = db->getUniqueCommonSitesPerBinary(bin1, bin2, sample_distance);
    //m_common_bin1 = ucommon_sites.first;
    //m_common_bin2 = ucommon_sites.second;
    

    m_list_common_sites_addr=db->getCommonSitesAddr(bin1, bin2, sample_distance);

}
*/
/*
void ClassificationEngine::computeBinSimilarityFromDbLikeSigtool(bool sample_distance, std::shared_ptr<DBManager> db, const std::string& bin1, const std::string& bin2){
    
    float nb_common_bin1 = 0, nb_common_bin2 = 0;

    if(m_bin_index_map.count(bin1) || m_bin_index_map.count(bin2)){
        SPDLOG_LOGGER_DEBUG(m_log, "Error: binaries already in the calssificatio engine ! Why use a database ?\n");
        throw std::runtime_error("");
    }

    // let's load the db bin data in the classification engine
    BINARYDATA bin1_data, bin2_data;
    bin1_data.sites_addr_data = db->getBinSitesAddr(bin1);
    bin1_data.bin_name = bin1;
    bin2_data.sites_addr_data = db->getBinSitesAddr(bin2);
    bin2_data.bin_name = bin2;
    addBinarySiteData(bin1_data);
    addBinarySiteData(bin2_data);


    m_list_common_sites_addr=db->getCommonSitesAddr(bin1, bin2, sample_distance);

}
*/

/*
// deprecated method
void ClassificationEngine::compareBinToDb(bool sample_distance, std::shared_ptr<DBManager> db, const std::string& bin1, const std::string& bin_db){
    // load db data in m_binary_data
    // bin1 data shoul dhave been already loaded there (processBinaryFileDistance)
    // just call getBinSimilarity then

    BINARYDATA bin_db_data;
    bin_db_data.sites_addr_data = db->getBinSitesAddr(bin_db);
    bin_db_data.bin_name = bin_db;
    addBinarySiteData(bin_db_data);

    computeBinSimilarity(sample_distance, bin1, bin_db);

}
*/

void ClassificationEngine::computeDistanceLikeSigtool(const std::string& bin1, const std::string& bin2){
    // trouver une maniere intelligente de calculer la distance entre deux binaires.
    float score = 0;

    //nb_common = findUniqueCommonSites(m_list_common_sites);
    uint nb_bin1_sites = m_unique_sites_bin1.size();
    uint nb_bin2_sites = m_unique_sites_bin2.size();

    score = (100 * (float)m_common_sites_bin_list.at(bin1) / nb_bin1_sites ) * (100 * (float) m_common_sites_bin_list.at(bin2) / nb_bin2_sites ) / 100 ; // same distance as in sigtool (TODO: be sure)

    m_distance = score;
}

void ClassificationEngine::computeDistance(const std::string& bin1, const std::string& bin2){
    // trouver une maniere intelligente de calculer la distance entre deux binaires.
    float score = 0;

    //nb_common = findUniqueCommonSites(m_list_common_sites);
    uint nb_bin1_sites = getBinNbSites(bin1);
    uint nb_bin2_sites = getBinNbSites(bin2);

    score = (100 * (float)m_common_sites_bin_list.at(bin1) / nb_bin1_sites ) * (100 * (float) m_common_sites_bin_list.at(bin2) / nb_bin2_sites ) / 100 ;

    m_distance = score;
}

// ------ Correlation mode

void ClassificationEngine::addBinaryFunctionsForCorrelation(std::shared_ptr<AnalysisEngine> analysis_engine, const Config* config){
    // We fill the m_function_vect member
    // and the m_binary_data, but only with the name of the bin
		// of the sim method involves "sum", we compute each sites's sum matrix
    FUNC_CORREL_DATA tmp_func_data;
    BINARYDATA new_bin;
    std::string tmp_fun_name;
    std::string tmp_fun_label;

    if(isBinInEngine(analysis_engine->getBinName())){
        SPDLOG_LOGGER_DEBUG(m_log, "Binary {} is already in classification engine, skipping it.\n", analysis_engine->getBinName());
        return;
    }

    new_bin.bin_name = utils::base_name(analysis_engine->getBinName());

    for(const std::shared_ptr<Function>& fun : analysis_engine->getFunToAnalyze()){
        if((int)fun->getSites().size() <= config->getConfigGlobal().min_nb_bb){ continue; }

				if(getSiteSimMethod() == "sum" || getSiteSimMethod() == "sum_general_reg"){
        	for(auto const& site_ptr : fun->getSites()){ site_ptr->computeSumMatrix(getSiteSimMethod()); }
    		}

				m_log->trace("\tCurrent func name {}\n", fun->getBinName());

        tmp_func_data.fun_ptr = fun;
        tmp_func_data.fun_correl_name = fun->getName() + "_" + analysis_engine->getBinSimpleName() + "_" + opt2str(analysis_engine->getBinOptLvl()) + "_" + analysis_engine->getBinary()->getBinVersion();
        tmp_func_data.fun_label = fun->getName();

        m_function_vect_correl.push_back(tmp_func_data);
        tmp_func_data={};
    }

    m_binary_data.push_back(new_bin);
    m_bin_index_map.insert ( std::pair<std::string,int>(
        utils::base_name(analysis_engine->getBinName()),
        m_binary_data.size()-1) );
}

/**
 * The inclusion score of f1 into f2 is:
 * # ssites from f1 that have at least a match in f2 /
 * # ssites in f1
 * @return float: inclusion score of f1 into f2
 * */
float ClassificationEngine::computeInclusionScore(sptrFunc f1, sptrFunc f2, const Config* config){
    float score=0.0;

    bool sample_distance = config->getConfigGlobal().sample_distance;

		if(sample_distance){
    	for(auto const& site_ptr1 : f1->getSites()){
        	for(auto const& site_ptr2 : f2->getSites()){
            	if(site_ptr1->isSimilarTo(site_ptr2, m_log, config)){
                	score++;
                	break;
            	} } }
    	if(config->getConfigGlobal().func_similarity_score == "inclusion"){
    		return score / f1->getSites().size(); }
    	else if (config->getConfigGlobal().func_similarity_score == "jaccard"){
    		return  2 * score / (f1->getSites().size() + f2->getSites().size()); }
    	else{ throw std::runtime_error("Error: func_similarity_score (option -f) not recognized ('jaccard' or 'inclusion' supported); Aborting\n");}
    	}
    else{
    	for(auto const& site_ptr1 : f1->getDataSites()){
        	for(auto const& site_ptr2 : f2->getDataSites()){
            	if(site_ptr1->isSimilarTo(site_ptr2, m_log, config)){
                	score++;
                	break;
                	// we are not worried about matching again later the same site
                	// because all sites are different in each function
            	}
        	}
    	}
    	if(config->getConfigGlobal().func_similarity_score == "inclusion"){
    		return score / f1->getDataSites().size(); // inclusion score
    	}
    	else if (config->getConfigGlobal().func_similarity_score == "jaccard"){
    		return  2 * score / (f1->getDataSites().size() + f2->getDataSites().size()); // jaccard index
    	}
    	else{ throw std::runtime_error("Error: func_similarity_score (option -f) not recognized ('jaccard' or 'inclusion' supported); Aborting\n");}
}
}

    /**
    Computes the similiarity/inclusion matrix of all the functions
    in the current classification engine.
    Stores everything in the m_function_vect_correl member.
    COMPLEXITY: this is O(n^2) complex in the nbr of functions
    */
void ClassificationEngine::computeFunctionsCorrelationScores(const Config* config){

  float tmp_score;
  uint index1=0, total_nb_it=0;
	auto time_tick = std::chrono::system_clock::now();
	// TODO: IMPORTANT: try to see if I can choose how many threads to use (m_config_global.multithreading)
  if(!config->getConfigGlobal().pairs_test_file.empty()){
    // pair comparison mode
    // iterate through all the paris and compute their similarity
    // ID of a function: its bin and its name
    total_nb_it = config->getConfigGlobal().test_funcs_pairs.size();
    for(auto& fun1_data : m_function_vect_correl){
    	index1++;
    	auto fun_elapsed_seconds = std::chrono::system_clock::now() - time_tick;
    	m_log->info("Fun pair {} / {} ({:.2f}p/s)\r", index1, total_nb_it,  float(index1)/((float)fun_elapsed_seconds.count())* 10e9);
    	for(auto const& fun_pair_data : config->getConfigGlobal().test_funcs_pairs){
    		if(fun_pair_data.fname1 == fun1_data.fun_ptr->getName() && fun_pair_data.bin1 == fun1_data.fun_ptr->getBinName()){
					std::shared_ptr<FUNC_CORREL_DATA> fun_data2;
					fun_data2 = getFuncCorrelDataOf(fun_pair_data.bin2, fun_pair_data.fname2);
					if(fun_data2 == nullptr){continue;}
      		tmp_score = computeInclusionScore(fun1_data.fun_ptr, fun_data2->fun_ptr, config);
      		fun1_data.score_map.insert(std::pair<sptrFunc, float>{fun_data2->fun_ptr, tmp_score});
      	}
    	}
    }
  }
  else{
  	// all functions against all functions
		total_nb_it = getCorrelFunctions().size();
		#pragma omp parallel for
  	for(auto& fun_data1 : m_function_vect_correl){
    	index1++;
    	auto fun_elapsed_seconds = std::chrono::system_clock::now() - time_tick;
    	m_log->info("Fun {} / {} ({:.2f}f/s)\r", index1, total_nb_it,  float(index1)/((float)fun_elapsed_seconds.count())* 10e9);
    	for(auto const& fun_data2 : getCorrelFunctions()){
    		if(fun_data2.fun_ptr == fun_data1.fun_ptr){continue;}
      	tmp_score = computeInclusionScore(fun_data1.fun_ptr, fun_data2.fun_ptr, config);
      	fun_data1.score_map.insert(std::pair<sptrFunc, float>{fun_data2.fun_ptr, tmp_score});
    	}
  	}
  }

	std::chrono::duration<double> elapsed_seconds = std::chrono::system_clock::now() - time_tick;
	m_log->info("Correlation elapsed time: {} sec\n", elapsed_seconds.count());
	m_elapsed_time = elapsed_seconds.count();
}

static bool compareFuncCorrelData(FUNC_CORREL_DATA fun1, FUNC_CORREL_DATA fun2){

    if( fun1.fun_label.compare(fun2.fun_label) < 0 ){
        return true;
    }
    else if(fun1.fun_label.compare(fun2.fun_label) > 0){
        return false;
    }
    else{
        return(fun1.fun_correl_name.compare(fun2.fun_correl_name) < 0 ); 
    }
}

void ClassificationEngine::sortCorrelFunctions(){
    std::sort(m_function_vect_correl.begin(), m_function_vect_correl.end(), compareFuncCorrelData);
}

/*
std::vector<FUNC_CORREL_DATA> ClassificationEngine::getSortedCorrelFunctions() const{
    std::vector<FUNC_CORREL_DATA> sorted_func_vect_correl = m_function_vect_correl;
    std::sort(sorted_func_vect_correl.begin(), sorted_func_vect_correl.end(), compareFuncCorrelData);
    return sorted_func_vect_correl;
}
*/

/**
 * This function attemps to check the True Positives score of our method
 * We derive a score from how successfully TANGUE managed to correlate
 * The same function name (label) accross the different binaries
 * 
 * */
float ClassificationEngine::computeLabelScore(const std::string& label){
    float score=0;
    std::set<FUNC_CORREL_DATA> common_fun_label;
    for(auto const& fun : m_function_vect_correl){
        if(fun.fun_label == label){common_fun_label.insert(fun);}
    }
    for(auto const& fun : common_fun_label){
        for(auto const& fun2 : common_fun_label){
            score += fun.score_map.at(fun2.fun_ptr);
        }
    }
    score = score/(common_fun_label.size()*common_fun_label.size());
    return score;
}


// ------------------------

// Stats

uint ClassificationEngine::getNbFunWithKSites(const std::string& name, uint k){
    uint res =0;
    for(const FUNCTIONDATA& fun_data : m_map_fun_sites.at(name)){
        if(fun_data.sites_list.size() > k){
            res++;
        }
    }
    return res;
}

std::vector<std::pair<std::string, uint>> ClassificationEngine::getFunBestMatchesInBin2(const FUNCTIONDATA& fun_data){

    std::vector<std::pair<std::string, uint>> output_vect;
    std::map<std::string, std::map<sptrSite, bool>> seen_bin1_sites; // fun2_str has already seen the bin1 site sptrSite ?
    std::map<std::string, uint> matching_fun_map;
    std::string tmp_fun2;
    for(sptrSite fun_site : fun_data.sites_list){
        if(m_map_common_sites.count(fun_site)){
            // site has matched
            for(sptrSite matching_site : m_map_common_sites.at(fun_site)){
                tmp_fun2 = matching_site->getFunctionName();
                if(!matching_fun_map.count(tmp_fun2)){ 
                    matching_fun_map.insert(std::pair{tmp_fun2,1});
                    std::map<sptrSite, bool> m = {{fun_site, true}};
                    seen_bin1_sites.insert(std::pair{tmp_fun2, m});    
                }
                else{
                    if(!seen_bin1_sites.at(tmp_fun2)[fun_site]){
                        matching_fun_map.at(tmp_fun2)++;
                        seen_bin1_sites.at(tmp_fun2)[fun_site] = true;
                    }
                }
            }
        }
    }
    for(auto const& [fun_str, nb_match] : matching_fun_map){
        output_vect.push_back(std::pair{fun_str, nb_match});
    }
    return output_vect;
}

std::vector<std::pair<std::string, uint>> ClassificationEngine::getFunBestMatchesInBin1(const FUNCTIONDATA& fun_data){

    std::vector<std::pair<std::string, uint>> output_vect;
    std::map<std::string, std::map<sptrSite, bool>> seen_bin2_sites; // fun2_str has already seen the bin1 site sptrSite ?
    std::map<std::string, uint> matching_fun_map;
    std::string tmp_fun1;
    for(sptrSite fun_site : fun_data.sites_list){

        for(sptrSite matching_site : getSitesMatchingSiteFromBin2(fun_site)){
            tmp_fun1 = matching_site->getFunctionName();
            if(!matching_fun_map.count(tmp_fun1)){ 
                matching_fun_map.insert(std::pair{tmp_fun1,1});
                std::map<sptrSite, bool> m = {{fun_site, true}};
                seen_bin2_sites.insert(std::pair{tmp_fun1, m});    
            }
            else{
                if(!seen_bin2_sites.at(tmp_fun1)[fun_site]){
                    matching_fun_map.at(tmp_fun1)++;
                    seen_bin2_sites.at(tmp_fun1)[fun_site] = true;
                }
            }
        }
    }
    for(auto const& [fun_str, nb_match] : matching_fun_map){
        output_vect.push_back(std::pair{fun_str, nb_match});
    }
    return output_vect;
}

std::string ClassificationEngine::printFunsSummary(){
	std::stringstream f;
	for(auto const& fun_data : getCorrelFunctions()){
		f << fun_data.fun_ptr->getName() << " " << addr2s(fun_data.fun_ptr->getEntryPointAddr()) << "\n";
	}
	return f.str();
}

std::string ClassificationEngine::printTopMatchedFun(const std::string& bin1, const std::string& bin2){
    std::stringstream f;

    uint nb_matching_sites = 0;
    std::vector<std::pair<std::string, uint>> tmp_matched;
    //std::map<uint, std::string> map_index_to_fun;

// bin 1
    f << "\n-----> " << bin1 << " :\n\n";

    for(const FUNCTIONDATA& fun_data_1 : m_map_fun_sites.at(bin1)){
        nb_matching_sites = 0;

        // get matching score (nb sites matching)
        for(sptrSite site : fun_data_1.sites_list){
            if(m_map_common_sites.count(site)){
                nb_matching_sites ++;
            }
        }
        if(nb_matching_sites!= 0){
            tmp_matched.push_back(std::pair{fun_data_1.fun_name, nb_matching_sites});
        }
    }
    

    // sort with highest nbr match first
    std::sort(tmp_matched.begin(), tmp_matched.end(), [](auto &left, auto &right) {
    return left.second > right.second; });

    for(auto const& [fun_str, nb_match]: tmp_matched){
        uint fun_size = getSitesOfFun(bin1, fun_str).size();
        std::vector<std::pair<std::string, uint>> fun_best_matches_bin2 = getFunBestMatchesInBin2(getFunData(bin1, fun_str));

        std::sort(fun_best_matches_bin2.begin(), fun_best_matches_bin2.end(), [](auto &left, auto &right) {
        return left.second > right.second; });

        for(auto const& [fun_str2, nb_match2]: fun_best_matches_bin2){
            f << "\t\t" << fun_str << " : " << nb_match2 << " sites match (" << (float(nb_match2) / float(fun_size)) * 100 << "%)";
            f <<" with " << bin2 << "\'s " << fun_str2;
            f << "; \n";
        }
    }

// bin2
    tmp_matched.clear();

    f << "\n-----> " << bin2 << " :\n\n";
    for(const FUNCTIONDATA& fun_data_2 : m_map_fun_sites.at(bin2)){
        nb_matching_sites = 0;

        // get matching score (nb sites matching)
        for(sptrSite site : fun_data_2.sites_list){

            nb_matching_sites += (uint) !getSitesMatchingSiteFromBin2(site).empty();

        }
        if(nb_matching_sites!= 0){
            tmp_matched.push_back(std::pair{fun_data_2.fun_name, nb_matching_sites});
        }    
    }

    // sort with highest nbr match first
    std::sort(tmp_matched.begin(), tmp_matched.end(), [](auto &left, auto &right) {
    return left.second > right.second; });

    for(auto const& [fun_str, nb_match]: tmp_matched){
        uint fun_size = getSitesOfFun(bin2, fun_str).size();
        std::vector<std::pair<std::string, uint>> fun_best_matches_bin1 = getFunBestMatchesInBin1(getFunData(bin2, fun_str));

        std::sort(fun_best_matches_bin1.begin(), fun_best_matches_bin1.end(), [](auto &left, auto &right) {
        return left.second > right.second; });

        for(auto const& [fun_str1, nb_match1]: fun_best_matches_bin1){
            f << "\t\t" << fun_str << " : " << nb_match1 << " sites match (" << (float(nb_match1) / float(fun_size)) * 100 << "%)";
            f <<" with " << bin1 << "\'s " << fun_str1;
            f << "; \n";
        }
    }

    return f.str();
}

FUNC_CORREL_DATA ClassificationEngine::getFuncCorrelDataOf(sptrFunc fun_ptr) const{
    for(auto& fun_data : getCorrelFunctions()){
        if(fun_data.fun_ptr == fun_ptr){
            return fun_data;
        }
    }
    throw std::runtime_error("Fun ptr " + fun_ptr->getName() + "not in the engine ! Aborting\n");
}

std::shared_ptr<FUNC_CORREL_DATA> ClassificationEngine::getFuncCorrelDataOf(std::string fbin, std::string fname) const{
    for(auto const& fun_data : getCorrelFunctions()){
        if(fun_data.fun_ptr->getName() == fname && fun_data.fun_ptr->getBinName() == fbin){
            return std::make_shared<FUNC_CORREL_DATA>(fun_data);
        }
    }
    m_log->debug("[Warning] Fun ptr {} of bin {} not in the engine ! Aborting\n", fname, fbin);
    return nullptr;
}

/**
 * Returns the rank of the prediction
 * (gets the lowest classification rank of a function of the same name)
 * NOTE: if several functions (nb_fun) have the same rank,
 * best_rank <= nb_fun <= worst_rank
 * @return: <best_rank, worst_rank, best_score>
 **/
std::tuple<int,int,float> ClassificationEngine::getRank(FUNC_CORREL_DATA fun_correl_data) const{
	int rank_best=0;
	int rank_worst=0;

	std::vector<std::pair<std::string, float>> copied_scores;

	for(auto const& [fun2_ptr,score] : fun_correl_data.score_map){
		// get the score from a DIFFERENT BIN
		if(fun_correl_data.fun_ptr->getBinName() != fun2_ptr->getBinName()){
			copied_scores.push_back({fun2_ptr->getName(),score});
		}
	}
	// sort them pairs, highest first
	std::sort(copied_scores.begin(), copied_scores.end(), [](auto &left, auto &right) {return left.second > right.second;});

	float rank_score=0;
	for(auto const& [name,score]: copied_scores){
		if(name == fun_correl_data.fun_label){
			rank_score = score;
			break;
		}
	}
	if(rank_score==0){return {-1,-1,0};}
	for(auto const& [name,score]: copied_scores){
		if(score >= rank_score){rank_worst++;}
	}
	for(auto const& [name,score]: copied_scores){
		rank_best++;
		if(score <= rank_score){break;}
	}
	return {rank_best,rank_worst,rank_score};
}


/**
 * returns a string constaining the best matches
 * except for the ones in the bin
 */
std::string ClassificationEngine::getBestMatchStr(FUNC_CORREL_DATA fun_correl_data) const{
    std::stringstream f;
    f << " ";
    std::unordered_set<std::string> best_matches_tmp;
		std::vector<std::pair<std::string, float>> copied_scores;

		for(auto const& [fun2_ptr,score] : fun_correl_data.score_map){
			// get the score from a DIFFERENT BIN
			if(fun_correl_data.fun_ptr->getBinName() != fun2_ptr->getBinName()){
				copied_scores.push_back({getFuncCorrelDataOf(fun2_ptr).fun_correl_name, score});
				//copied_scores.push_back({fun2_ptr->getBinName(), score});
			}
		}
		// sort them pairs, highest first
		std::sort(copied_scores.begin(), copied_scores.end(), [](auto &left, auto &right) {return left.second > right.second;});

		float current_max=copied_scores.at(0).second;
		std::string current_max_str = std::to_string(current_max);
    f << current_max_str.substr(0, current_max_str.find(".")+3) << "/";
		for(auto const& [name,score] : copied_scores){
			if(score < current_max){
				// write the current value in the current line
				// go to next cell in the line
				current_max = score;
				f << ";";
				current_max_str = std::to_string(current_max);
    		f << current_max_str.substr(0, current_max_str.find(".")+3) << "/";
			}
			f << name << "/";
		}
    return f.str();
}


// Printing

std::string ClassificationEngine::createCommonDotWithoutDigraph(bool is_reduced){
  std::stringstream f;

  // add each Site, then connect the roots of similar sites with a dotted line

  // add each site
  uint pair_index = 1;

    for(auto const& site_common_key : m_map_common_sites){
      if(pair_index > 3){break;}

        f << site_common_key.first->createDotWithoutDigraph(pair_index, is_reduced) << " ";
        // TODO: this is not normal. Check how to remedy (hint: use site ptr instead of BINARY_DATA scheiss)
        if(site_common_key.first->getSiteDataflow().empty()){
            site_common_key.first->extractSiteDataflowGraph();
        } 
        f << site_common_key.first->createDataflowDotWithoutDigraph(pair_index) << " ";

      for(const sptrSite& bin2_site : site_common_key.second){

        f << bin2_site->createDotWithoutDigraph(pair_index, is_reduced) << " ";
        f << '"' << waddr2s({pair_index, site_common_key.first->getEntryPointAddr()}) << "\" -> \"" << waddr2s({pair_index, bin2_site->getEntryPointAddr()}) << "\"[style=dotted];" << std::endl;

        if(bin2_site->getSiteDataflow().empty()){
            bin2_site->extractSiteDataflowGraph();
        }
        f << bin2_site->createDataflowDotWithoutDigraph(pair_index) << " ";

      }
    pair_index++; 
  }
  return f.str();
}

std::string ClassificationEngine::createCommonSitesDot(bool is_reduced){
  return "Digraph G {\n" + createCommonDotWithoutDigraph( is_reduced) + "}";
}

std::string ClassificationEngine::printCommonSitesEp(){
    std::stringstream f;
    for(auto const& common_map_i : m_map_common_sites){
        for(const sptrSite& bin2_common_site : common_map_i.second){
            f << addr2s(common_map_i.first->getEntryPointAddr()) << " and " << addr2s(bin2_common_site->getEntryPointAddr()) << "\n";
        }
    }
    return f.str();
}

static inline bool sortSitesFun2(sptrSite site1, sptrSite site2){
  return (site1->getEntryPointAddr() > site2->getEntryPointAddr());
}

std::string ClassificationEngine::printBinDetailedInfos(const std::string& bin_name){
  std::stringstream f;
    f << "\n-----> Binary " << bin_name << "\n\n";

  for(const FUNCTIONDATA& fun_data : m_map_fun_sites.at(bin_name)){

      if(fun_data.sites_list.empty()){
          continue;
      }
  // sort the sites by starting addr
    std::vector<sptrSite> copy_fun_vect = fun_data.sites_list;
    
    std::sort(copy_fun_vect.begin(), copy_fun_vect.end(), sortSitesFun2);

    f << "\t";
    f << fun_data.fun_name << " :{ ";
    for(const sptrSite& site_ptr : fun_data.sites_list){
      f << addr2s(site_ptr->getEntryPointAddr()) << "; ";
    }
    f << "\n\t\t} #" << std::to_string(fun_data.sites_list.size()) << ";\n";
  }
  return f.str();
}

std::string ClassificationEngine::printCommonDetailedInfo(const std::string& bin1, const std::string& bin2){
    std::stringstream f;

    f << "\n\n";

    f << "\tCommon ssites between " << bin1 << " and " << bin2 << " :\n\n";

    std::map<std::string, bool> bin1_fun_name_map; // init this :
    std::vector<sptrSite> bin1_matched_copy;
    std::string fun_name;

    for(auto const& [site1, vect] : m_map_common_sites){
        bin1_matched_copy.push_back(site1);
    }

    for(auto const & [site_ptr,vect] : m_map_common_sites){
        fun_name = site_ptr->getFunctionName();
        if(!bin1_fun_name_map[fun_name]){
            // not already seen
            bin1_fun_name_map[fun_name] =true;

            // gather all other sites with this fun name

            std::vector<sptrSite> tmp_matched_of_specific_fun;

            for(auto iterator = bin1_matched_copy.rbegin(); iterator != bin1_matched_copy.rend(); ++iterator ){ // reverse loop throught copy vect
                if(!(iterator->get()->getFunctionName().compare(fun_name))){  // if match between strings

                    tmp_matched_of_specific_fun.push_back(*iterator); // add data to tmp vect
                    bin1_matched_copy.erase(--(iterator.base())); // remove elt from copy vect

                }
            }
            uint nb_matches=0;
            // print stuff for every one of those sites
            f << "\t" << fun_name << "{\n";

            for(const sptrSite& matched_site_ptr_bin1 : tmp_matched_of_specific_fun){
                f << "\t\t" << addr2s(matched_site_ptr_bin1->getEntryPointAddr()) << ": ";
                // print all common ssites info for this given ssite
                std::string sep = "";
                for(const sptrSite& matched_site_ptr_bin2 : m_map_common_sites.at(matched_site_ptr_bin1)){
                    f << sep << addr2s(matched_site_ptr_bin2->getEntryPointAddr()) << "(" << matched_site_ptr_bin2->getFunctionName() << ")";
                    sep = ", ";
                    nb_matches++;
                }
                f << ";\n";
            }
            f << "\t\t} #"<< nb_matches << "\n";

        }
    }

    f << " \t};\n";


  return f.str();
}

std::string ClassificationEngine::printNbrFunctionWithMoreThanKSites(const std::string& bin_name){
    uint nb_fun = 0;

    std::stringstream f;
    f << "\n-----> " << bin_name << "\n\n";

    std::array<uint, 5> k_values = {0, 5, 10, 20, 50};

    for(uint k : k_values){
        f << "\t\t";
        nb_fun = getNbFunWithKSites(bin_name, k);
        f << "k > " << k << " : " << nb_fun << " functions;\n";
    }
    f << "\n";
    return f.str();   
}


} // namespace boa
