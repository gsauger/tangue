#ifndef OPAQUE_PREDICATE_H
#define OPAQUE_PREDICATE_H

#include "binary/binary.hpp"
#include "boa_disassembling/exec_formula.hpp"
#include "boa_disassembling/machine_state.hpp"
#include "boa_disassembling/symbexec/jcc_symb_exec.hpp"
#include "common/cfg.hpp"
#include "utils/utils.hpp"
#include "x86_to_smtlib/x86_to_smtlib.hpp"

namespace boa
{

class OpaquePredicateAnalysis
{

public:
  OpaquePredicateAnalysis(CFG* cfg, SolverManager& solver_manager, Binary* binary, x86ToSmtlib* x86_to_smtlib);

  OpaquePredicateAnalysis(OpaquePredicateAnalysis const&) = delete;
  OpaquePredicateAnalysis& operator=(OpaquePredicateAnalysis const&) = delete;

  OpaquePredicateAnalysis(OpaquePredicateAnalysis&&) = delete;
  OpaquePredicateAnalysis& operator=(OpaquePredicateAnalysis&&) = delete;

  ~OpaquePredicateAnalysis() = default;

  void startAnalysis(unsigned int nb_thread);

private:
  std::shared_ptr<spdlog::logger> m_log;
  CFG* m_cfg;
  SolverManager& m_solver_manager;
  Binary* m_binary;
  x86ToSmtlib* m_x86_to_smtlib;

  void startAnalysisOneThread(const std::unordered_set<const BasicBlock*>& jcc_bbs, unsigned int index_to_run,
                              unsigned int nb_thread);
};

} // namespace boa

#endif
