#ifndef ANALYSIS_H
#define ANALYSIS_H

#include <stack>

#include "common/cfg.hpp"
#include "config.hpp"
#include "utils/utils.hpp"
#include "function.hpp"
#include "site.hpp"

#include <sstream>
#include <iomanip>

namespace boa {

class AnalysisEngine {
  /* Analysis engine. Performs different types of
  analysis on a cfg */

public:
  // Constructor and destructor
  AnalysisEngine(sptrBinary bin_ptr, const Config* config);
  AnalysisEngine(std::shared_ptr<CFG> cfg, uint sites_size, sptrBinary bin_ptr, const Config* config);
  ~AnalysisEngine();

  // Setters and getters

  void setCFG(std::shared_ptr<CFG> cfg);
  //void setName(std::string name);
  std::string getBinName() const;
  std::string getBinSimpleName() const;
  std::shared_ptr<CFG> getCFG() const;
  const std::unordered_set<std::shared_ptr<Function>>& getFunctions() const;
  void setFunctions( std::unordered_set<sptrFunc> new_funcs){m_functions = new_funcs;}
  std::shared_ptr<Function> getFunction(addr_t addr) const;
  std::shared_ptr<Function> getFunction(const std::string& name) const;
  //std::vector<sptrSite> getSites() const;
  unsigned int getNumberOfSites() const;
  void setSitesSize(uint size);
  sptrSite getSite(addr_t);
  sptrSite getSiteCheckingAlsoEnd(addr_t addr);
  sptrBinary getBinary() const{ return m_binary;};

  std::set<std::shared_ptr<Function>> getThunkFuns() const;
  std::set<std::shared_ptr<Function>> getCoreFuns() const;

  uint getNonEmptyFuncNbr() const;
  OptLevel getBinOptLvl() const {return getBinary()->getBinOptLvl();};

  // Analysis
  bool isThereEdgeToBB(std::shared_ptr<Function> function, sptrBasicBlock BB);
  void computeEntryPoints(std::shared_ptr<CFG> cfg, std::shared_ptr<Function> function);
  void colorGraphFromBB(std::shared_ptr<CFG> cfg, std::shared_ptr<Function> current_function, sptrBasicBlock current_BB, color_t current_color);
  void setFunctionBoundaries();

    // Sites
  void extractBinarySites();
  /*
  float computeSimilarityScore(std::shared_ptr<Function> fun1,std::shared_ptr<Function> fun2);
  float computeSimilarityScore(std::unordered_set<sptrSite> sites1, std::unordered_set<sptrSite> sites2);
  */
  void encodeSites();
  void hashSites();

    // Dataflow
  void extractSitesDataflowGraphs(); // store the data in the function's sites
	std::unordered_set<sptrFunc> getFuncsFromPairs();
  void hashDataflow();

  // Functions
  std::shared_ptr<Function> insertFunction(addr_t bb_ep, color_t color, std::string name, std::string binary_name);
  std::shared_ptr<Function> insertFunction(addr_t bb_ep, color_t color, std::string binary_name);


  size_t getNumberofFunctions() const;

  // Printing
	void printOutputLog(sptrLog log) const;
  void printSitesEncoding();
  std::string printSitesSigtoolCompare() const;
  std::string getBinDetailedInfos() const;
  std::string printSitesHash() const;
  std::string printFunctionsSummary() const;
  std::string printThunkCoreNumbers() const;
  uint getNbFunWithKSites(uint k) const;
  std::string printNbrFunctionWithMoreThanKSites() const;
  std::string createFunMapDot();
  std::string createFunMapDotWithoutDigraph();
	std::vector<std::shared_ptr<Function>> getSortedFun() const;

	const std::unordered_set<sptrFunc>& getFunToAnalyze() const {return m_functions;}
  // Misc

  const Config* getConfig() const{return m_config;}

	void removeFunction(sptrFunc fun);

	unsigned int getSitesSize() const{ return m_sites_size;}


private:
	const Config* m_config;
	const sptrBinary m_binary; // all the info of the binary are here
  std::shared_ptr<spdlog::logger> m_log;
  std::shared_ptr<CFG> m_cfg;
  std::unordered_set<sptrFunc> m_functions;
	std::unordered_set<sptrFunc>* m_fun_to_analyze;
  unsigned int m_sites_size;



}; // analysis
using sptrAnalysis = std::shared_ptr<AnalysisEngine>;
} // namespace boa

#endif
