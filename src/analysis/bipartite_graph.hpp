#ifndef BIPARTITE_GRAPH_H
#define BIPARTITE_GRAPH_H

#include "utils/boa_types.hpp"
#include "common/instr.hpp"

namespace boa{


class BipartiteGraph{

    // One bipartitegraph per Basic block ?

public:

    // Constructor

    //BipartiteGraph(addr_t BB_ep_addr, sptrBasicBlock BB, InstrType type);
    BipartiteGraph(addr_t ep_addr, const std::vector<sptrInstr>& instr_vect);
    BipartiteGraph(addr_t ep_addr);

    //void initToIdentity();
    void updateOneRegGenVector() const;

    // Setters and getters
    const biGraphVect& getDataflowVector() const {return m_bipartite_graph_vector;}
    biGraphVect getOneGenRegNodeVect() const {return m_one_gen_reg_node_vector;}
    sptrBvertex getBvertexOfReg(x86_reg_class reg) const;
    void setExitAddr(addr_t addr){m_exit_addr=addr;}

    addr_t getEntryPointAddr() const {return m_ep_addr;}
    addr_t getLastInstrAddr() const{
        if(m_instr_vect.size()==0){return 0;}
        return m_instr_vect.back()->getAddr();}
    addr_t getFirstInstrAddr() const{
        if(m_instr_vect.size()==0){return 0;}
        return m_instr_vect.front()->getAddr();}

    InstrType getLastInstrType() const{
        if(m_instr_vect.size()==0){return InstrType::UNKNOWN;}
        return m_instr_vect.back()->getType();}

    // std::unordered_map<std::string,std::string> getEdges() const;

    void addTaintingRegsTo(x86_reg_class reached_reg, x86_reg_class tainting_reg);

    std::shared_ptr<BipartiteGraph> concatenateAndCreateNewGraphWith(std::shared_ptr<BipartiteGraph> second_graph, std::shared_ptr<spdlog::logger> log)const;

    // Analysis

		// strictly the same
		bool isEqual(const std::shared_ptr<BipartiteGraph> b2,std::shared_ptr<spdlog::logger> log);
    // Equivalent dataflow (forgetting some nodes labels)
    bool isEquivalentTo(const std::shared_ptr<BipartiteGraph>& b2, std::shared_ptr<spdlog::logger> log, const std::string& bgraph_comp_method) const;

    std::set<x86_reg_class> getRegsTaintedBy(x86_reg_class input_reg) const;

    // Printing

    std::string toString() const;
    std::string instrsToString() const;
    std::string toGraphString(const std::string& dataflow_id);

protected:

    //friend bool operator==(const std::shared_ptr<BipartiteGraph> b1, const std::shared_ptr<BipartiteGraph> b2);

private:

    // members
    std::vector<sptrBvertex> m_bipartite_graph_vector;
		mutable std::vector<sptrBvertex> m_one_gen_reg_node_vector;
    //InstrType m_last_instr_type;
    addr_t m_ep_addr; // start of the path
    addr_t m_exit_addr; // end of the path
    std::vector<sptrInstr> m_instr_vect;
    //sptrBasicBlock m_bb;

    // init method
    void createBipartiteGraphFromInstrs();


};

using sptrBipartite = std::shared_ptr<BipartiteGraph>;

} // namespace boa

#endif
