#ifndef SITE_H
#define SITE_H

#include "common/cfg.hpp"
#include "utils/boa_types.hpp"
#include "config.hpp"

#include <iostream>
#include <limits.h>
#include <queue>
#include <string.h>

namespace boa{



class Site : public CFG {

public:

    Site(const std::vector<addr_t>& entry_points, unsigned int wave, std::string fun_name);
    ~Site();

    // Setters and getters
    std::size_t getHash() const;
    void setHash(std::size_t hash);
    std::vector<uint32_t> getEncoding() const;
    //void setEncodingBeforeHashing(uint16_t* encoding);
    std::string getDataflowEncoding() const;
    std::string getBBDataflowEncoding(sptrBasicBlock bb)const ;
    void setEncoding(std::vector<uint32_t> vect);
    std::vector<sptrBasicBlock> getBFSSearchBBSort() const;

    bool isSimilarTo(const std::shared_ptr<Site> & site, std::shared_ptr<spdlog::logger> log, const Config* config);

    void setFunctionName(std::string name);
    std::string getFunctionName();

    addr_t getEntryPointAddrSigtoolStyle() const;

		std::map<x86_reg_class, std::map<x86_reg_class, uint>> getSumMatrix() const {return m_sum_matrix;}
    void computeSumMatrix(const std::string& site_sim_method);
    // Analysis
    void encodeGraph(std::shared_ptr<spdlog::logger> log);

    void hashGraph(std::shared_ptr<spdlog::logger> log);
    uint getEncodingSize() const;

    uint16_t* getEncodingBeforeHashing() const;
    uint8_t* getEncodingHash() const;

    void hashDataflow(std::shared_ptr<spdlog::logger> log);
    uint8_t* getDataflowHash() const;

    sptrBipartite getBBDataflow(sptrBasicBlock BB) const;
    std::vector<sptrBipartite> getSiteDataflow() const;
    bool isDataflowEqual(std::shared_ptr<Site> site2, std::shared_ptr<spdlog::logger> log) const;
    bool isDataflowEqualV2( std::shared_ptr<Site> site2) const;
    bool beadth_first_search(int *rGraph[], int s, int t, int parent[]);
    float computeDataflowSimilarity (const std::shared_ptr<Site> site2, std::shared_ptr<spdlog::logger> log, const Config* config) const;


    void extractSiteDataflowGraph();
    void extractSiteBBDataflowGraphs();


    // Printing / outputing

    void PrintDifferentPaths(std::shared_ptr<spdlog::logger> log);

    // std::string printSiteDataflow();


private:

    std::vector<uint32_t> m_encoding;
    //std::vector<biGraphVect> m_dataflow_graph;
    std::vector<sptrBipartite> m_dataflow_graphs;
    std::string m_function_name;

    // Used in dataflow computing, to match the encoding order

		std::map<x86_reg_class, std::map<x86_reg_class, uint>> m_sum_matrix;
		// each elt a line: key is the reg and value the source regs

    std::vector<sptrBasicBlock> m_BFS_search_BB_sort;

    // Encoding
    uint16_t* m_encoding_before_hashing;
    uint m_encoding_size; // size before encoding, in #uint16_t
    uint8_t* m_encoding_hash;

    // dataflow encoding
    uint m_dataflow_size;
    uint8_t* m_dataflow_hash;

};

using sptrSite = std::shared_ptr<Site>;

} // namespace boa

#endif
