#include "function.hpp"

#include <sstream>


namespace boa{

// MARK:- Constructor
Function::Function(const std::vector<addr_t>& entry_points,unsigned int sites_size, unsigned int wave, color_t color, OptLevel opt, std::string binary_name) : CFG(entry_points, wave)
{
    m_sites_size = sites_size;
    m_color = color;
    m_name = "fun_" + std::to_string(color);
    m_ep = entry_points.at(0);
    m_binary_name = binary_name;
    m_opt_lvl = opt;

    m_sites.reserve((getNumberBasicBlocks() + 1));
}

Function::Function(const std::vector<addr_t>& entry_points,unsigned int sites_size, unsigned int wave, color_t color, OptLevel opt, std::string name, std::string binary_name) : CFG(entry_points, wave)
{
    m_sites_size = sites_size;
    m_color = color;
    m_name = name;
    m_ep = entry_points.at(0);
    m_binary_name = binary_name;
    m_opt_lvl = opt;

    m_sites.reserve((getNumberBasicBlocks() + 1));
}

// Setters and getters

void Function::setName(std::string new_name) const{
  m_name = new_name;
}

addr_t Function::getEntryPointAddr() const{
  return m_ep;
}

std::string Function::getName() const{
  if(!m_name.empty()){
    return m_name;
  }
  else{
    std::string color_s = "fun_" + std::to_string(m_color);
    return color_s;
  }
}

const color_t Function::getColor() const{
  return m_color;
}

void Function::setColor(color_t color){
  m_color = color;
}


const std::unordered_set<std::shared_ptr<Site>>& Function::getSites() const{
    return m_sites;
}

bool sortSitesFun(sptrSite site1, sptrSite site2){
  return (site1->getEntryPointAddr() > site2->getEntryPointAddr());
}

std::vector<sptrSite> Function::getOrderedSites(){
  std::vector<sptrSite> ordered_sites;
  ordered_sites.insert(ordered_sites.end(), m_sites.begin(), m_sites.end());
  std::sort(ordered_sites.begin(), ordered_sites.end(), sortSitesFun);
  return ordered_sites;
}


void Function::setSitesSize(unsigned int size){
  m_sites_size = size;
}

uint Function::getSitesSize() const{
  //float val = getNumberBasicBlocks() / 20;
  return m_sites_size;
 // return std::max(m_sites_size, (uint)std::ceil(val));
  return getNumberBasicBlocks();
}


// Sites


void Function::extractFunctionSites(std::shared_ptr<spdlog::logger> log){
    /**
    Extracts all sites from a function and stores them in this function's m_sites member.
    */
    for(auto const& [BB_addr, BB] : getSortedBasicBlocks()){
        std::shared_ptr<Site> site = this->createSite(BB, log);
        if(site != NULL) { m_sites.insert( site );}
    }
}

    /**
    Builds a site of size m_size_sites starting from ep_addr and BFS'ing throught the function.
    @param ep_BB Entry block of the site.
    @return A shared pointer to the created site.
    */
std::shared_ptr<Site> Function::createSite(sptrBasicBlock ep_BB, std::shared_ptr<spdlog::logger> log){
    std::vector<addr_t> vect_ep = {ep_BB->getEntryPointAddr()};
    std::shared_ptr<Site> site_ptr = std::make_shared<Site>(vect_ep, 0, m_name);
    log->trace("Attenmpting to build site ep {:x}\n", ep_BB->getEntryPointAddr());

    std::queue<sptrBasicBlock> BFS_queue;
    std::set<addr_t> seen_BFS;
    sptrBasicBlock current_BB = ep_BB;

    BFS_queue.push(current_BB);
    while(!BFS_queue.empty() && site_ptr->getNumberBasicBlocks() < getSitesSize()){

        current_BB = BFS_queue.front();
        BFS_queue.pop();

        if(seen_BFS.count(current_BB->getEntryPointAddr())) { continue; }

        seen_BFS.insert(current_BB->getEntryPointAddr());

        site_ptr->insertBasicBlock(this->getBasicBlocks().at(current_BB->getEntryPointAddr()));
				log->trace("inserted new bb \n{}\n", current_BB->printFullBB());

        // Find children of this BB and add them to the queue
        const std::map<waddr_t, sptrConstEdge>& current_edges = current_BB->getEdges();
        for(auto const& [key, edge] : current_edges){
            log->trace("\t found a chid at addr {:x}. Is it in c_func ? : {}\n", edge->getDst().second, isBasicBlockExists(edge->getDst().second));
            if(isBasicBlockExists(edge->getDst().second)){
            		log->trace("\t\t Added addr2queue\n");
                BFS_queue.push(this->getBasicBlock( edge->getDst().second));
            }
        }
    }

		log->trace("Site size: {} ; Min site size : {}\n", site_ptr->getNumberBasicBlocks(), getSitesSize());
    if(site_ptr->getNumberBasicBlocks() < getSitesSize()){
        return NULL;
    }

    return site_ptr;

}
void Function::encodeFunctionSites(std::shared_ptr<spdlog::logger> log){
  for(const sptrSite& site : getSites()){
    site->encodeGraph(log);
    //log->info("Site encoding : {} \n", vec2str32(site->getEncoding()));
  }
}

void Function::hashFunctionSites(std::shared_ptr<spdlog::logger> log){
   for(const sptrSite& site : getSites()){
     //log->info("hashing site {:x}\n", site->getEntryPointAddr());
    site->hashGraph(log);
  }
}

void Function::hashFunctionSitesDataflow(std::shared_ptr<spdlog::logger> log){
   for(const sptrSite& site : getSites()){
     //log->info("hashing site {:x}\n", site->getEntryPointAddr());
    site->hashDataflow(log);
  }
}

void Function::addDataSite(std::shared_ptr<Site> site){
	m_datasites.insert(site);
}

inline bool isSiteInDatasitesAlready(sptrSite site_ptr,const std::unordered_set<sptrSite>& datasites, sptrLog log, const Config* config){
  for(auto const& datasite : datasites){
    if(datasite->isSimilarTo(site_ptr, log, config)){
			return true;
    }
  }
  return false;
}

/**
 * For each site, extract its dataflow
 * Then if the DFT has not been seen before, add it to the m_datasites
 **/
void Function::extractSitesDataflow(std::shared_ptr<spdlog::logger> log, const Config* config){
  for(auto const & site_ptr : getSites()){
    // NOTE: the strongly connected component things are not needed anymore
    SPDLOG_LOGGER_DEBUG(log, "\tStarting dataflow tensor extraction...\n");

    site_ptr->extractDataflowTensor(log);

    if(!isSiteInDatasitesAlready(site_ptr, getDataSites(), log, config)){
  		addDataSite(site_ptr);
		}

  }
}

/*void Function::addSiteHash(addr_t site_ep_addr, std::size_t hash_value_site){
  m_sites_hashes_map[site_ep_addr] = hash_value_site;
}

void Function::addSiteEncoding(addr_t addr, std:vector<uint32_t> vect){
  m_sites_encoding[addr] = vect;
}


std::unordered_map<addr_t, std::vector<uint32_t>> Function::getSitesEncoding(){
  return m_sites_encoding;
}*/

// Misc
std::unordered_set<addr_t> Function::getCallAddrs(){
  std::unordered_set<addr_t> output;
  for(auto const& bb : getBasicBlocks()){
    for(auto const& inst : bb.second->getInstrs()){
      if(inst->getType() == InstrType::CALL){
        addr_t addr = inst->getJmpAddr();
        output.insert(addr);
      }
    }
  }
  return output;
}
uint Function::getSize() const{
  uint size = 0;

  for(auto const& bb : getVectorBasicBlocks()){
    for(auto const& instr : bb->getInstrs()){
      size += instr->getSize();
    }
  }

  return size;
}

// Print

std::string Function::getSignature() const{

  if(!this->getSection().compare(".plt")){
    return "thunk";
  }
  else{
    return "core";
  }
}


void Function::setEntryPoint(addr_t ep){
  m_ep = ep;
}

std::string Function::printFullFunction() const{
  std::stringstream f;
  f << "List of bb in fun " + m_name + " : \n";
  for(auto const& [bb_ep, bb] : getSortedBasicBlocks()){
    f << addr2s(bb_ep) << "; ";
  }
  f << "\n";
  return f.str();
}

std::string Function::createDotWithSitesWithoutDigraph() const
{
  std::stringstream f;

  // First, we add each BB without edges
  auto bbs = getSortedBasicBlocks();
  for(auto const& bb_pair : bbs)
  {
    waddr_t bb_ep = {m_wave, bb_pair.first};
    sptrBasicBlock bb = bb_pair.second;

    // Key: EP of the BB
    f << '"' << waddr2s(bb_ep) << "\"[label=\"";
    std::string bb_instrs = "";
    for(auto const& instr : bb->getInstrs())
    {
      bb_instrs += instr->toStringLight() + "\\l";
    }

    f << bb_instrs;

    f << "\",shape=";
    if(bb->isEntryBlock()){f << "component,style=\"filled\"";}
    else{f << "box,style=\"filled\"";}
    f << ", fontweight=\"bold\",fillcolor=\"";
    if(bb->getColor() == 0) { f << "white"; }
    else if(bb->getColor() <= 8){ f << "/greens9/" + std::to_string(bb->getColor()); }
    else if(bb->getColor() <= 16){ f << "/oranges9/" + std::to_string(bb->getColor()-8); }
    else if(bb->getColor() <= 24){ f << "/pubu9/" + std::to_string(bb->getColor()-16); }
    else {f << "white";}



    f << "\"];" << std::endl;
  }

  // Second, we add edges
  for(auto const& bb_pair : bbs)
  {
    std::string src = waddr2s({m_wave, bb_pair.first});
    for(auto const& edge_pair : bb_pair.second->getEdges())
    {
      std::string dst = waddr2s(edge_pair.first);

      // If STATIC DISAS --> black
      if(edge_pair.second->getFoundMethod() == EdgeFoundMethod::STATIC_DISAS)
      {
        f << '"' << src << "\" -> \"" << dst << "\";" << std::endl;
      }

      // If BOA DISAS --> blue
      else if(edge_pair.second->getFoundMethod() == EdgeFoundMethod::BOA_DISAS)
      {
        f << '"' << src << "\" -> \"" << dst << "\"[style=dotted];" << std::endl;
      }
    }
  }

  // now we add the sites
  std::unordered_set<std::shared_ptr<Site>> sites = getSites();
  //std::cout << "FOUND SOME SITES: " << sites.size() << "\n";
  int site_label = 0;
  for(auto const& site : sites){
      site_label ++;
      // for every site we do a graph
      auto bbs = site->getSortedBasicBlocks();
      // the blocks
      for(auto const& [bb_ep,bb] : bbs){
          // Key: EP of the BB
          // HERE is the site label, very important
          f << '"' << "s" + std::to_string(site_label) + addr2s(bb_ep) << "\"[label=\"";

          std::string bb_instrs = "";
          for(auto const& instr : bb->getInstrs())
          {
            bb_instrs += instr->toStringLight() + "\\l";
          }
          f << bb_instrs;
          // to add the DATAFLOW info on every site BB:
          //f << site->printDataflow(bb);
          f << "\",shape=";
          if(bb->isEntryBlock()){f << "component,style=\"filled\"";}
          else{f << "box,style=\"filled\"";}
          f << ", fontweight=\"bold\",fillcolor=\"";
          f << "white";
          f << "\"];" << std::endl;
      }
      // the edges between those blocks
      // CAREFUL about the labels
      for(auto const& [bb_ep,bb] : bbs)
      {
          std::string src = "s" + std::to_string(site_label);
          (src) += addr2s(bb_ep);

          for(auto const& [waddr_dst, edge] : bb->getEdges())
          {
              std::string dst = "s" + std::to_string(site_label);
              dst +=  addr2s(waddr_dst.second);

              // If STATIC DISAS --> black
              if(edge->getFoundMethod() == EdgeFoundMethod::STATIC_DISAS)
              {
                  f << '"' << src << "\" -> \"" << dst << "\";" << std::endl;
              }

              // If BOA DISAS --> blue
              else if(edge->getFoundMethod() == EdgeFoundMethod::BOA_DISAS)
              {
                  f << '"' << src << "\" -> \"" << dst << "\"[style=dotted];" << std::endl;
              }
          }
      }


    }

  return f.str();
}

std::string Function::createDotWithSitesWithDataflowWithoutDigraph(){

  std::stringstream f;

  // First, we add each BB without edges
  auto bbs = getSortedBasicBlocks();
  for(auto const& bb_pair : bbs)
  {
    waddr_t bb_ep = {m_wave, bb_pair.first};
    sptrBasicBlock bb = bb_pair.second;

    // Key: EP of the BB
    f << '"' << waddr2s(bb_ep) << "\"[label=\"";
    std::string bb_instrs = "";
    for(auto const& instr : bb->getInstrs())
    {
      bb_instrs += instr->toStringLight() + "\\l";
    }

    f << bb_instrs;

    f << "\",shape=";
    if(bb->isEntryBlock()){f << "component,style=\"filled\"";}
    else{f << "box,style=\"filled\"";}
    f << ", fontweight=\"bold\",fillcolor=\"";
    if(bb->getColor() == 0) { f << "white"; }
    else if(bb->getColor() <= 8){ f << "/greens9/" + std::to_string(bb->getColor()); }
    else if(bb->getColor() <= 16){ f << "/oranges9/" + std::to_string(bb->getColor()-8); }
    else if(bb->getColor() <= 24){ f << "/pubu9/" + std::to_string(bb->getColor()-16); }
    else {f << "white";}    
    f << "\"];" << std::endl;
  }

  // Second, we add edges
  for(auto const& bb_pair : bbs)
  {
    std::string src = waddr2s({m_wave, bb_pair.first});
    for(auto const& edge_pair : bb_pair.second->getEdges())
    {
      std::string dst = waddr2s(edge_pair.first);
      f << '"' << src << "\" -> \"" << dst << "\";" << std::endl;
    }
  }

  // now we add the sites, and their dataflow
  std::unordered_set<std::shared_ptr<Site>> sites = getSites();
  //std::cout << "FOUND SOME SITES: " << sites.size() << "\n";
  uint site_label = 0;
  for(std::shared_ptr<Site> site : sites){

      site_label++;

      // for every site we do a graph
      auto bbs = site->getSortedBasicBlocks();
      // the blocks
      for(auto const& [bb_ep,bb] : bbs){
          // Key: EP of the BB
          // HERE is the site label, very important
          waddr_t bb_ep_waddr = {site_label, bb_ep};
          f << '"' << waddr2s(bb_ep_waddr) << "\"[label=\"";

          std::string bb_instrs = "";
          for(auto const& instr : bb->getInstrs())
          {
            bb_instrs += instr->toStringLight() + "\\l";
          }
          f << bb_instrs; 
          // to add the DATAFLOW info on every site BB:
          //f << site->printDataflow(bb);
          f << "\",shape=";
          if(bb->isEntryBlock()){f << "component,style=\"filled\"";}
          else{f << "box,style=\"filled\"";}
          f << ", fontweight=\"bold\",fillcolor=\"";
          f << "white";
          f << "\"];" << std::endl;

        // the edges between those blocks
        // CAREFUL about the labels

          std::string src = waddr2s(bb_ep_waddr);
          
          for(auto const& [waddr_dst, edge] : bb->getEdges())
          {   
            waddr_t waddr_dst_labeled = {site_label, waddr_dst.second};
            std::string dst = waddr2s(waddr_dst_labeled);
            f << '"' << src << "\" -> \"" << dst << "\";" << std::endl;
          }
      }

      f << site->createDataflowDotWithoutDigraph(site_label) << " ";
      // and the dataflow
    }
  return f.str();
}

std::string Function::createDotWithSitesWithDataflow(){
    return "Digraph G {\n" + createDotWithSitesWithDataflowWithoutDigraph() + "}";
}

std::string Function::createDotWithSites() const
{
  return "Digraph G {\n" + createDotWithSitesWithoutDigraph() + "}";
}

std::string Function::createDotWithDataflow(bool is_reduced){
  std::stringstream f;
  f << "Digraph G {\n ";
  f << createDotWithoutDigraph(0, is_reduced);

  uint site_index=1;
  std::string site_id;
  for(auto const& site_ptr: getDataSites()){
    site_id = addr2s(site_ptr->getEntryPointAddr()) + std::to_string(site_index);
    f << "subgraph cluster_site_" + site_id << "{\n";
    f << "color=\"red\"\n";

      f << site_ptr->createDotWithoutDigraph(site_index,  is_reduced);
      f << site_ptr->createDataflowDotWithoutDigraph(site_index);

    f << "}\n";
    site_index++;
  }
  f<< "}";
  return f.str();
}


} // namespace boa
