#ifndef FUNCTION_H
#define FUNCTION_H

#include "common/cfg.hpp"
#include "utils/boa_types.hpp"
#include "site.hpp"
#include "config.hpp"

namespace boa{

class Function: public CFG 
{

public:

    Function(const std::vector<addr_t>& entry_points, unsigned int sites_size, unsigned int wave, color_t color, OptLevel opt_lvl, std::string binary_name);
    Function(const std::vector<addr_t>& entry_points, unsigned int sites_size, unsigned int wave, color_t color, OptLevel opt_lvl, std::string name, std::string binary_name);

    // Setters and getters
    const color_t getColor() const;
    void setColor(color_t color);
    void setName(std::string name) const;
    void setEntryPoint(addr_t ep);
    void addCalledAddr(addr_t addr){ m_called_addrs.insert(addr);}

    std::string getName() const;
		std::string getBinName() const {return m_binary_name;}
		std::unordered_set<addr_t> getCallAddrSet(){return m_called_addrs;}
    const std::unordered_set<std::shared_ptr<Site>>& getSites() const;
    const std::unordered_set<std::shared_ptr<Site>>& getDataSites() const { return m_datasites;}
    void addDataSite(std::shared_ptr<Site> site);
    std::vector<sptrSite> getOrderedSites();
    addr_t getEntryPointAddr() const;
    std::string getSection() const { return m_section; }
    void setSection(std::string& sect) { m_section = sect;}

    void setSitesSize(unsigned int size);
    uint getSitesSize() const;
    OptLevel getOptLvl() const { return m_opt_lvl; }

    // Sites
    void encodeFunctionSites(std::shared_ptr<spdlog::logger> log);
    void hashFunctionSites(std::shared_ptr<spdlog::logger> log);
    void hashFunctionSitesDataflow(std::shared_ptr<spdlog::logger> log);

    void extractFunctionSites(std::shared_ptr<spdlog::logger> log);
    std::shared_ptr<Site> createSite(sptrBasicBlock ep_BB, std::shared_ptr<spdlog::logger> log);
    void extractSitesDataflow(std::shared_ptr<spdlog::logger> log, const Config* config);
    //void addSiteEncoding(addr_t addr, std:vector<uint32_t> vect);
    //void addSiteHash(addr_t addr, std::size_t);
    //std::unordered_map<addr_t, std::vector<uint32_t>> getSitesEncoding();

    // Misc
    std::unordered_set<addr_t> getCallAddrs();
    uint getSize() const;

    // Print

    std::string getSignature() const;

    std::string printFullFunction() const;

    std::string createDotWithSites() const;
    std::string createDotWithSitesWithDataflow();

    std::string createDotWithSitesWithoutDigraph() const;
    std::string createDotWithSitesWithDataflowWithoutDigraph();

    std::string createDotWithDataflow(bool is_reduced) override ;

    private:

    mutable std::string m_name;
		std::string m_binary_name;
		OptLevel m_opt_lvl;
    addr_t m_ep;
    std::string m_section;
    std::unordered_set<std::shared_ptr<Site>> m_sites;
		std::unordered_set<std::shared_ptr<Site>> m_datasites; // datasites are a collection of unique sites, one of each DFT
    //std::unordered_map<addr_t, std::vector<uint32_t>> m_sites_encoding;
    //std::unordered_map<addr_t, std::size_t> m_sites_hashes_map;
    color_t m_color;
		std::unordered_set<addr_t> m_called_addrs;

    unsigned int m_sites_size;


};

using sptrFunc = std::shared_ptr<Function>;

} // namespace boa

#endif
