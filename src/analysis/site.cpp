#include "site.hpp"

namespace boa{

// MARK:- Constructor
Site::Site(const std::vector<addr_t>& entry_points, unsigned int wave, std::string fun_name) : CFG(entry_points, wave)
{
  m_function_name = fun_name;
  m_encoding_before_hashing = nullptr;
  m_encoding_hash = new uint8_t[MD5_HASH_SIZE];
  m_encoding_size =0;

  m_dataflow_hash = new uint8_t[MD5_HASH_SIZE];
  //m_dataflow_size;
}

Site::~Site(){
  delete[] m_encoding_before_hashing;
  delete[] m_encoding_hash;
  delete[] m_dataflow_hash;
}

// Getters and setters


void Site::setEncoding(std::vector<uint32_t> vect){
  m_encoding = vect;
}

std::vector<uint32_t> Site::getEncoding() const{
  return m_encoding;
}

std::vector<sptrBipartite> Site::getSiteDataflow() const{
  return m_dataflow_graphs;
}

sptrBipartite Site::getBBDataflow(sptrBasicBlock BB) const{
  if(!isBasicBlockExists(BB->getEntryPointAddr())){
    throw std::out_of_range("Basic block at address 0x" + addr2s(BB->getEntryPointAddr()) + " not found in Site");
  }
  for(sptrBipartite bgraph_ptr : m_dataflow_graphs){
    if(BB->getEntryPointAddr() == bgraph_ptr->getEntryPointAddr()){
      return bgraph_ptr;
    }
  }
  throw std::runtime_error("Could not find a bipartite graph associated with BB at addr " + addr2s(BB->getEntryPointAddr()) + " in given site.\n");
}

std::vector<sptrBasicBlock> Site::getBFSSearchBBSort() const{
  return m_BFS_search_BB_sort;
}

addr_t Site::getEntryPointAddrSigtoolStyle() const{
  return getBasicBlockWithEntryPoint(getEntryPointAddr())->getLastInstr()->getAddr();
}

void Site::setFunctionName(std::string name){
  m_function_name = name;
}
std::string Site::getFunctionName(){
  return m_function_name;
}

/**
 * Updates the member m_sum_matrix according to the selected
 * site sim method (sum or sum_general_reg)
 **/
void Site::computeSumMatrix(const std::string& similarity_method){
	x86_reg_class line_reg, column_reg, general_reg = x86_reg_class::X86_CLASS_GEN_REG;
	//init the map
	for(x86_reg_class reg_class_int = X86_CLASS_BEGIN; reg_class_int <= X86_CLASS_ENDING; reg_class_int = (x86_reg_class)(reg_class_int+1)){
			if(similarity_method == "sum_general_reg" && isRegGeneralPurpose(reg_class_int)){line_reg = general_reg;}
			else{ line_reg = reg_class_int;}
		for(x86_reg_class reg_class_int2 = X86_CLASS_BEGIN; reg_class_int2 <= X86_CLASS_ENDING; reg_class_int2 = (x86_reg_class)(reg_class_int2+1)){
			if(similarity_method == "sum_general_reg" && isRegGeneralPurpose(reg_class_int2)){column_reg = general_reg;}
			else{ column_reg = reg_class_int2; }
			m_sum_matrix[line_reg][column_reg]=0;
		}
	}
	for(auto const& df : getDataflowTensor()){ // for each dataflow graph of the DFT
		for(auto const& reg_line : df->getDataflowVector()){ // for each register's info in this dataflow graph
			if(similarity_method == "sum_general_reg" && isRegGeneralPurpose(reg_line->reg_class_name)){ line_reg = general_reg; }
			else{ line_reg = reg_line->reg_class_name;}
			for(auto const& source_reg : reg_line->source_regs){ // for each source reg
				if(similarity_method == "sum_general_reg" && isRegGeneralPurpose(source_reg)){ column_reg = general_reg; }
				else{ column_reg = source_reg;}
				m_sum_matrix.at(line_reg).at(column_reg)++; 
// this adds one to the number at line reg_line->reg_class_name and column source_reg
			}
		}
	}
}
// Analysis

uint16_t getRegHexValue(sptrBasicBlock BB){
  std::map<InstrType, uint16_t> RegHexValues = {
  {InstrType::CALL, 0x1},
  {InstrType::HLT, 0x2},
  {InstrType::INT, 0x3},
  {InstrType::JCC, 0x4},
  {InstrType::JMP, 0x5},
  {InstrType::LIB_MOCK, 0x6},
  {InstrType::RET, 0x7},
  {InstrType::SEQ, 0x8},
  {InstrType::TRAP, 0x9},
  {InstrType::UNKNOWN, 0xa},
  {InstrType::FAKE_LIB_FUNC, 0xb}
};
  return RegHexValues[BB->getLastInstrType()];
}

uint32_t createBuffer(uint16_t reg_value, uint16_t nbr_children){
  // return a 4 bytes element such as :
  // with reg_value = 0xffff, nbr_children = 0x001a
  // then buffer = 0xffff001a
  // wich is the encoding for a node or an edge
  uint32_t buffer = 0;
  buffer = buffer ^ reg_value;
  buffer = buffer << 16;
  buffer = buffer ^ nbr_children;
  return buffer;
}

uint16_t createBuffer16Bits(uint8_t reg_value, uint16_t nbr_children){
  // return a 2 bytes element such as :
  // with reg_value = 0x5 (4 bits), nbr_children = 0x001aef (12 bits)
  // then buffer = 0x5001aef
  // wich is the encoding for a node or an edge
  uint16_t buffer = 0;
  buffer = ((reg_value & 0xf) << 12) | (nbr_children & 0xfff);
  return buffer;
}

bool sortFunction (sptrBasicBlock bb1, sptrBasicBlock bb2){
  return(bb1->getEntryPointAddr() < bb2->getEntryPointAddr());
   }

// follow sigtool's encoding site principle
// basically do a BFS and appends the visited nodes
// and the edges that are not part of the BFS search (those are implicit)
// TODO order nodes by addr value when adding them to the queue
void Site::encodeGraph(std::shared_ptr<spdlog::logger> log)
{
  addr_t current_addr;
  sptrBasicBlock current_BB;
  uint16_t node_value;
  uint16_t nbr_children;
  uint16_t target_index;
  uint32_t buffer;


  std::map<addr_t, bool> seen;
  std::queue<sptrBasicBlock> bfs_queue;
  std::vector<sptrBasicBlock> bfs_search_BB_sort;

  std::vector<uint32_t> graph_encoding;
  std::map<addr_t, uint16_t> node_index_map;

  std::vector<sptrBasicBlock> neighbours_list_tmp;
  //SPDLOG_LOGGER_DEBUG(log, "TEST REACH: {}\n", getSortedBasicBlocks().size());

  for(auto const ep : getEntryPoints()){
    // push the first entry point (should be only 1 anyway)
    if(bfs_queue.empty()){bfs_queue.push( getBasicBlock(ep) );};
  }

  //SPDLOG_LOGGER_DEBUG(log, "Beginning encoding of graph {:x}, of size {}\n", bfs_queue.front()->getEntryPointAddr(), getBasicBlocks().size());
  while(!bfs_queue.empty()){
  //SPDLOG_LOGGER_DEBUG(log, "Queue is of size {:x} / {} \n", bfs_queue.front()->getEntryPointAddr(), graph_encoding.size());

    nbr_children = 0;

    current_BB = bfs_queue.front();
    current_addr=current_BB->getEntryPointAddr();
    bfs_queue.pop();

    if(isBasicBlockExists(current_addr) && !seen[current_addr]){
      // go fetch children only if not seen and in the CFG
      for(auto const addr : current_BB->getLastInstr()->getChildren()){

        if(!isBasicBlockExists(addr)){
          // in that case we create a dummy BB for outgoing edge check later
          waddr_t dummy_ep = std::make_pair(0,addr);
           sptrBasicBlock BB_ptr = std::make_shared<BasicBlock>(dummy_ep);
          neighbours_list_tmp.push_back(BB_ptr);
        }
        else{
          neighbours_list_tmp.push_back(getBasicBlockWithoutCheck(addr));
          }
        nbr_children++;
      }
    }
    std::sort(neighbours_list_tmp.begin(), neighbours_list_tmp.end(), sortFunction);
    // this ensure we always visit the voisins by their ep address order
    for(auto const& elt : neighbours_list_tmp){
      bfs_queue.push(elt);
    }
    neighbours_list_tmp.clear();

    if(seen[current_addr]){
      if(!isBasicBlockExists(current_addr)){
        node_value = PATH_UNDEF;
        target_index = 0;
      }
      else{
      node_value = PATH;
      // add a check here
      target_index = node_index_map[current_addr];
      }
      buffer = createBuffer(node_value, target_index);
      graph_encoding.push_back(buffer);
    }
    else if(!isBasicBlockExists(current_addr)){
      node_value = PATH_UNDEF;
      // add a check here
      target_index = 0;
      buffer = createBuffer(node_value, target_index);
      graph_encoding.push_back(buffer);
    }
    else{
      // stores the index in the output of the current node
      node_index_map[current_addr] = graph_encoding.size();
      node_value = getRegHexValue(current_BB);
      // add a check here
      buffer = createBuffer(node_value, nbr_children);
      graph_encoding.push_back(buffer);

      // update the "BFS search" member
      bfs_search_BB_sort.push_back(current_BB);
    }

    seen[current_addr] = true;

  }
 // SPDLOG_LOGGER_DEBUG(log, "Size of output: {}\n", graph_encoding.size());
  m_BFS_search_BB_sort = bfs_search_BB_sort;
  m_encoding = graph_encoding;
}

// Function to hash things, using md5.h functions
// template function
void encoding_to_md5(void * encoding, uint encoding_size, uint8_t *hash_buffer){
    md5_ctx Context;
    md5_init(&Context);
    md5_update(&Context,(const md5_byte_t  *) encoding, sizeof (uint16_t) * ((unsigned long) encoding_size));
    md5_final(hash_buffer, &Context);
}
/*
bool Site::isDataflowEqual(std::shared_ptr<Site> site2) const{

    if(m_dataflow_graphs.size() != site2->getSiteDataflow().size()){ return false; }

    //int corresponding_df_index = -1;

    std::vector<sptrBipartite> cpy_dataflow2 = site2->getSiteDataflow(); // get a copy
    
    for(auto const& df_graph_1 : m_dataflow_graphs){
      // try to find a corresponding df_graph in cpy_dataflow2
      for(uint i = 0; i < cpy_dataflow2.size(); i++){
        if(df_graph_1->isEqualTo(cpy_dataflow2.at(i))){ // we found a match for df_graph_1
          //corresponding_df_index = i;
          cpy_dataflow2.erase(cpy_dataflow2.begin() + i); // remove it from the cpy vector
          break; // and carry on to the next df_graph_1
        }
      }
      return false; // we could not find a match for this df_graph
    }
    // if we reach here, it means all of this site's df_graph found a match
    return true;
}

bool Site::isDataflowEqualV2(std::shared_ptr<Site> site2) const{

  bool found_no_match = true;

    if(getDataflowV2().size() != site2->getDataflowV2().size()){ return false; }

    //int corresponding_df_index = -1;

    std::set<sptrBipartite> cpy_dataflow2 = site2->getDataflowV2(); // get a copy set (we're going to modify it)
    
    for(auto const& df_graph_1 : getDataflowV2()){
      found_no_match = true;
      // try to find a corresponding df_graph in cpy_dataflow2

      for(auto const & df_graph_2 : cpy_dataflow2){
        if(df_graph_1->isEqualTo(df_graph_2)){ // we found a match for df_graph_1
          //corresponding_df_index = i;
          cpy_dataflow2.erase(df_graph_2); // remove it from the cpy set
          found_no_match = false;
          break; // and carry on to the next df_graph_1
        }
      }
      if(found_no_match){
        return false; // we could not find a match for this df_graph_1
      }
    }
    // if we reach here, it means all of this site's df_graph found a match
    return true;
}
*/

/**
 * @returns nullptr if no node is allowed, the min arity node of grpah_matches otherwise
 * */
static sptrBipartite getNodeWithMinArity(std::map<sptrBipartite, std::unordered_set<sptrBipartite>>* graph_matches, std::map<sptrBipartite, bool>* graph_nodes){
  uint min_arity = graph_matches->begin()->second.size();
  sptrBipartite output_min = nullptr;
  for(auto const& [graph_X, matching_graphs] : *graph_matches){
    if(graph_nodes->at(graph_X)){ // this node is allowed (not visited yet)
      if(graph_matches->at(graph_X).size() <= min_arity){
        min_arity = graph_matches->at(graph_X).size();
        output_min = graph_X;
      }
    }
  }
  return output_min;
}

/**
 * 
 * @returns nullptr is no node is available, the first available one otherwise
 **/
static sptrBipartite getNextNode(std::map<sptrBipartite, std::unordered_set<sptrBipartite>>* graph_matches, std::map<sptrBipartite, bool>* graph_nodes1){
  for(auto const& [graph_X, matching_graphs] : *graph_matches){
    if(graph_nodes1->at(graph_X)){
      return graph_X;
    }
  }
  return nullptr; // didn't find any
}

/**
 * Stupid algorithm to recursively find the max number of unique pairs you can build in a bipartite graph
 * @return nb de pair max
 **/
static uint backtrackingDataflow(std::map<sptrBipartite, bool>* graph_nodes1,std::map<sptrBipartite, bool>* graph_nodes2, std::map<sptrBipartite, std::unordered_set<sptrBipartite>>* graph_matches, uint max_pairs){

  uint n_max=0;
  sptrBipartite min_arity_node = getNextNode(graph_matches, graph_nodes1);
  if(min_arity_node == nullptr){ // case where no node was available
    return n_max;
  }
  graph_nodes1->at(min_arity_node) = false; // Node is visited and not allowed anymore

  for(auto const& mate : graph_matches->at(min_arity_node)){
    if(graph_nodes2->at(mate)){ // if node is allowed
      graph_nodes2->at(mate) = false; // we note it as visited
      n_max = std::max(n_max, 1 + backtrackingDataflow(graph_nodes1, graph_nodes2, graph_matches, max_pairs));
      graph_nodes2->at(mate) = true; // set mate access bool back to true (for next iterations)
      if(n_max == max_pairs){
        break;
      }
    }
  }
  graph_nodes1->at(min_arity_node) = true; // same as previous mate access bool, for the min arity node
  return n_max;
}

/* Returns true if there is a path from source 's' to sink
  't' in residual graph. Also fills parent[] to store the
  path */
bool beadth_first_search(int *rGraph[], int s, int t, int parent[])
{
    // Create a visited array and mark all vertices as not
    // visited
    bool visited[(uint) t];
    std::memset(visited, 0, sizeof(visited));
 
    // Create a queue, enqueue source vertex and mark source
    // vertex as visited
    std::queue<int> q;
    q.push(s);
    visited[s] = true;
    parent[s] = -1;
 
    // Standard BFS Loop
    while (!q.empty()) {
        int u = q.front();
        q.pop();
 
        for (int v = 0; v < t + 1; v++) {
            if (visited[v] == false && rGraph[u][v] > 0) {
                // If we find a connection to the sink node,
                // then there is no point in BFS anymore We
                // just have to set its parent and can return
                // true
                if (v == t) {
                    parent[v] = u;
                    return true;
                }
                q.push(v);
                parent[v] = u;
                visited[v] = true;
            }
        }
    }

    // We didn't reach sink in BFS starting from source, so
    // return false
    return false;
}

static int ford_fulkersonDataFlow(std::map<sptrBipartite, uint> graph_index1,std::map<sptrBipartite, uint> graph_index2, std::map<sptrBipartite, std::unordered_set<sptrBipartite>> graph_matches, uint index, uint index_lim1_2) {
  // Transform the bipartite graph to a network flow
  int graph[index + 1][index + 1]; // graph matrix M[i][j] give the possible flow of i to j
  graph[0][0] = 0;
  graph[0][index] = 0;
  graph[index][0] = 0;
  graph[index][index] = 0;
  for (uint j = 1; j < index_lim1_2; j++) {
    graph[0][j] = 1;
    graph[index][j] = 0;
    graph[j][0] = 0;
    graph[j][index]= 0;
  }
  for (uint j = index_lim1_2; j < index; j++) {
    graph[0][j] = 0;
    graph[index][j] = 1;
    graph[j][0] = 0;
    graph[j][index]= 1;
  }

  for (uint i = 1 ; i < index; i++) {
    for (uint j = 1; j < index_lim1_2; j++) {
      graph[i][j] = 0;
    }
  }

  for (uint i = 1; i < index; i++) {
    for (uint j = index_lim1_2; j < index; j++) {
      graph[i][j] = 0;
    }
  }
  uint index1, index2;
  for(auto const& [key, value] : graph_matches){
    index1 = graph_index1[key];
    for(auto const& df_graph2 : value){
      index2 = graph_index2[df_graph2];
      graph[index1][index2] = 1;
    }
  }

  // Ford Fulkerson algorithm, with BFS
  int *rGraph[index + 1];
    for(uint i = 0; i < index + 1; i++){
        rGraph[i] = new int[index + 1];
    }
  // Create a residual graph and fill the residual graph with given capacities in the original graph as residual capacities in residual graph
  // Residual graph where rGraph[i][j] indicates residual capacity of edge from i to j (if there is an edge. If rGraph[i][j] is 0, then there is not)
  for (uint u = 0; u < index + 1; u++){
    for (uint v = 0; v < index + 1; v++){
      rGraph[u][v] = graph[u][v];
    }
  }

  int parent[index + 1]; // This array is filled by breadth-first search and to store path
  int max_flow = 0;
  // Augment the flow while there is path from source to sink
  while (beadth_first_search(rGraph, 0, (int) index, parent)) {
    // Find minimum residual capacity of the edges along the path filled by BFS. Or we can say find the maximum flow through the path found.
    int path_flow = INT_MAX;
    int u;
    for (int v = (int) index; v != 0; v = parent[v]) {
      u = parent[v];
      path_flow = std::min(path_flow, rGraph[u][v]);
    }

    // update residual capacities of the edges and reverse edges along the path
    for (int v = (int) index; v != 0; v = parent[v]) {
      u = parent[v];
      rGraph[u][v] -= path_flow;
      rGraph[v][u] += path_flow;
      }

    // Add path flow to overall flow
    max_flow += path_flow;
  }
  return(max_flow);
}

static std::unordered_set<sptrBipartite> getMatchesOfDF(sptrBipartite DF_graph,  std::unordered_set<sptrBipartite> DF_Tensor, const std::string& bgraph_comp_method, std::shared_ptr<spdlog::logger> log){
  std::unordered_set<sptrBipartite> output;
  for(auto const& df_graph_2 : DF_Tensor){
      //if(exit_bb_1->getLastInstrType() != exit_bb_2->getLastInstrType()){continue;}
      if(DF_graph->isEquivalentTo(df_graph_2, log, bgraph_comp_method)){ // we found a match for df_graph_1
        output.insert(df_graph_2);
      }
  }
  return output;
}

bool Site::isSimilarTo(const std::shared_ptr<Site> & site2, std::shared_ptr<spdlog::logger> log, const Config* config){

    if(config->getConfigGlobal().sample_distance){ // SAMPLE DISTANCE IS FOR OLD COMPARISONS. DATS GUT
        // architecture comparison
        return(getEncoding() == site2->getEncoding());
    }
    else{
        float sim_score = computeDataflowSimilarity(site2, log, config);
        return (sim_score > config->getConfigGlobal().site_similarity_threshold); // use of the threshold
    }

}

/**
 * Computes the sim score between the two sites.
 * Defined as: # sites that found a match / # total of sites (in both sites each time)
 * We compute the maximal couplage between dataflows of site1 and site2
 * : "bijection" - prefered method, others are mostly obsolete now. returns a bool.
 * : "backtracking" - a slow but accurate way to compute the max nbr of pairs of dataflow that match among the 2 DFTs
 * : "sum" - fast but theorically innacurate. We sum the dataflow of the DFTs and compare them directly
 * : "sum_general_reg" - same as sum, but the general registers are set in the same category
 * : "ford-fulkerson" - also max number of pair but faster than "backtracking"
 * : "dunno-yet"
 * @return float: the similarity score
 **/
float Site::computeDataflowSimilarity(const std::shared_ptr<Site> site2, std::shared_ptr<spdlog::logger> log, const Config* config) const{
	std::string bgraph_sim_method = config->getConfigGlobal().bgraph_sim_method;
	std::string similarity_method = config->getConfigGlobal().site_sim_method;
	SPDLOG_LOGGER_TRACE(log, "computeDataflowSimilarity with sim_method {} and bgraph_sim {}\n", similarity_method, bgraph_sim_method);
	float score=0;
	// Prefered method: bijection
	// Note: Each bipartite graph in a DFT is unique
	if(similarity_method == "bijection"){

		if(this->getDataflowTensor().size() != site2->getDataflowTensor().size()){return false;}

		bool found_a_match = false;
		std::unordered_set<sptrBipartite> equiv_set;
		for(auto const& bp1 : this->getDataflowTensor()){
			found_a_match = false;
			for(auto const& bp2 : site2->getDataflowTensor()){
				if(bp1->isEquivalentTo(bp2, log, bgraph_sim_method)){
					found_a_match = true;
					break;
				}
			}
			// for one graph in bp1, we didn't find any match
			if(!found_a_match){return 0;}
		}
		score = 1;
		//score = (float)equiv_set.size() / (float)(getDataflowTensor().size() + site2->getDataflowTensor().size() );
	}
  if (similarity_method == "ford-fulkerson"){
    std::unordered_set<sptrBipartite> matches;
		std::map<sptrBipartite, std::unordered_set<sptrBipartite>> graph_matches; // key: site1's DF, value: this DF's matches in site2
    std::map<sptrBipartite, uint> graph_index1, graph_index2;

		// First round to fill the graph structure
    uint index = 1;
		for(auto const& df_graph_1 : getDataflowTensor()){
      graph_index1.insert({df_graph_1, index});
      index++;
			matches = getMatchesOfDF(df_graph_1, site2->getDataflowTensor(), bgraph_sim_method, log);
			graph_matches.insert({df_graph_1, matches});
		}
    uint index_lim1_2 = index;
    for(auto const& df_graph_2 : site2->getDataflowTensor()){
			graph_index2.insert({df_graph_2, index});
      index++;
		}

    int max_flow = ford_fulkersonDataFlow(graph_index1, graph_index2, graph_matches, index, index_lim1_2);

    // Return the overall flow
    score = 2 * (float)max_flow / (float)( getDataflowTensor().size() + site2->getDataflowTensor().size() );
    return score;
  }

	if(similarity_method == "backtracking"){
		uint nb_pairs=0;
		std::unordered_set<sptrBipartite> matches;
		std::map<sptrBipartite, bool> graph_nodes1, graph_nodes2; // X represents the first set of the nodes we try to match site1's DF Tensor
		std::map<sptrBipartite, std::unordered_set<sptrBipartite>> graph_matches; // key: site1's DF, value: this DF's matches in site2

		// First round to fill the graph structure
		for(auto const& df_graph_1 : getDataflowTensor()){
			matches = getMatchesOfDF(df_graph_1, site2->getDataflowTensor(), bgraph_sim_method, log);
			graph_matches.insert({df_graph_1, matches});
			graph_nodes1.insert({df_graph_1, true});
		}
		for(auto const& df_graph_2 : site2->getDataflowTensor()){
			graph_nodes2.insert({df_graph_2, true});
		}
		SPDLOG_LOGGER_DEBUG(log, "Launching backpropagation on sites {:x} and {:x}\n", getEntryPointAddr(), site2->getEntryPointAddr());
		// Apply the recursive function to it
		nb_pairs = backtrackingDataflow(&graph_nodes1, &graph_nodes2, &graph_matches, std::min(getDataflowTensor().size(), site2->getDataflowTensor().size()));
		score = 2 * (float)nb_pairs / (float)( getDataflowTensor().size() + site2->getDataflowTensor().size() );
	}
	if(similarity_method == "sum_general_reg" ){
		if(this->getSumMatrix() == site2->getSumMatrix()){score=1;}
		else{score=0;}
	}
  return score;
}

// same as encodeGraph using hashing
void Site::hashGraph(std::shared_ptr<spdlog::logger> log)
{
  addr_t current_addr;
  sptrBasicBlock current_BB;
  uint8_t node_value;
  uint16_t nbr_children;
  uint16_t target_index;
  uint16_t buffer;
  uint run_element_index=0;

  std::map<addr_t, bool> seen;
  std::queue<sptrBasicBlock> bfs_queue;
  std::vector<sptrBasicBlock> bfs_search_BB_sort;
  
  std::map<addr_t, uint16_t> node_index_map;

  std::vector<sptrBasicBlock> neighbours_list_tmp;
  //SPDLOG_LOGGER_DEBUG(log, "TEST REACH: {}\n", getSortedBasicBlocks().size());

  // Init the graph encoding and BFS queue
  uint nb_element_run = getNumberBasicBlocks() + getNumberEdges();
  if (nb_element_run > MAX_SUBGRAPH_SIZE){
    log->info("ERROR: max subgraph size limit reached. Aborting graph encoding before hash.\n");
    return;
  }

  //m_encoding_before_hashing = (uint16_t*) malloc(nb_element_run * sizeof(uint16_t));
  m_encoding_before_hashing = new uint16_t[nb_element_run];

  for(auto const ep : getEntryPoints()){
    // push the first entry point (should be only 1 anyway)
    if(bfs_queue.empty()){bfs_queue.push( getBasicBlock(ep) );};
  }

  while(!bfs_queue.empty()){

    nbr_children = 0;

    current_BB = bfs_queue.front();
    current_addr=current_BB->getEntryPointAddr();
    bfs_queue.pop();

    if(isBasicBlockExists(current_addr) && !seen[current_addr]){
      // go fetch children only if not seen and in the CFG
      for(auto const addr : current_BB->getLastInstr()->getChildren()){

        if(!isBasicBlockExists(addr)){
          // in that case we create a dummy BB for outgoing edge check later
          waddr_t dummy_ep = std::make_pair(0,addr);
           sptrBasicBlock BB_ptr = std::make_shared<BasicBlock>(dummy_ep);
          neighbours_list_tmp.push_back(BB_ptr);
        }
        else{
          neighbours_list_tmp.push_back(getBasicBlockWithoutCheck(addr));
          }
        nbr_children++;
      }
    }

    std::sort(neighbours_list_tmp.begin(), neighbours_list_tmp.end(), sortFunction);
    // this ensure we always visit the voisins by their ep address order
    for(auto const& elt : neighbours_list_tmp){
      bfs_queue.push(elt);
    }
    neighbours_list_tmp.clear();

    if(seen[current_addr]){
      if(!isBasicBlockExists(current_addr)){
        node_value = PATH_UNDEF_8;
        target_index = 0;
      }
      else{
      node_value = PATH_8;
      // add a check here
      target_index = node_index_map[current_addr];
      }
    }
    else if(!isBasicBlockExists(current_addr)){
      node_value = PATH_UNDEF_8;
      // add a check here
      target_index = 0;
    }
    else{
      // stores the index in the output of the current node
      node_value = (uint8_t) getRegHexValue(current_BB);  
      target_index = nbr_children;    

      // update the "BFS search" member
      bfs_search_BB_sort.push_back(current_BB);
      node_index_map[current_addr] = run_element_index;
    }

    buffer = createBuffer16Bits(node_value, target_index);
    m_encoding_before_hashing[run_element_index] = buffer;
    run_element_index++;
    seen[current_addr] = true;

  }
 // SPDLOG_LOGGER_DEBUG(log, "Size of output: {}\n", graph_encoding.size());
  m_encoding_size = run_element_index;
  m_BFS_search_BB_sort = bfs_search_BB_sort;

  // hash the encoding and store it in graph's corresponding member


  encoding_to_md5(m_encoding_before_hashing, m_encoding_size, m_encoding_hash);

}

uint Site::getEncodingSize() const{
  return m_encoding_size;
}

uint16_t* Site::getEncodingBeforeHashing() const{
  return m_encoding_before_hashing;
}

uint8_t* Site::getEncodingHash() const{
  return m_encoding_hash;
}

uint8_t getRegClassHexValue(x86_reg_class reg){
  uint8_t class_hex;
  switch(reg){
    case X86_CLASS_INVALID:
      class_hex = 0x0;
      break;
    case X86_CLASS_GEN_REG:
    	class_hex = 0x1;
    /*case X86_CLASS_A:
      class_hex = 0x1;
      break;
    case X86_CLASS_C:
      class_hex = 0x2;
      break;
    case X86_CLASS_D:
      class_hex = 0x3;
      break;
    case X86_CLASS_B:
      class_hex = 0x4;
      break;
    case X86_CLASS_SP:
      class_hex = 0x5;
      break;
    case X86_CLASS_BP:
      class_hex = 0x6;
      break;
    case X86_CLASS_SI:
      class_hex = 0x7;
      break;
    case X86_CLASS_DI:
      class_hex = 0x8;
      break;
    case X86_CLASS_R:
      class_hex = 0x9;
      break;*/
    case X86_CLASS_SEGMENT:
      class_hex = 0xa;
      break;
    case X86_CLASS_EFLAGS:
      class_hex = 0xb;
      break;
    case X86_CLASS_INST:
      class_hex = 0xc;
      break;
    case X86_CLASS_CR:
      class_hex = 0xd;
      break;
    case X86_CLASS_DR:
      class_hex = 0xe;
      break;
    case X86_CLASS_MYSTERY:
      class_hex = 0xf;
      break;
    default:
      class_hex = 0x0; // TODO: is this intelligent ?
  }
  return class_hex;
}

bool df_vect_sort_fun(sptrBvertex b1, sptrBvertex b2){
  return b1->reg_class_name < b2->reg_class_name;
}

void Site::hashDataflow(std::shared_ptr<spdlog::logger> log){
  uint8_t * site_dataflow_encoding = nullptr;
  uint8_t vertex_encoding=0;
  uint graph_index=0;

  m_encoding_size = m_dataflow_graphs.size();
  site_dataflow_encoding = new uint8_t[m_encoding_size*NUMBER_REG_CLASSES*sizeof(uint8_t)];

  for(sptrBipartite b_graph : m_dataflow_graphs){

    biGraphVect df_vector_copy = b_graph->getDataflowVector();
    std::sort(df_vector_copy.begin(), df_vector_copy.end(), df_vect_sort_fun);

    for(sptrBvertex vertex : df_vector_copy){
      // TODO/ make sure they are sorted
      vertex_encoding = getRegClassHexValue(vertex->reg_class_name);
      vertex_encoding = vertex_encoding << 4;
      if(vertex->source_regs.size() > 15){
        log->info("WARNING: More than 16 children for the current reg class. This should not be. Aborting.\n");
        return;
      }
      vertex_encoding = vertex_encoding | vertex->source_regs.size();
      site_dataflow_encoding[graph_index] = vertex_encoding;
      graph_index++;
    }
  }
  
  encoding_to_md5(site_dataflow_encoding, m_encoding_size*NUMBER_REG_CLASSES*sizeof(uint8_t), m_dataflow_hash);
  delete[] site_dataflow_encoding;
}

uint8_t* Site::getDataflowHash() const{
  return m_dataflow_hash;
}

void Site::extractSiteDataflowGraph(){
  // NOTE: Depreciated technique.
  for(sptrBasicBlock BB : getBFSSearchBBSort()){
    sptrBipartite b_graph_ptr = std::make_shared<BipartiteGraph>(BB->getEntryPointAddr(), BB->getInstrs());
    // TODO: Check why... sometimes the address is 0
    if(BB->getEntryPointAddr() != 0){
      //b_graph_ptr->createBipartiteGraphFromInstrs();
      m_dataflow_graphs.push_back(b_graph_ptr);
    }
    else{
      std::cout << "ERROR: Basic block ep addr is: " << addr2s(BB->getEntryPointAddr()) << " ! Skippping dataflow creation.\n";
    }
  }
}
/*
void Site::extractSiteBBDataflowGraphs(){
  // NOTE: Depreciated technique.
  // use extractSiteDataflowGraph instead
  for(sptrBasicBlock BB : getBFSSearchBBSort()){
    sptrBipartite b_graph_ptr = std::make_shared<BipartiteGraph>(BB->getEntryPointAddr(), BB, BB->getLastInstrType());
    // TODO: Check why... sometimes the address is 0
    if(!BB->getEntryPointAddr() == 0){
      b_graph_ptr->createBipartiteGraphFromNode();
      m_dataflow_graphs.push_back(b_graph_ptr);
    }
    else{
      std::cout << "ERROR: Basic block ep addr is: " << addr2s(BB->getEntryPointAddr()) << " !\n";
    }
  }
}
*/

/*
void Site::dataflowExtractionPhase2(std::map<sptrBasicBlock, uint> chemin){

  // we take the chemin i and try to find all its paths
  // from entry point to every exit point (the uint in the map: 0 = entry, 1 = exit, 2 = else)
  
  liste_path = {};
  exhausted_paths = {};
  tmp_exhausted_nodes = {};

  queue = [];
  path = [getEntryPoint(chemin)];

  while(path){

    current_node = path[-1]; // choose the next node and pop it from the path

    if(chemin.at(current_node)==1){
        // reached an exit. we note that somewhere and start the exploration from the previous node again
        exhausted_paths.insert(path);
        path.pop(); // remove the last node
        continue;
    }

    next_node = chooseNeighbour(current_node); // we choose the least frequent node already present in the path

    if(!(path + current_node) in exhausted_paths){

      path += current_node;
      voisins = getChildren(current_node);
      path.push(voisins);

    }
    else{

    }

  }


}
*/
// Printing / Outputing
/*
std::string Site::printSiteDataflow(){
  std::stringstream f;
  for(auto const& [exit_bb,bipartite_ptr] : getDataflowTensor()){
    f << "\n";
    f << "\t" << addr2s(bipartite_ptr->getEntryPointAddr()) << " ";
    f << bipartite_ptr->toString();
  }
  f << "\n";
  return f.str();
}
*/


std::string Site::getDataflowEncoding() const{
  std::stringstream f;
  for(sptrBipartite b_ptr : getSiteDataflow()){
    f << b_ptr->toString();
  }
  return f.str();
}

std::string Site::getBBDataflowEncoding(sptrBasicBlock bb) const{
  std::stringstream f;
  f << getBBDataflow(bb)->toString();  
  return f.str();
}


} // namespace boa
