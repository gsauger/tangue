#include "bipartite_graph.hpp"

namespace boa
{

uint getRegIndexInDFVector(std::vector<sptrBvertex> reg_dataflow_vector, x86_reg_class reg){
    uint index=0;
    for(sptrBvertex bvertex : reg_dataflow_vector){
        if(bvertex->reg_class_name == reg){return index;}
        index++;
    }
    throw std::runtime_error("Error: reg " + reg2str(reg) + " was not in dataflow vector\n");
}
// MARK:- Constructors and destructors

/*
BipartiteGraph::BipartiteGraph(addr_t BB_ep_addr, sptrBasicBlock BB, InstrType type){
    m_ep_addr = BB_ep_addr;
    m_bb = BB;
    m_last_instr_type = type;
    // here fill the sets of the graph with registers from boa_types
}*/

BipartiteGraph::BipartiteGraph(addr_t ep_addr, const std::vector<sptrInstr>& instr_vect){
    m_ep_addr = ep_addr;
    m_instr_vect = instr_vect;
    //m_last_instr_type = instr_vect.back()->getType();

    // Init the one reg vect
  	for(x86_reg_class reg_class_int = X86_CLASS_BEGIN_ONE_GEN_REG; reg_class_int <= X86_CLASS_ENDING_ONE_GEN_REG; reg_class_int = (x86_reg_class)(reg_class_int+1)){
      sptrBvertex BV = std::make_shared<BVERTEX>();

      //BV.reg_name = reg_int;
      BV->reg_class_name = reg_class_int;
      BV->source_regs.insert(reg_class_int);
      m_one_gen_reg_node_vector.push_back(BV);
    }

    createBipartiteGraphFromInstrs();
}

BipartiteGraph::BipartiteGraph(addr_t ep_addr){
    m_ep_addr = ep_addr;

    for(x86_reg_class reg_class_int = X86_CLASS_BEGIN; reg_class_int <= X86_CLASS_ENDING; reg_class_int = (x86_reg_class)(reg_class_int+1)){
        sptrBvertex BV = std::make_shared<BVERTEX>();
        //BV.reg_name = reg_int;
        BV->reg_class_name = reg_class_int;
        //BV->source_regs.insert(reg_class_int); TODO: check if this should be commented or not
        m_bipartite_graph_vector.push_back(BV);
    }
}
void BipartiteGraph::updateOneRegGenVector() const{
	// Updates the one reg gen vector to have it match the bipartite_graph_vector
	uint gen_reg_index = getRegIndexInDFVector(m_one_gen_reg_node_vector, x86_reg_class::X86_CLASS_GEN_REG);
  for(auto const& bvertex_ptr : m_bipartite_graph_vector){
  	uint current_reg_index = getRegIndexInDFVector(m_one_gen_reg_node_vector, bvertex_ptr->reg_class_name);
  	if(isRegGeneralPurpose(bvertex_ptr->reg_class_name)){
			for(auto const& source : bvertex_ptr->source_regs){
				// make all source regs point to gen_reg_index
				if(isRegGeneralPurpose(source)){
					m_one_gen_reg_node_vector.at(gen_reg_index)->source_regs.insert(x86_reg_class::X86_CLASS_GEN_REG);
				}
				else{
					m_one_gen_reg_node_vector.at(gen_reg_index)->source_regs.insert(source);
				}
			}
  	}
  	else{
  		for(auto const& source : bvertex_ptr->source_regs){
  			if(isRegGeneralPurpose(source)){
  				m_one_gen_reg_node_vector.at(current_reg_index)->source_regs.insert(x86_reg_class::X86_CLASS_GEN_REG);
  			}
  			else{
  				m_one_gen_reg_node_vector.at(current_reg_index)->source_regs.insert(source);
  			}
  		}
  	}
  }
	return;
}

/*
void BipartiteGraph::initToIdentity(){
    for(x86_reg_class reg_class_int = X86_CLASS_BEGIN; reg_class_int <= X86_CLASS_ENDING; reg_class_int = (x86_reg_class)(reg_class_int+1)){
        getBvertexOfReg(reg_class_int)->source_regs.insert(reg_class_int);
    }
}
*/


/**
 * Checks if, for two given sets of source regs (from two dataflow graph nodes), they are the same
 * We check that the number of gen regs are the same in the 2 sets. That's all for the genregs.
 * We compare the remaining registers.
 **/
bool areSourceRegsEqual(std::set<x86_reg_class> set1, std::set<x86_reg_class> set2){
    if(set1.size() != set2.size()){ return false; }
    std::unordered_set<x86_reg_class> tmp_reg_class; // to store the NOT GENREG of set1
    uint set2_gen_reg_count=0;
    for(auto const& reg1 : set1){
        if(!isRegGeneralPurpose(reg1)){
            tmp_reg_class.insert(reg1);
        }
    }
    for(auto const& reg2 : set2){
        if(isRegGeneralPurpose(reg2)){
            set2_gen_reg_count++;
        }
    }

    if(set2_gen_reg_count != (set1.size()-tmp_reg_class.size())){
        // different number of gen registers
        return false;
    }

    for(auto const& reg1 : tmp_reg_class){
        if(set2.count(reg1)==0){
            return false;
        }
    }

    return true;
}

static sptrBvertex getNextNodeBipartite(std::map<sptrBvertex, std::unordered_set<sptrBvertex>>& graph_matches, std::map<sptrBvertex, bool>& graph_nodes1){
  for(auto const& [graph_X, matching_graphs] : graph_matches){
    if(graph_nodes1.at(graph_X)){
      return graph_X;
    }
  }
  return nullptr; // didn't find any
}

static uint backtrackingBipartite(std::map<sptrBvertex, bool>& graph_nodes1,std::map<sptrBvertex, bool>& graph_nodes2, std::map<sptrBvertex, std::unordered_set<sptrBvertex>>& graph_matches, uint max_pairs, std::shared_ptr<spdlog::logger> log, uint depth, uint* n_pairs){
    uint n_max=0;
    std::string depth_str, matches_str;
    SPDLOG_LOGGER_TRACE(log, "{} *n_pairs = {}\n",depth_str, *n_pairs);
    for(uint i=0; i <= depth; i++){
        depth_str+="-";
    }
    sptrBvertex min_arity_node = getNextNodeBipartite(graph_matches, graph_nodes1);
    if(min_arity_node == nullptr || *n_pairs == max_pairs){ // case where no node was available
        SPDLOG_LOGGER_TRACE(log, "{} No more nodes available !\n",depth_str);
        return *n_pairs;
    }
    graph_nodes1.at(min_arity_node) = false; // Node is visited and not allowed anymore
    for(auto const& mate : graph_matches.at(min_arity_node)){
        matches_str += " " + reg2str(mate->reg_class_name);
    }
    SPDLOG_LOGGER_DEBUG(log, "{}Backtracking node {}... matches: {}\n",depth_str , reg2str(min_arity_node->reg_class_name), matches_str);

    for(auto const& mate : graph_matches.at(min_arity_node)){
        if(graph_nodes2.at(mate)){ // if node is allowed
            SPDLOG_LOGGER_TRACE(log, "{}Visiting {}\n",depth_str, reg2str(mate->reg_class_name));

            graph_nodes2.at(mate) = false; // we note it as visited
            (*n_pairs)++;
            n_max = std::max(n_max, 1 + backtrackingBipartite(graph_nodes1, graph_nodes2, graph_matches, max_pairs, log, depth+1, n_pairs));
            if(n_max == max_pairs ){
                return n_max;
            }
            if(*n_pairs == max_pairs){
                return *n_pairs;
            }
            (*n_pairs)--;
            graph_nodes2.at(mate) = true; // set mate access bool back to true (for next iterations)
        }
    }
    graph_nodes1.at(min_arity_node) = true; // same as previous mate access bool, for the min arity node

    return n_max;
}

/**
 * Checks if two bipartite graphs are stricly the same. This checks:
 * - The exit address
 * - The graph equivalence, without "forgetting" any label
 *   @returns: true if the two graphs are the same
 **/
bool BipartiteGraph::isEqual(const std::shared_ptr<BipartiteGraph> b2,std::shared_ptr<spdlog::logger> log){
 	// last instr type are differents ?
  // if(this->getLastInstrType() != b2->getLastInstrType()){return false;}

  for(uint i=0; i < this->getDataflowVector().size(); i++){
  	// compare sources of both dfs at elt i
  	if(getDataflowVector().at(i)->source_regs != b2->getDataflowVector().at(i)->source_regs){return false;}
  }
  return true;
}

/**
 * Different "modes" of comparisons to be tested.
 * @return True if the two dataflow graphs are equivalent. False otherwise.
 **/
bool BipartiteGraph::isEquivalentTo(const std::shared_ptr<BipartiteGraph>& b2,std::shared_ptr<spdlog::logger> log,const std::string& bgraph_comp_method) const{
    //if(this->getLastInstrType() != b2->getLastInstrType()){return false;} // last instr type are differents ?
    bool output=false;

  	if(bgraph_comp_method == "backtracking"){

    	uint nb_id_1=0, nb_id_2=0, n_max_pairs=0;
    	uint* n_pairs = new uint;
    	*n_pairs=0;
    	std::unordered_set<sptrBvertex> tmp_matches;
    	std::map<sptrBvertex, std::unordered_set<sptrBvertex>> bp_matches;
    	std::map<sptrBvertex, bool> bp_nodes1, bp_nodes2;

    	if(this->getDataflowVector().size() != b2->getDataflowVector().size()){return false;} // graph sizes are differents ? (should never happen)

    	for(sptrBvertex const& node : this->getDataflowVector()){
        	nb_id_1 += node->source_regs.size();
    	}
    	for(sptrBvertex const& node : b2->getDataflowVector()){
        	nb_id_2 += node->source_regs.size();
    	}
    	if(nb_id_1 != nb_id_2){return false;} // different number of sources (edges)

    	// ----- get matches of bg nodes
    	for(sptrBvertex const& node1 : this->getDataflowVector()){
        	tmp_matches.clear();
        	for(auto const& node2 : b2->getDataflowVector()){
            	if(areSourceRegsEqual(node1->source_regs, node2->source_regs)){ // we found a match for df_graph_1
                	tmp_matches.insert(node2);
            	}
        	}
        	bp_matches.insert({node1, tmp_matches});
        	bp_nodes1.insert({node1, true});
    	}
    	for(auto const& node2 : b2->getDataflowVector()){
        	bp_nodes2.insert({node2, true});
    	}
    	// -----

    	// ----- recursive function
    	SPDLOG_LOGGER_TRACE(log, "\tAbout to backpropagate bipartite\n");
    	n_max_pairs = backtrackingBipartite(bp_nodes1, bp_nodes2, bp_matches, getDataflowVector().size(), log, 0, n_pairs);
    	SPDLOG_LOGGER_TRACE(log, "\tDone. Found {} pairs\n", n_max_pairs);
    	delete n_pairs;
    	output = n_max_pairs == getDataflowVector().size();
		}
		else if(bgraph_comp_method == "all_reg_different"){
    	for(uint i=0; i< this->getDataflowVector().size(); i++){
    		// keep in memory every node already matched (non allowed blocks)
        if(this->getDataflowVector().at(i)->source_regs != b2->getDataflowVector().at(i)->source_regs){ // source regs for a given reg are differents ?
        	return false;
        }
    	}
    	output = true;
    }
    else if(bgraph_comp_method == "gen_reg_one_node"){
    	// check if the 2 bgraphs already have their "one_gen_reg_node" filled; otherwise fill it
    	if(this->getOneGenRegNodeVect().size()==0){
    		this->updateOneRegGenVector();
    	}
    	if( b2->getOneGenRegNodeVect().size()==0 ){
    		b2->updateOneRegGenVector();
    	}

    	// HERE compare the two m_one_gen_reg_node_vector (should be pretty straigthforward)
  		for(uint i=0; i < this->getOneGenRegNodeVect().size(); i++){
  			for(auto const& source : getOneGenRegNodeVect().at(i)->source_regs){
  				if(!b2->getOneGenRegNodeVect().at(i)->source_regs.count(source)){ return false; }
  			}
  		}
			output = true;
		}
		return output;
}

// MARK:- Setters and getters

/**
 * @return A vector of x86_reg_class containing the sorted reg classes that were sources to all the read regs
 * */
std::set<x86_reg_class> getSourceRegsInDFVector(std::vector<sptrBvertex> reg_dataflow_vector, std::vector<x86_reg_class> read_reg_vect){
    std::set<x86_reg_class> output;
    std::unordered_set<x86_reg_class> seen_regs;
    for(auto const& read_reg : read_reg_vect){     // for every read reg
        for(sptrBvertex bvertex : reg_dataflow_vector){ // check every vertex
            if(bvertex->reg_class_name == read_reg){ // we found it
                for(auto const& source_reg : bvertex->source_regs){ // add all  source regs to output
                    if(!seen_regs.count(source_reg)){
                        seen_regs.insert(source_reg);
                        output.insert(source_reg);
                    }
                }
                continue; // go to next read reg
            }

        }
    }
    // std::sort(output.begin(), output.end()); (maybe not ?)
    return output;

}


/**
 * Updates the dataflow graph's registers's source registers according to the current instruction
 **/
void updateRegisterDataflow(std::vector<sptrBvertex>* reg_dataflow_vector, uptrConstInstr inst){

    if(inst->getWrittenRegsClasses().size()==0){ return; }
    uint written_reg_index=0;
    if(inst->getReadRegsClasses().size()==0){
        // special case where regs have been written but none have been read
        // in that case we clean the written regs of any reg taint
        for(auto const & written_reg_class : inst->getWrittenRegsClasses()){
            written_reg_index = getRegIndexInDFVector(*reg_dataflow_vector, written_reg_class);
            reg_dataflow_vector->at(written_reg_index)->source_regs.clear();
        }
        return;
    }

    // here we now have written registers AND read registers.
    // we assume that, apart for exceptions, EVERY written register is tainted by ALL the read ones

    /*

    Exceptions go there : XCHG, CPUI, CLEAR idioms

    */

    for(auto const & written_reg_class : inst->getWrittenRegsClasses()){
        written_reg_index = getRegIndexInDFVector(*reg_dataflow_vector, written_reg_class);
        reg_dataflow_vector->at(written_reg_index)->source_regs = getSourceRegsInDFVector(*reg_dataflow_vector, inst->getReadRegsClasses());
        // change the value by the source regs at the read reg indices in the vector
    }
}

void BipartiteGraph::createBipartiteGraphFromInstrs(){
    // vector of sptrBvertex, each sptrBvertex representing the tainting state of the registers CLASS !
    std::vector<sptrBvertex> reg_dataflow_vector;
    std::vector<sptrBvertex> one_node_gen_reg_vector;
    //std::set<x86_reg_class> seen_reg_class;
    //uint nb_of_reg_classes = 0;
    //x86_reg_class current_reg_class;
    // Init the datafow vector
    std::vector<x86_reg_class> tmp_init_vect;
    std::sort(tmp_init_vect.begin(), tmp_init_vect.end());

    // we init the reg_dataflow_vector
  for(x86_reg_class reg_class_int = X86_CLASS_BEGIN; reg_class_int <= X86_CLASS_ENDING; reg_class_int = (x86_reg_class)(reg_class_int+1)){
        sptrBvertex BV = std::make_shared<BVERTEX>();

        //BV.reg_name = reg_int;
        BV->reg_class_name = reg_class_int;
        BV->source_regs.insert(reg_class_int);
        reg_dataflow_vector.push_back(BV);
    }

    // Iterate over the instructions to modify the dataflow vector
    for(auto const& inst : m_instr_vect){
        updateRegisterDataflow(&reg_dataflow_vector, inst);
    }

    m_bipartite_graph_vector = reg_dataflow_vector;
}

std::set<x86_reg_class> BipartiteGraph::getRegsTaintedBy(x86_reg_class input_reg) const{
    std::set<x86_reg_class> output;
    for(auto const& node : getDataflowVector()){
        if(node->source_regs.count(input_reg)>0){
            output.insert(node->reg_class_name);
        }
    }

    return output;
}

/**
 * 
 * "Manually" adds tainting_reg to the list of the source regs of reached_reg
 * */
void BipartiteGraph::addTaintingRegsTo(x86_reg_class reached_reg, x86_reg_class tainting_reg){
    m_bipartite_graph_vector.at(reached_reg)->source_regs.insert(tainting_reg);
}

sptrBvertex BipartiteGraph::getBvertexOfReg(x86_reg_class reg) const{
    for(auto const& bvertex : getDataflowVector()){
        if(bvertex->reg_class_name == reg){
            return bvertex;
        }
    }
    std::cout <<"Error: reg class not a vertex in the current dataflow graph ! Aborting\n";
    return nullptr;
}

// Printing

std::string BipartiteGraph::toString() const{
    // TODO: need the m_bb to only print the regs that appear in its instructions
    // also TODO: rename de registers a better way
    // We want to print a vector with the dataflow data
    std::stringstream f;
    f << "[";
    for(sptrBvertex bvertex : m_bipartite_graph_vector){
        if(bvertex->reg_class_name == x86_reg_class::X86_CLASS_INVALID){continue;} // ugly
        if(bvertex->source_regs.size()==1){
            if(bvertex->source_regs.count(bvertex->reg_class_name)>0){continue;} // We don't print the unchanged regs
        }
        f << reg2str(bvertex->reg_class_name) << ":" << regVect2str(bvertex->source_regs) << " ";
    }
    f << "]";
    return f.str();
}

std::string BipartiteGraph::instrsToString() const{
    std::stringstream f;
    f << " ";
    if(m_instr_vect.size() != 0){
        for(auto const& instr : m_instr_vect){
            f << instr->toStringLight() << "\\l";
        }
    }
    else{
        f << addr2s(m_ep_addr) << " -> " << addr2s(m_exit_addr);
    }
    return f.str();
}

/**
 * We choose not to represent nodes that are identity (only points to themselves)
 * */
std::string BipartiteGraph::toGraphString(const std::string& dataflow_id){
    
    std::stringstream f;
    std::string src,dst;
    std::unordered_set<x86_reg_class> regs_to_ignore;
    for(auto const& bvertex : getDataflowVector()){
        if(bvertex->source_regs.size() == 1){
            if(bvertex->source_regs.count(bvertex->reg_class_name) >0 ){
                regs_to_ignore.insert(bvertex->reg_class_name);
            }
        }
    }
    for(auto const& bvertex : getDataflowVector()){

        for(auto const& source_reg : bvertex->source_regs){
        if(source_reg != bvertex->reg_class_name){
            if(regs_to_ignore.count(source_reg)>0){
                regs_to_ignore.erase(source_reg);
                }
            }
        }
    }

    f << "\"" + dataflow_id + "\"[label =\"\" style=\"invis\"]\n";

    // init nodes

    for(x86_reg_class reg_class_int = X86_CLASS_BEGIN; reg_class_int <= X86_CLASS_ENDING; reg_class_int = (x86_reg_class)(reg_class_int+1)){
        if(regs_to_ignore.count(reg_class_int)>0){
            continue;
        }
        f << "\"" + reg2str(reg_class_int) + dataflow_id +"\"";
        f << "[label=\"" + reg2str(reg_class_int) + "\"]\n";
    } 

    // add edges
    for(auto const& bvertex : getDataflowVector()){
        if(regs_to_ignore.count(bvertex->reg_class_name)>0){
            continue;
        }
        dst = reg2str(bvertex->reg_class_name) + dataflow_id;
        for(auto const& source_reg : bvertex->source_regs){
            src = reg2str(source_reg) + dataflow_id;
            f << src + "->" + dst + "\n";
        }
    }
    return f.str();
}


// Concatenation
/*

  Creates a new graph, that is the concatenation of the current graph and the second one.
  @arg A pointer to the second graph, to concatenate to this one
  @return A pointer to the newly created graph. 

sptrBipartite BipartiteGraph::concatenateAndCreateNewGraphWith(sptrBipartite second_graph, std::shared_ptr<spdlog::logger> log)const{
    // TODO: Make it work, it returns empty everything !
    sptrBipartite output_graph = std::make_shared<BipartiteGraph>(getEntryPointAddr());
    output_graph->setExitAddr(second_graph->getEntryPointAddr());

    //SPDLOG_LOGGER_DEBUG(log, "-> Concatening {} and \n {}\n", toString(), second_graph->toString());
    for(auto& out_bvertex : output_graph->getDataflowVector()){
        for(auto const & current_source_reg : second_graph->getBvertexOfReg(out_bvertex->reg_class_name)->source_regs){
            // for each source reg in the second's dataflow vector's current bvertex
            // get which reg tainted it in the first graph
            for(auto const& reg: getBvertexOfReg(current_source_reg)->source_regs){
                out_bvertex->source_regs.insert(reg);
            }
        }
    }

    return output_graph;
}
*/


} // namespace boa
