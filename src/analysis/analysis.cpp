#include "analysis.hpp"
#include "spdlog/fmt/bin_to_hex.h"

namespace boa {

AnalysisEngine::AnalysisEngine(std::shared_ptr<CFG> cfg, uint sites_size, sptrBinary bin_ptr, const Config* config) : m_config(config), m_binary(bin_ptr){
  m_log = spdlog::get(utils::analysis_logger);
  m_cfg = cfg;
  m_sites_size = sites_size;
  SPDLOG_LOGGER_TRACE(m_log, "AnalysisEngine::AnalysisEngine()\n");

  //m_functions = getFuncsFromPairs();
	//m_fun_to_analyze = &m_functions;

}

AnalysisEngine::AnalysisEngine( sptrBinary bin_ptr, const Config* config) : m_config(config), m_binary(bin_ptr) {
  m_log = spdlog::get(utils::analysis_logger);
  SPDLOG_LOGGER_TRACE(m_log, "AnalysisEngine::AnalysisEngine()\n");

	//m_functions = getFuncsFromPairs();
	//m_fun_to_analyze = &m_functions;
}

AnalysisEngine::~AnalysisEngine() {
  SPDLOG_LOGGER_TRACE(m_log, "AnalysisEngine::~AnalysisEngine()\n");
}

// Getters and setters

void AnalysisEngine::setCFG(std::shared_ptr<CFG> cfg){
  m_cfg = cfg;
}

/*
void AnalysisEngine::removeFunction(sptrFunc fun){
	getFunctions().erase(fun);
}
*/

std::string AnalysisEngine::getBinName() const{
  return utils::base_name(getBinary()->getName());
}

std::string AnalysisEngine::getBinSimpleName() const{
  return utils::bin_simple_name(getBinary()->getName());
}

std::shared_ptr<CFG> AnalysisEngine::getCFG() const{
  return m_cfg;
}

void AnalysisEngine::setSitesSize(uint size){
  m_sites_size = size;
}
/*
std::vector<std::shared_ptr<Site>> AnalysisEngine::getSites() const{
  std::vector<std::shared_ptr<Site>> output_vect;
  for (auto const& fun : m_functions){
    for(auto const& site : fun->getSites()){
      output_vect.push_back(site);
    }
  }
  return output_vect;
}*/

unsigned int AnalysisEngine::getNumberOfSites() const{
  unsigned int nb = 0;
  for(std::shared_ptr<Function> fun : m_functions){
     nb += fun->getSites().size();    
    }
  return nb;
}

std::shared_ptr<Site> AnalysisEngine::getSite(addr_t addr){
  for( auto const& fun : m_functions){
    for(auto const& site : fun->getSites()){
      if(site->getEntryPointAddr() == addr){
        return site;
      }
    }
  }
  throw std::runtime_error("Could not find site with entry addr " + addr2s(addr) + " in current binary\n");
}

std::shared_ptr<Site> AnalysisEngine::getSiteCheckingAlsoEnd(addr_t addr){
  for( auto const& fun : m_functions){
    for(auto const& site : fun->getSites()){
      if(site->getEntryPointAddr() == addr){
        return site;
      }
    }
  }
  for( auto const& fun : m_functions){
    for(auto const& site : fun->getSites()){
      if(site->getEntryPointAddrSigtoolStyle() == addr){
        return site;
      }
    }
  }  
  throw std::runtime_error("Could not find site with entry addr " + addr2s(addr) + " in current binary\n");
}

uint AnalysisEngine::getNonEmptyFuncNbr() const{
  uint nbr = 0;
  for(auto const& fun : m_functions){
    if(fun->getSites().size() != 0){ nbr++; }
  }
  return nbr;
}


// Analysis

    /**
    Computes if a BB has an edge targeting it, inside a function.
    @param function Function object.
    @param BB Basic Block object.
    @return True if there is an edge in function the BB
    */
bool AnalysisEngine::isThereEdgeToBB(std::shared_ptr<Function> function, sptrBasicBlock BB){
  for(auto const& [BB_addr, basic_block] : function->getBasicBlocks()){
    for(auto const& [waddr, edge] : basic_block->getEdges()){
      if(edge.get()->getDst().second == BB->getEntryPointAddr()){ 
        return true; }
    }
  }
  return false;
}

    /**
    Performs a BFS in a CFG starting from a BB.
    Colors all connected BB the same way.
    Ignores CALL instructions edges during search.
    Search is interrupted when reaching another known function's symbol.
    @param cfg CFG to search.
    @param current_function Function object to be filled during the search.
    @param current_BB Basic Block where the search begins.
    @param current_color Color of the function being searched.
    */
void AnalysisEngine::colorGraphFromBB(std::shared_ptr<CFG> cfg, std::shared_ptr<Function> current_function, sptrBasicBlock initial_BB, color_t current_color){
    std::queue<sptrBasicBlock> BFS_queue;
    //std::map<addr_t, bool> seen_BFS;
    std::set<addr_t> seen_BFS;
    sptrBasicBlock current_BB;
    addr_t current_addr;
    addr_t next_addr;
    //current_function->setName("fun_" + std::to_string(current_function->getColor()));
    SPDLOG_LOGGER_DEBUG(m_log, "---- Starting coloring graph from {:x} : {}\n", initial_BB->getEntryPointAddr(), current_function->getName());

    BFS_queue.push(initial_BB);

    while(!BFS_queue.empty()){

      current_BB = BFS_queue.front();
      if(current_BB == NULL){ continue; }
      current_addr = current_BB->getEntryPointAddr();
      BFS_queue.pop();
      if(seen_BFS.count(current_addr)) { continue; }
      if((cfg->getFunSymbolsMap().count(current_addr) > 0)){ // if we hit a symbol
        if(cfg->getFunSymbolsMap().at(current_addr) != current_function->getName()){ // and it doesn't correspond to current function's name
          continue; // it means we've hit the start of another function, so we skip it
        }
      }

      //SPDLOG_LOGGER_INFO(m_log, "Current node is {:x}\n", current_BB->getEntryPointAddr());
      seen_BFS.insert(current_addr);
      current_BB->setColor(current_color);
      current_BB->setFunName(current_function->getName());
      current_function->insertBasicBlock(current_BB); // ? why so complicated

      // add edges target to queue, except if CALL
      if(current_BB->getLastInstrType() == InstrType::CALL){
        // We only add the nextAddr (instruction after the call)

        next_addr = current_BB->getLastInstr()->getNextAddr();
        SPDLOG_LOGGER_DEBUG(m_log, "\t\tAdded {:x} to queue, from BB {:x} (CALL - next BB)\n", next_addr, current_addr);
        BFS_queue.push(cfg->getBasicBlock(next_addr));

      }
      else{
        const std::map<waddr_t, sptrConstEdge>& current_edges = current_BB->getEdges();
        // Add all childrens of this BB
        for(auto const& [key, edge] : current_edges){
          next_addr = edge.get()->getDst().second;
          SPDLOG_LOGGER_DEBUG(m_log, "\t\tAdded {:x} to queue, from BB {:x} (NO CALL - child BB)\n", next_addr, current_addr);
          BFS_queue.push( cfg->getBasicBlock( next_addr));
        }

      }

      // But also the BBs that points to current_BB, except if they are CALL inst
      // NOTE: and except if the current BB is the ep of the function
      if(current_addr == current_function->getEntryPointAddr()){
        continue;
      }
      for(auto const& edge : cfg->getEdges()){
        if(edge->getDst().second==current_addr){
          if(cfg->getBasicBlock(edge->getSrc().second) == NULL){continue;}
          if(cfg->getBasicBlock(edge->getSrc().second)->getLastInstrType() != InstrType::CALL ||
            cfg->getBasicBlock(edge->getSrc().second)->getLastInstr()->getJmpAddr() != current_addr){
              next_addr = edge->getSrc().second;
            SPDLOG_LOGGER_DEBUG(m_log, "\t\tAdded {:x} to queue, from BB {:x} (parent BB)\n", next_addr, current_addr);
            BFS_queue.push(cfg->getBasicBlock(next_addr));
          }
        }
      }
    }

    SPDLOG_LOGGER_DEBUG(m_log, "Finished coloring graph from {:x}\n", initial_BB->getEntryPointAddr());

}

    /**
    Computes the entry points of a given function.
    Only called if the fun has entry point 0; and should not be in the symbols.
    If the function has no entry point already, finds a BB targeted by no edges.
    If none is found, get the lowest address BB.
    @param function A function of the binary.
    */
void AnalysisEngine::computeEntryPoints(std::shared_ptr<CFG> cfg, std::shared_ptr<Function> function){

  bool init=true;
  addr_t min_addr = 0;

  for(auto const& [BB_addr,BB] : function->getBasicBlocks()){

    if(init){
      min_addr = BB_addr;
      init=false;
    }

    if(BB_addr < min_addr){ min_addr = BB_addr;}
    //std::cout << "DEBUG: " << std::hex << BB->getEntryPointAddr() << " in fun: " << function->getColor() << "\n";

    if(!isThereEdgeToBB(function, BB)){
      BB->setEntryBlockBool(true);
      function->setEntryPoint(BB_addr);
      return;
    }      
  // in case of a loop we have to do something too, like:
  //   if the edge points to strictly inside a BB (offset within it)
  //   else I have ot think
  }

  // here, we could not find any ep
  // we just take the lowest addr

  function->setEntryPoint(min_addr);

}

bool epSortFun(addr_t ep1, addr_t ep2){
  return(ep1 > ep2);
}

    /**
    Finds functions boundaries of the analysis engine's cfg.
    Fills the engine's m_functions member.
    This is slow
    */
void AnalysisEngine::setFunctionBoundaries(){

  std::vector<addr_t> fun_entry_points;
  sptrBasicBlock current_BB = NULL;
  std::shared_ptr<Function> current_function;
  color_t current_color = 0;
  std::set<addr_t> seen_ep;
  //std::map<addr_t, bool> seen;

  /*
    Gather all entry points of functions:
    - call targets
    - functions symbols found in the CFG
  */
  for(auto const& [sym_addr, fun_name] : m_cfg->getFunSymbolsMap()){
    // symbols 
    fun_entry_points.push_back(sym_addr);
    //seen_ep[sym_addr] = true;
    seen_ep.insert(sym_addr);
  }

  for(auto const& BB : m_cfg->getBasicBlocks()){
    // call targets (ifnot already seen)
    if(BB.second->getLastInstrType() == InstrType::CALL){
      addr_t target = (BB.second->getLastInstr()->getJmpAddr());
      if(!seen_ep.count(target) && m_cfg->isBasicBlockExists(target)){
        fun_entry_points.push_back(target);
        seen_ep.insert(target);
      }
    }
  }

  std::sort(fun_entry_points.begin(), fun_entry_points.end(), epSortFun);

  SPDLOG_LOGGER_DEBUG(m_log, "{} entry points detected\n", fun_entry_points.size());

  for(auto const& fun_ep : fun_entry_points){

    if(!m_cfg->isBasicBlockExists(fun_ep)){
      SPDLOG_LOGGER_DEBUG(m_log, "Basic block not found for ep {:x}\n", fun_ep);
      continue;
    }
/*
    if(m_cfg->getBasicBlock(fun_ep) != 0){
      continue;
    }
    */

    current_color++;
    SPDLOG_LOGGER_DEBUG(m_log, "Building graph from ep {:x}, color is {}\n", fun_ep, current_color);

    current_BB = m_cfg->getBasicBlock(fun_ep);
    current_BB->setEntryBlockBool(true);

    // and we create a new graph for this new function
    if(m_cfg->getFunSymbolsMap().count(fun_ep)>0){
      SPDLOG_LOGGER_DEBUG(m_log, "And name is {}\n", m_cfg->getFunSymbolsMap().at(fun_ep));
      current_function = insertFunction(fun_ep, current_color, m_cfg->getFunSymbolsMap().at(fun_ep), getBinary()->getName() );
      current_function->setSection(m_cfg->getInstr(fun_ep)->getSectName());
    }
    else{
      // case where we don't have symbol. The name will just be the color
      current_function = insertFunction(fun_ep, current_color, getBinary()->getName());
      current_function->setSection(m_cfg->getInstr(fun_ep)->getSectName());

    }

    //current_function = getFunction(fun_ep);

    colorGraphFromBB(m_cfg, current_function, current_BB, current_color);

  }

  SPDLOG_LOGGER_DEBUG(m_log, "Got all called functions, searching for more...\n", fun_entry_points.size());

  for(auto const &[BB_addr, BB] : m_cfg->getSortedBasicBlocks()){

    if(BB->getColor() != 0){ continue; }

    current_color++;
    SPDLOG_LOGGER_DEBUG(m_log, "Building graph from BB {:x}, color is {}\n", BB_addr, current_color);

    if(m_cfg->getFunSymbolsMap().count(BB_addr)>0){
    current_function = insertFunction(0, current_color, m_cfg->getFunSymbolsMap().at(BB_addr), getBinary()->getName() );
    // we set ep to 0
    current_function->setSection(m_cfg->getInstr(BB_addr)->getSectName());

    }
    else{
    current_function = insertFunction(0, current_color, getBinary()->getName());
    current_function->setSection(m_cfg->getInstr(BB_addr)->getSectName());

    }

    colorGraphFromBB(m_cfg, current_function, BB, current_color);

    computeEntryPoints(m_cfg, current_function); // in this case we need to find the ep of the fun

  }

}

void AnalysisEngine::extractBinarySites(){
  for(auto const& function : getFunToAnalyze()){
    function->extractFunctionSites(m_log);
  }
}

unsigned int vect_to_hex(std::vector<uint32_t> vect){
  // WRONG
  unsigned int buffer=0;
  for(auto const& elt : vect){
    buffer = buffer ^ elt;
    buffer = buffer << 8; // check this ??
  }
  return buffer;
}

std::size_t uint_hashing(unsigned int input){
  std::hash<unsigned int> hash_uint;
  return hash_uint(input);
}

void AnalysisEngine::encodeSites(){
  for(const std::shared_ptr<Function>& fun : m_functions){
    fun->encodeFunctionSites(m_log);
  }
  // save this somewhere in the function's members
}

void AnalysisEngine::hashSites(){
  for(std::shared_ptr<Function> fun : m_functions){
    fun->hashFunctionSites(m_log);
  }
}

void AnalysisEngine::hashDataflow(){
  for(std::shared_ptr<Function> fun : m_functions){
    fun->hashFunctionSitesDataflow(m_log);
  }
}

// Functions

std::shared_ptr<Function> AnalysisEngine::insertFunction(addr_t bb_ep, color_t color, std::string binary_name){
  std::vector<addr_t> vect_ep = {bb_ep};
  std::shared_ptr<Function> new_function = std::make_shared<Function>(vect_ep, m_sites_size, 0, color, getBinOptLvl(), binary_name);
  m_functions.insert(new_function);
  return new_function;
  }

std::shared_ptr<Function> AnalysisEngine::insertFunction(addr_t bb_ep, color_t color, std::string name, std::string binary_name){
  std::vector<addr_t> vect_ep = {bb_ep};
  std::shared_ptr<Function> new_function = std::make_shared<Function>(vect_ep, m_sites_size, 0, color, getBinOptLvl(), name, binary_name);

  m_functions.insert(new_function);

  return new_function;
}

const std::unordered_set<std::shared_ptr<Function>>& AnalysisEngine::getFunctions() const{
  return m_functions;
}

std::shared_ptr<Function> AnalysisEngine::getFunction(addr_t addr) const{

  for(auto const& function : m_functions){
    if(function->getEntryPointAddr() == addr){
      return function;
    }
    if(function->isInstrExists(addr)){
      return function;
    }
  }

  //m_log->info("[WARNING] Could not find a function starting at {} or containing such instr\n",addr2s(addr));
  return nullptr;
}

std::shared_ptr<Function> AnalysisEngine::getFunction( const std::string& name) const{

  for(auto const& function : m_functions){
    if(function->getName() == name){
      return function;
    }
  }
  return nullptr;
}

size_t AnalysisEngine::getNumberofFunctions() const{
  return m_functions.size();
}

// Dataflow

std::unordered_set<sptrFunc> AnalysisEngine::getFuncsFromPairs(){
	std::unordered_set<sptrFunc> funToAnalyze;
	uint nb_bin_match = 0;
  for(auto const& pair_data : getConfig()->getConfigGlobal().test_funcs_pairs){
  	sptrFunc cfunc;
		if(pair_data.bin1 == getBinName()){
			nb_bin_match++;
			cfunc = getFunction(pair_data.fva1);
			if(cfunc == nullptr){
				cfunc = getFunction(pair_data.fname1);
				if(cfunc == nullptr){continue;}
			}
			m_log->debug("\tBin {} matched, csv fun {}, tangue fun: {}\n",pair_data.bin1, pair_data.fname1, cfunc->getName());
			funToAnalyze.insert(cfunc);
		}
  	if(pair_data.bin2 == getBinName()){
			nb_bin_match++;
  		cfunc = getFunction(pair_data.fva2);
			if(cfunc == nullptr){
				cfunc = getFunction(pair_data.fname2);
				if(cfunc == nullptr){continue;}
			}
			m_log->debug("\tBin {} matched, csv fun {}, tangue fun: {}\n",pair_data.bin2, pair_data.fname2, cfunc->getName());
  		funToAnalyze.insert(cfunc);
  	}
  }
  m_log->info("Nb times bin match : {}, nb func matches (should be equal) : {}\n", nb_bin_match, funToAnalyze.size());
  return funToAnalyze;
}

/**
 * Note: if a file with the pairs of functions of interest is provided,
 * we only compute the dataflow of these functions
 **/
void AnalysisEngine::extractSitesDataflowGraphs(){
  // each sites member is filled
  for(auto const& fun_ptr : getFunToAnalyze() ){
    SPDLOG_LOGGER_DEBUG(m_log,"Func {} has {} sites \n",fun_ptr->getName(), fun_ptr->getSites().size());
    fun_ptr->extractSitesDataflow(m_log, getConfig());
  }
}

// Printing

void AnalysisEngine::printOutputLog(sptrLog output_log) const{

  output_log->info("REPORT OF TANGUE LEARN {}\n", getBinName());
  output_log->info("\n--------------------------------\n");
  output_log->info("Part I : Summary\n");
  output_log->info("--------------------------------\n\n");
  output_log->info("\t-> Functions: {}\n", this->getNumberofFunctions());
  output_log->info("\t-> Sites of size {}: {}\n", getSitesSize(), this->getNumberOfSites());
  output_log->info("\t-> Basic blocks: {}\n", getCFG()->getNumberBasicBlocks());
  output_log->info("\t-> Instructions: {}\n", getCFG()->getNumberInstrs());
  output_log->info("\t-> Edges: {}\n", getCFG()->getNumberEdges());
  output_log->info("Part II : Functions details\n");
  output_log->info("--------------------------------\n\n");
  output_log->info("\tEach function -> each site (entry point) -> each dataflow path (path exit point : [dataflow graph])\n\n");
  output_log->info(this->getBinDetailedInfos());
}

bool sortFun(std::shared_ptr<Function> fun1, std::shared_ptr<Function> fun2){
  return(fun1->getEntryPointAddr() < fun2->getEntryPointAddr());
}

std::vector<std::shared_ptr<Function>> AnalysisEngine::getSortedFun() const{
  std::vector<std::shared_ptr<Function>> sorted_fun;
  sorted_fun.insert(sorted_fun.end(), getFunctions().begin(), getFunctions().end());
  std::sort(sorted_fun.begin(), sorted_fun.end(), sortFun);
  return sorted_fun;
}

void AnalysisEngine::printSitesEncoding(){
    // prints the sites encoding stored into the analysis engine's 18 first functions;
    int i = 0;
    for(auto const & fun : this->getFunctions()){
      if(i == 18){break;}
      SPDLOG_LOGGER_DEBUG(m_log, "Number of sites for function  {} : {}\n", fun->getName(), fun->getSites().size());

      for(auto const& site : fun->getSites()){
        std::stringstream ss;
        std::string buffer;
        //SPDLOG_LOGGER_DEBUG(log, "Encoding graph\n");
        int k = 0;
        for(auto const & ep : site->getEntryPoints()){
          if(k==0){ss << std::hex << ep << " "; }
          k++;
        }

        for(auto const& elt: site->getEncoding()){
            //SPDLOG_LOGGER_DEBUG(log, "Size of elt: {:x}\n", elt);
            ss << std::setfill('0') << std::setw(8)<< std::hex << elt <<" ";
        }

        buffer = ss.str();
        ss.clear();
        SPDLOG_LOGGER_DEBUG(m_log, "Site encoding : {}\n", buffer);
      }
      i++;
    }
  }

std::string AnalysisEngine::printSitesSigtoolCompare() const{
  std::stringstream f;
  for(std::shared_ptr<Function> fun : getFunctions()){
    for(sptrSite site : fun->getSites()){
      f << addr2s(site->getEntryPointAddrSigtoolStyle()) << ",";
    }
  }
  std:: string f_str = f.str();
  f_str.pop_back();
  return f_str;
}


std::string AnalysisEngine::getBinDetailedInfos() const{
  std::stringstream f;
  f << getBinary()->getName() << "\n";

  for(std::shared_ptr<Function> const & fun_ptr : getSortedFun()){
    f << "\t";
    f << fun_ptr->getName() << " :{ \n";

    for(sptrSite site_ptr : fun_ptr->getOrderedSites()){
      f << "\t\t";
      //f << addr2s(site_ptr->getEntryPointAddr()) << ": " << site_ptr->printSiteDataflow() <<  " ;\n" ;
      f << addr2s(site_ptr->getEntryPointAddr()) << "( " << addr2s(site_ptr->getEntryPointAddr()-fun_ptr->getEntryPointAddr()) << " )"; // site address (+ offset from fun's ep)
      f << " : " ;
      f << site_ptr->printGraphDataflowTensor("\t\t", fun_ptr->getEntryPointAddr());// site dataflow tensor
    }
    f << " \t\t} #" << std::to_string(fun_ptr->getSites().size()) << ";\n";
  }

  return f.str();
}

std::string AnalysisEngine::printSitesHash() const{
  std::stringstream f;

  for(std::shared_ptr<Function> fun_ptr : getSortedFun()){
    if(fun_ptr->getSites().empty()){
      continue;
    }
    f << "\t";
    f << fun_ptr->getName() << " :{ \n";

    for(sptrSite site_ptr : fun_ptr->getOrderedSites()){
      f << "\t\t";
      for(uint i=0; i<site_ptr->getEncodingSize(); i++){
        f << site_ptr->getEncodingHash()[i] << " ";
      }
      f << "; \n";
    }
    f << " \t\t} #" << std::to_string(fun_ptr->getSites().size()) << ";\n";
  }

  return f.str();
}

std::string AnalysisEngine::printFunctionsSummary() const{
  std::stringstream f;

  f << "\n\t" << "Name Addr Signature Size \n";
  f << "\t-----\n";

  for(auto const& fun : m_functions){
    f << "\t";
    f << fun->getName() << " ";
    f << addr2s(fun->getEntryPointAddr()) << " ";
    f << fun->getSignature() << " ";
    f << std::to_string(fun->getSize()) << " \n";
  }
  return f.str();
}

std::set<std::shared_ptr<Function>> AnalysisEngine::getThunkFuns() const{
  std::set<std::shared_ptr<Function>> out;
  for(auto const& fun : m_functions){
    if(!fun->getSection().compare(".plt")){ out.insert(fun);}
  }
  return out;
}

std::set<std::shared_ptr<Function>> AnalysisEngine::getCoreFuns() const{
  std::set<std::shared_ptr<Function>> out;
  for(auto const& fun : m_functions){
    if(fun->getSection().compare(".plt")){ out.insert(fun);}
  }
  return out;
}

std::string AnalysisEngine::printThunkCoreNumbers() const{
  std::stringstream f;
  f << "\t Thunk functions: " << std::to_string(getThunkFuns().size());
  f << "\n\t Core functions: " << std::to_string(getCoreFuns().size());
  f << "\n";
  return f.str();
    
}

uint AnalysisEngine::getNbFunWithKSites(uint k) const {
    uint res =0;
    for(auto const& fun_ptr : m_functions){
        if(fun_ptr->getSites().size() > k){
            res++;
        }
    }
    return res;
}

std::string AnalysisEngine::printNbrFunctionWithMoreThanKSites() const {
    uint nb_fun = 0;

    std::stringstream f;
    f << "\n-----> " << getBinName() << "\n\n";

    std::array<uint, 5> k_values = {0, 5, 10, 20, 50};

    for(uint k : k_values){
        f << "\t\t";
        nb_fun = getNbFunWithKSites(k);
        f << "k > " << k << " : " << nb_fun << " functions;\n";
    }
    f << "\n";
    return f.str();   
}

std::string AnalysisEngine::createFunMapDotWithoutDigraph(){
  std::stringstream f;
  addr_t src, dst;
// Vertices: function labels
  for(auto const& fun_ptr : getFunctions()){
    f << '"' << addr2s(fun_ptr->getEntryPointAddr()) << "\"[label=\"";
    std::string fun_vertex_label = fun_ptr->getName();
    f << fun_vertex_label;
    f << "\",shape=";
    f << "box,style=\"filled\"";
    f << ", fontweight=\"bold\",fillcolor=\"";
    f << "white";
    f << "\"];" << std::endl;
  }
// Edges: calls and rets
  for(auto const& fun_ptr : getFunctions()){
    src = fun_ptr->getEntryPointAddr();
    for(auto const& call_addr : fun_ptr->getCallAddrs()){
      dst = 0;
      for(auto const& fun_ptr2 : getFunctions()){
        if(dst !=0){
          break;
        }
        for(auto const& bb : fun_ptr2->getBasicBlocks()){
          if(bb.second->isAddrInBB(call_addr)){
            dst = fun_ptr2->getEntryPointAddr();
            break;
          }
        }
      }
      f << "\"" << addr2s(src) << "\" -> \"" << addr2s(dst) << "\"";
      // f << "[ label = \" call from " << addr2s(src) << " to " << addr2s(dst) << "\"];";
      f << std::endl;
    }
  }
  return f.str();
}

std::string AnalysisEngine::createFunMapDot(){
 return "Digraph G {\n" + createFunMapDotWithoutDigraph() + "}";
}

} // namespace boa
