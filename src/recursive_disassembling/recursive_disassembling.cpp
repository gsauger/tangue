#include "recursive_disassembling.hpp"

#include <deque>
#include <set>
#include <sstream>
#include <stack>

namespace boa
{

// Constructors and destructors
RecursiveDisassembling::RecursiveDisassembling(Disassembler* disassembler, Binary& binary,
                                               const config_rec_disassembling_t& config_rec_disassembling,
                                               const config_global_t& config_global)
    : m_disassembler(disassembler), m_binary(binary), m_config_rec_disassembling(config_rec_disassembling),
      m_config_global(config_global)
{
  m_log = spdlog::get(utils::rec_disas_logger);
  SPDLOG_LOGGER_TRACE(m_log, "RecursiveDisassembling::RecursiveDisassembling()");
}

RecursiveDisassembling::~RecursiveDisassembling()
{
  SPDLOG_LOGGER_TRACE(m_log, "RecursiveDisassembling::~RecursiveDisassembling()");
}

// Other functions
void RecursiveDisassembling::disassembleBinary(CFGManager& cfg_manager) const
{
}

} // namespace boa
