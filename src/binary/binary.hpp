#ifndef BINARY_H
#define BINARY_H

#include <algorithm>
#include <fstream>
#include <map>
#include <memory>
#include <string>
#include <unordered_map>

#include "utils/boa_types.hpp"
#include "utils/utils.hpp"

namespace boa
{

class Symbol;
class Section;


// MARK:- Data describing the binary
struct binary_specs
{
	OptLevel opt_lvl;
	BinaryCompiler compiler;
	BinaryArch arch;
	BinaryFormat format;
	std::string version;
};

enum class SymbolType{
  FUNC,
  OBJ,
  UKN,
  SECT,
  NOTYPE,
};

enum class SectionType{
  SEC_TYPE_NONE,
  SEC_TYPE_CODE,
  SEC_TYPE_DATA
};

// TODO: set members to private and create get/set functions
class Binary{
  public:
  // --- Methods

  Binary(const std::string &filepath);

  Section* get_text_section();
  std::vector<Section*> getExecutableSections();

  void printInfo(std::shared_ptr<spdlog::logger> log);

  // - Getters and setters
  addr_t getEntryPoint();
  OptLevel getBinOptLvl() const {return m_bin_specs.opt_lvl;};
  BinaryFormat getBinFormat() const{ return m_bin_specs.format;};
  BinaryArch getBinArch() const{ return m_bin_specs.arch;};
  BinaryCompiler getBinCompiler() const{ return m_bin_specs.compiler;};
	std::string getBinVersion() const {return m_bin_specs.version;};
	const std::string getName() const{ return m_basename;};

  void setBinOptLvl(OptLevel opt) {m_bin_specs.opt_lvl = opt;};
  void setBinFormat(BinaryFormat opt) {m_bin_specs.format = opt;};
  void setBinArch(BinaryArch arch) {m_bin_specs.arch = arch;};
  void setBinVersion(std::string ver) {m_bin_specs.version = ver;};
  void setBinCompiler(BinaryCompiler compiler) {m_bin_specs.compiler = compiler;};

  // binary spec

  void setBinSpecs(OptLevel opt, BinaryFormat format, BinaryArch arch, BinaryCompiler comp);

  //void setArchStr(const std::string &arch);
  //void setFormatStr(const std::string &format);
  void setEntryPoint(addr_t entry_point);
  void pushBackCode(std::string instr_str);

  void addSymbol(std::string type, std::string m_name, addr_t m_vaddr, uint size, std::shared_ptr<spdlog::logger> log);
  void addSection( uint64_t size, addr_t vsize, addr_t vaddr,  std::string name, std::vector<Permission> perms, std::shared_ptr<Binary> bin_ptr);
  void addSection(uint64_t size, addr_t vsize, addr_t vaddr,  std::string name, std::vector<Permission> perms, std::shared_ptr<Binary> bin_ptr, std::vector<byte_t> bytes);
  void addSectionRaw(uint64_t size, addr_t vsize, addr_t vaddr,  std::string name, std::vector<Permission> perms, std::shared_ptr<Binary> bin_ptr, const std::string& bytes_str);

  Section* getSection(addr_t addr);

  std::vector<std::shared_ptr<Symbol>> getSymbols() const{return m_symbols;};

  std::vector<std::shared_ptr<Symbol>> getFunSymbols();
  bool isFunInBin(const std::string& name, addr_t vaddr) const;
  bool isSymbolAlreadyInBin(std::shared_ptr<Symbol> sym,std::shared_ptr<spdlog::logger> log ) const;
	std::string getFilepath(){return m_filepath;};
  // --- Members

  private:
    const std::string& m_filepath;

    binary_specs m_bin_specs;
		const std::string m_mangled_name;
		const std::string m_basename;

    unsigned m_bits; // ??

    std::vector<Section*> m_sections;
    std::vector<std::shared_ptr<Symbol>> m_symbols;

    addr_t m_entry;
    std::string m_disas_code;
};

class Section{
  public:

  // constructor with member initializer lists
  Section( uint64_t size, addr_t vsize, addr_t vaddr, std::string name, std::vector<Permission> perms, std::shared_ptr<Binary> bin_ptr) : m_bytes_raw (new byte_t[size]), m_size(size), m_vsize(vsize), m_vaddr(vaddr), m_name(name), m_perms(perms), m_binary(bin_ptr){}

  // to check if a section contains given address
  bool contains(uint64_t addr) { return (addr >= m_vaddr) && (addr-m_vaddr < m_size); }
  std::string getName() { return m_name ;}
  std::shared_ptr<Binary> getBinary() { return m_binary;}
  std::vector<Permission> getPerms() { return m_perms;}
  std::vector<byte_t> getBytesVector() const {return m_bytes;}
  byte_t* const getBytesRaw() const {return m_bytes_raw;}
	uint const getNbInst() {return m_nb_inst;}
  addr_t getEntryPoint() { return m_vaddr; }
  void setNbInst(uint nb_inst){m_nb_inst = nb_inst;}
  uint const getSize(){ if(m_size!=0){return m_vsize;} return m_size;}
	std::vector<byte_t> getBytes() const {return m_bytes;};
  bool isSectionExecutable();

  private:

  std::vector<byte_t> m_bytes; // raw bytes
  byte_t* const m_bytes_raw;

  SectionType m_type; // code or data ?

  // from old BOA

  uint64_t m_size; // in bytes
  addr_t m_vsize;
  addr_t m_vaddr;

  // from tangue


  addr_t m_vma;
  addr_t m_offset;

    std::string m_name; // name of the section (.text)
    std::vector<Permission> m_perms;
    std::shared_ptr<Binary> m_binary; // pointer back to the binary
  	uint m_nb_inst;

};

class Symbol{
  public:


  // set values of symbol
  void setSymbol();
  std::string getSymbolTypeStr();
  
  Symbol(SymbolType type=SymbolType::UKN, std::string name="unknown", addr_t addr=0, uint size =0) : m_type(type), m_name(name), m_vaddr(addr), m_size(size) {}

  SymbolType m_type;
  std::string m_name;  
  addr_t m_vaddr;
  uint m_size;

  
};

using sptrSymbol = std::shared_ptr<Symbol>;

namespace binary
{

struct import_entry_t
{
  int ordinal;
  std::string lib;
  std::string name;
  addr_t vaddr;         // vaddr of the entry
  addr_t function_addr; // This is the "hardcoded" address found at @[vaddr] in the binary file. Don't know (yet) if
                        // this addr is a "preferred" addr added by the compiler? During real execution @[vaddr] is
                        // patched by the OS with the "real" external function entry point address (e.g 0x77e59f93)
};

struct export_entry_t
{
  int ordinal;
  std::string name;
  addr_t vaddr;
};

} // namespace binary

using sptrBinary = std::shared_ptr<Binary>;

} // namespace boa

#endif
