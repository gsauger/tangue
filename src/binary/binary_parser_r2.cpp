#include "binary_parser_r2.hpp"

#include <algorithm>
#include <iomanip>
#include <iostream>
# include <vector>
namespace boa
{

// Constructors and destructors
BinaryParserRadare2::BinaryParserRadare2()
{
  SPDLOG_LOGGER_TRACE(m_log, "BinaryParserRadare2::BinaryParserRadare2()\n");
}

BinaryParserRadare2::~BinaryParserRadare2()
{
  SPDLOG_LOGGER_TRACE(m_log, "BinaryParserRadare2::~BinaryParserRadare2()\n");
}

std::map<std::string, std::vector<Permission>> perms_map{
    {"----", {}},
    {"-r--", {Permission::READ}},
    {"-rw-", {Permission::READ, Permission::WRITE}},
    {"-r-x", {Permission::READ, Permission::EXECUTE}},
    {"-rwx", {Permission::READ, Permission::WRITE, Permission::EXECUTE}}
};

std::vector<byte_t> string2bytevect(const std::string& hex_string,
                                std::string section_name){

  std::vector<byte_t> output;
  /*std::cout << hex_string << " is of length :" << hex_string->length() << std::endl;*/

  std::string copy = hex_string;
  while(!copy.empty()){
    if(copy.length() == 1 && !isspace(copy[0])){
      std::cout << "Warning: incomplete byte in current section: " << section_name << std::endl;
      break;
    }
    std::string firstTwoChar = copy.substr(0, 2);
    copy.erase(0,2);

    byte_t d = (byte_t) strtoul(firstTwoChar.c_str(), NULL, 16);
    output.push_back(d);
   // std::cout << "Pushed: " << unsigned(d) << "... remains: " << copy << std::endl;
  }

  return output;
}

  /**
    Loads a binary (ELF or PE file) using RADARE2 into a binary object.
    @param binary Binary object to be filled
    @param load_base_addr Optional, address to where to open R2
    @return EXIT_SUCCESS if successful
  */
int BinaryParserRadare2::loadBinary(std::shared_ptr<Binary> binary, addr_t load_base_addr)
{
  std::string rabin2_cmd;
  std::string answer;
  nlohmann::json answer_parsed;
  std::string binary_filepath = binary->getFilepath();

  SPDLOG_LOGGER_TRACE(m_log, "BinaryParserRadare2::loadBinary\n");
  SPDLOG_LOGGER_DEBUG(m_log,"Parse header of {} to collect info \n", binary_filepath);

  // Open binary file with radare2
  SPDLOG_LOGGER_TRACE(m_log, "R2 open file: {}\n", binary_filepath);
  std::string parameters = {};
  if (load_base_addr)
    parameters = "-B " + std::to_string(load_base_addr);

  // Create R2 object (see r2_utils.hpp)
  R2 r2 = R2(binary_filepath, parameters);

  // ---------- Fill basic data of Binary object
  // basic data (exe header)
  rabin2_cmd = "iIj";
  SPDLOG_LOGGER_TRACE(m_log, "R2 sendCmd: {}\n", rabin2_cmd);
  try{
    answer = r2.sendCmd(rabin2_cmd); // data is stocked in "bin_infos"
  }
  catch(...){
    m_log->info("Error sending command {} to r2. Aborting.\n", rabin2_cmd);
    return EXIT_FAILURE;
  }
  answer_parsed = nlohmann::json::parse(answer); // then parsed into a json

  binary->setBinArch(str2arch(answer_parsed["arch"]));
  binary->setBinFormat(str2format(answer_parsed["bintype"]));
  binary->setBinCompiler(str2compiler(answer_parsed["compiler"]));

	std::string version, o_level;
	std::vector<std::string> parsed_filename = utils::getParsedStringVect(binary->getName());
    if(parsed_filename.size()!=3){
      // Non standard filename
      version = "1";
      o_level = "01";
    }
    else{
      version = parsed_filename.at(1);
      o_level = parsed_filename.at(2);
    }

  binary->setBinOptLvl(str2opt(o_level));
  binary->setBinVersion(version);

  // entry point
  rabin2_cmd = "iej";
  SPDLOG_LOGGER_TRACE(m_log, "R2 sendCmd: {}\n", rabin2_cmd);
    try{
    answer = r2.sendCmd(rabin2_cmd); // data is stocked in "bin_infos"
  }
  catch(...){
    m_log->info("Error sending command {} to r2. Aborting.\n", rabin2_cmd);
    return EXIT_FAILURE;
  }
  answer_parsed = nlohmann::json::parse(answer);

  addr_t entry_point = 0x0;
  //m_log->info(" TEST : {} \n", answer_parsed[0].contains("vaddr"));
  if(answer_parsed[0].contains("vaddr")){
    entry_point = (addr_t)answer_parsed[0]["vaddr"];
  }

  SPDLOG_LOGGER_DEBUG(m_log, "Binary ep: {0:#x}\n", entry_point);

  binary->setEntryPoint(entry_point); // or ?


  // ---------- Fill Sections of Binary object
  rabin2_cmd = "iSj";
  SPDLOG_LOGGER_DEBUG(m_log, "R2 sendCmd: {}\n", rabin2_cmd);
    try{
    answer = r2.sendCmd(rabin2_cmd); // data is stocked in "bin_infos"
  }
  catch(...){
    m_log->info("Error sending command {} to r2. Aborting.\n", rabin2_cmd);
    return EXIT_FAILURE;
  } 
  answer_parsed = nlohmann::json::parse(answer); 

  // Fill the sections members

  std::string section_dump_str;
  for(auto section_json : answer_parsed){
    // get the raw bytes

    rabin2_cmd = "iO d/S/" + section_json["name"].dump();
    SPDLOG_LOGGER_DEBUG(m_log, "R2 sendCmd: {}\n", rabin2_cmd);
    try{
      section_dump_str = r2.sendCmd(rabin2_cmd); // data is stocked in "bin_infos"
      if(section_dump_str.size() == 0){
      continue;
      }
    }
    catch(...){
      m_log->info("Error sending command {} to r2. Aborting.\n", rabin2_cmd);
      return EXIT_FAILURE;
    }

    std::vector<Permission> perms = perms_map.at(section_json["perm"]);

    //SPDLOG_LOGGER_TRACE(m_log, "section_dump_str: {}, \n section dump : {}\n", section_dump_str, section_json.dump());

    // add a new section
    binary->addSectionRaw(section_json["size"],section_json["vsize"],section_json["vaddr"], section_json["name"], perms, binary, section_dump_str);
    //// compute approximate number of instructions for each executable section
    //uint nb_inst=0;
    //for( auto& section : binary->getExecutableSections()){
    //	rabin2_cmd = "pD " + std::to_string(section->getSize()) + "~?"; // this grabs the nbr of line is that disassembly
    //	try{
    //  	nb_inst = std::stoul(r2.sendCmd(rabin2_cmd)); // data is stocked in "bin_infos"
    //	}
    //	catch(...){
    //  	m_log->info("Error sending command {} to r2. Aborting.\n", rabin2_cmd);
    //  	return true;
    //	}
    //	section->setNbInst(nb_inst);
    //	nb_inst=0;
    //}
  }

  // ---------- Fill Symbols of Binary object - Only functions
  rabin2_cmd = "isj";
  SPDLOG_LOGGER_DEBUG(m_log, "R2 sendCmd: {}\n", rabin2_cmd);
  try{
    answer = r2.sendCmd(rabin2_cmd); // data is stocked in "bin_infos"
  }
  catch(...){
    m_log->info("Error sending command {} to r2. Aborting.\n", rabin2_cmd);
    return EXIT_FAILURE;
  }
  answer_parsed = nlohmann::json::parse(answer);

  for( auto symbol_json : answer_parsed){

    if(symbol_json["type"] == "FUNC"){
   	 	 SPDLOG_LOGGER_DEBUG(m_log, "Found function among symbols: {}\n", symbol_json["name"].dump());
      binary->addSymbol(symbol_json["type"], symbol_json["name"], symbol_json["vaddr"], symbol_json["size"], m_log);
    }
  }

  // ---------- Also add all the items gathered after R2's analysis

  // NOTE: this is useful if the bin is stripped; the method binary.cpp::addSymbol() includes
  // a check to not add twice the same symbol (same type, addr, and one name contained in the other)


  // ---------- Check if we found a .text section
//  rabin2_cmd = "aaaa"; // analysis tool (detects functions)
//  SPDLOG_LOGGER_TRACE(m_log, "R2 sendCmd: {}\n", rabin2_cmd);
//  try{
//    answer = r2.sendCmd(rabin2_cmd); // data is stocked in "bin_infos"
//  }
//  catch(...){
//    m_log->info("Error sending command {} to r2. Aborting.\n", rabin2_cmd);
//    return true;
//  } 
//
//  rabin2_cmd = "afls a"; // Sort fun by addr
//
//  rabin2_cmd = "afltj"; // list detected function in a nice manner for us
//  SPDLOG_LOGGER_TRACE(m_log, "R2 sendCmd: {}\n", rabin2_cmd);
//  try{
//    answer = r2.sendCmd(rabin2_cmd); // data is stocked in "bin_infos"
//  }
//  catch(...){
//    m_log->info("Error sending command {} to r2. Aborting {}.\n", rabin2_cmd, binary->getFilepath());
//    return true;
//  }
//  if(!answer.empty()){
//    answer_parsed = nlohmann::json::parse(answer);
//    // answer_parsed.erase(std::remove_if(answer_parsed.begin(), answer_parsed.end(), [](nlohmann::json& el) { return not el.is_string(); }), answer_parsed.end()); // attempts to remove all the null ptr ?....
//    SPDLOG_LOGGER_DEBUG(m_log, "Nb of detected fun: {}\n", answer_parsed.size());
//    uint count=0;
//    for(auto elt : answer_parsed){
//      count++;
//
//      SPDLOG_LOGGER_DEBUG(m_log, "\tCurrent item (nb {}) : {}\n", count, elt.dump());
//
//      if(elt.contains("name") && elt.contains("addr") && elt.contains("size")){
//        std::string name = elt["name"];
//        //uint add_elt = elt["addr"];
//        //uint size_elt = elt["size"];
//        binary->addSymbol("FUNC", elt["name"], elt["addr"], elt["size"], m_log);
//      }
//    }
//  }
//  else{
//    SPDLOG_LOGGER_TRACE(m_log, "\tR2 cmd {} returned empty answer\n", rabin2_cmd);
//  }
//



  // ---------- Check if we found a .text section

  if(!binary->get_text_section()){
    SPDLOG_LOGGER_INFO(m_log,"No .text section found by R2. Dumping whole binary as .text section.\n");

    std::vector<Permission> perms;
    try{
      const std::string answer = r2.sendCmd("p8 $s"); 
    }
    catch(...){
      m_log->info("Error sending command {} to r2. Aborting.\n", "p8 $s");
      return EXIT_FAILURE;
    } 
    try{
      binary->addSection(std::stoul( r2.sendCmd("iZ") ),
      0, entry_point, ".text", perms, binary, string2bytevect(answer, ".text"));
    }

    catch(...){
       m_log->info("Error sending command {} to r2. Aborting.\n", "iZ");
       return EXIT_FAILURE;
} 

  }

  // ---------- Return the bin
  return EXIT_SUCCESS;
  } 

} // namespace boa
