#ifndef BINARY_PARSER_RADARE2_H
#define BINARY_PARSER_RADARE2_H

#include <string>

#include <nlohmann/json.hpp>

#include "binary/binary_parser.hpp"
#include "binary/binary.hpp"
#include "utils/boa_types.hpp"
#include "utils/r2_utils.hpp"

namespace boa
{

class BinaryParserRadare2 : public BinaryParser
{

public:
  // Constructors and destructors
  BinaryParserRadare2();
  virtual ~BinaryParserRadare2();

  virtual int loadBinary(std::shared_ptr<Binary> binary, addr_t load_base_addr = 0x0000000000000000ULL);
};

} // namespace boa

#endif
