#include "binary.hpp"

#include <iomanip>
#include <iostream>
#include <sstream>

namespace boa
{

// table used to convert strings to SymbolType
std::map<std::string, SymbolType> symbol_table = {
    {"FUNC", SymbolType::FUNC}, {"OBJ", SymbolType::OBJ},
    {"SECT", SymbolType::SECT}, {"NOTYPE", SymbolType::NOTYPE}};
std::map<SymbolType, std::string> symbol_table_to_str = {
    {SymbolType::FUNC, "FUNC"}, {SymbolType::OBJ, "OBJ"},
    {SymbolType::SECT, "SECT"}, { SymbolType::NOTYPE, "NOTYPE"}};

  // -------- Binary
  Binary::Binary(const std::string& filepath) : m_filepath(filepath), m_mangled_name(utils::mangled_name(filepath)), m_basename(utils::base_name(filepath)), m_bits(0), m_entry(0) {
  	m_bin_specs.opt_lvl = OptLevel::Opt_unk;
  	m_bin_specs.version = "unk";
  }


  Section* Binary::get_text_section(){
    for( auto &s : m_sections){
      if(s->getName() == ".text") return s;
    }
    // log "no text section found" here
    return NULL;
  }

  std::vector<Section*> Binary::getExecutableSections(){
    std::vector<Section*> output;
    output.reserve(m_sections.size());
    for(Section* section : m_sections){
      if(section->isSectionExecutable()){
        output.push_back(section);
      }
    }
    return output;
  }


  void Binary::setEntryPoint(addr_t entry_point){
    m_entry = entry_point;
  }

  addr_t Binary::getEntryPoint(){
    return m_entry;
  }

  void Binary::printInfo(std::shared_ptr<spdlog::logger> log){
    log->info("Infos on binary {}\n", m_filepath);
    log->info("Binary format : {}\n", format2str(m_bin_specs.format));
    log->info("Binary architecture: {}\n", arch2str(m_bin_specs.arch));
    log->info("Binary entry point: {}\n", m_entry);
    log->info("Binary sections name:\n");
    for(Section* section : m_sections){
      log->info(" {}\n", section->getName());
    }
    log->info("Binary symbols:\n");
    for(sptrSymbol& symbol : m_symbols){
      log->info(" {}\n", symbol->m_name);
    }
  }

	// Binary specs
	void Binary::setBinSpecs(OptLevel opt, BinaryFormat format, BinaryArch arch, BinaryCompiler comp){
		setBinOptLvl(opt);
		setBinFormat(format);
		setBinArch(arch);
		setBinCompiler(comp);
	}

std::vector<sptrSymbol> Binary::getFunSymbols(){
  std::vector<sptrSymbol> output;
  output.reserve(m_symbols.size());
  for(const sptrSymbol& symbol : m_symbols){
    if(symbol->m_type == SymbolType::FUNC){
      output.push_back(symbol);
    }
  }
  return output;
}

Section* Binary::getSection(addr_t addr){
  for(auto const& section : m_sections){
    if(section->contains(addr)){
      return section;
    }
  }
  //std::cout << "WARNING: Address " + addr2s(addr) + " is not in the binary, skipping it\n";
  return NULL;
}

  // Methods

  bool Binary::isSymbolAlreadyInBin(sptrSymbol new_sym, std::shared_ptr<spdlog::logger> log) const {
    for(auto const& sym : m_symbols){
      // if addresses are the same AND same type
      if(sym->m_vaddr == new_sym->m_vaddr && sym->m_type == new_sym->m_type){
        // if one of the strings is contained in the other
        SPDLOG_LOGGER_DEBUG(log, "\t\tFound a symbol with the same type AND vaddr : {}\n", sym->m_vaddr);
        if(sym->m_name.find(new_sym->m_name) || new_sym->m_name.find(sym->m_name) || !(new_sym->m_name.compare(sym->m_name))){
          SPDLOG_LOGGER_DEBUG(log, "\t\tIt also has a similiar name : {} !\n", sym->m_name);
          return true;
        }
        else{
          SPDLOG_LOGGER_DEBUG(log, "ERROR: same symbol address and type but not same name ! Aborting; {}: {:x} and {} : {:x}\n", sym->m_name, sym->m_vaddr, new_sym->m_name, new_sym->m_vaddr);
          throw std::runtime_error("");
        }
      }
    }
    return false;
  }

  void Binary::addSymbol(  std::string type, std::string name, addr_t addr, uint size, std::shared_ptr<spdlog::logger> log){
    // Check if symbol is already present
    std::shared_ptr<Symbol> sym_ptr = std::make_shared<Symbol>(symbol_table[type], name, addr, size);
    if(getSection(addr) == NULL){
      SPDLOG_LOGGER_DEBUG(log, "\tSkipping sym {}. Address was not in the binary.\n", name, addr);
      return;
    }
    if(!isSymbolAlreadyInBin(sym_ptr, log)){
      SPDLOG_LOGGER_DEBUG(log, "\tAdded sym {} : {:x};\n", name, addr);
      m_symbols.push_back(sym_ptr);
    }
    return;
  }

  bool Binary::isFunInBin(const std::string& name, addr_t vaddr) const{
  	for(auto const& symbol : getSymbols()){
  		if(symbol->getSymbolTypeStr() == "FUNC"){
  			if(symbol->m_name == name){return true;}
  			else if(symbol->m_vaddr == vaddr){return true;}
  		}
  	}
  	return false;
  }

  void Binary::addSection( uint64_t size, addr_t vsize, addr_t vaddr,  std::string name,std::vector<Permission> perms, std::shared_ptr<Binary> bin_ptr){
    Section *sec_ptr = new Section(size, vsize, vaddr, name, perms, bin_ptr);
    m_sections.push_back(sec_ptr);
  }

  void Binary::addSection(uint64_t size, addr_t vsize, addr_t vaddr,  std::string name, std::vector<Permission> perms, std::shared_ptr<Binary> bin_ptr, std::vector<byte_t> bytes){
    Section *sec_ptr = new Section(size, vsize, vaddr, name, perms, bin_ptr);
    sec_ptr->getBytes() = bytes;
    m_sections.push_back(sec_ptr);
  }

  static void getSectionContentRaw(byte_t* const byte_ptr, const std::string& byte_str){
    std::vector<std::string> substr_vect;
    std::string tmp_two_chars;
    uint str_size = byte_str.size();
    byte_t hex_value;

    if(str_size % 2 == 0){ // this is normal, to accout for the EOL char (probably)
      std::cout << "Warning: incomplete byte string in current section !! : " << str_size << std::endl;
    }
    for(uint i = 0; i < (str_size/2); i++){
      tmp_two_chars = byte_str.substr(2 * i, 2);
      substr_vect.push_back(tmp_two_chars);
    //std::cout << "\t\tJust added : " << tmp_two_chars <<std::endl;

    }

    uint j = 0;
    for(auto const & byte_str : substr_vect){
      hex_value = std::strtoul(byte_str.data(), NULL, 16);
      byte_ptr[j] = hex_value;
      j++;
    }
    return;
  }

  void Binary::addSectionRaw(uint64_t size, addr_t vsize, addr_t vaddr,  std::string name, std::vector<Permission> perms, std::shared_ptr<Binary> bin_ptr, const std::string& byte_str){
    Section *sec_ptr = new Section(size, vsize, vaddr, name, perms, bin_ptr);
    if(size != byte_str.size()/2){
      //std::cout << "Name: " << name << " ; " <<" Byte str: " << byte_str << "\n";

      std::cout << "Size and byte_str.size() " << addr2s(size) << " ; " << addr2s(byte_str.size()) << "\n";
      throw std::runtime_error("Error, size of the section was different than the size of its byte dump divided by two ! Investigate\n");
    }
    getSectionContentRaw(sec_ptr->getBytesRaw(), byte_str);
    m_sections.push_back(sec_ptr);
  }

  std::string Symbol::getSymbolTypeStr(){
    return symbol_table_to_str[m_type];
  }

  // ---- Section

  bool Section::isSectionExecutable(){
    for(Permission perm : m_perms){
      if(perm == Permission::EXECUTE){
        return true;
      }
    }
    return false;
  }

// Section
// constructor




} // namespace boa
