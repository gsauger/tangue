#include "binary_parser.hpp"

namespace boa
{

BinaryParser::BinaryParser() : m_log(spdlog::get(utils::bin_parser_logger))
{
  SPDLOG_LOGGER_TRACE(m_log, "BinaryParser::BinaryParser()\n");
}

BinaryParser::~BinaryParser()
{
  SPDLOG_LOGGER_TRACE(m_log, "BinaryParser::~BinaryParser()\n");
}

} // namespace boa
