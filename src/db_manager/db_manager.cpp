#include "db_manager.hpp"

namespace boa
{

DBManager::DBManager(std::string db_name)
{   
    m_log = spdlog::get(utils::database_logger);
    SPDLOG_LOGGER_TRACE(m_log, "DBManager::DBManager\n");
    m_database_name = db_name;

    // init stmt ptr

    // init sqlite3 here

    int exit = 0;
    exit = sqlite3_open(db_name.c_str(), &m_DB);
  
    if (exit) {
        throw std::runtime_error("Error open DB " + db_name + "\n");
    }
    else{
        m_log->info("Opened Database {} Successfully!\n", db_name);
    }

    // TODO: find a way to check if the tables already exist
    initTables();

    

}

DBManager::~DBManager()
{
    SPDLOG_LOGGER_TRACE(m_log, "DBManager::~DBManager\n"); 

    sqlite3_close(m_DB);
    sqlite3_finalize(m_stmt_ptr);
}

void DBManager::initTables(){
    execSQLCommand("CREATE TABLE functions (fun_name varchar(255), entry_point binary(8), color int, nb_sites int, binary varchar(255), PRIMARY KEY (fun_name));");
    execSQLCommand("CREATE TABLE sites ( site_name varchar(255), entry_point binary(8), fun_entry_point binary(8), nb_BB int, encoding varchar(255), binary varchar(255), dataflow_encoding varchar(255), PRIMARY KEY(site_name));");
    execSQLCommand("CREATE TABLE basicblocks (bb_name varchar(255), entry_point binary(8), site_entry_point binary(8), type varchar(255), dataflow varchar(255) , binary varchar(255), PRIMARY KEY (bb_name));");
}

void DBManager::addBinaryData(std::shared_ptr<AnalysisEngine> analysis_engine){
    std::string bin_name = utils::get_filename(analysis_engine->getBinName());

    if(std::find(m_binaries_name.begin(), m_binaries_name.end(), bin_name) == m_binaries_name.end()) {
    // we never saw the binary
        m_binaries_name.push_back(bin_name);
    }

    cleanTablesFromBin(bin_name);

    for(std::shared_ptr<Function> fun_ptr : analysis_engine->getFunctions()){
        addFunctionData(fun_ptr);
        for(sptrSite site_ptr : fun_ptr->getSites()){
        addSiteData(site_ptr, fun_ptr->getEntryPointAddr());
            for(sptrBasicBlock bb_ptr : site_ptr->getVectorBasicBlocks()){
                addBBData(bb_ptr, site_ptr->getEntryPointAddr());
            }
        }      
    }
    m_log->info("Added data from '{}' to {}...\n", bin_name, m_database_name);
}

void DBManager::cleanTablesFromBin(std::string bin_name){
    //m_log->info("Cleaning tables from binary '{}'...\n", bin_name);
    execSQLCommand("DELETE FROM functions WHERE functions.binary = '" + bin_name +"';");
    execSQLCommand("DELETE FROM sites WHERE sites.binary = '" + bin_name +"';" );
    execSQLCommand("DELETE FROM basicblocks WHERE basicblocks.binary = '" + bin_name +"';" );
}

void DBManager::addFunctionData(std::shared_ptr<Function> fun_ptr){
    std::stringstream values_str;
    values_str << "(";
    values_str << "'" << fun_ptr->getName() + "_" + m_binaries_name.back() << "',";
    values_str << "'" << addr2s(fun_ptr->getEntryPointAddr()) << "',";
    values_str << "'" << std::to_string(fun_ptr->getColor()) << "',";
    values_str << "'" << std::to_string(fun_ptr->getSites().size()) << "',";
    values_str << "'" << m_binaries_name.back() <<"'";
    values_str << ")";
    execSQLCommand("INSERT INTO functions (fun_name,entry_point, color, nb_sites, binary) VALUES" + values_str.str() +";");
}

void DBManager::addSiteData(std::shared_ptr<Site> site_ptr, addr_t fun_ep){
    std::stringstream values_str;
    values_str << "(";
    values_str << "'" << addr2s(site_ptr->getEntryPointAddr()) + "_" + m_binaries_name.back() << "',";
    values_str << "'" << addr2s(site_ptr->getEntryPointAddr()) << "',";
    values_str << "'" << addr2s(fun_ep) << "',";
    values_str << "'" << std::to_string(site_ptr->getNumberBasicBlocks()) << "',";
    values_str << "'" << vec2str32(site_ptr->getEncoding()) << "',";
    values_str << "'" << m_binaries_name.back() << "',";
    values_str << "'" << addr2s(0x0) << "'";

    values_str << ")";
    execSQLCommand("INSERT INTO sites (site_name, entry_point , fun_entry_point , nb_BB , encoding , binary , dataflow_encoding) VALUES" + values_str.str() +";");
}

void DBManager::addBBData(sptrBasicBlock bb_ptr, addr_t site_ep){
    std::stringstream values_str;
    values_str << "(";
    values_str << "'" << addr2s(bb_ptr->getEntryPointAddr()) + "_" + addr2s(site_ep) + "_" + m_binaries_name.back() << "',";
    values_str << "'" << addr2s(bb_ptr->getEntryPointAddr()) << "',";
    values_str << "'" << addr2s(site_ep) << "',";
    values_str << "'" << bb_ptr->getLastInstr()->type2String() << "',";
    values_str << "'" << bb_ptr->printDataflow() << "',";
    values_str << "'" << m_binaries_name.back() << "'";
    values_str << ")";
    execSQLCommand("INSERT INTO basicblocks (bb_name , entry_point , site_entry_point , type ,dataflow, binary ) VALUES" + values_str.str() +";");
}

void DBManager::execSQLCommand(std::string sql_command){
    int exit = 0;
    //exit = sqlite3_open(m_database_name.c_str(), &m_DB);

    char* messageError;
    exit = sqlite3_exec(m_DB, sql_command.c_str(), NULL, 0, &messageError);
  
    if (exit != SQLITE_OK) {
        SPDLOG_LOGGER_DEBUG(m_log,"Error: {}\n", messageError);
        sqlite3_free(messageError);
    }
}

std::vector<addr_t> DBManager::getBinSitesAddr(std::string bin){

    std::vector<addr_t> sites_addrs;

    std::string sql_command = "SELECT entry_point FROM sites WHERE sites.binary='" + bin + "';";

    int exit = 0;

    // prepare the statement
    SPDLOG_LOGGER_DEBUG(m_log,"Preparing statement...\n");
    exit = sqlite3_prepare_v2(m_DB, sql_command.c_str(), -1, &m_stmt_ptr, NULL);

    if (exit != SQLITE_OK){
        SPDLOG_LOGGER_DEBUG(m_log, "Could not prepare statement\n");
    }

    // execute it
    while(sqlite3_step(m_stmt_ptr) == SQLITE_ROW){
        SPDLOG_LOGGER_DEBUG(m_log, "Recovered  0 - 1: {} {}\n", sqlite3_column_text(m_stmt_ptr, 0),sqlite3_column_text(m_stmt_ptr, 1) );
        sites_addrs.push_back((uint)sqlite3_column_int(m_stmt_ptr, 0));
    }

    SPDLOG_LOGGER_DEBUG(m_log,"Finished reading db: #{}\n");

    return sites_addrs;
}

std::vector<std::pair<addr_t,addr_t>> DBManager::getCommonSitesAddr(std::string bin1, std::string bin2, bool sample_distance){

    std::vector<std::pair<addr_t,addr_t>> common_sites_addrs;
    std::string sql_command;

    if(!sample_distance){
        sql_command = "SELECT A.entry_point, B.entry_point FROM sites A, sites B WHERE A.encoding = B.encoding AND A.dataflow_encoding = B.dataflow_encoding AND A.binary = '" + bin1 + "' AND B.binary = '" + bin2 + "' GROUP BY A.entry_point;";
    }
    else{
        sql_command = "SELECT A.entry_point, B.entry_point FROM sites A, sites B WHERE A.encoding = B.encoding AND A.binary = '" + bin1 + "' AND B.binary = '" + bin2 + "' GROUP BY A.entry_point;";
    }


    int exit = 0;

    // prepare the statement
    SPDLOG_LOGGER_DEBUG(m_log,"Preparing statement...\n");
    exit = sqlite3_prepare_v2(m_DB, sql_command.c_str(), -1, &m_stmt_ptr, NULL);

    if (exit != SQLITE_OK){
        SPDLOG_LOGGER_DEBUG(m_log, "Could not prepare statement\n");
    }

    // execute it
    while(sqlite3_step(m_stmt_ptr) == SQLITE_ROW){
        SPDLOG_LOGGER_DEBUG(m_log, "Recovered  0 - 1: {} {}\n", sqlite3_column_text(m_stmt_ptr, 0),sqlite3_column_text(m_stmt_ptr, 1) );
        common_sites_addrs.push_back(std::make_pair((uint)sqlite3_column_int(m_stmt_ptr, 0), (uint)sqlite3_column_int(m_stmt_ptr, 1)));
    }

    SPDLOG_LOGGER_DEBUG(m_log,"Finished reading db: #{}\n");

    return common_sites_addrs;
}

float DBManager::getBinarySitesNumber(std::string bin){
    float nb = 0;
    std::string sql_command = "select count(*) from sites where sites.binary = '" + bin +"';";

    std::vector<std::pair<addr_t,addr_t>> sites_addrs;

    // exit
    int exit = 0;

    // prepare the statement
    SPDLOG_LOGGER_DEBUG(m_log,"Preparing statement...\n");
    exit = sqlite3_prepare_v2(m_DB, sql_command.c_str(), -1, &m_stmt_ptr, NULL);

    if (exit != SQLITE_OK){
        SPDLOG_LOGGER_DEBUG(m_log, "Could not prepare statement\n");
    }

    // execute it
    while(sqlite3_step(m_stmt_ptr) == SQLITE_ROW){
        SPDLOG_LOGGER_DEBUG(m_log, "Found {} sites\n", (uint)sqlite3_column_int(m_stmt_ptr, 0));
        nb += (uint)sqlite3_column_int(m_stmt_ptr, 0);
    }
    SPDLOG_LOGGER_DEBUG(m_log,"Finished reading db: #{}\n");

    return nb;

}

std::pair<uint, uint> DBManager::getUniqueCommonSitesPerBinary(std::string bin1, std::string bin2, bool sample_distance){
    uint nb1 = 0, nb2 =0;
    std::string sql_command1,sql_command2;

    if(!sample_distance){
        sql_command1 = "SELECT COUNT(DISTINCT A.site_name) FROM sites A, sites B WHERE A.encoding = B.encoding AND A.dataflow_encoding = B.dataflow_encoding AND A.binary = '" + bin1 + "' AND B.binary = '" + bin2 + "' GROUP BY A.entry_point;";
        sql_command2 = "SELECT COUNT(DISTINCT B.site_name) FROM sites A, sites B WHERE A.encoding = B.encoding AND A.dataflow_encoding = B.dataflow_encoding AND A.binary = '" + bin1 + "' AND B.binary = '" + bin2 + "' GROUP BY A.entry_point;";

    }
    else{
        std::string sql_command1 = "SELECT COUNT(DISTINCT A.site_name) FROM sites A, sites B WHERE A.encoding = B.encoding AND A.binary = '" + bin1 + "' AND B.binary = '" + bin2 + "' GROUP BY A.entry_point;";
        std::string sql_command2 = "SELECT COUNT(DISTINCT B.site_name) FROM sites A, sites B WHERE A.encoding = B.encoding AND A.binary = '" + bin1 + "' AND B.binary = '" + bin2 + "' GROUP BY A.entry_point;";

    }

    // ------------ bin 1
    // exit
    int exit = 0;

    // prepare the statement
    SPDLOG_LOGGER_DEBUG(m_log,"Preparing statement...\n");
    exit = sqlite3_prepare_v2(m_DB, sql_command1.c_str(), -1, &m_stmt_ptr, NULL);

    if (exit != SQLITE_OK){
        SPDLOG_LOGGER_DEBUG(m_log, "Could not prepare statement\n");
    }

    // execute it
    while(sqlite3_step(m_stmt_ptr) == SQLITE_ROW){
        SPDLOG_LOGGER_DEBUG(m_log, "Found {} sites\n", (uint)sqlite3_column_int(m_stmt_ptr, 0));
        nb1 = (uint)sqlite3_column_int(m_stmt_ptr, 0);
    }
    SPDLOG_LOGGER_DEBUG(m_log,"Finished reading db: #{}\n");

    // ------------ bin 2

    // prepare the statement
    SPDLOG_LOGGER_DEBUG(m_log,"Preparing statement...\n");
    exit = sqlite3_prepare_v2(m_DB, sql_command2.c_str(), -1, &m_stmt_ptr, NULL);

    if (exit != SQLITE_OK){
        SPDLOG_LOGGER_DEBUG(m_log, "Could not prepare statement\n");
    }

    // execute it
    while(sqlite3_step(m_stmt_ptr) == SQLITE_ROW){
        SPDLOG_LOGGER_DEBUG(m_log, "Found {} sites\n", (uint)sqlite3_column_int(m_stmt_ptr, 0));
        nb2 = (uint)sqlite3_column_int(m_stmt_ptr, 0);
    }
    SPDLOG_LOGGER_DEBUG(m_log,"Finished reading db: #{}\n");

    std::pair<uint, uint> output_pair = std::make_pair(nb1, nb2);

    return output_pair;

}

} // namespace boa