#ifndef DBMANAGER_H
#define DBMANAGER_H

#include "../thirdparty/sqlite/sqlite3.h"
#include "utils/utils.hpp"
#include "analysis/analysis.hpp"

namespace boa{

class DBManager
{


public:
    DBManager(std::string db_name);
    ~DBManager();

    void initTables();
    void addBinaryData(std::shared_ptr<AnalysisEngine> analysis_engine);
    void addFunctionData(std::shared_ptr<Function> fun_ptr);
    void addSiteData(std::shared_ptr<Site> site_ptr, addr_t fun_ep);
    void addBBData(sptrBasicBlock bb_ptr, addr_t site_ep);

    void cleanTablesFromBin(std::string bin_name);

    void execSQLCommand(std::string command_str);

    // comparison
    std::vector<addr_t> getBinSitesAddr(std::string bin);

    std::vector<std::pair<addr_t, addr_t>> getCommonSitesAddr(std::string bin1, std::string bin2, bool sample_distance);
    float getBinarySitesNumber(std::string bin);
    std::pair<uint, uint> getUniqueCommonSitesPerBinary(std::string bin1, std::string bin2, bool sample_distance);



private:
    std::string m_database_name;
    std::vector<std::string> m_binaries_name;
    sqlite3* m_DB;
    sqlite3_stmt* m_stmt_ptr;

    std::shared_ptr<spdlog::logger> m_log;
    /* data */
};

    
}
#endif