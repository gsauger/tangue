#include "config.hpp"

#include <filesystem>
#include "CLI/CLI.hpp"
#include <iostream>

namespace boa
{

Config::Config(int argc, char* argv[])
{

  CLI::App app{"TANGUE: hybrid disassembler by Basic Block Analysis"};
  app.require_subcommand(1,1);
	// multi-threading
	m_config_global.multithreading = 0;
	app.add_option("-M,--multithreading", m_config_global.multithreading, "Number of core to use. Just put -M for max number of cores. Default is MAX.");
  // MODES

  // Subcommand learn

  CLI::App* learn = app.add_subcommand("learn", "Subcommand learn a binary");

  learn->add_option("-b, --binaries", m_config_global.bin_str_vect, "FOLDERS or FILES. Processes all functions in binaries.")
    ->required()
    ->check(CLI::ExistingPath);

  learn->add_option("-o, --output_folder", m_config_global.output_folder, "Output folder path")
  	->required()
  ->check(CLI::ExistingDirectory);

  learn->add_option("-D , --database_file", m_config_global.database_output, "Path for the database .db file");

  m_config_global.quiet = false;
  learn->add_flag("-q, --quiet", m_config_global.quiet, "Displays minimal information in the output log");

  m_config_global.condensed_output = false;
  learn->add_flag("-c, --condensed", m_config_global.condensed_output, "Only outputs the similarity matrix");

  // -- CFG DOTFILE / PDF

	m_config_global.dotfile = false;
  CLI::Option* print_opt_ptr = learn->add_flag("-p,--output_dotfile_pdf", m_config_global.dotfile, "Flag to also output the dotfile and pdf of the CFG (add -e / -n or -d for additional info)");

  m_config_global.output_function_map= false;
  learn->add_flag("-M, --map", m_config_global.output_function_map, "Add the interprocedural mapping .pdf of the binary to the CFG's output folder")
  ->needs(print_opt_ptr);


  learn->add_option("-e , --site_entry_point", m_config_global.site_entry_point, "Print the site starting at the given entry point. Should be like 'Ox5e' or decimal")
  ->needs(print_opt_ptr);

  learn->add_option("-E , --fun_entry_point", m_config_global.fun_entry_point, "Print the function starting at the given entry point. Should be like 'Ox5e' or decimal")
  ->needs(print_opt_ptr);

  learn->add_option("-n , --fun_name", m_config_global.fun_name, "Print the function with given name (string)")
  ->needs(print_opt_ptr);

  m_config_global.is_reduced = false;
  learn->add_flag("-r , --reduced_cfg", m_config_global.is_reduced, "Reduce the basic blocks in the CFG print to their last instr")
  ->needs(print_opt_ptr);


  m_config_global.print_functions = false;
  learn->add_flag("-f,--print_functions", m_config_global.print_functions, "Print functions in CFG .pdf file");

  m_config_global.print_sites = false;
  learn->add_flag("-s,--print_sites", m_config_global.print_sites, "Print sites in CFG .pdf file, used after -f");

  m_config_global.print_sites_dataflow = false;
  learn->add_flag("-d,--print_sites_dataflow", m_config_global.print_sites_dataflow, "Add dataflow to the print sites in CFG .pdf, used after -e");




  // subcommand correlation

  CLI::App* correlation = app.add_subcommand("correlation", "Subcommand correlation function of binaries");

  correlation->add_option("-b, --binaries", m_config_global.bin_str_vect, "FOLDERS or FILES. Compute correlation between all functions in binaries.")
  ->required()
  ->check(CLI::ExistingPath);

  correlation->add_option("-o, --output_folder", m_config_global.output_folder, "Output folder path")
  ->check(CLI::ExistingDirectory)
  ->required();

  correlation->add_option("-p, --pairs_csv", m_config_global.pairs_test_file, ".csv file about which pair of function to compute similarity of. Columns must be: idb_path_1 | fva_1 | idb_path_2 | fva_2")
  ->check(CLI::ExistingFile);

  m_config_global.site_similarity_threshold = 0.5;
  correlation->add_option("-t, --site_similarity_threshold", m_config_global.site_similarity_threshold, "Threshold similarity score for two sites to be similar (check dataflow tensor similarity score computation. DEFAULT: 0.5");


	m_config_global.func_similarity_score = "jaccard";
  correlation->add_option("-f, --fun_sim_score", m_config_global.func_similarity_score, "Function similarity score. Default in 'jaccard'. Can also use 'inclusion score");


  correlation->add_flag("-s, --sample_distance", m_config_global.sample_distance, "Disables the use of dataflow during site comparison");

  correlation->add_flag("-v, --verbose", m_config_global.corr_verbose, "Verbose mode for printing learning infos.");

  m_config_global.condensed_output = false;
  correlation->add_flag("-c, --condensed", m_config_global.condensed_output, "Output only the inclusion matrix");



  // Subcommand distance
/*
  CLI::App* distance = app.add_subcommand("distance", "Subcommand distance between 2 binaries; DEPRECATED. Use correlation instead.");


  distance->add_option("-b, --binaries", m_config_global.distance_str_vect, "FOLDERS or FILES. Compute distance between arg 1 and arg 2")
    ->required()
    ->expected(2)
    ->check(CLI::ExistingPath);

  distance->add_option("-o, --output_file", m_config_global.log_output, "Path for the distance log file");

  m_config_global.distance_print = false;
  distance->add_flag("-v, --verbose", m_config_global.distance_print, "Print additionnal info on each binary's analysis");
  
  m_config_global.recover_common_sites = false;
  distance->add_flag("-r, --recover_common", m_config_global.recover_common_sites, "Display common sites data in a pdf, saved in the working directory");

  m_config_global.quiet = false;
  distance->add_flag("-q, --quiet", m_config_global.quiet, "Displays minimal information about distance in the output log");  

  m_config_global.sample_distance = false;
  distance->add_flag("-s, --sample_distance", m_config_global.sample_distance, "Disables the use of dataflow during comparison");

  //m_config_global.condensed_output = false;
  //distance->add_flag("-c, --condensed", m_config_global.condensed_output, "Displays only sites addresses in output log (usefull for comaprisons with sigtool"); 

  // subcommand dbdistance

 
  CLI::App* dbdistance = app.add_subcommand("dbdistance", "Compute distance between a database and a file");

  dbdistance->add_option("-d, --database", m_config_global.dbdistance_database, "Database path")
  ->required()
  ->check(CLI::ExistingFile);

  dbdistance->add_option("-b, --binary", m_config_global.bin_filepath, "File path (to compare to database)")
  ->required()
  ->check(CLI::ExistingFile);

  dbdistance->add_option("-n, --bin_name_db", m_config_global.bin_db_name, "Binary name IN THE DB to compare the -b file to")
  ->required();

  dbdistance->add_option("-o, --output_file", m_config_global.log_output, "Path for the log file");

  m_config_global.sample_distance = false;
  dbdistance->add_flag("-s, --sample_distance", m_config_global.sample_distance, "Disables the use of dataflow during comparison");

  // subcommand dbcompare

  CLI::App* dbcompare = app.add_subcommand("dbcompare", "Subcommand dbcompare. Search in a db two binaries and compare them");

  dbcompare->add_option("-d, --database", m_config_global.dbcompare_database, "Database used to compare the binaries")
  ->required()
  ->check(CLI::ExistingFile);

  dbcompare->add_option("-b , --binaries", m_config_global.dbcompare_binaries_vect, "Binaries names to compare using the db")
  ->expected(2)
  ->check(CLI::ExistingFile)
  ->required();

  dbcompare->add_option("-o, --output_file", m_config_global.log_output, "Path for the learn log file");


  m_config_global.sample_distance = false;
  dbcompare->add_flag("-s, --sample_distance", m_config_global.sample_distance, "Disables the use of dataflow during comparison");
*/
	// GENERAL OPTIONS

  // INI config file support
  app.set_config("-c,--config", "", "Configuration ini file", false);

  // VERBOSE
  m_config_global.verbose = false;
  app.add_flag("-v,--verbose", m_config_global.verbose, "Enable verbose mode");

  // PRINT DISAS
  m_config_global.print = false;
  app.add_flag("-p,--print_disas", m_config_global.print, "Print disassembled code");

  // SITES

  std::string default_sites_size = "24";
  app.add_option("-S,--sites_size", m_config_global.sites_size, "Size of the extracted sites")
      ->default_str(default_sites_size)
      ->check(CLI::PositiveNumber)
      ->group("Global");

	app.add_option("-m, --site_sim_method", m_config_global.site_sim_method, "Method to be used to compute sites similarity. Default is bijection")
->group("Global");

	m_config_global.bgraph_sim_method = "all_reg_different";
	app.add_option("-g, --bgraph_sim_method", m_config_global.bgraph_sim_method, "Method to be used to compute bgraph similarity. Default is all_reg_different")
->group("Global");


  // DATA SAVING

  /*m_config_global.saveDataRaw = false;
  app.add_flag("-o,--output_data_raw", m_config_global.saveDataRaw, "Save the learnt data in the binary's folder");*:

  // MARK:- Global config group

  app.add_option("-b,--binary", m_config_global.bin_filepath, "Binary file to disassemble")
      ->required()
      ->check(CLI::ExistingFile)
      ->group("Global");

  app.add_option("-B,--binary2", m_config_global.bin2_filepath, "Second binary file to disassemble (case -m 1)")
      ->check(CLI::ExistingFile)
      ->group("Global");
*/



  std::vector<std::pair<std::string, DisassemblyMode>> disassembly_mode_map{{"static", DisassemblyMode::STATIC},
                                                                            {"boa", DisassemblyMode::BOA}};
  // Disas type
  /*m_config_global.disas_type = DisassemblyType::RECURSIVE;
  app.add_option("-t,--disas-type", m_config_global.disas_type, "Disassembly type (recursive?")
    ->transform(CLI::CheckedTransformer(disassembly_mode_map, CLI::ignore_case))
    ->group("Global");*/

  m_config_global.disas_mode = DisassemblyMode::STATIC;
  /*
  app.add_option("-d,--disas-mode", m_config_global.disas_mode, "Disassembly mode")
      ->transform(CLI::CheckedTransformer(disassembly_mode_map, CLI::ignore_case))
      ->group("Global");*/

  app.add_option("-s,--start-addrs", m_config_global.start_addrs, "Start addresses to disassemble [Binary entry point]")
      ->group("Global");

  std::vector<std::pair<std::string, DisassemblerEngine>> disassember_map{{"cs", DisassemblerEngine::CAPSTONE}, {"ida", DisassemblerEngine::IDA}};
  m_config_global.disassembler = DisassemblerEngine::IDA;

  app.add_option("-d,--disassembler", m_config_global.disassembler, "Disassembler engine to use [capstone, IDA]")
      ->transform(CLI::CheckedTransformer(disassember_map, CLI::ignore_case))
      ->group("Global");

m_config_global.path_to_ida_script = "/home/gab/TANGUE/ida_scripts/";
  app.add_option("-i,--ida-script", m_config_global.path_to_ida_script, "Path to the idapro running bash script FOLDER. Default is \"/home/gab/TANGUE/ida_scripts/\";")
      ->check(CLI::ExistingDirectory)
      ->group("Global");

  std::vector<addr_t> addrs_not_to_disas;

  // min_nb_bb aligned by default to the sites size
  m_config_global.min_nb_bb = -1;
  app.add_option("-b, --min_nb_bb", m_config_global.min_nb_bb, "Minimum number of basic blocks in a function. Smaller functions will not be considered. By default, equal to the sites size");

  // MARK: - Logging group
  std::vector<std::pair<std::string, spdlog::level::level_enum>> loglevel_map{
      {"trace", spdlog::level::trace},  {"debug", spdlog::level::debug}, {"info", spdlog::level::info},
      {"warning", spdlog::level::warn}, {"error", spdlog::level::err},   {"critical", spdlog::level::critical},
      {"off", spdlog::level::off}};

  spdlog::level::level_enum default_loglevel = spdlog::level::info;

  spdlog::level::level_enum loglevel_global = default_loglevel;
  app.add_option("--loglevel-global", loglevel_global,
                 "Global logging level (if not set, other module levels are set to this level) [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_core = default_loglevel;
  app.add_option("--loglevel-core", loglevel_core, "Log level core [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_bin_parser = default_loglevel;
  app.add_option("--loglevel-bin-parser", loglevel_bin_parser, "Log level binary parser module [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_disassembler = default_loglevel;
  app.add_option("--loglevel-disassembler", loglevel_disassembler, "Log level disassembler module [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_rec_disas = default_loglevel;
  app.add_option("--loglevel-rec-disas", loglevel_rec_disas, "Log level recursive disassembling module [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_boa_disas = default_loglevel;
  app.add_option("--loglevel-boa-disas", loglevel_boa_disas, "Log level BOA disassembling module [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_x86_to_smtlib = default_loglevel;
  app.add_option("--loglevel-x86-to-smtlib", loglevel_x86_to_smtlib, "Log level x86ToSmtlib module [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_solver = default_loglevel;
  app.add_option("--loglevel-solver", loglevel_solver, "Log level solver module [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_tainting = default_loglevel;
  app.add_option("--loglevel-tainting", loglevel_tainting, "Log level tainting module [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_analysis = default_loglevel;
  app.add_option("--loglevel-analysis", loglevel_analysis, "Log level analysis module [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_classification = default_loglevel;
  app.add_option("--loglevel-classification", loglevel_classification, "Log level classification module [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  spdlog::level::level_enum loglevel_database = default_loglevel;
  app.add_option("--loglevel-database", loglevel_database, "Log level database module [info]")
      ->transform(CLI::CheckedTransformer(loglevel_map, CLI::ignore_case))
      ->group("Logging");

  // MARK:- Debug group
  app.add_flag("--test", m_config_global.test_mode, "Enable test mode (only for experimental purpose) [false]")
      ->group("Debug");

  // Parse argv
  try
  {
    app.parse(argc, argv);
  }
  catch(const CLI::ParseError& e)
  {
    std::cout << "CLI error: " << e.get_name() << std::endl;
    throw app.exit(e);
  }

  // Copy addrs_not_to_disas vector in set
  for(auto addr : addrs_not_to_disas)
  {
    m_config_global.addrs_not_to_disas.insert(addr);
  }

  // Check for Radare2
  std::pair<int, std::string> result = utils::exec_shell_command("which radare2");
  if(result.first != 0)
  {
    throw std::runtime_error("Radare2 is needed to run BOA but it is not found in your PATH, see "
                             "https://github.com/radare/radare2 to install it");
  }

  // Create and register loggers
  if(app.count("--loglevel-core") == 0)
  {
    loglevel_core = loglevel_global;
  }
  if(app.count("--loglevel-bin-parser") == 0)
  {
    loglevel_bin_parser = loglevel_global;
  }
  if(app.count("--loglevel-disassembler") == 0)
  {
    loglevel_disassembler = loglevel_global;
  }
  if(app.count("--loglevel-rec-disas") == 0)
  {
    loglevel_rec_disas = loglevel_global;
  }
  if(app.count("--loglevel-boa-disas") == 0)
  {
    loglevel_boa_disas = loglevel_global;
  }
  if(app.count("--loglevel-x86-to-smtlib") == 0)
  {
    loglevel_x86_to_smtlib = loglevel_global;
  }
  if(app.count("--loglevel-solver") == 0)
  {
    loglevel_solver = loglevel_global;
  }
  if(app.count("--loglevel-tainting") == 0)
  {
    loglevel_tainting = loglevel_global;
  }
  if(app.count("--loglevel-analysis") == 0)
  {
    loglevel_analysis = loglevel_global;
  }
  if(app.count("--loglevel-classification") == 0)
  {
    loglevel_classification = loglevel_global;
  }
  if(app.count("--loglevel-database") == 0)
  {
    loglevel_database = loglevel_global;
  }

  // https://github.com/gabime/spdlog/wiki/3.-Custom-formatting
  // std::string loggers_pattern = "[%H:%M:%S.%e] [%=13n] [%^%L%$] %v";
  std::string loggers_pattern = "%L [%=13n] %v";

  auto core_logger = spdlog::stdout_color_st(utils::core_logger);
  core_logger->set_level(loglevel_core);
  core_logger->set_pattern(loggers_pattern);

  auto bin_parser_logger = spdlog::stdout_color_st(utils::bin_parser_logger);
  bin_parser_logger->set_level(loglevel_bin_parser);
  bin_parser_logger->set_pattern(loggers_pattern);

  auto disassembler_logger = spdlog::stdout_color_st(utils::disassembler_logger);
  disassembler_logger->set_level(loglevel_disassembler);
  disassembler_logger->set_pattern(loggers_pattern);

  auto rec_disas_logger = spdlog::stdout_color_st(utils::rec_disas_logger);
  rec_disas_logger->set_level(loglevel_rec_disas);
  rec_disas_logger->set_pattern(loggers_pattern);

  auto boa_disas_logger = spdlog::stdout_color_st(utils::boa_disas_logger);
  boa_disas_logger->set_level(loglevel_boa_disas);
  boa_disas_logger->set_pattern(loggers_pattern);

  auto x86_to_smtlib_logger = spdlog::stdout_color_st(utils::x86_to_smtlib_logger);
  x86_to_smtlib_logger->set_level(loglevel_x86_to_smtlib);
  x86_to_smtlib_logger->set_pattern(loggers_pattern);

  auto solver_logger = spdlog::stdout_color_st(utils::solver_logger);
  solver_logger->set_level(loglevel_solver);
  solver_logger->set_pattern(loggers_pattern);

  auto tainting_logger = spdlog::stdout_color_st(utils::tainting_logger);
  tainting_logger->set_level(loglevel_tainting);
  tainting_logger->set_pattern(loggers_pattern);

  auto analysis_logger = spdlog::stdout_color_st(utils::analysis_logger);
  analysis_logger->set_level(loglevel_analysis);
  analysis_logger->set_pattern(loggers_pattern);

  auto classification_logger = spdlog::stdout_color_st(utils::classification_logger);
  classification_logger->set_level(loglevel_classification);
  classification_logger->set_pattern(loggers_pattern);

  auto database_logger = spdlog::stdout_color_st(utils::database_logger);
  database_logger->set_level(loglevel_database);
  database_logger->set_pattern(loggers_pattern);

  // Other
  if(m_config_global.min_nb_bb < 0){
  	// by default equal to site size
  	m_config_global.min_nb_bb = (int)m_config_global.sites_size;
  }
  m_config_global.is_learn_mode = false;
  m_config_global.is_distance_mode = false;
  m_config_global.is_dbcompare_mode = false;
  m_config_global.is_dbdistance_mode = false;

  if(app.got_subcommand(learn)){
    m_config_global.is_learn_mode = true;
    for(auto const& input_path : m_config_global.bin_str_vect){

      if(std::experimental::filesystem::is_directory(input_path)){ // if input is directory, loop through it
        for (const auto & bin_path : std::experimental::filesystem::directory_iterator(input_path)){
          m_config_global.bin_folder.insert(bin_path.path());
        }
      }
      if(std::experimental::filesystem::is_regular_file(input_path)){ // if input is file, just add it
          m_config_global.bin_folder.insert(input_path);
      }
    }
  }
  else if(app.got_subcommand(correlation)){
    m_config_global.is_correlation_mode = true;

    for(auto const& input_path : m_config_global.bin_str_vect){

      if(std::experimental::filesystem::is_directory(input_path)){ // if input is directory, loop through it
        for (const auto & bin_path : std::experimental::filesystem::directory_iterator(input_path)){
          m_config_global.bin_folder.insert(bin_path.path());
        }
      }
      if(std::experimental::filesystem::is_regular_file(input_path)){ // if input is file, just add it
          m_config_global.bin_folder.insert(input_path);
      }
    }

  }
  else{
    throw std::runtime_error("No mode is selected. Please select one.");
  }
	std::unordered_set<std::string> authorized_bgraph_sim_methods ({"backtracking", "all_reg_different", "gen_reg_one_node", "ford-fulkerson"});
	std::unordered_set<std::string> authorized_site_sim_methods ({"backtracking", "bijection", "ford-fulkerson", "sum_gen_reg"});
  if(app.got_subcommand(correlation) && authorized_bgraph_sim_methods.count(m_config_global.bgraph_sim_method)==0){
  	throw std::runtime_error("Please select a valid bipartite graph similarity computation method: backtracking, all_reg_different, gen_reg_one_node, ford-fulkerson");
	}
  if(app.got_subcommand(correlation) && authorized_site_sim_methods.count(m_config_global.site_sim_method)==0){
  	throw std::runtime_error("Please select a valid site similarity computation method: backtracking, bijection, sum_gen_reg, or ford-fulkerson");
	}
}

// MARK:- Getters
const config_global_t& Config::getConfigGlobal() const
{
  return m_config_global;
}

// MARK:- Setters

void Config::setCurrentStartAddr(addr_t addr)
{
  m_config_global.current_start_addr = addr;
}

void Config::addStartAddr(addr_t addr)
{
  m_config_global.start_addrs.push_back(addr);
}

void Config::printOptions(std::shared_ptr<spdlog::logger> log){
  log->info("-- General options:\n");
  log->info("\t Use of function pairs : {}\n", !m_config_global.pairs_test_file.empty());
  log->info("\t Disassembler: {}\n", utils::disassemblerToString(getConfigGlobal().disassembler));
  log->info("\t Sites size: {}\n", std::to_string(getConfigGlobal().sites_size));
  log->info("\t Min bb per function : {}\n", m_config_global.min_nb_bb);
  if(m_config_global.is_correlation_mode){
  	log->info("-- Correlation:\n");
  	if(m_config_global.sample_distance){
  	log->info("\tDataflow is NOT used for site comparison\n");}
  	else{
  	log->info("\tDataflow IS used for site comparison\n");}
		log->info("\tSite sim method: {}\n", m_config_global.site_sim_method);
		log->info("\tBgraph sim method: {}\n",m_config_global.bgraph_sim_method);
		log->info("\tFunction similarity score: {}\n", m_config_global.func_similarity_score);
	}
  return;
}

std::string Config::printOptionsCondensed() const{
	std::stringstream f;
	f << "s" << std::to_string(getConfigGlobal().sites_size) << "_";
	f << "b" << std::to_string(getConfigGlobal().min_nb_bb) << "_";
	f << "bgraph_sim_" << getConfigGlobal().bgraph_sim_method << "_";
	f << "site_sim_" << getConfigGlobal().site_sim_method << "_";
	f << utils::disassemblerToString(getConfigGlobal().disassembler);
	return f.str();
}

const std::unordered_set<std::string>& Config::getBinFolder() const{
	if(m_config_global.pairs_test_file.empty()){
		return m_config_global.bin_folder;
	}
	else{
		return m_config_global.bin_folder_test_pairs;
	}
}

/**
 * Parses the .csv pairs file input and updates the test_func_pairs member
 * Also gathers the name of the bins that should be analyzed
 * */
void Config::checkForCsvInput(sptrLog log){
	if(!m_config_global.pairs_test_file.empty()){
		// parse the csv file and fill the m_config_global.pairs_test_functions
		std::filebuf fb;
		if (fb.open (m_config_global.pairs_test_file,std::ios::in))
  	{
			std::istream csv_file(&fb);
			std::vector<std::string> csv_line = utils::getNextLineAndSplitIntoTokens(csv_file);
			csv_line = utils::getNextLineAndSplitIntoTokens(csv_file);
			while(csv_line.size() > 2){
				log->trace("Parsing vect of length {}\n", csv_line.size());
				std::string bin1_str = csv_line.at(1);
				std::string bin2_str = csv_line.at(4);

				bin1_str = utils::base_name(bin1_str);
				bin2_str = utils::base_name(bin2_str);

				bin1_str = bin1_str.substr(0,bin1_str.find_last_of(".")); // remove the .i64
				bin2_str = bin2_str.substr(0,bin2_str.find_last_of("."));

				func_pair_data_t new_pair;
				new_pair.bin1 = bin1_str;
				new_pair.fva1 = std::strtoul(csv_line.at(2).data(), NULL, 16);
				new_pair.fname1 = csv_line.at(3);
				new_pair.bin2 = bin2_str;
				new_pair.fva2 = std::strtoul(csv_line.at(5).data(), NULL, 16);
				new_pair.fname2 = csv_line.at(6);
				m_config_global.test_funcs_pairs.push_back(new_pair); // add the test pair data to the test_func pairs member

				for(auto const & bin : m_config_global.bin_folder){
					if(bin2_str == utils::base_name(bin) || bin1_str == utils::base_name(bin)){m_config_global.bin_folder_test_pairs.insert(bin);}
				}

				csv_line = utils::getNextLineAndSplitIntoTokens(csv_file);
			}
		}
	}
}
} // namespace boa
