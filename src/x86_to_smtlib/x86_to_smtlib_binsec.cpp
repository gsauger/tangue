#include "x86_to_smtlib_binsec.hpp"

#include <nlohmann/json.hpp>
#include <sstream>
#include <string>

namespace boa
{

x86ToSmtlibBinsec::x86ToSmtlibBinsec()
{
  SPDLOG_LOGGER_TRACE(m_log, "x86ToSmtlibBinsec::x86ToSmtlibBinsec()");
  // Check for BinSec
  if(utils::exec_shell_command("which binsec").first != 0)
  {
    throw std::runtime_error("BinSec is not found in your PATH, please run 'make binsec' in your build directory or if "
                             "you use Xcode, select binsec in Xcode target and build it");
  }
}

x86ToSmtlibBinsec::~x86ToSmtlibBinsec()
{
  SPDLOG_LOGGER_TRACE(m_log, "x86ToSmtlibBinsec::~x86ToSmtlibBinsec()");
}

void x86ToSmtlibBinsec::binsecUnsupportedHook(const Instr* instr) const
{
  const auto instr_size = instr->getSize();
  const auto& instr_bytes = instr->getBytes();
  const auto& instr_opcode = instr->getOpcode();

  // INT case
  if(instr_size == 1 && (instr_bytes[0] == 0xcc || instr_bytes[0] == 0xcd))
  {
    SPDLOG_LOGGER_DEBUG(m_log, "Do not use BinSec with INT instructions");
    instr->initMicroInstrs();
    return;
  }

  if(instr_size == 1 && (instr_bytes[0] == 0x6d || instr_bytes[0] == 0x6f))
  {
    m_log->warn("Do not use BinSec with 'outsd' and 'insd' instructions");
    instr->initMicroInstrs();
    return;
  }

  if((instr_size == 1 && (instr_bytes[0] == 0xee || instr_bytes[0] == 0xef)) ||
     (instr_size == 2 && (instr_bytes[0] == 0xe6 || instr_bytes[0] == 0xe7)))
  {
    m_log->warn("Do not use BinSec with 'out' instruction");
    instr->initMicroInstrs();
    return;
  }

  if((instr_size == 1 && (instr_bytes[0] == 0xec || instr_bytes[0] == 0xed)) ||
     (instr_size == 2 && (instr_bytes[0] == 0xe4 || instr_bytes[0] == 0xe5)))
  {
    m_log->warn("Do not use BinSec with 'in' instruction");
    instr->initMicroInstrs();
    return;
  }

  if(instr_size == 2 && (instr_bytes[0] == 0x0F && instr_bytes[1] == 0x01))
  {
    m_log->warn("Do not use BinSec with 'SIDT' instruction");
    instr->initMicroInstrs();
    return;
  }

  if(instr_size == 2 && (instr_bytes[0] == 0x0f && instr_bytes[1] == 0x31))
  {
    m_log->warn("Do not use BinSec with 'rdtsc' instruction");
    instr->initMicroInstrs();
    return;
  }

  if(instr_opcode.find("fld ") != std::string::npos)
  {
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\tFLD instr (pushes the source operand onto the FPU register stack)");
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\t\t--> nothing to do");
    return;
  }

  if(instr_opcode.find("fstp") != std::string::npos)
  {
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\tFST instr (copies the value in the ST(0) register to the destination operand)");
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\t\t--> nothing to do");
    return;
  }

  if(instr_opcode.find("fxch") != std::string::npos)
  {
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\tFXCH instr (Exchanges the contents of registers ST(0) and ST(i))");
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\t\t--> nothing to do");
    return;
  }

  if(instr_opcode.find("fchs") != std::string::npos)
  {
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\tFCHS instr (Complements the sign bit of ST(0))");
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\t\t--> nothing to do");
    return;
  }

  if(instr_opcode.find("fild") != std::string::npos)
  {
    SPDLOG_LOGGER_TRACE(m_log,
                        "\t\t\tFILD instr (Converts the signed-integer source operand into double extended-precision "
                        "floating-point format and pushes the value onto the FPU register stack))");
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\t\t--> nothing to do");
    return;
  }

  if(instr_opcode.find("fmul") != std::string::npos || instr_opcode.find("fmulp") != std::string::npos)
  {
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\tFMUL instr (Multiplies the destination and source operands and stores the "
                               "product in the destination location)");
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\t\t--> nothing to do");
    return;
  }

  if(instr_opcode.find("fadd") != std::string::npos || instr_opcode.find("faddp") != std::string::npos)
  {
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\tFADD instr (Adds the destination and source operands and stores the sum in "
                               "the destination location)");
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\t\t--> nothing to do");
    return;
  }

  if(instr_opcode.find("fsub") != std::string::npos || instr_opcode.find("fsubp") != std::string::npos ||
     instr_opcode.find("fsubr") != std::string::npos || instr_opcode.find("fsubrp") != std::string::npos)
  {
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\tFSUB(R) instr (Subtracts the destination operand from the source operand and "
                               "stores the difference in the destination location)");
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\t\t--> nothing to do");
    return;
  }

  if(instr_opcode.find("fdivr") != std::string::npos || instr_opcode.find("fdivrp") != std::string::npos ||
     instr_opcode.find("fdiv") != std::string::npos || instr_opcode.find("fdivp") != std::string::npos)
  {
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\tFDIV(R) instr (Divides the source (destination) operand by the destination "
                               "(source) operand and stores the result in the destination location)");
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\t\t--> nothing to do");
    return;
  }

  if(instr_opcode.find("fld") != std::string::npos)
  {
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\tFLD1/FLDL2T/FLDL2E/FLDPI/FLDLG2/FLDLN2/FLDZ instr (Push one of seven commonly "
                               "used constants onto the FPU register stack)");
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\t\t--> nothing to do");
    return;
  }

  if(instr_opcode.find("fcomi ") != std::string::npos || instr_opcode.find("fcomip ") != std::string::npos ||
     instr_opcode.find("fucomi ") != std::string::npos || instr_opcode.find("fucomip ") != std::string::npos)
  {
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\tFCOMI instr (performs an unordered comparison of the contents of registers ST(0) "
                               "and ST(i) and sets the status flags ZF, PF, and CF in the EFLAGS)");
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\t\t--> TOP for ZF, PF and CF");

    std::unique_ptr<const MicroInstr> micro_instr = std::make_unique<const MicroInstr>(
        MicroInstrType::VAR_RESET, "ZF<1> := undef", "(declare-fun ZF_l () (_ BitVec 1))", "ZF_l");
    instr->pushBackMicroInstr(std::move(micro_instr));

    std::unique_ptr<const MicroInstr> micro_instr1 = std::make_unique<const MicroInstr>(
        MicroInstrType::VAR_RESET, "PF<1> := undef", "(declare-fun PF_l () (_ BitVec 1))", "PF_l");
    instr->pushBackMicroInstr(std::move(micro_instr1));

    std::unique_ptr<const MicroInstr> micro_instr2 = std::make_unique<const MicroInstr>(
        MicroInstrType::VAR_RESET, "CF<1> := undef", "(declare-fun CF_l () (_ BitVec 1))", "CF_l");
    instr->pushBackMicroInstr(std::move(micro_instr2));
    return;
  }

  if(instr_opcode.find("fst word") != std::string::npos || instr_opcode.find("fst dword") != std::string::npos ||
     instr_opcode.find("fst qword") != std::string::npos || instr_opcode.find("fstp word") != std::string::npos ||
     instr_opcode.find("fstp dword") != std::string::npos || instr_opcode.find("fstp qword") != std::string::npos)
  {
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\tFST instr (copies the value in the ST(0) register to the destination operand)");
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\t\t--> TOP for memory :-( --> nothing to do");
    return;
  }

  if(instr_opcode.find("fistp word") != std::string::npos ||
     instr_opcode.find("fistp dword") != std::string::npos || instr_opcode.find("fistp qword [") != std::string::npos)
  {
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\tFIST instr (The FIST instruction converts the value in the ST(0) register to a "
                               "signed integer and stores the result in the destination operand)");
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\t\t--> TOP for memory :-( --> nothing to do");
    return;
  }

  if(instr_opcode.find("fnstcw qword") != std::string::npos ||
     instr_opcode.find("fnstcw dword") != std::string::npos ||
     instr_opcode.find("fnstcw word") != std::string::npos)
  {
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\tFSTCW instr (Stores the current value of the FPU control word at the "
                               "specified destination in memory)");
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\t\t--> TOP for memory :-( --> nothing to do");
    return;
  }
  
  if(instr_opcode.find("fnclex") != std::string::npos)
  {
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\tfnclex");
    return;
  }
  
  if(instr_opcode.find("cpuid") != std::string::npos)
  {
    SPDLOG_LOGGER_TRACE(m_log, "\t\t\tcpuid");
    return;
  }

  throw std::runtime_error("BinSec unsuported instruction (" + instr->toStringLight() + ")");
}

void x86ToSmtlibBinsec::getMicroInstrs(const BasicBlock* basic_block) const
{
  SPDLOG_LOGGER_TRACE(m_log, "x86ToSmtlibBinsec::getMicroInstrs({:#x})", basic_block->getEntryPointAddr());

  if(basic_block->isAllMicroInstrsAlreadyKnown())
  {
    SPDLOG_LOGGER_DEBUG(m_log, "All micro instrs of {:#x} already known", basic_block->getEntryPointAddr());
    return;
  }

  const auto& instr = basic_block->getLastInstr();

  // FAKE LIB FUNCTION case
  if(instr->getType() == InstrType::FAKE_LIB_FUNC)
  {
    m_log->debug("Do not use BinSec with external library function");
    return;
  }

  // REP case
  static const std::vector<uint8_t> bnd_ret_bytes = {0xf2, 0xc3};
  if(instr->getSubType() == InstrSubType::REP && instr->getBytes() != bnd_ret_bytes)
  {
    m_log->debug("Do not use BinSec with REP instructions");
    return;
  }

  // Else we use BinSec
  std::string raw_hex_instrs = "";
  for(auto const& instr : basic_block->getInstrs())
  {
    raw_hex_instrs += instr->getBytesString() + "-";
  }
  raw_hex_instrs.pop_back();

  m_log->info("Get MicroInstrs of BB {:#x} with BinSec", basic_block->getEntryPointAddr());

  // Build BinSec command and call BinSec
  std::string binsec_cmd = "binsec -isa x86 -disasm-at " + addr2s(basic_block->getEntryPointAddr()) +
                           " -disasm-decode-smtlib " + raw_hex_instrs + " -x86-handle-seg fs";
  SPDLOG_LOGGER_TRACE(m_log, "BinSec cmd: {}", binsec_cmd);
  std::pair<int, std::string> result = utils::exec_shell_command(binsec_cmd);
  if(result.first != 0 || result.second.find("Exit with success!") == std::string::npos)
  {
    throw std::runtime_error("BinSec error " + result.second);
  }
  SPDLOG_LOGGER_TRACE(m_log, "BinSec result: {}", result.second);

  // Parse BinSec result in JSON object
  std::string line_to_find = "[disasm:result] JSON: ";
  std::istringstream result_ss(result.second);
  std::string line;
  nlohmann::json result_json;
  while(std::getline(result_ss, line))
  {
    if(line.find(line_to_find) != std::string::npos)
    {
      line.erase(0, line_to_find.length());
      result_json = nlohmann::json::parse(line);
      break;
    }
  }

  // Parse JSON object to extract DBA instrs info
  for(auto instr_json : result_json["instrs"])
  {

    size_t instr_key = instr_json["key"];
    const Instr* instr = basic_block->getInstrs().at(instr_key).get();

    SPDLOG_LOGGER_DEBUG(m_log, "Extract DBA instructions of instr n°{} : {} ({:#x})", instr_key, instr->getOpcode(),
                        instr->getAddr());

    if(instr->getMicroInstrs().has_value())
    {
      continue;
    }

    instr->initMicroInstrs();

    for(auto dba_instr : instr_json["dba_instrs"])
    {

      // key in instr
      int dba_instr_key = dba_instr["key"].get<int>();
      SPDLOG_LOGGER_TRACE(m_log, "\tExtract info of DBA instr n°{})", dba_instr_key);

      // mnemonic
      std::string mnemonic = dba_instr["mnemonic"];
      SPDLOG_LOGGER_TRACE(m_log, "\t\tMnemonic: {}", mnemonic);

      // Detect unsupported instruction BinSec
      if(mnemonic.find("unsupported") != std::string::npos)
      {
        binsecUnsupportedHook(instr);
      }
      else
      {
        // type
        std::string type_s = dba_instr["type"];
        SPDLOG_LOGGER_TRACE(m_log, "\t\tType: {}", type_s);

        MicroInstrType type;
        if(type_s == "var_write" || type_s == "mem_write")
        {
          type = MicroInstrType::VAR_UPDATE;
        }
        else if(type_s == "undef")
        {
          type = MicroInstrType::VAR_RESET;
        }
        else if(type_s == "dyn_jmp")
        {
          type = MicroInstrType::DYN_JMP;
        }
        else if(type_s == "cond")
        {
          type = MicroInstrType::COND;
        }
        else if(type_s == "assert")
        {
          type = MicroInstrType::ASSERT;
        }
        else if(type_s == "stop" || type_s == "static_jmp")
        {
          continue;
        }
        else
        {
          throw std::runtime_error("Need to treat DBA instr type: " + type_s);
        }

        // left var
        if(dba_instr.find("left_var") == dba_instr.end())
        {
          throw std::runtime_error("DBA instr should have a left var");
        }
        std::string left_var = dba_instr["left_var"];
        SPDLOG_LOGGER_TRACE(m_log, "\t\tLeft var: {}", left_var);

        // right vars
        unSetString right_vars;
        if(dba_instr.find("right_vars") != dba_instr.end())
        {
          if(!dba_instr["right_vars"].empty())
          {
            for(std::string right_var : dba_instr["right_vars"])
            {
              SPDLOG_LOGGER_TRACE(m_log, "\t\tRight var: {}", right_var);
              right_vars.insert(right_var);
            }
          }
        }

        // mem readings
        optMemOp mem_reading;
        if(dba_instr.find("mem_readings") != dba_instr.end())
        {
          if(!dba_instr["mem_readings"].empty())
          {

            int cnt = 0;
            std::string last_read_location_expr = "";
            for(auto mem_reading_j : dba_instr["mem_readings"])
            {
              if(cnt != 0)
              {
                if(last_read_location_expr == mem_reading_j["read_location_expr"])
                {
                  continue;
                }
              }
              SPDLOG_LOGGER_TRACE(m_log, "\t\tMem reading");
              unsigned int bytes_size = mem_reading_j["bytes_size"];
              SPDLOG_LOGGER_TRACE(m_log, "\t\t\tBytes size: {}", bytes_size);
              std::string read_location_expr = mem_reading_j["read_location_expr"];
              last_read_location_expr = read_location_expr;
              SPDLOG_LOGGER_TRACE(m_log, "\t\t\tRead location expr: {}", read_location_expr);

              std::unordered_set<std::string> loc_expr_right_vars{};
              std::unordered_set<std::string> loc_expr_right_vars_without_suffix{};
              for(const auto& right_var : right_vars)
              {
                if(read_location_expr.find(right_var) != std::string::npos)
                {
                  SPDLOG_LOGGER_TRACE(m_log, "\t\t\t\tRight vars in location expr: {}", right_var);
                  loc_expr_right_vars.insert(right_var);
                  loc_expr_right_vars_without_suffix.insert(MicroInstr::removeVarSuffix(right_var));
                }
              }

              microinstr::mem_op_t mem_read = {bytes_size, read_location_expr, loc_expr_right_vars,
                                               loc_expr_right_vars_without_suffix};
              mem_reading = mem_read;
              cnt++;
            }
            if(cnt != 1)
            {
              throw std::runtime_error("More than one mem read for this micro instr?! Need to fix it");
            }
          }
        }

        // mem writing
        optMemOp mem_writing;
        if(dba_instr.find("mem_writing") != dba_instr.end())
        {
          auto mem_writing_j = dba_instr["mem_writing"];
          SPDLOG_LOGGER_TRACE(m_log, "\t\tMem writing");
          unsigned int bytes_size = mem_writing_j["bytes_size"];
          SPDLOG_LOGGER_TRACE(m_log, "\t\t\tBytes size: {}", bytes_size);
          std::string write_location_expr = mem_writing_j["write_location_expr"];
          SPDLOG_LOGGER_TRACE(m_log, "\t\t\tWrite location expr: {}", write_location_expr);

          std::unordered_set<std::string> loc_expr_right_vars{};
          std::unordered_set<std::string> loc_expr_right_vars_without_suffix{};
          for(const auto& right_var : right_vars)
          {
            if(write_location_expr.find(right_var) != std::string::npos)
            {
              SPDLOG_LOGGER_TRACE(m_log, "\t\t\t\tRight vars in location expr: {}", right_var);
              loc_expr_right_vars.insert(right_var);
              loc_expr_right_vars_without_suffix.insert(MicroInstr::removeVarSuffix(right_var));
            }
          }

          microinstr::mem_op_t mem_w = {bytes_size, write_location_expr, loc_expr_right_vars,
                                        loc_expr_right_vars_without_suffix};
          mem_writing = mem_w;
        }

        // smtlib_expr
        std::string smtlib_expr = dba_instr["smtlib_expr"];
        SPDLOG_LOGGER_TRACE(m_log, "\t\tSmtlib expr: {}", smtlib_expr);

        std::unique_ptr<const MicroInstr> micro_instr = std::make_unique<const MicroInstr>(
            type, mnemonic, smtlib_expr, left_var, right_vars, mem_reading, mem_writing);

        // Move micro instr in the corresponding instruction
        instr->pushBackMicroInstr(std::move(micro_instr));
      }
    }
  }
}

} // namespace boa
