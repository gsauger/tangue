#ifndef x86_TO_SMTLIB_H
#define x86_TO_SMTLIB_H

#include <stdio.h>

#include "binary/binary.hpp"
#include "common/basic_block.hpp"
#include "utils/utils.hpp"

namespace boa
{

class x86ToSmtlib
{
public:
  x86ToSmtlib();
  virtual ~x86ToSmtlib() = 0;

  virtual void getMicroInstrs(const BasicBlock* basic_block) const = 0;

protected:
  std::shared_ptr<spdlog::logger> m_log;
};

} // namespace boa

#endif /* x86_TO_SMTLIB_H */
