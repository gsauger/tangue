#ifndef x86_TO_SMTLIB_BINSEC_H
#define x86_TO_SMTLIB_BINSEC_H

#include <fstream>
#include <stdio.h>

#include "binary/binary.hpp"
#include "x86_to_smtlib.hpp"

namespace boa
{

class x86ToSmtlibBinsec : public x86ToSmtlib
{

public:
  x86ToSmtlibBinsec();
  virtual ~x86ToSmtlibBinsec();

  virtual void getMicroInstrs(const BasicBlock* basic_block) const;

private:
  void binsecUnsupportedHook(const Instr* instr) const;
};

} // namespace boa

#endif /* SMTLIB_ENGINE_BINSEC_H */
