
#ifndef EXEC_FORMULA_H
#define EXEC_FORMULA_H

#include <stdio.h>

#include "binary/binary_parser.hpp"
#include "boa_disassembling/machine_state.hpp"
#include "common/basic_block.hpp"
#include "solver/solver_manager.hpp"
#include "utils/smtlib_utils.hpp"
#include "utils/utils.hpp"

namespace boa
{

enum class JccBranchTaken
{
  BOTH,
  NEXT_ADDR,
  TARGET
};

using SPtrBB = std::shared_ptr<BasicBlock>;

enum class ResultSrc
{
  FROM_SOLVER,
  FROM_CACHE
};

namespace execformula
{

struct smtlib_formula_t
{
  std::string smtlib_text;

  // Entry symb vars
  std::map<Register, std::string> regs_entry_symb_var;
  std::string mem_entry_symb_var;

  // Exit symb vars
  std::map<Register, std::string> regs_exit_symb_var;
  std::string mem_exit_symb_var;

  // Other symb vars
  std::map<const microinstr::mem_op_t*, std::string> read_mems_symb_var;
  std::map<const microinstr::mem_op_t*, std::string> write_mems_symb_var;
  std::optional<std::string> cond_symb_var;    // If BB ends with a Jcc
  std::optional<std::string> dyn_jmp_symb_var; // If BB ends with a dyn jmp
  std::map<const MicroInstr*, std::string> asserts_symb_var; // DIV case

  // Toutes les variables pour lesquelles on a un assert avec une valeur en entrée
  std::unordered_map<std::string, bool> entry_conc_vars;

  // Toutes les cases mémoire pour lesquelles on a un assert avec une valeur en entrée
  std::unordered_map<addr_t, bool> entry_conc_mem_cells;
};

struct conc_values_t
{
  std::unordered_map<const microinstr::mem_op_t*, std::optional<uint32_t>> mem_read_locs;
  std::unordered_map<const microinstr::mem_op_t*, std::optional<uint32_t>> mem_write_locs;
  std::unordered_map<Register, std::optional<uint32_t>> exit_regs;
  std::unordered_map<addr_t, std::optional<uint8_t>> exit_mem_cases;
  std::optional<uint32_t> dyn_jmp;
  std::optional<int> cond;
  std::unordered_map<const MicroInstr*, std::optional<int>> asserts; // DIV case
  ResultSrc result_src;
};

} // namespace execformula

class ExecFormula
{
public:
  // MARK:- Constructors and destructors
  ExecFormula() = delete;
  ExecFormula(MachineState* entry_machine_state, const BasicBlock* bb);

  ExecFormula(ExecFormula const&) = default;
  ExecFormula& operator=(ExecFormula const&) = default;

  ExecFormula(ExecFormula&&) = default;
  ExecFormula& operator=(ExecFormula&&) = default;

  ~ExecFormula() = default;

  // MARK:- Getters and setters

  // m_entry_machine_state
  MachineState* getEntryMachineState() const;

  // m_bb
  const BasicBlock* getBB() const;

  const execformula::smtlib_formula_t& getSmtlibFormula(Binary* binary, SolverManager& solver_manager,
                                                        bool use_fake_val_for_symb_values,
                                                        bool compute_read_mems = true,
                                                        bool add_binary_bytes_if_needed = true);

  const execformula::conc_values_t* getConcValues(SolverManager& solver_manager, Binary* binary);

  std::string toString() const;

  static const char* resultSrcToString(ResultSrc r);

private:
  // MARK:- Private member variables

  // Entry machine state of the formula
  MachineState* m_entry_machine_state;

  // The basic block
  const BasicBlock* m_bb;

  std::shared_ptr<spdlog::logger> m_log;
  std::shared_ptr<spdlog::logger> m_tainting_log;

  std::optional<execformula::smtlib_formula_t> m_smtlib_formula;
  std::optional<std::shared_ptr<execformula::conc_values_t>> m_conc_values;

  // Previous computed conc_values
  // Key 1: Basic block
  // Key 2: smtlib formula text
  // Value: Corresponding conc_values
  static std::unordered_map<const BasicBlock*,
                            std::unordered_map<std::string, std::shared_ptr<execformula::conc_values_t>>>
      s_computed_conc_values;

  // Private methods
};

} // namespace boa

#endif /* EXEC_FORMULA_H */
