#include "jcc_symb_exec.hpp"

namespace boa
{

// MARK:- Constructors and destructors
JccSymbExec::JccSymbExec() : m_cond_status(JccCondStatus::UNKNOWN), m_result_src(std::nullopt)
{
}

JccSymbExec::JccSymbExec(ExecFormula& exec_formula, SolverManager& solver_manager, Binary* binary)
    : m_cond_status(JccCondStatus::UNKNOWN), m_result_src(std::nullopt)
{

  auto conc_values = exec_formula.getConcValues(solver_manager, binary);
  if(conc_values->cond.has_value())
  {
    int value = *conc_values->cond;
    if(value == 1)
    {
      m_cond_status = JccCondStatus::TRUE;
    }
    else
    {
      m_cond_status = JccCondStatus::FALSE;
    }
  }
  m_result_src = conc_values->result_src;
}

// MARK:- Getters and setters
JccCondStatus JccSymbExec::getCondStatus() const
{
  return m_cond_status;
}

void JccSymbExec::setCondStatus(const JccCondStatus& cond_status)
{
  m_cond_status = cond_status;
}

ResultSrc JccSymbExec::getResultSrc() const
{
  if(m_result_src == std::nullopt)
  {
    throw std::runtime_error("This JccSymbExec should have a ResultSrc");
  }
  return *m_result_src;
}

// MARK:- Other functions
bool JccSymbExec::isCondKnown() const
{
  if(m_cond_status == JccCondStatus::UNKNOWN)
  {
    return false;
  }
  return true;
}

} // namespace boa
