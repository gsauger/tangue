
#ifndef DYN_JMP_SYMB_EXEC_H
#define DYN_JMP_SYMB_EXEC_H

#include "boa_disassembling/exec_formula.hpp"
#include "boa_disassembling/solver/solver_manager.hpp"
#include "common/instr.hpp"
#include <stdio.h>
#include <unordered_map>

namespace boa
{

struct ret_result_t
{
  unsigned int seen_genuine = 0;
  unsigned int seen_violated = 0;
  unsigned int seen_unknown = 0;
};

struct dyn_jmp_result_t
{
  unsigned int seen_concrete = 0;
  unsigned int seen_unknown = 0;
};

struct dyn_call_result_t
{
  unsigned int seen_concrete = 0;
  unsigned int seen_unknown = 0;
};

enum class DynJmpStatus
{
  CONCRETE,
  TOP
};

class DynJmpSymbExec
{

public:
  // MARK:- Constructors and destructors
  DynJmpSymbExec();

  DynJmpSymbExec(ExecFormula& exec_formula, SolverManager& solver_manager, Binary* binary);

  DynJmpSymbExec(DynJmpSymbExec const&) = default;
  DynJmpSymbExec& operator=(DynJmpSymbExec const&) = default;

  DynJmpSymbExec(DynJmpSymbExec&&) = default;
  DynJmpSymbExec& operator=(DynJmpSymbExec&&) = default;

  ~DynJmpSymbExec() = default;

  // Getters and setters
  DynJmpStatus getStatus() const;
  void setStatus(const DynJmpStatus status);
  const uint32_t& getJmpAddr() const;
  std::string toString() const;
  ResultSrc getResultSrc() const;

private:
  DynJmpStatus m_status;
  std::optional<uint32_t> m_jmp_addr;
  std::optional<ResultSrc> m_result_src;
};

} // namespace boa

#endif /* DYN_JMP_SYMB_EXEC_H */
