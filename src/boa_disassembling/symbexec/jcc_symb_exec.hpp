#ifndef JCC_SYMB_EXEC_H
#define JCC_SYMB_EXEC_H

#include "boa_disassembling/exec_formula.hpp"
#include "boa_disassembling/solver/solver_manager.hpp"
#include "common/instr.hpp"
#include <stdio.h>
#include <unordered_map>

namespace boa
{

struct jcc_result_t
{
  unsigned int seen_only_target = 0;
  unsigned int seen_only_next_mem = 0;
  unsigned int seen_both_branches = 0;
};

enum class JccCondStatus
{
  UNKNOWN,
  TRUE,
  FALSE
};

class JccSymbExec
{

public:
  // MARK:- Constructors and destructors
  JccSymbExec();

  JccSymbExec(ExecFormula& exec_formula, SolverManager& solver_manager, Binary* binary);

  JccSymbExec(JccSymbExec const&) = default;
  JccSymbExec& operator=(JccSymbExec const&) = default;

  JccSymbExec(JccSymbExec&&) = default;
  JccSymbExec& operator=(JccSymbExec&&) = default;

  ~JccSymbExec() = default;

  // MARK:- Getters and setters
  JccCondStatus getCondStatus() const;
  void setCondStatus(const JccCondStatus& cond_status);
  ResultSrc getResultSrc() const;

  // MARK:- Other functions
  bool isCondKnown() const;

private:
  JccCondStatus m_cond_status;
  std::optional<ResultSrc> m_result_src;
};

} // namespace boa

#endif /* JCC_SYMB_EXEC_H */
