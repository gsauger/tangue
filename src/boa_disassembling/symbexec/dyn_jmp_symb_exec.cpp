#include "dyn_jmp_symb_exec.hpp"

namespace boa
{

const char* dynJmpStatusToString(const DynJmpStatus& dyn_jmp_status)
{
  switch(dyn_jmp_status)
  {
  case DynJmpStatus::TOP:
    return "TOP";
  case DynJmpStatus::CONCRETE:
    return "CONCRETE";
  }
  return "unknown status";
}

// MARK:- Constructors and destructors
DynJmpSymbExec::DynJmpSymbExec() : m_status(DynJmpStatus::TOP), m_jmp_addr(std::nullopt), m_result_src(std::nullopt)
{
}

DynJmpSymbExec::DynJmpSymbExec(ExecFormula& exec_formula, SolverManager& solver_manager, Binary* binary)
    : m_status(DynJmpStatus::TOP)
{
  auto conc_values = exec_formula.getConcValues(solver_manager, binary);
  if(conc_values->dyn_jmp.has_value())
  {
    m_jmp_addr = *conc_values->dyn_jmp;
    m_status = DynJmpStatus::CONCRETE;
  }
  m_result_src = conc_values->result_src;
}

// Getters and setters
DynJmpStatus DynJmpSymbExec::getStatus() const
{
  return m_status;
}

void DynJmpSymbExec::setStatus(const DynJmpStatus status)
{
  m_status = status;
}

const uint32_t& DynJmpSymbExec::getJmpAddr() const
{
  if(m_status != DynJmpStatus::CONCRETE)
  {
    throw std::runtime_error("Trying to get jmp addr of a no CONCRETE DynJmp");
  }
  if(m_jmp_addr == std::nullopt)
  {
    throw std::runtime_error("A VarState with CONCRETE status should have a value");
  }
  return *m_jmp_addr;
}

std::string DynJmpSymbExec::toString() const
{
  std::string s{};
  s += dynJmpStatusToString(m_status);
  if(m_status == DynJmpStatus::CONCRETE)
  {
    s += " (" + utils::uint32_to_string(*m_jmp_addr) + ")";
  }
  return s;
}

ResultSrc DynJmpSymbExec::getResultSrc() const
{
  if(m_result_src == std::nullopt)
  {
    throw std::runtime_error("This DynJmpSymbExec should have a ResultSrc");
  }
  return *m_result_src;
}

} // namespace boa
