#include "machine_state.hpp"

#include <assert.h>
#include <fstream>
#include <stack>

namespace boa
{

// MARK:- Constructors and destructors
MachineState::MachineState(Binary* binary, addr_t instr_ptr, BinaryParser* bin_parser, const std::string& windows_dlls_dir)
    : m_log(spdlog::get(utils::boa_disas_logger)), m_instr_ptr(instr_ptr), m_binary(binary), m_bin_parser(bin_parser),
      m_windows_dlls_dir(windows_dlls_dir)
{
  // Add each regs
  for(const auto& reg_p : getAllCpuRegs())
  {
    m_reg_values.insert({reg_p.first, std::nullopt});
  }

  auto bin_format = binary->getFormat();

  if(bin_format == BinaryFormat::RAW)
  {
    m_stack_start_addr = 0x00070000;
    m_heap_ptr_addr = 0x00090000;

    m_memory_perms_range.insert({0x0, {Permission::READ, Permission::WRITE}});
  }
  else if(bin_format == BinaryFormat::PE)
  {
    // 0x00000000 --> 0x0000FFFF nothing
    m_memory_perms_range.insert({0x00000000, std::set<Permission>()});
    
    // 0x00010000 --> 0x0006FFFF stack
    m_stack_start_addr = 0x00060000; // On fait démarrer un peu plus bas pour laisser la place aux params etc
    m_memory_perms_range.insert({0x00010000, {Permission::READ, Permission::WRITE}});
    
    // 0x00070000 --> 0x0007FFFF nothing
    m_memory_perms_range.insert({0x00070000, std::set<Permission>()});
    
    // 0x00080000 --> 0x002FFFFF heap
    m_heap_ptr_addr = 0x00080000;
    m_memory_perms_range.insert({0x00080000, {Permission::READ, Permission::WRITE}});
    
    // 0x00300000 --> 0x003FFFFF nothing
    m_memory_perms_range.insert({0x00300000, std::set<Permission>()});
    
    // 0x00400000 --> binary base addr -1 : nothing?
    //m_memory_perms_range.insert({0x00400000, std::set<Permission>()});
    
    // Binary header file : READ perm?
    m_memory_perms_range.insert({binary->getBaseAddr(), {Permission::READ}});
    
    // Add bianry section perms
    addr_t last_pe_addr = 0x0;
    // Add sections perms
    for(const auto& section : binary->getSections())
    {
      m_memory_perms_range.insert({section.first, section.second.perms});
      if(section.second.vaddr + section.second.size - 1 > last_pe_addr)
      {
        last_pe_addr = section.second.vaddr + section.second.size - 1;
      }
    }

    m_memory_perms_range.insert({last_pe_addr + 1, {Permission::READ, Permission::WRITE}});
    
    // 0x10000000 --> 0x71FFFFFF  Free space (virtual alloc) :
    m_virtual_alloc_ptr_addr = 0x10000000;
    m_memory_perms_range.insert({m_virtual_alloc_ptr_addr, {Permission::READ, Permission::WRITE}});
    
    // 0x72000000 --> 0x7FFDCFFF DLL space
    m_dlls_loading_ptr = 0x72000000;
    m_memory_perms_range.insert({m_dlls_loading_ptr, {Permission::READ, Permission::WRITE}});
    
    // 0x7FFDD000 --> 0x7FFDEFFF TEB
    m_memory_perms_range.insert({0x7FFDD000, {Permission::READ, Permission::WRITE}});
    
    // 0x7FFDF000 --> 0x7FFDFFFF PEB
    m_memory_perms_range.insert({0x7FFDF000, {Permission::READ, Permission::WRITE}});
    
    // 0x7FFE0000 --> 0x7FFE0FFF Shared user page
    m_memory_perms_range.insert({0x7FFE0000, {Permission::READ, Permission::WRITE}});
    
    // 0x7FFE1000 --> 0xFFFFFFFF : No access and kernel space
    m_memory_perms_range.insert({0x7FFE1000, {}});
    
  }
  else if(bin_format == BinaryFormat::ELF)
  {

    m_stack_start_addr = 0xBF000000;
    m_heap_ptr_addr = 0xAEFA0000;

    m_memory_perms_range.insert({0x00000000, std::set<Permission>()});
    m_memory_perms_range.insert({0xAEFA0000, {Permission::READ, Permission::WRITE}});
    m_memory_perms_range.insert({0xC0000000, {}}); // Kernel space

    addr_t last_elf_addr = 0x0;
    // Add segments perms
    for(const auto& segment : binary->getSegments())
    {
      m_memory_perms_range.insert({segment.first, segment.second.perms});
      if(segment.second.vaddr + segment.second.size - 1 > last_elf_addr)
      {
        last_elf_addr = segment.second.vaddr + segment.second.size - 1;
      }
    }
    m_memory_perms_range.insert({last_elf_addr + 1, {Permission::READ, Permission::WRITE}});
  }
  else
  {
    throw std::runtime_error("Need to implement this Binary format");
  }
}

// MARK:- Getters and setters

// Written addrs history
const std::unordered_set<addr_t>& MachineState::getWrittenAddrsHistory() const
{
  return m_written_addrs_history;
}

void MachineState::addWrittenAddrInHistory(addr_t addr)
{
  m_written_addrs_history.insert(addr);
}

void MachineState::clearWrittenAddrsHistory()
{
  m_written_addrs_history.clear();
}

// Registers
const std::unordered_map<Register, std::optional<uint32_t>>& MachineState::getRegValues() const
{
  return m_reg_values;
}

std::optional<uint32_t> MachineState::getRegValue(Register reg) const
{
  // All regs are already here (added by the ocnstructor)
  return m_reg_values.at(reg);
}

void MachineState::setRegValue(Register reg, std::optional<uint32_t> value)
{
  m_reg_values[reg] = value;
}

void MachineState::setRegValueFromMemDword(Register reg, addr_t mem_case, bool use_binary_value_if_bottom)
{
  m_reg_values[reg] = getMemDword(mem_case, use_binary_value_if_bottom);
}

// Mem cases
const std::unordered_map<addr_t, std::optional<uint8_t>>& MachineState::getMemCellValues() const
{
  return m_mem_cell_values;
}

std::optional<uint8_t> MachineState::getMemCellValue(addr_t mem_case, bool use_binary_value_if_bottom) const
{
  if(m_mem_cell_values.find(mem_case) == m_mem_cell_values.end())
  {
    if(use_binary_value_if_bottom)
    {
      // First, check if we are in the binary
      auto byte = m_binary->getByte(mem_case);
      if (byte.has_value())
      {
        return *byte;
      }
      
      // Second, check if we are in a DLL
      for (const auto& dll : m_loaded_dlls)
      {
        if(dll.second.binary != nullptr)
        {
          byte = dll.second.binary->getByte(mem_case);
          if (byte.has_value())
          {
            m_log->warn("Read operation in DLL binary {}", dll.first);
            return *byte;
          }
        }
      }
      
      return std::nullopt;
    }
    else
    {
      return std::nullopt;
    }
  }
  else
  {
    return m_mem_cell_values.at(mem_case);
  }
}

void MachineState::setMemCellValue(addr_t mem_case, uint8_t value)
{
  m_mem_cell_values[mem_case] = value;
}

void MachineState::setMemCellValue(addr_t mem_case, std::optional<uint8_t> value)
{
  m_mem_cell_values[mem_case] = value;
}

void MachineState::setMemCellDwordValue(addr_t mem_case, uint32_t value)
{
  auto bytes = utils::dword2bytes(value);
  m_mem_cell_values[mem_case] = std::get<0>(bytes);
  m_mem_cell_values[mem_case + 1] = std::get<1>(bytes);
  m_mem_cell_values[mem_case + 2] = std::get<2>(bytes);
  m_mem_cell_values[mem_case + 3] = std::get<3>(bytes);
}

void MachineState::setMemCellDwordValue(addr_t mem_case, std::optional<uint32_t> value)
{
  if(value.has_value())
  {
    auto bytes = utils::dword2bytes(*value);
    m_mem_cell_values[mem_case] = std::get<0>(bytes);
    m_mem_cell_values[mem_case + 1] = std::get<1>(bytes);
    m_mem_cell_values[mem_case + 2] = std::get<2>(bytes);
    m_mem_cell_values[mem_case + 3] = std::get<3>(bytes);
  }
  else
  {
    m_mem_cell_values[mem_case] = std::nullopt;
    m_mem_cell_values[mem_case + 1] = std::nullopt;
    m_mem_cell_values[mem_case + 2] = std::nullopt;
    m_mem_cell_values[mem_case + 3] = std::nullopt;
  }
}

void MachineState::setMemCellWordValue(addr_t mem_case, uint16_t value)
{
  auto bytes = utils::word2bytes(value);
  m_mem_cell_values[mem_case] = std::get<0>(bytes);
  m_mem_cell_values[mem_case + 1] = std::get<1>(bytes);
}

std::optional<uint16_t> MachineState::getMemWord(addr_t addr, bool use_binary_value_if_bottom) const
{
  uint16_t mem_word = 0x0;
  for(unsigned int i = 0; i < 2; i++)
  {
    std::optional<uint8_t> byte_value = getMemCellValue(addr + i, use_binary_value_if_bottom);
    if(!byte_value.has_value())
    {
      return std::nullopt;
    }
    mem_word = mem_word + (*byte_value * std::pow(256, i));
  }
  return mem_word;
}

std::optional<uint32_t> MachineState::getMemDword(addr_t addr, bool use_binary_value_if_bottom) const
{
  uint32_t mem_word = 0x0;
  for(unsigned int i = 0; i < 4; i++)
  {
    std::optional<uint8_t> byte_value = getMemCellValue(addr + i, use_binary_value_if_bottom);
    if(!byte_value.has_value())
    {
      return std::nullopt;
    }
    mem_word = mem_word + (*byte_value * std::pow(256, i));
  }
  return mem_word;
}

std::optional<std::string> MachineState::getAsciiText(addr_t addr, bool use_binary_value_if_bottom) const
{
  std::vector<uint8_t> module_bytes{};
  unsigned int i = 0;
  while(true)
  {
    std::optional<uint8_t> byte_value = getMemCellValue(addr + i, use_binary_value_if_bottom);
    if(!byte_value.has_value())
    {
      return std::nullopt;
    }
    //std::cout << std::hex << *byte_value << std::endl;
    if(*byte_value >= 0x00 && *byte_value <= 0x7F)
    {
      if(*byte_value == 0x00)
      {
        std::string text(module_bytes.begin(), module_bytes.end());
        return text;
      }

      module_bytes.push_back(*byte_value);
      i++;
    }

    // If we found a non ascii char
    else
    {
      return std::nullopt;
    }

    if(i > 10000)
    {
      return std::nullopt;
    }
  }
}

std::optional<std::string> MachineState::getUnicodeText(addr_t addr, bool use_binary_value_if_bottom) const
{
  // TODO: Regarder comment on fait la conversion "correctement" parce que là je suppose que c'est de l'ASCII...
  std::vector<uint8_t> module_bytes{};
  unsigned int i = 0;
  while(true)
  {
    std::optional<uint16_t> char_value = getMemWord(addr + i, use_binary_value_if_bottom);
    if(!char_value.has_value())
    {
      return std::nullopt;
    }

    if(*char_value == 0x0000U)
    {
      std::string text(module_bytes.begin(), module_bytes.end());
      return text;
    }
    module_bytes.push_back((uint8_t)*char_value);
    i = i + 2;

    if(i > 10000)
    {
      return std::nullopt;
    }
  }
}

bool MachineState::isMemCellConc(addr_t addr, bool use_binary_value_if_bottom) const
{
  return (getMemCellValue(addr, use_binary_value_if_bottom).has_value());
}

bool MachineState::areMemCellsConc(addr_t first_addr, unsigned int size, bool use_binary_value_if_bottom) const
{
  for(unsigned int i = 0; i < size; i++)
  {
    if(!getMemCellValue(first_addr + i, use_binary_value_if_bottom).has_value())
    {
      return false;
    }
  }
  return true;
}

// Mem cases perms
const std::set<Permission>& MachineState::getMemCellPerms(addr_t mem_case) const
{
  // On part en arrière
  // Dès que la borne min est plus petite ou égale que notre addresse
  // c'est qu'on est dans le bon intervalle
  for(auto it = m_memory_perms_range.rbegin(); it != m_memory_perms_range.rend(); it++)
  {
    if(it->first <= mem_case)
    {
      return it->second;
    }
  }

  // On ne devrait pas arriver ici car il doit y avoir la borne 0x0 dans le map
  throw std::runtime_error("getMemCellPerms() failed");
}

void MachineState::setMemoryPermsRange(addr_t first_addr, addr_t last_addr, const std::set<Permission>& perms)
{
  // On récupère les perms à l'@ last_addr + 1
  auto perms_to_restore = getMemCellPerms(last_addr + 1);

  // On ajoute le nouveau range (enfin la borne min)
  m_memory_perms_range[first_addr] = perms;

  // On supprime les anciennes bornes entre first_addr et last_addr
  std::unordered_set<addr_t> keys_to_remove;
  for(const auto& p : m_memory_perms_range)
  {
    if(p.first > last_addr)
    {
      break;
    }
    if(p.first > first_addr && p.first <= last_addr)
    {
      keys_to_remove.insert(p.first);
    }
  }
  for(const auto& k : keys_to_remove)
  {
    m_memory_perms_range.erase(k);
  }

  // On restaure l'ancienne permission
  m_memory_perms_range[last_addr + 1] = perms_to_restore;
}

// Exception context
void MachineState::setExceptionContext(const exception_context_t& exception_context)
{
  m_current_exception_context = exception_context;
}

std::optional<exception_context_t>& MachineState::getExceptionContext()
{
  return m_current_exception_context;
}

void MachineState::clearExceptionContext()
{
  m_current_exception_context = std::nullopt;
}

// Virtual alloc ptr
addr_t MachineState::getVirtualAllocPtr() const
{
  return m_virtual_alloc_ptr_addr;
}

void MachineState::setVirtualAllocPtr(addr_t v)
{
  m_virtual_alloc_ptr_addr = v;
}

// Heap ptr
addr_t MachineState::getHeapPtr() const
{
  return m_heap_ptr_addr;
}

void MachineState::setHeapPtr(addr_t v)
{
  m_heap_ptr_addr = v;
}

// Tls stuff
uint32_t MachineState::getTlsSlot() const
{
  return m_tls_slot;
}

void MachineState::setTlsSlot(uint32_t v)
{
  m_tls_slot = v;
}

void MachineState::linkDll(const std::string& lib, bool load_dlls)
{

  if (lib.empty())
  {
    return;
  }
  
  // If this DLLs has already been loaded, nothing to do
  if (m_loaded_dlls.find(lib) != m_loaded_dlls.end())
  {
    return;
  }
  
  bool perform_fake_dll_loading = true;
  auto dll_path = m_windows_dlls_dir + "/" + lib;
  
  if(load_dlls)
  {
    // If we don't have this DLL file, perform a fake dll loading
    if (!std::ifstream(dll_path).good())
    {
      m_log->warn("Missing DLL file {}", dll_path);
    }
    else
    {
      perform_fake_dll_loading = false;
    }
  }
  
  if(perform_fake_dll_loading)
  {
    auto loading_addr = m_dlls_loading_ptr;
    // Reserve space for fake entries
    m_dlls_loading_ptr += 0x0000000000010000ULL;
    
    m_loaded_dlls.insert({lib, {loading_addr, nullptr}});

    m_log->info("Fake linked DLL {} at {:#x} (next: {:#x})", lib, loading_addr, m_dlls_loading_ptr);
  }
  else
  {
    auto bin_dll = m_bin_parser->loadBinary(dll_path, m_dlls_loading_ptr);
    m_loaded_dlls.insert({lib, {m_dlls_loading_ptr, bin_dll}});

    auto offset = bin_dll->getLastAddr() - bin_dll->getBaseAddr();
    offset &= 0xffffffffffff0000ULL;
    offset += 0x0000000000010000ULL;
    m_dlls_loading_ptr += offset;

    m_log->info("Linked DLL {} at {:#x} (next: {:#x})", lib, bin_dll->getBaseAddr(), m_dlls_loading_ptr);

    // Recursive DLL linking
    for (const auto& import : bin_dll->getImports())
    {
      linkDll(import.lib, load_dlls);
      patchIat(import);
    }
  }
}

void MachineState::patchIat(const binary::import_entry_t& import_entry)
{
  if (import_entry.lib.empty())
  {
    return;
  }

  auto& dll = m_loaded_dlls.at(import_entry.lib);
  
  if(dll.binary != nullptr)
  {
    auto export_entry = dll.binary->getExportByName(import_entry.name);
    if(export_entry == nullptr)
    {
      throw std::runtime_error("Function " + import_entry.name + " should be in exported functions of DLL " + import_entry.lib);
    }
    setMemCellDwordValue(import_entry.vaddr, (uint32_t)export_entry->vaddr);
    m_log->info("Patched IAT entry {}_{} ({:#x}) with {:#x} address", import_entry.lib, import_entry.name,
                import_entry.vaddr, export_entry->vaddr);
  }
  // Fake case
  else
  {
    auto fake_function_vaddr = dll.handle + dll.exported_functions.size();
    
    setMemCellDwordValue(import_entry.vaddr, fake_function_vaddr);
    m_log->info("Patched IAT entry {}_{} ({:#x}) with {:#x} address (fake entry)", import_entry.lib, import_entry.name,
                import_entry.vaddr, fake_function_vaddr);
    dll.exported_functions.insert({import_entry.name, fake_function_vaddr});
  }
}

// Instruction pointer
addr_t MachineState::getInstrPtr() const
{
  return m_instr_ptr;
}

void MachineState::setInstrPtr(addr_t instr_ptr)
{
  m_instr_ptr = instr_ptr;
}

// Returnsite stack
const std::vector<addr_t>& MachineState::getReturnsiteStack() const
{
  return m_returnsite_stack;
}

void MachineState::pushReturnsite(addr_t returnsite)
{
  m_returnsite_stack.push_back(returnsite);
}

void MachineState::popReturnsiteIfNeeded(addr_t returnsite)
{
  if(!m_returnsite_stack.empty() && returnsite == m_returnsite_stack.back())
  {
    m_returnsite_stack.pop_back();
  }
}

// MARK:- Other functions
addr_t MachineState::getStackStartAddr() const
{
  return m_stack_start_addr;
}

const std::unordered_map<std::string, dll_t>& MachineState::getLoadedDlls() const
{
  return m_loaded_dlls;
}

dll_t* MachineState::getLoadedDllAt(addr_t module_handle)
{
  for(auto& dll : m_loaded_dlls)
  {
    if(dll.second.handle == module_handle)
    {
      return &dll.second;
    }
  }
  return nullptr;
}


void MachineState::setInitMachineState(BinaryFormat bin_format, bool load_dlls, const std::unordered_map<Register, uint32_t>& user_reg_values, const std::unordered_map<addr_t, uint8_t>& user_mem_cell_values)
{

  m_reg_values[Register::TF] = 0x0;
  m_reg_values[Register::DF] = 0x0;

  // Clear debug control & status registers
  m_reg_values[Register::DR6] = 0x00000000U;
  m_reg_values[Register::DR7] = 0x00000000U;

  if(bin_format == BinaryFormat::PE)
  {
    m_reg_values[Register::EAX] = 0x0;
    m_reg_values[Register::EBX] = 0x7FFD8000;
    m_reg_values[Register::ECX] = m_stack_start_addr - 0x14;
    m_reg_values[Register::EDX] = 0x64870914;

    m_reg_values[Register::ESI] = 0x9;
    m_reg_values[Register::EDI] = 0x7c920202;

    m_reg_values[Register::ESP] = m_stack_start_addr;
    m_reg_values[Register::EBP] = m_stack_start_addr + 0x2C;

    /*
     * At entrypoint, some packers (tElock 0.99) assume fixed values of EFLAGS
     * are set by Windows.
     * Following constants are found using the `dump_initial_context` tool.
     */
    m_reg_values[Register::CF] = 0x0;
    m_reg_values[Register::PF] = 0x1;
    m_reg_values[Register::AF] = 0x0;
    m_reg_values[Register::ZF] = 0x1;
    m_reg_values[Register::SF] = 0x0;
    m_reg_values[Register::TF] = 0x0;
    m_reg_values[Register::DF] = 0x0;
    m_reg_values[Register::OF] = 0x0;
    
    // Thread Information Block
    m_reg_values[Register::FS] = 0x7FFDD000;
    
    // Process Environment Block
    uint32_t peb_start_addr = 0x7FFDF000;
    
    // SEH frame
    setMemCellDwordValue(*m_reg_values[Register::FS] + 0x00, m_stack_start_addr + 0x1C);
    
    // Process ID (SVK read this mem cell at 6th instruction of the trace)
    setMemCellDwordValue(*m_reg_values[Register::FS] + 0x20, 0x100);

    // Linear address of Process Environment Block (PEB)
    setMemCellDwordValue(*m_reg_values[Register::FS] + 0x30, peb_start_addr);
        
    // Neolite reads 0x7c920222 value at [ESP + 0x4] at 2nd instruction of the trace
    // Don't know where this value come from :-/
    setMemCellDwordValue(*m_reg_values[Register::ESP] + 0x04, 0x7c920222);
    
    // RLpack reads 0x9 value at [ESP + 0x8] at 5th instruction of the trace
    // Don't know where this value come from :-/
    setMemCellDwordValue(*m_reg_values[Register::ESP] + 0x08, 0x9);
        
    // Fake environment variables of the process
    std::vector<uint16_t> env_vars = {0x3d, 0x43, 0x3a, 0x3d, 0x43, 0x3a, 0x5c, 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x73, 0x5c, 0x53, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x33, 0x32, 0x00, 0x3d, 0x45, 0x3a, 0x3d, 0x45, 0x3a, 0x5c, 0x00, 0x41, 0x4c, 0x4c, 0x55, 0x53, 0x45, 0x52, 0x53, 0x50, 0x52, 0x4f, 0x46, 0x49, 0x4c, 0x45, 0x3d, 0x43, 0x3a, 0x5c, 0x50, 0x72, 0x6f, 0x67, 0x72, 0x61, 0x6d, 0x44, 0x61, 0x74, 0x61, 0x00, 0x41, 0x52, 0x43, 0x48, 0x3d, 0x33, 0x32, 0x00, 0x43, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x50, 0x72, 0x6f, 0x67, 0x72, 0x61, 0x6d, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x3d, 0x43, 0x3a, 0x5c, 0x50, 0x72, 0x6f, 0x67, 0x72, 0x61, 0x6d, 0x20, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x20, 0x28, 0x78, 0x38, 0x36, 0x29, 0x5c, 0x43, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x20, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x00, 0x43, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x50, 0x72, 0x6f, 0x67, 0x72, 0x61, 0x6d, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x28, 0x78, 0x38, 0x36, 0x29, 0x3d, 0x43, 0x3a, 0x5c, 0x50, 0x72, 0x6f, 0x67, 0x72, 0x61, 0x6d, 0x20, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x20, 0x28, 0x78, 0x38, 0x36, 0x29, 0x5c, 0x43, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x20, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x00, 0x43, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x50, 0x72, 0x6f, 0x67, 0x72, 0x61, 0x6d, 0x57, 0x36, 0x34, 0x33, 0x32, 0x3d, 0x43, 0x3a, 0x5c, 0x50, 0x72, 0x6f, 0x67, 0x72, 0x61, 0x6d, 0x20, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x5c, 0x43, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x20, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x00, 0x43, 0x4f, 0x4d, 0x50, 0x55, 0x54, 0x45, 0x52, 0x4e, 0x41, 0x4d, 0x45, 0x3d, 0x4c, 0x48, 0x53, 0x2d, 0x50, 0x43, 0x00, 0x43, 0x6f, 0x6d, 0x53, 0x70, 0x65, 0x63, 0x3d, 0x43, 0x3a, 0x5c, 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x73, 0x5c, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x33, 0x32, 0x5c, 0x63, 0x6d, 0x64, 0x2e, 0x65, 0x78, 0x65, 0x00, 0x46, 0x50, 0x5f, 0x4e, 0x4f, 0x5f, 0x48, 0x4f, 0x53, 0x54, 0x5f, 0x43, 0x48, 0x45, 0x43, 0x4b, 0x3d, 0x4e, 0x4f, 0x00, 0x48, 0x4f, 0x4d, 0x45, 0x44, 0x52, 0x49, 0x56, 0x45, 0x3d, 0x43, 0x3a, 0x00, 0x48, 0x4f, 0x4d, 0x45, 0x50, 0x41, 0x54, 0x48, 0x3d, 0x5c, 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x73, 0x5c, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x33, 0x32, 0x00, 0x4e, 0x55, 0x4d, 0x42, 0x45, 0x52, 0x5f, 0x4f, 0x46, 0x5f, 0x50, 0x52, 0x4f, 0x43, 0x45, 0x53, 0x53, 0x4f, 0x52, 0x53, 0x3d, 0x32, 0x00, 0x4f, 0x53, 0x3d, 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x73, 0x5f, 0x4e, 0x54, 0x00, 0x50, 0x61, 0x74, 0x68, 0x3d, 0x43, 0x3a, 0x5c, 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x73, 0x5c, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x33, 0x32, 0x3b, 0x43, 0x3a, 0x5c, 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x73, 0x3b, 0x43, 0x3a, 0x5c, 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x73, 0x5c, 0x53, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x33, 0x32, 0x5c, 0x57, 0x62, 0x65, 0x6d, 0x3b, 0x43, 0x3a, 0x5c, 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x73, 0x5c, 0x53, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x33, 0x32, 0x5c, 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x73, 0x50, 0x6f, 0x77, 0x65, 0x72, 0x53, 0x68, 0x65, 0x6c, 0x6c, 0x5c, 0x76, 0x31, 0x2e, 0x30, 0x5c, 0x00, 0x50, 0x41, 0x54, 0x48, 0x45, 0x58, 0x54, 0x3d, 0x2e, 0x43, 0x4f, 0x4d, 0x3b, 0x2e, 0x45, 0x58, 0x45, 0x3b, 0x2e, 0x42, 0x41, 0x54, 0x3b, 0x2e, 0x43, 0x4d, 0x44, 0x3b, 0x2e, 0x56, 0x42, 0x53, 0x3b, 0x2e, 0x56, 0x42, 0x45, 0x3b, 0x2e, 0x4a, 0x53, 0x3b, 0x2e, 0x4a, 0x53, 0x45, 0x3b, 0x2e, 0x57, 0x53, 0x46, 0x3b, 0x2e, 0x57, 0x53, 0x48, 0x3b, 0x2e, 0x4d, 0x53, 0x43, 0x00, 0x50, 0x52, 0x4f, 0x43, 0x45, 0x53, 0x53, 0x4f, 0x52, 0x5f, 0x41, 0x52, 0x43, 0x48, 0x49, 0x54, 0x45, 0x43, 0x54, 0x55, 0x52, 0x45, 0x3d, 0x78, 0x38, 0x36, 0x00, 0x50, 0x52, 0x4f, 0x43, 0x45, 0x53, 0x53, 0x4f, 0x52, 0x5f, 0x41, 0x52, 0x43, 0x48, 0x49, 0x54, 0x45, 0x57, 0x36, 0x34, 0x33, 0x32, 0x3d, 0x41, 0x4d, 0x44, 0x36, 0x34, 0x00, 0x50, 0x52, 0x4f, 0x43, 0x45, 0x53, 0x53, 0x4f, 0x52, 0x5f, 0x49, 0x44, 0x45, 0x4e, 0x54, 0x49, 0x46, 0x49, 0x45, 0x52, 0x3d, 0x49, 0x6e, 0x74, 0x65, 0x6c, 0x36, 0x34, 0x20, 0x46, 0x61, 0x6d, 0x69, 0x6c, 0x79, 0x20, 0x36, 0x20, 0x4d, 0x6f, 0x64, 0x65, 0x6c, 0x20, 0x36, 0x30, 0x20, 0x53, 0x74, 0x65, 0x70, 0x70, 0x69, 0x6e, 0x67, 0x20, 0x33, 0x2c, 0x20, 0x47, 0x65, 0x6e, 0x75, 0x69, 0x6e, 0x65, 0x49, 0x6e, 0x74, 0x65, 0x6c, 0x00, 0x50, 0x52, 0x4f, 0x43, 0x45, 0x53, 0x53, 0x4f, 0x52, 0x5f, 0x4c, 0x45, 0x56, 0x45, 0x4c, 0x3d, 0x36, 0x00, 0x50, 0x52, 0x4f, 0x43, 0x45, 0x53, 0x53, 0x4f, 0x52, 0x5f, 0x52, 0x45, 0x56, 0x49, 0x53, 0x49, 0x4f, 0x4e, 0x3d, 0x33, 0x63, 0x30, 0x33, 0x00, 0x50, 0x72, 0x6f, 0x67, 0x72, 0x61, 0x6d, 0x44, 0x61, 0x74, 0x61, 0x3d, 0x43, 0x3a, 0x5c, 0x50, 0x72, 0x6f, 0x67, 0x72, 0x61, 0x6d, 0x44, 0x61, 0x74, 0x61, 0x00, 0x50, 0x72, 0x6f, 0x67, 0x72, 0x61, 0x6d, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x3d, 0x43, 0x3a, 0x5c, 0x50, 0x72, 0x6f, 0x67, 0x72, 0x61, 0x6d, 0x20, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x20, 0x28, 0x78, 0x38, 0x36, 0x29, 0x00, 0x50, 0x72, 0x6f, 0x67, 0x72, 0x61, 0x6d, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x28, 0x78, 0x38, 0x36, 0x29, 0x3d, 0x43, 0x3a, 0x5c, 0x50, 0x72, 0x6f, 0x67, 0x72, 0x61, 0x6d, 0x20, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x20, 0x28, 0x78, 0x38, 0x36, 0x29, 0x00, 0x50, 0x72, 0x6f, 0x67, 0x72, 0x61, 0x6d, 0x57, 0x36, 0x34, 0x33, 0x32, 0x3d, 0x43, 0x3a, 0x5c, 0x50, 0x72, 0x6f, 0x67, 0x72, 0x61, 0x6d, 0x20, 0x46, 0x69, 0x6c, 0x65, 0x73, 0x00, 0x50, 0x52, 0x4f, 0x4d, 0x50, 0x54, 0x3d, 0x24, 0x50, 0x24, 0x47, 0x00, 0x50, 0x53, 0x4d, 0x6f, 0x64, 0x75, 0x6c, 0x65, 0x50, 0x61, 0x74, 0x68, 0x3d, 0x43, 0x3a, 0x5c, 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x73, 0x5c, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x33, 0x32, 0x5c, 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x73, 0x50, 0x6f, 0x77, 0x65, 0x72, 0x53, 0x68, 0x65, 0x6c, 0x6c, 0x5c, 0x76, 0x31, 0x2e, 0x30, 0x5c, 0x4d, 0x6f, 0x64, 0x75, 0x6c, 0x65, 0x73, 0x5c, 0x00, 0x50, 0x55, 0x42, 0x4c, 0x49, 0x43, 0x3d, 0x43, 0x3a, 0x5c, 0x55, 0x73, 0x65, 0x72, 0x73, 0x5c, 0x50, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x00, 0x53, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x44, 0x72, 0x69, 0x76, 0x65, 0x3d, 0x43, 0x3a, 0x00, 0x53, 0x79, 0x73, 0x74, 0x65, 0x6d, 0x52, 0x6f, 0x6f, 0x74, 0x3d, 0x43, 0x3a, 0x5c, 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x73, 0x00, 0x54, 0x45, 0x4d, 0x50, 0x3d, 0x43, 0x3a, 0x5c, 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x73, 0x5c, 0x54, 0x45, 0x4d, 0x50, 0x00, 0x54, 0x4d, 0x50, 0x3d, 0x43, 0x3a, 0x5c, 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x73, 0x5c, 0x54, 0x45, 0x4d, 0x50, 0x00, 0x55, 0x53, 0x45, 0x52, 0x44, 0x4f, 0x4d, 0x41, 0x49, 0x4e, 0x3d, 0x6c, 0x68, 0x73, 0x2d, 0x50, 0x43, 0x00, 0x55, 0x53, 0x45, 0x52, 0x4e, 0x41, 0x4d, 0x45, 0x3d, 0x41, 0x64, 0x6d, 0x69, 0x6e, 0x69, 0x73, 0x74, 0x72, 0x61, 0x74, 0x65, 0x75, 0x72, 0x00, 0x55, 0x53, 0x45, 0x52, 0x50, 0x52, 0x4f, 0x46, 0x49, 0x4c, 0x45, 0x3d, 0x43, 0x3a, 0x5c, 0x55, 0x73, 0x65, 0x72, 0x73, 0x5c, 0x44, 0x65, 0x66, 0x61, 0x75, 0x6c, 0x74, 0x00, 0x77, 0x69, 0x6e, 0x64, 0x69, 0x72, 0x3d, 0x43, 0x3a, 0x5c, 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x73, 0x00, 0x00};
    unsigned int i = 0;
    for(auto b : env_vars)
    {
      setMemCellWordValue(0x006607a8 + i, b);
      i = i + 2;
    }
    
    // Fake command line string ("e:\Exec\7mQKun8Qs.exe")
    std::vector<uint16_t> cmd_line_string = {0x65, 0x3A, 0x5C, 0x45, 0x78, 0x65, 0x63, 0x5C, 0x37, 0x6D, 0x51, 0x4B, 0x75, 0x6E, 0x38, 0x51, 0x73, 0x2E, 0x65, 0x78, 0x65, 0x00};
    i = 0;
    for(auto b : cmd_line_string)
    {
      setMemCellValue(0x642bd0 + i, b);
      i = i + 1;
    }
    
    // ntdll.dll is "always loaded" (emomet assumes that...)
    linkDll("ntdll.dll", load_dlls);

    // Load-time dynamic linking
    for(const auto& import : m_binary->getImports())
    {
      linkDll(import.lib, load_dlls);
      patchIat(import);
    }
    
    m_tls_slot = 0x0;
  }
  else
  {
    m_reg_values[Register::EAX] = 0x0;
    m_reg_values[Register::EBX] = 0x7FFDE000;
    m_reg_values[Register::ECX] = 0x0;
    m_reg_values[Register::EDX] = 0x0;

    m_reg_values[Register::ESI] = 0x0;
    m_reg_values[Register::EDI] = 0x0;

    m_reg_values[Register::ESP] = m_stack_start_addr;
    m_reg_values[Register::EBP] = m_stack_start_addr;
  }

  // Apply user reg values
  for(const auto& reg_value : user_reg_values)
  {
    m_reg_values[reg_value.first] = reg_value.second;
  }

  // Apply user mem cell values
  for(const auto& mem_cell_value : user_mem_cell_values)
  {
    m_mem_cell_values[mem_cell_value.first] = mem_cell_value.second;
  }
}

std::string MachineState::toString() const
{
  std::string s{};
  for(auto const& reg_p : m_reg_values)
  {
    s += "* " + reg2s(reg_p.first) + ": ";
    if(reg_p.second.has_value())
    {
      s += "✅ " + utils::uint32_to_string(*reg_p.second);
    }
    else
    {
      s += "❌";
    }
    s += "\n";
  }
  for(auto const& mem_cell_p : m_mem_cell_values)
  {
    s += "* " + addr2s(mem_cell_p.first) + ": ";
    if(mem_cell_p.second.has_value())
    {
      s += "✅ " + byte2s(*mem_cell_p.second);
    }
    else
    {
      s += "❌";
    }
    s += "\n";
  }
  s += "\n";
  s += "* Instruction pointer: " + addr2s(m_instr_ptr);
  return s;
}

std::string MachineState::toStringCompare(const MachineState& other_ms) const
{
  std::string s{};
  
  // Regs
  std::unordered_set<Register> regs;
  for(auto reg_p : other_ms.getRegValues())
  {
    regs.insert(reg_p.first);
  }
  for(auto reg_p : m_reg_values)
  {
    regs.insert(reg_p.first);
  }
  
  for(auto const& reg : regs)
  {
    auto reg_value = getRegValue(reg);
    auto other_ms_reg_value = other_ms.getRegValue(reg);
    
    if(reg_value != other_ms_reg_value)
    {
      s += "* " + reg2s(reg) + ": ";
      
      if(reg_value.has_value())
      {
        s += "(this) ✅ " + utils::uint32_to_string(*reg_value);
      }
      else
      {
        s += "(this) ❌";
      }
      
      if(other_ms_reg_value.has_value())
      {
        s += " (other) ✅ " + utils::uint32_to_string(*other_ms_reg_value);
      }
      else
      {
        s += " (other) ❌";
      }
      s += "\n";
    }
  }
  
  // Mem cells values
  std::unordered_set<addr_t> mem_cells;
  for(auto mem_cell_p : other_ms.getMemCellValues())
  {
    mem_cells.insert(mem_cell_p.first);
  }
  for(auto mem_cell_p : m_mem_cell_values)
  {
    mem_cells.insert(mem_cell_p.first);
  }
  
  for(auto mem_cell : mem_cells)
  {
    auto mem_cell_value = getMemCellValue(mem_cell, true);
    auto other_mem_cell_value = other_ms.getMemCellValue(mem_cell, true);
  
    if(mem_cell_value != other_mem_cell_value)
    {
      s += "* " + addr2s(mem_cell) + ": ";
      
      if(mem_cell_value.has_value())
      {
        s += "(this) ✅ " + byte2s(*mem_cell_value);
      }
      else
      {
        s += "(this) ❌";
      }
      
      if(other_mem_cell_value.has_value())
      {
        s += " (other) ✅ " + byte2s(*other_mem_cell_value);
      }
      else
      {
        s += " (other) ❌";
      }
      
      s += "\n";
    }
  }
  return s;
}

bool MachineState::mergeWithMachineState(const MachineState& machine_state_to_be_merged)
{

  // TODO: Merge perms?... Yes, need to do
  // Pour le moment on merge seulement :
  // - La valeur des registres
  // - La valeur des cases mémoire
  
  // Return true if at least one modification
  bool at_least_one_change = false;
  
  // Registers
  // First, we need to get all regs to treat
  std::unordered_set<Register> regs_to_treat;
  for(auto reg_p : machine_state_to_be_merged.getRegValues())
  {
    regs_to_treat.insert(reg_p.first);
  }
  for(auto reg_p : m_reg_values)
  {
    regs_to_treat.insert(reg_p.first);
  }

  // Now we can treat each reg
  for(auto reg : regs_to_treat)
  {
    auto reg_value = getRegValue(reg);
    auto machine_state_to_be_merged_reg_value = machine_state_to_be_merged.getRegValue(reg);

    if(reg_value.has_value())
    {
      if(machine_state_to_be_merged_reg_value.has_value())
      {
        if(*reg_value != *machine_state_to_be_merged_reg_value)
        {
          m_reg_values[reg] = std::nullopt;
          at_least_one_change = true;
        }
      }
      else
      {
        m_reg_values[reg] = std::nullopt;
        at_least_one_change = true;
      }
    }
  }

  // Memory
  // First, we need to get all mem cases to treat
  std::unordered_set<addr_t> mem_cells_to_treat;
  for(auto mem_cell_p : machine_state_to_be_merged.getMemCellValues())
  {
    mem_cells_to_treat.insert(mem_cell_p.first);
  }
  for(auto mem_cell_p : m_mem_cell_values)
  {
    mem_cells_to_treat.insert(mem_cell_p.first);
  }

  // Now we can treat each mem case
  for(auto mem_cell : mem_cells_to_treat)
  {
    auto mem_cell_value = getMemCellValue(mem_cell, true);
    auto machine_state_to_be_merged_mem_cell_value = machine_state_to_be_merged.getMemCellValue(mem_cell, true);

    if(mem_cell_value.has_value())
    {
      if(machine_state_to_be_merged_mem_cell_value.has_value())
      {
        if(*mem_cell_value != *machine_state_to_be_merged_mem_cell_value)
        {
          m_mem_cell_values[mem_cell] = std::nullopt;
          at_least_one_change = true;
        }
      }
      else
      {
        m_mem_cell_values[mem_cell] = std::nullopt;
        at_least_one_change = true;
      }
    }
  }
  return at_least_one_change;
}


bool MachineState::isEquivalent(const MachineState& ms) const
{
  // This function compare two machine states and check if they are "equivalent"
  // We use this function to know if we add the target after a loop (prvent BOA infinite loop)
  
  // Pour l'instant on se fout des permissions des cases mémoire
  // On regarde seulement :
  // - La valeur des registres
  // - La valeur des cases mémoire
  
  // Reg values
  if(m_reg_values != ms.m_reg_values)
  {
    return false;
  }

  // Mem cell values 1
  for(auto& mem_cell : m_mem_cell_values)
  {
    if(mem_cell.second != ms.getMemCellValue(mem_cell.first, true))
    {
      return false;
    }
  }

  // Mem cell values 2
  for(auto& mem_cell : ms.m_mem_cell_values)
  {
    if(mem_cell.second != getMemCellValue(mem_cell.first, true))
    {
      return false;
    }
  }
  
  return true;
}

void MachineState::dumpMemInFile(const std::string& filepath) const
{
  std::ofstream f(filepath, std::ios::out | std::ios::binary);
  if(!f)
  {
    throw std::runtime_error("Unable to open file " + filepath);
  }
  for(addr_t i = 0x1000002b ; i <= 0x1000002b + 0x20000 ; i++)
  {
    auto b = getMemCellValue(i, false);
    uint8_t byte = 0x00;
    if(b.has_value())
    {
      byte = *b;
      std::cout << "@[" << std::hex << i << "] = " << std::hex << static_cast<int>(byte) << std::endl;
    }
    else
    {
      std::cout << "@[" << std::hex << i << "] = ??" << std::endl;
    }
    f.write(reinterpret_cast<char *>(&byte), 1);
  }
  f.close();
}

} // namespace boa
