#include "exec_formula.hpp"
#include <unordered_map>

namespace boa
{

// MARK:- Static variables
std::unordered_map<const BasicBlock*, std::unordered_map<std::string, std::shared_ptr<execformula::conc_values_t>>>
    ExecFormula::s_computed_conc_values;

// MARK:- Constructors and destructors
ExecFormula::ExecFormula(MachineState* entry_machine_state, const BasicBlock* bb)
    : m_entry_machine_state(entry_machine_state), m_bb(bb), m_log(spdlog::get(utils::boa_disas_logger)),
      m_tainting_log(spdlog::get(utils::tainting_logger))
{
}

// MARK:- Getters and setters
// m_entry_machine_state
MachineState* ExecFormula::getEntryMachineState() const
{
  return m_entry_machine_state;
}

// m_bb
const BasicBlock* ExecFormula::getBB() const
{
  return m_bb;
}

std::string getSmtVarName(const std::string& var_with_suffix, bool need_to_inc,
                          std::unordered_map<std::string, int>& vars_counter)
{
  std::string var_without_suffix = MicroInstr::removeVarSuffix(var_with_suffix);
  if(vars_counter.find(var_without_suffix) == vars_counter.end())
  {
    vars_counter[var_without_suffix] =  0;
  }
  else if(need_to_inc)
  {
    vars_counter[var_without_suffix]++;
  }
  return var_without_suffix + std::to_string(vars_counter[var_without_suffix]);
}

const execformula::smtlib_formula_t& ExecFormula::getSmtlibFormula(Binary* binary, SolverManager& solver_manager,
                                                                   bool use_fake_val_for_symb_values,
                                                                   bool compute_read_mems,
                                                                   bool add_binary_bytes_if_needed)
{
  // SPDLOG_LOGGER_DEBUG(m_log, "[+] Generate formula in SMT-LIBv2 format from ExecFormula object");

  if(!m_smtlib_formula.has_value())
  {
    m_smtlib_formula = std::make_optional<execformula::smtlib_formula_t>();
    auto& smtlib_formula = *m_smtlib_formula;

    // Detect REP or FAKE_LIB_CASE case
    if(m_bb->getSize() == 1)
    {
      if(m_bb->getLastInstr()->getSubType() == InstrSubType::REP ||
         m_bb->getLastInstr()->getType() == InstrType::FAKE_LIB_FUNC)
      {
        return *m_smtlib_formula;
      }
    }

    // For each used var, keep a counter number
    std::unordered_map<std::string, int> vars_counter;

    // Disable print-success
    // smtlib_formula.smtlib_text += smtlib_utils::smt_set_option(":print-success false");

    // Enable produce-models
    smtlib_formula.smtlib_text += smtlib_utils::smt_set_option(":produce-models true");

    // Set logic
    smtlib_formula.smtlib_text += smtlib_utils::smt_set_logic("QF_ABV");
    
    // Declare memory array (declare-fun)
    static const std::pair<std::string, std::string> mem_var = {
    "memory", smtlib_utils::smt_sort_array(smtlib_utils::smt_sort_bv(32), smtlib_utils::smt_sort_bv(8))};
    smtlib_formula.mem_entry_symb_var = getSmtVarName(mem_var.first, true, vars_counter);
    smtlib_formula.smtlib_text += smtlib_utils::smt_decl_fun(smtlib_formula.mem_entry_symb_var, mem_var.second);
    
    // Declare or define each register (declare-fun/define-fun)
    // (here, we simplify declare-fun/assert by a simple define-fun)
    const auto& bb_tainting_info = m_bb->getTaintingInfo();
    for(auto const& reg_p : m_entry_machine_state->getRegValues())
    {
      const auto& reg_smt = reg2smt(reg_p.first);
      std::string smt_var_name = getSmtVarName(reg_smt.first, true, vars_counter);
      smtlib_formula.regs_entry_symb_var.insert({reg_p.first, smt_var_name});

      bool use_define_fun = false;
      if(reg_p.second.has_value())
      {
        // No need to add this reg if
        // - this reg is never read in this BB
        // - this reg is read in this BB, but it is written before
        use_define_fun = true;

        if(bb_tainting_info.regs_read.find(reg_p.first) == bb_tainting_info.regs_read.end())
        {
          use_define_fun = false;
        }
        else
        {
          bool seen_read = false;
          bool seen_write = false;
          for(const auto& instr : m_bb->getInstrs())
          {
            const auto& instr_tainting_info = instr->getTaintingInfo();

            if(instr_tainting_info.regs_read.find(reg_p.first) != instr_tainting_info.regs_read.end())
            {
              seen_read = true;
            }
            if(instr_tainting_info.regs_write.find(reg_p.first) != instr_tainting_info.regs_write.end())
            {
              seen_write = true;
            }

            if(seen_write && !seen_read)
            {
              // On a vu une écriture avant une lecture
              use_define_fun = false;
              break;
            }
          }
        }
      }
      if(use_define_fun)
      {
        // Use define-fun with concrete value
        SPDLOG_LOGGER_TRACE(m_log, "Add {}_entry = {:#x} in formula", reg2s(reg_p.first), *reg_p.second);
        smtlib_formula.smtlib_text += smtlib_utils::smt_def_fun(smt_var_name, reg_smt.second, smtlib_utils::smt_bv_val(*reg_p.second, reg2size(reg_p.first)));
        smtlib_formula.entry_conc_vars[reg2s(reg_p.first)] = true;
      }
      else
      {
        // Use declare-fun
        smtlib_formula.smtlib_text += smtlib_utils::smt_decl_fun(smt_var_name, reg_smt.second);
      }
    }

    // Add BB formula

    // Add smtlib formula of the BB
    // s += smtlib_utils::smt_comment("Formula BB " + m_bb->toString());
    for(auto const& instr : m_bb->getInstrs())
    {

      if(!instr->getMicroInstrs().has_value())
      {
        throw std::runtime_error("Instr " + instr->toStringLight() +
                                 " should have a MicroInstr vector (even if it is empty)");
      }

      // s += smtlib_utils::smt_comment(instr->toStringLight());
      // SPDLOG_LOGGER_TRACE(m_log, "\tAdd instr in smtlib: {}", instr->toStringLight());
      for(auto const& micro_instr : *instr->getMicroInstrs())
      {
        // Add micro instr smtlib expr

        // s += smtlib_utils::smt_comment(micro_instr->toString());
        // SPDLOG_LOGGER_TRACE(m_log, "\t\t* micro instr: {}", micro_instr->toString());

        // First, we take right and left vars
        std::unordered_map<std::string, std::string> smt_vars;

        for(auto const& right_var : micro_instr->getRightVars())
        {
          smt_vars[right_var] = getSmtVarName(right_var, false, vars_counter);
        }
        smt_vars[micro_instr->getLeftVar()] = getSmtVarName(micro_instr->getLeftVar(), true, vars_counter);

        // We append the smtlib expr
        // We need to find and replace each var name suffix (_l and _r)
        std::string smtlib_expr = micro_instr->getSmtlibExpr();
        for(const auto& smt_var_pair : smt_vars)
        {
          utils::find_and_replace(&smtlib_expr, smt_var_pair.first, smt_var_pair.second);
        }
        smtlib_formula.smtlib_text += smtlib_expr + "\n";

        // We need to track each mem readings (always right side)
        if(micro_instr->getMemRead().has_value())
        {
          const auto* mem_read = &(*micro_instr->getMemRead());
          std::string mem_read_location = mem_read->loc_expr;
          for(auto smt_var_pair : smt_vars)
          {
            utils::find_and_replace(&mem_read_location, smt_var_pair.first, smt_var_pair.second);
          }

          smtlib_formula.read_mems_symb_var[mem_read] = mem_read_location;
        }

        // We need to track Jcc cond expr var
        if(micro_instr->getType() == MicroInstrType::COND)
        {
          smtlib_formula.cond_symb_var = micro_instr->getLeftVar();
          for(const auto& smt_var_pair : smt_vars)
          {
            utils::find_and_replace(&(*smtlib_formula.cond_symb_var), smt_var_pair.first, smt_var_pair.second);
          }
        }
        
        // We need to track assert expr var
        if(micro_instr->getType() == MicroInstrType::ASSERT)
        {
          std::string assert_var = micro_instr->getLeftVar();
          for(const auto& smt_var_pair : smt_vars)
          {
            utils::find_and_replace(&assert_var, smt_var_pair.first, smt_var_pair.second);
          }
          smtlib_formula.asserts_symb_var[micro_instr.get()] = assert_var;
        }

        // We need to track DynJmp cond expr var
        else if(micro_instr->getType() == MicroInstrType::DYN_JMP)
        {
          smtlib_formula.dyn_jmp_symb_var = micro_instr->getLeftVar();
          for(const auto& smt_var_pair : smt_vars)
          {
            utils::find_and_replace(&(*smtlib_formula.dyn_jmp_symb_var), smt_var_pair.first, smt_var_pair.second);
          }
        }

        // We need to track each mem writing
        if(micro_instr->getMemWrite().has_value())
        {
          const auto* mem_write = &(*micro_instr->getMemWrite());
          std::string mem_write_location = mem_write->loc_expr;
          for(const auto& smt_var_pair : smt_vars)
          {
            utils::find_and_replace(&mem_write_location, smt_var_pair.first, smt_var_pair.second);
          }
          smtlib_formula.write_mems_symb_var[mem_write] = mem_write_location;
        }
      }
    }

    // We need to keep in mind each exit var of the formula
    smtlib_formula.mem_exit_symb_var = getSmtVarName(mem_var.first, false, vars_counter);
    for(const auto& reg_p : getAllCpuRegs())
    {
      smtlib_formula.regs_exit_symb_var[reg_p.first] = getSmtVarName(reg_p.second, false, vars_counter);
    }

    if(!compute_read_mems)
    {
      SPDLOG_LOGGER_TRACE(m_log, "Do not compute read mems");
    }
    else if(smtlib_formula.read_mems_symb_var.empty())
    {
      SPDLOG_LOGGER_TRACE(m_log, "No memory read in this BB");
    }
    else
    {
      SPDLOG_LOGGER_TRACE(m_log, "Compute memory read in this BB");
      bool compute_mem_readings_again = true;

      while(compute_mem_readings_again)
      {
        compute_mem_readings_again = false;
        bool at_least_one_new_addr_added_in_entry_machine_state = false;

        // Compute mem readings
        auto conc_values = getConcValues(solver_manager, binary);

        for(const auto& instr : m_bb->getInstrs())
        {
          for(const auto& micro_instr : *instr->getMicroInstrs())
          {
            if(!micro_instr->getMemRead().has_value())
            {
              continue;
            }
            SPDLOG_LOGGER_DEBUG(m_log, "Read operation by instr {}: ", instr->toStringLight());
            const auto& mem_read = *micro_instr->getMemRead();
            const auto& mem_read_loc_result = conc_values->mem_read_locs.at(&mem_read);

            if(mem_read_loc_result.has_value())
            {
              SPDLOG_LOGGER_DEBUG(m_log, "`--> ✅ {} Read @: {:#x}_{}", resultSrcToString(conc_values->result_src),
                                  *mem_read_loc_result, mem_read.size);

              for(size_t i = 0; i < mem_read.size; i++)
              {
                addr_t read_addr = *mem_read_loc_result + i;

                if(smtlib_formula.entry_conc_mem_cells.find(read_addr) == smtlib_formula.entry_conc_mem_cells.end())
                {
                  // Check if we have this addr in init machine state
                  auto mem_case_value = m_entry_machine_state->getMemCellValue(read_addr, add_binary_bytes_if_needed);
                  if(mem_case_value.has_value())
                  {
                    SPDLOG_LOGGER_DEBUG(m_log, "\tAdd @[{:#x}]_entry = {:#x} in formula", read_addr, *mem_case_value);
                    smtlib_formula.entry_conc_mem_cells[read_addr] = true;
                    m_entry_machine_state->setMemCellValue(read_addr, *mem_case_value);
                    at_least_one_new_addr_added_in_entry_machine_state = true;
                    std::string mem_case_value_symb = smtlib_utils::smt_select(smtlib_formula.mem_entry_symb_var,
                                                                               smtlib_utils::smt_bv_val(read_addr, 32));
                    smtlib_formula.smtlib_text += smtlib_utils::smt_assert(
                        smtlib_utils::smt_equal(mem_case_value_symb, smtlib_utils::smt_bv_val(*mem_case_value, 8)));
                    m_conc_values = std::nullopt;
                  }
                  else
                  {
                    SPDLOG_LOGGER_DEBUG(m_log, "\t@[{:#x}]_entry is unknown", read_addr);
                    smtlib_formula.entry_conc_mem_cells[read_addr] = false;
                  }
                }
              }
            }
            else
            {
              SPDLOG_LOGGER_DEBUG(m_log, "`--> ❌ {} Read @: unknown", resultSrcToString(conc_values->result_src));
            }
          }
        }
        if(at_least_one_new_addr_added_in_entry_machine_state)
        {
          SPDLOG_LOGGER_TRACE(m_log, "Entry machine state has been modified --> Compute mem reads location again");
          compute_mem_readings_again = true;
        }
      }

      // AsPack hack
      if(use_fake_val_for_symb_values)
      {
        std::unordered_set<addr_t> written_addrs;
        // const auto& stack_start_addr = m_entry_machine_state->getStackStartAddr();
        auto conc_values = getConcValues(solver_manager, binary);
        for(const auto& instr : m_bb->getInstrs())
        {
          for(const auto& micro_instr : *instr->getMicroInstrs())
          {
            const auto& mem_write = micro_instr->getMemWrite();
            if(mem_write.has_value())
            {
              const auto& mem_write_loc = conc_values->mem_write_locs.at(&(*mem_write));
              if(mem_write_loc.has_value())
              {
                for(size_t i = 0; i < mem_write->size; i++)
                {
                  written_addrs.insert(*mem_write_loc + i);
                }
              }
            }

            const auto& mem_read = micro_instr->getMemRead();
            if(mem_read.has_value())
            {
              const auto& mem_read_loc = conc_values->mem_read_locs.at(&(*mem_read));
              if(mem_read_loc.has_value())
              {
                for(size_t i = 0; i < mem_read->size; i++)
                {
                  auto read_addr = *mem_read_loc + i;
                  if(!smtlib_formula.entry_conc_mem_cells.at(read_addr))
                  {
                    if(written_addrs.find(read_addr) == written_addrs.end())
                    {
                      uint8_t byte = 0x12;
                      m_log->warn("Mem read at @ {:#x} without any value --> use byte {:#x}", read_addr, byte);
                      utils::pushMessage("Add a fake byte value at @ " + addr2s(read_addr));
                      smtlib_formula.entry_conc_mem_cells[read_addr] = true;
                      m_entry_machine_state->setMemCellValue(read_addr, byte);
                      std::string mem_case_addr_symb = smtlib_utils::smt_bv_val(read_addr, 32);
                      std::string mem_case_value_symb =
                          smtlib_utils::smt_select(smtlib_formula.mem_entry_symb_var, mem_case_addr_symb);
                      std::string value_bv = smtlib_utils::smt_bv_val(byte, 8);
                      smtlib_formula.smtlib_text +=
                          smtlib_utils::smt_assert(smtlib_utils::smt_equal(mem_case_value_symb, value_bv));
                      m_conc_values = std::nullopt;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  // SPDLOG_LOGGER_TRACE(m_log, "Smtlib exec formula: \n" + smtlib_formula.smtlib_text);
  // SPDLOG_LOGGER_DEBUG(m_log, "[-] Exit from getSmtlibFormula");

  return *m_smtlib_formula;
}

const execformula::conc_values_t* ExecFormula::getConcValues(SolverManager& solver_manager, Binary* binary)
{
  SPDLOG_LOGGER_TRACE(m_log, "[+] getConcValues");
  // SPDLOG_LOGGER_DEBUG(m_log, "Smtlib exec formula: \n" + m_smtlib_formula->smtlib_text);

  if(!m_smtlib_formula.has_value())
  {
    throw std::runtime_error("Call getSmtlibFormula before getConcValues");
  }

  const auto& smtlib_formula = *m_smtlib_formula;

  // If m_conc_values is already ready
  if(m_conc_values.has_value())
  {
    SPDLOG_LOGGER_TRACE(m_log, "[-] getConcValues (1)");
    return m_conc_values->get();
  }

  // Else check if we have a previous result for this BB and with this smtlib_text
  const auto it1 = s_computed_conc_values.find(m_bb);
  if(it1 != s_computed_conc_values.end())
  {
    const auto it2 = it1->second.find(smtlib_formula.smtlib_text);
    if(it2 != it1->second.end())
    {
      m_conc_values = it2->second;
      (*m_conc_values)->result_src = ResultSrc::FROM_CACHE;
      SPDLOG_LOGGER_TRACE(m_log, "[-] getConcValues (2)");
      return m_conc_values->get();
    }
  }

  // Else we need to compute with solver and tainting

  // Prepare symb vars for solver
  std::unordered_map<std::string, uint32_t> symb_vars_to_compute_with_solver;

  // Registers
  const auto& bb_tainting_info = m_bb->getTaintingInfo();
  for(const auto& reg_exit_p : smtlib_formula.regs_exit_symb_var)
  {
    // Do not add not modified regs
    if(bb_tainting_info.regs_write.find(reg_exit_p.first) != bb_tainting_info.regs_write.end())
    {
      symb_vars_to_compute_with_solver[reg_exit_p.second];
    }
  }

  // Read mems
  for(const auto& read_mem_p : smtlib_formula.read_mems_symb_var)
  {
    // Loc
    symb_vars_to_compute_with_solver[read_mem_p.second];
    // Val
    /*
    std::string read_mem_val_expr = smtlib_utils::smt_select_n_array_cases(read_mem_p.first->size,
    smtlib_formula.mem_exit_symb_var, read_mem_p.second);
    symb_vars_to_compute_with_solver.insert({read_mem_val_expr, read_mem_p.first->size * 8});
     */
  }

  // Write mems
  for(const auto& write_mem_p : smtlib_formula.write_mems_symb_var)
  {
    // Loc
    symb_vars_to_compute_with_solver[write_mem_p.second];
    // Val
    std::string write_mem_val_expr = smtlib_utils::smt_select_n_array_cases(
        write_mem_p.first->size, smtlib_formula.mem_exit_symb_var, write_mem_p.second);
    symb_vars_to_compute_with_solver[write_mem_val_expr];
  }

  // Cond
  if(smtlib_formula.cond_symb_var.has_value())
  {
    symb_vars_to_compute_with_solver[*smtlib_formula.cond_symb_var];
  }

  // Dyn jmp
  if(smtlib_formula.dyn_jmp_symb_var.has_value())
  {
    symb_vars_to_compute_with_solver[*smtlib_formula.dyn_jmp_symb_var];
  }
  
  // Asserts (DIV)
  for(const auto& assert_p : smtlib_formula.asserts_symb_var)
  {
    symb_vars_to_compute_with_solver[assert_p.second];
  }

  // Ask solver for symb vars sat value
  solver_manager.getSymbVarsSatValue(symb_vars_to_compute_with_solver, smtlib_formula.smtlib_text);

  // Apply tainting to find conc vars
  m_conc_values = std::make_shared<execformula::conc_values_t>();
  auto conc_value = *m_conc_values;

  // Ces deux map permettent de savoir à tout instant les vars/cases mémoire qui sont conc ou symb
  std::unordered_map<std::string, bool> conc_vars;
  std::unordered_map<addr_t, bool> conc_mem_cells;

  // On initialise avec les vars/cases mémoire qui sont conc en entrée
  conc_vars.insert(smtlib_formula.entry_conc_vars.begin(), smtlib_formula.entry_conc_vars.end());
  conc_mem_cells.insert(smtlib_formula.entry_conc_mem_cells.begin(), smtlib_formula.entry_conc_mem_cells.end());

  for(const auto& instr : m_bb->getInstrs())
  {
    SPDLOG_LOGGER_DEBUG(m_tainting_log, "* {}", instr->toStringLight());
    for(const auto& micro_instr : *instr->getMicroInstrs())
    {
      SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t- {}", micro_instr->getMnemonic());
      auto micro_instr_type = micro_instr->getType();
      const auto& left_var = micro_instr->getLeftVar();
      const auto& left_var_without_suffix = micro_instr->getLeftVarWithoutSuffix();

      // Mem read
      bool has_mem_read = false;
      bool mem_read_loc_is_conc = false;
      bool mem_read_val_is_conc = false;
      const auto& mem_read = micro_instr->getMemRead();
      if(mem_read.has_value())
      {
        const auto* mem_read_ptr = &(*mem_read);
        has_mem_read = true;
        mem_read_loc_is_conc = true;
        SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t* Mem read {}_{}", mem_read->loc_expr, mem_read->size);

        for(const auto& var_used_by_read_loc_without_suffix : mem_read->loc_expr_right_vars_without_suffix)
        {
          SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t\t- Used var by read loc: {}", var_used_by_read_loc_without_suffix);
          if(conc_vars.find(var_used_by_read_loc_without_suffix) == conc_vars.end())
          {
            conc_vars[var_used_by_read_loc_without_suffix] = false;
          }
          if(!conc_vars.at(var_used_by_read_loc_without_suffix))
          {
            SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t\t\t* Symb --> Mem read loc is symb");
            mem_read_loc_is_conc = false;
            break;
          }
        }

        if(mem_read_loc_is_conc)
        {
          // mem read loc is conc
          mem_read_val_is_conc = true;
          const auto& read_loc =
              symb_vars_to_compute_with_solver.at(smtlib_formula.read_mems_symb_var.at(mem_read_ptr));
          SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t\t- Read loc: {:#x}", read_loc);
          conc_value->mem_read_locs.insert({mem_read_ptr, read_loc});

          // Check if mem read val is conc
          for(unsigned int i = 0; i < mem_read->size; i++)
          {
            addr_t addr = read_loc + i;
            SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t\t\t* Read byte at @[{:#x}]", addr);
            if(conc_mem_cells.find(addr) == conc_mem_cells.end())
            {
              conc_mem_cells[addr] = false;
            }
            if(!conc_mem_cells.at(addr))
            {
              SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t\t\t\t- Symb --> Mem read val is symb");
              mem_read_val_is_conc = false;
            }
          }
        }
        else
        {
          // Mem read loc is symb :-/
          conc_value->mem_read_locs.insert({mem_read_ptr, std::nullopt});
        }
      }

      // Right vars
      bool right_vars_are_all_conc = true;
      if(micro_instr_type == MicroInstrType::VAR_RESET)
      {
        SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t* Left var is reset");
        right_vars_are_all_conc = false;
      }
      else
      {
        for(const auto& right_var_without_suffix : micro_instr->getRightVarsWithoutSuffix())
        {
          if(right_var_without_suffix == "memory")
          {
            continue;
          }
          SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t* Right var: {}", right_var_without_suffix);
          if(conc_vars.find(right_var_without_suffix) == conc_vars.end())
          {
            conc_vars[right_var_without_suffix] = false;
          }
          if(!conc_vars.at(right_var_without_suffix))
          {
            SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t\t- Symb --> Left var is symb");
            right_vars_are_all_conc = false;
            break;
          }
        }
      }

      // Var write
      if(micro_instr_type == MicroInstrType::VAR_UPDATE || micro_instr_type == MicroInstrType::VAR_RESET)
      {

        if(micro_instr->getMemWrite().has_value())
        {
          // Memory write
          const auto& mem_write = *micro_instr->getMemWrite();
          SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t* Mem write {}_{}", mem_write.loc_expr, mem_write.size);

          bool mem_write_loc_is_conc = true;
          bool mem_write_val_is_conc = false;

          for(const auto& var_used_by_write_loc_without_suffix : mem_write.loc_expr_right_vars_without_suffix)
          {
            SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t\t- Used var by write loc: {}",
                                var_used_by_write_loc_without_suffix);
            if(conc_vars.find(var_used_by_write_loc_without_suffix) == conc_vars.end())
            {
              conc_vars[var_used_by_write_loc_without_suffix] = false;
            }
            if(!conc_vars.at(var_used_by_write_loc_without_suffix))
            {
              SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t\t\t* Symb --> Mem write loc is symb");
              mem_write_loc_is_conc = false;
              break;
            }
          }

          if(mem_write_loc_is_conc)
          {
            mem_write_val_is_conc = true;

            // Si l'adresse de l'écriture est conc, on regarde maintenant si la valeur écrite est conc
            const auto& write_loc =
                symb_vars_to_compute_with_solver.at(smtlib_formula.write_mems_symb_var.at(&mem_write));
            conc_value->mem_write_locs.insert({&mem_write, write_loc});
            SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t\t- Write loc: {:#x}", write_loc);

            if(has_mem_read)
            {
              if(!mem_read_loc_is_conc || !mem_read_val_is_conc)
              {
                mem_write_val_is_conc = false;
              }
            }
            if(mem_write_val_is_conc && !right_vars_are_all_conc)
            {
              mem_write_val_is_conc = false;
            }

            // Conclusion
            if(mem_write_val_is_conc)
            {

              std::vector<uint8_t> bytes;

              std::string write_mem_val_expr = smtlib_utils::smt_select_n_array_cases(
                  mem_write.size, smtlib_formula.mem_exit_symb_var, smtlib_formula.write_mems_symb_var.at(&mem_write));
              const auto& write_val = symb_vars_to_compute_with_solver.at(write_mem_val_expr);
              SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t\t- Write val: {:#x}", write_val);
              if(mem_write.size == 1)
              {
                bytes.push_back((uint8_t)write_val);
              }
              else if(mem_write.size == 2)
              {
                bytes.push_back(write_val & 0x000000FF);
                bytes.push_back((write_val & 0x0000FF00) >> 8);
              }
              else if(mem_write.size == 4)
              {
                bytes.push_back(write_val & 0x000000FF);
                bytes.push_back((write_val & 0x0000FF00) >> 8);
                bytes.push_back((write_val & 0x00FF0000) >> 16);
                bytes.push_back((write_val & 0xFF000000) >> 24);
              }

              for(unsigned int i = 0; i < mem_write.size; i++)
              {
                addr_t addr = write_loc + i;
                conc_mem_cells[addr] = true;
                conc_value->exit_mem_cases[addr] = bytes.at(i);
              }
            }
            else
            {
              SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t\t- Write val is symb");
              for(unsigned int i = 0; i < mem_write.size; i++)
              {
                addr_t addr = write_loc + i;
                conc_mem_cells[addr] = false;
                conc_value->exit_mem_cases[addr] = std::nullopt;
              }
            }
          }

          else
          {
            // Si la localication est symb, alors on ne sait pas où l'écriture est faite
            // donc toute la mémoire passe à TOP
            conc_value->mem_write_locs[&mem_write] = std::nullopt;
          }
        }
        else
        {
          // Var write
          bool write_var_is_conc = true;

          if(has_mem_read)
          {
            if(!mem_read_loc_is_conc || !mem_read_val_is_conc)
            {
              write_var_is_conc = false;
            }
          }
          if(write_var_is_conc && !right_vars_are_all_conc)
          {
            write_var_is_conc = false;
          }

          // Conclusion
          if(write_var_is_conc)
          {
            conc_vars[left_var_without_suffix] = true;
            SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t* Write var {} is conc", left_var_without_suffix);
          }
          else
          {
            conc_vars[left_var_without_suffix] = false;
            SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t* Write var {} is symb", left_var_without_suffix);
          }
        }
      }

      else if(micro_instr_type == MicroInstrType::COND || micro_instr_type == MicroInstrType::DYN_JMP || micro_instr_type == MicroInstrType::ASSERT)
      {

        SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t* Left var (cond or dyn jmp) {}", left_var_without_suffix);

        bool write_var_is_conc = true;

        if(has_mem_read)
        {
          if(!mem_read_loc_is_conc || !mem_read_val_is_conc)
          {
            write_var_is_conc = false;
          }
        }
        if(write_var_is_conc && !right_vars_are_all_conc)
        {
          write_var_is_conc = false;
        }

        // Conclusion
        conc_vars[left_var] = write_var_is_conc;

        if(micro_instr_type == MicroInstrType::COND)
        {
          if(write_var_is_conc)
          {
            conc_value->cond = symb_vars_to_compute_with_solver[*smtlib_formula.cond_symb_var];
            SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t\t- Cond is conc: {}", *conc_value->cond);
          }
          else
          {
            conc_value->cond = std::nullopt;
            SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t\t- Cond is symb");
          }
        }
        else if(micro_instr_type == MicroInstrType::ASSERT)
        {
          if(write_var_is_conc)
          {
            conc_value->asserts[micro_instr.get()] = symb_vars_to_compute_with_solver[smtlib_formula.asserts_symb_var.at(micro_instr.get())];
            SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t\t- Assert expr is conc: {}", *conc_value->asserts[micro_instr.get()]);
          }
          else
          {
            conc_value->asserts[micro_instr.get()] = std::nullopt;
            SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t\t- Assert expr is symb");
          }
        }
        else if(micro_instr_type == MicroInstrType::DYN_JMP)
        {
          if(write_var_is_conc)
          {
            conc_value->dyn_jmp = symb_vars_to_compute_with_solver[*smtlib_formula.dyn_jmp_symb_var];
            SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t\t- Dyn jmp is conc: {:#x}", *conc_value->dyn_jmp);
          }
          else
          {
            conc_value->dyn_jmp = std::nullopt;
            SPDLOG_LOGGER_DEBUG(m_tainting_log, "\t\t\t- Dyn jmp is symb");
          }
        }
      }
    }
  }

  for(const auto& var_conc : conc_vars)
  {
    if(isAValidReg(var_conc.first))
    {
      Register reg = s2reg(var_conc.first);
      if(bb_tainting_info.regs_write.find(reg) != bb_tainting_info.regs_write.end())
      {
        if(var_conc.second)
        {
          conc_value->exit_regs[reg] = symb_vars_to_compute_with_solver[smtlib_formula.regs_exit_symb_var.at(reg)];
        }
        else
        {
          conc_value->exit_regs[reg] = std::nullopt;
        }
      }
    }
  }

  conc_value->result_src = ResultSrc::FROM_SOLVER;

  // Save this result
  s_computed_conc_values[m_bb][smtlib_formula.smtlib_text] = conc_value;

  SPDLOG_LOGGER_TRACE(m_log, "[-] getConcValues (3)");
  return m_conc_values->get();
}

std::string ExecFormula::toString() const
{
  std::string s{};
  s += "[*] BB " + m_bb->toString();

  s += " with entry machine state:\n";

  std::string machine_state_s = m_entry_machine_state->toString();
  utils::find_and_replace(&machine_state_s, "\n", "\n\t");
  s += "\t" + machine_state_s + "\n";

  return s;
}

const char* ExecFormula::resultSrcToString(ResultSrc r)
{
  switch(r)
  {
  case ResultSrc::FROM_SOLVER:
    return "⚙️";
  case ResultSrc::FROM_CACHE:
    return "💾";
  }
}

} // namespace boa
