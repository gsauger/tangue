#ifndef BOA_DISASSEMBLING_H
#define BOA_DISASSEMBLING_H

#include <list>
#include <queue>
#include <stack>

#include "exec_formula.hpp"
#include "binary/binary_parser.hpp"
#include "boa_disassembling/machine_state.hpp"
#include "common/basic_block.hpp"
#include "common/cfg_manager.hpp"
#include "common/edge.hpp"
#include "common/instr.hpp"
#include "disassembler/disassembler.hpp"
#include "solver/solver.hpp"
#include "solver/solver_manager.hpp"
#include "symbexec/dyn_jmp_symb_exec.hpp"
#include "symbexec/jcc_symb_exec.hpp"
#include "symbexec/machine_state_symb_exec.hpp"
#include "utils/boa_types.hpp"
#include "utils/utils.hpp"
#include "x86_to_smtlib/x86_to_smtlib.hpp"

namespace boa
{

extern const char* boa_disas_logger;

struct config_boa_disassembling_t
{
  bool assume_all_rets_genuine;
  bool not_compute_dyn_jmps_targets;
  config_solver_t config_solver;
  std::string windows_dlls_json_filepath;
  std::string windows_dlls_dir;
  int max_loop_lap;
  std::unordered_map<addr_t, addr_t> dyn_jmp_hooks;
  bool use_fake_val_for_symb_values;
  std::unordered_map<Register, uint32_t> user_reg_values;
  std::unordered_map<addr_t, uint8_t> user_mem_cell_values;
  int max_waves_to_disassemble;
  bool disable_lib_fun_hooks;
  bool load_dlls;
};

struct loop_handler_t
{
  int cnt;
  std::unique_ptr<MachineState> machine_state;
};

// Key 1 : returnsite stack
// Key 2 : BB src
// Key 3 : target addr
using exec_flow_history_t =
  std::unordered_map<
    std::vector<addr_t>,
    std::unordered_map<
      addr_t,
      std::unordered_map<
        addr_t,
        loop_handler_t>
      >,
    VectorAddrHasher
  >;

struct exploration_path_t
{
  std::unique_ptr<MachineState> machine_state; // Machine state to use
  std::optional<const Instr*> last_instr;      // Last instr (to create the edge)
  EdgeFoundMethod found_method;                // found method between the last disassembled BB and the addr to disas (only used to choose the edge color in the CFG)
};

class BoaDisassemblingWave
{
public:
  // MARK:- Constructors and destructors
  BoaDisassemblingWave(Disassembler* disassembler, Binary* binary, x86ToSmtlib* x86_to_smtlib,
                       SolverManager& solver_manager, const config_boa_disassembling_t& config_boa_disassembling,
                       const config_global_t& config_global, unsigned int wave);

  BoaDisassemblingWave() = delete;

  BoaDisassemblingWave(BoaDisassemblingWave const&) = delete;
  BoaDisassemblingWave& operator=(BoaDisassemblingWave const&) = delete;

  BoaDisassemblingWave(BoaDisassemblingWave&&) = delete;
  BoaDisassemblingWave& operator=(BoaDisassemblingWave&&) = delete;

  ~BoaDisassemblingWave();

  void disassembleBinary(CFGManager& cfg_manager);

  void disassembleWave(CFG* cfg, std::list<std::unique_ptr<exploration_path_t>>& paths_to_explore,
                       std::optional<std::unique_ptr<exploration_path_t>>& next_wave_to_explore,
                       unsigned int& symb_executed_instrs_cnt_all_waves);

  // MARK:- Rets results getters
  std::map<const Instr*, std::string> getRetsGenuine() const;
  std::map<const Instr*, std::string> getRetsViolated() const;
  std::map<const Instr*, std::string> getRetsPartiallyUnknown() const;
  std::map<const Instr*, std::string> getRetsUnknown() const;
  std::string getRetsResults() const;
  std::map<const Instr*, std::string> getRetsResultsDetails() const;

  // MARK:- DynJmps results getters
  std::map<const Instr*, std::string> getDynJmpsConcrete() const;
  std::map<const Instr*, std::string> getDynJmpsPartiallyUnknown() const;
  std::map<const Instr*, std::string> getDynJmpsUnknown() const;
  std::string getDynJmpsResults() const;
  std::map<const Instr*, std::string> getDynJmpsResultsDetails() const;

  // MARK:- DynCalls results getters
  std::map<const Instr*, std::string> getDynCallsConcrete() const;
  std::map<const Instr*, std::string> getDynCallsPartiallyUnknown() const;
  std::map<const Instr*, std::string> getDynCallsUnknown() const;
  std::string getDynCallsResults() const;
  std::map<const Instr*, std::string> getDynCallsResultsDetails() const;

  // MARK:- Jccs results getters
  std::string getJccsResults() const;
  std::map<const Instr*, std::string> getJccsResultsDetails() const;
  std::set<const Instr*> getJccsOnlyTarget() const;
  std::set<const Instr*> getJccsOnlyNextMemAddr() const;
  std::set<const Instr*> getJccsBothBranches() const;

  // MARK:- Symb exec stats getters
  size_t getSymbExecutedBbsCnt() const;
  size_t getSymbExecutedInstrsCnt() const;
  long long getTotalTime() const;

private:
  // MARK:- Private member variables
  Disassembler* m_disassembler;
  Binary* m_binary;
  x86ToSmtlib* m_x86_to_smtlib;
  SolverManager& m_solver_manager;

  const config_boa_disassembling_t& m_config_boa_disassembling;
  const config_global_t& m_config_global;

  std::shared_ptr<spdlog::logger> m_log;

  unsigned int m_wave;

  size_t m_symb_executed_bbs_cnt;
  size_t m_symb_executed_instrs_cnt;
  std::unordered_set<addr_t> m_symb_executed_instrs;

  // DynSymbExec results
  std::map<const Instr*, ret_result_t> m_rets_results;
  std::map<const Instr*, dyn_jmp_result_t> m_dyn_jmps_results;
  std::map<const Instr*, dyn_call_result_t> m_dyn_calls_results;

  // Jcc results
  std::map<const Instr*, jcc_result_t> m_jccs_results;
  
  exec_flow_history_t m_exec_flow_history;

  long long m_total_time;

  // MARK:- Private methods
  int loopChecker(const std::vector<addr_t>& returnsite_stack, addr_t bb_ep, addr_t target_addr);
  std::unique_ptr<exploration_path_t> deepCopyExplorationPath(exploration_path_t* exploration_path) const;
  void getSuccsToTreat(std::vector<std::unique_ptr<exploration_path_t>>& succs_to_treat,
                       std::unique_ptr<exploration_path_t> addr_to_symb_exec, const BasicBlock* const bb,
                       ExecFormula& bb_exit_formula);
};

class BoaDisassembling
{
public:
  // MARK:- Constructors and destructors
  BoaDisassembling(Disassembler* disassembler, Binary* binary, x86ToSmtlib* x86_to_smtlib,
                   SolverManager& solver_manager, BinaryParser& bin_parser,
                   const config_boa_disassembling_t& config_boa_disassembling,
                   const config_global_t& config_global);

  BoaDisassembling() = delete;

  BoaDisassembling(BoaDisassembling const&) = delete;
  BoaDisassembling& operator=(BoaDisassembling const&) = delete;

  BoaDisassembling(BoaDisassemblingWave&&) = delete;
  BoaDisassembling& operator=(BoaDisassembling&&) = delete;

  ~BoaDisassembling() = default;

  void disassembleBinary(CFGManager& cfg_manager);

  const std::map<unsigned int, std::unique_ptr<BoaDisassemblingWave>>& getBoaDisassemblingWaves() const;

private:
  // MARK:- Private member variables
  Disassembler* m_disassembler;
  Binary* m_binary;
  x86ToSmtlib* m_x86_to_smtlib;
  SolverManager& m_solver_manager;
  BinaryParser& m_bin_parser;

  const config_boa_disassembling_t& m_config_boa_disassembling;
  const config_global_t& m_config_global;

  unsigned int m_symb_executed_instrs_cnt;

  std::shared_ptr<spdlog::logger> m_log;

  // Store the BoaDisassembling of each wave
  std::map<unsigned int, std::unique_ptr<BoaDisassemblingWave>> m_boa_disassembling_waves;
};

} // namespace boa

#endif
