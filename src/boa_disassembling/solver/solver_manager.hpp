#ifndef SOLVER_MANAGER_H
#define SOLVER_MANAGER_H

#include <future>
#include <map>
#include <queue>
#include <reproc++/drain.hpp>
#include <reproc++/reproc.hpp>
#include <stdio.h>
#include <string>
#include <unordered_set>

#include "boa_disassembling/solver/solver.hpp"
#include "common/basic_block.hpp"
#include "utils/smtlib_utils.hpp"
#include "utils/utils.hpp"

namespace boa
{

extern const char* solver_logger;

struct config_solver_t
{
  SMTSolver smt_solver;
  // unsigned int timeout;
};

class SolverManager
{
public:
  // MARK:- Constructors and destructors
  SolverManager(const config_solver_t& config_solver, bool incremental_mode = false, unsigned int solvers_to_start = 1);

  SolverManager() = delete;

  SolverManager(SolverManager const&) = delete;
  SolverManager& operator=(SolverManager const&) = delete;

  SolverManager(SolverManager&&) = delete;
  SolverManager& operator=(SolverManager&&) = delete;

  ~SolverManager();

  // MARK:- Other public functions
  void getSymbVarsSatValue(std::unordered_map<std::string, uint32_t>& symb_vars_sat_value,
                           const std::string& smtlib_formula_text);

  void getUniqueSymbVarValuation(std::pair<std::string, std::optional<uint32_t>>& symb_var_unique_sat_value,
                                 const std::string& smtlib_formula_text, unsigned int symb_var_size,
                                 unsigned int solver_id_to_use = 0);

  // MARK: - Statistics
  int getNumberGetSymVarValuationWithSolvers() const;
  int getNumberGetSymVarValuationWithSavedResults() const;
  int getNumberCheckSat() const;
  int getNumberGetValue() const;

private:
  std::shared_ptr<spdlog::logger> m_log;
  const config_solver_t& m_config_solver;

  std::vector<std::unique_ptr<Solver>> m_solvers;

  std::vector<std::thread> m_reset_solver_th;

  std::queue<Solver*> m_solvers_to_restart;
  std::mutex m_solvers_to_restart_mutex;
  std::condition_variable m_solvers_to_restart_cv;

  // Statistics
  // std::map<SMTSolver, int> m_winner_solver_cnt;
  int m_number_of_get_symb_var_valuation_with_solvers;
  int m_number_of_get_symb_var_valuation_with_saved_results;
  std::atomic_int m_check_sat_cnt;
  std::atomic_int m_get_value_cnt;
  // MARK:- Solver management private functions
  void restartSolver(Solver* solver);
};

} // namespace boa

#endif /* SOLVER_MANAGER_H */
