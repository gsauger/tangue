#ifndef SOLVER_H
#define SOLVER_H

#include <fstream>
#include <future>
#include <map>
#include <reproc++/drain.hpp>
#include <reproc++/reproc.hpp>
#include <stdio.h>
#include <string>
#include <unordered_set>

#include "utils/smtlib_utils.hpp"
#include "utils/utils.hpp"

namespace boa
{

extern const char* solver_logger;

enum class SMTSolver
{
  CVC4,
  BOOLECTOR,
  Z3,
  YICES,
  MATHSAT
};

enum class SolverSatStatus
{
  SAT,
  UNSAT
};

enum class SolverStatus
{
  SUCCESS,
  FAIL
};

class Solver
{
public:
  // MARK:- Constructors and destructors
  Solver(SMTSolver solver_smt, unsigned int solver_id, /*unsigned int timeout,*/ bool incremental_mode = false);

  Solver() = delete;

  Solver(Solver const&) = delete;
  Solver& operator=(Solver const&) = delete;

  Solver(Solver&&) = delete;
  Solver& operator=(Solver&&) = delete;

  ~Solver();

  // MARK:- Other public functions
  void startProcess();
  void stopProcess();
  bool isReady() const;
  void setReady(bool ready);
  unsigned int getId() const;
  SolverStatus sendString(std::string const& s);

  void getSymbVarsValuation(std::string smtlib_formula_text,
                            std::unordered_map<std::string, uint32_t>& symb_vars_sat_value, SolverStatus& status,
                            std::atomic_int& check_sat_cnt, std::atomic_int& get_value_cnt);

  void getUniqueSymbVarValuation(std::string smtlib_formula_text,
                                 std::pair<std::string, std::optional<uint32_t>>& symb_var_unique_sat_value,
                                 unsigned int symb_var_size, SolverStatus& status, std::atomic_int& check_sat_cnt,
                                 std::atomic_int& get_value_cnt);

  static const char* solverToString(const SMTSolver& s);

private:
  std::shared_ptr<spdlog::logger> m_log;
  reproc::process m_process;
  SMTSolver m_solver_smt;
  bool m_is_ready;
  std::vector<std::string> m_start_cmd;
  unsigned int m_id;
  // std::ofstream m_debug_file;

  SolverStatus purgeStderr(std::string& s);
  SolverStatus readString(std::string& s);
  SolverStatus checkSat(SolverSatStatus& sat_status, std::atomic_int& check_sat_cnt);
  std::string getStringValue(std::string const& symbol);
  SolverStatus getValues(std::unordered_map<std::string, uint32_t>& symb_vars_sat_value,
                         std::atomic_int& get_value_cnt);
  SolverStatus sendAssert(std::string const& s);
};

} // namespace boa

#endif /* SOLVER_H */
