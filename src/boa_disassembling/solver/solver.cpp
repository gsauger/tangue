#include "solver.hpp"

#include <assert.h>
#include <fstream>
#include <iostream>
#include <map>
#include <vector>

namespace boa
{

// MARK: - Constructor and destructor
Solver::Solver(SMTSolver solver_smt, unsigned int solver_id, /*unsigned int timeout,*/ bool incremental_mode)
    : m_log(spdlog::get(utils::solver_logger)), m_solver_smt(solver_smt), m_is_ready(false), m_id(solver_id)
{
  if(m_solver_smt == SMTSolver::BOOLECTOR)
  {
    m_start_cmd = {"boolector", "--smt2", "--dec", "--pretty-print=0",
                   "--exit-codes=0" /*, "--time=" + std::to_string(timeout)*/};
    if(incremental_mode)
    {
      m_start_cmd.push_back("--incremental");
    }
    // m_debug_file = std::ofstream("/tmp/boolector.smt2");
  }
  else if(m_solver_smt == SMTSolver::CVC4)
  {
    m_start_cmd = {"cvc4", "--lang=smt2.6",
                   "--bv-print-consts-as-indexed-symbols" /*, "--tlimit=" + std::to_string(1000 * timeout)*/};
    if(incremental_mode)
    {
      m_start_cmd.push_back("--incremental");
    }
    // m_debug_file = std::ofstream("/tmp/cvc4.smt2");
  }
  else if(m_solver_smt == SMTSolver::Z3)
  {
    m_start_cmd = {"z3", "-smt2", "-in", "pp.bv_literals=false" /*, "-T:" + std::to_string(timeout)*/};
    if(incremental_mode)
    {
      m_start_cmd.push_back("--in");
    }
    // m_debug_file = std::ofstream("/tmp/z3.smt2");
  }
  else if(m_solver_smt == SMTSolver::YICES)
  {
    m_start_cmd = {"yices-smt2",
                   /*"--interactive", */ "--bvconst-in-decimal" /*, "--timeout=" + std::to_string(timeout)*/};
    if(incremental_mode)
    {
      m_start_cmd.push_back("--incremental");
    }
    // m_debug_file = std::ofstream("/tmp/yices.smt2");
  }
  else if(m_solver_smt == SMTSolver::MATHSAT)
  {
    m_start_cmd = {"mathsat"};
    if(incremental_mode)
    {
      // m_start_cmd.push_back("--incremental");
    }
    // m_debug_file = std::ofstream("/tmp/mathsat.smt2");
  }
}

Solver::~Solver()
{
}

// MARK: - Interaction and communication
void Solver::startProcess()
{
  m_process = reproc::process();
  std::error_code ec = m_process.start(m_start_cmd);
  if(ec == std::errc::no_such_file_or_directory)
  {
    throw std::runtime_error("Solver " + std::string(Solver::solverToString(m_solver_smt)) +
                             " not found on your system, make sure it is available from the PATH or remove it from "
                             "'--solvers' arguments (see --help)");
  }

  if(ec)
  {
    throw std::runtime_error("Unable to start process (" + ec.message() + ")");
  }
}

void Solver::stopProcess()
{
  reproc::stop_actions process_stop_actions = {
      {reproc::stop::wait, reproc::milliseconds(10)},
      {reproc::stop::terminate, reproc::milliseconds(10)},
      {reproc::stop::kill, reproc::milliseconds(10)},
  };
  m_process.stop(process_stop_actions);
}

bool Solver::isReady() const
{
  return m_is_ready;
}

void Solver::setReady(bool ready)
{
  m_is_ready = ready;
}

unsigned int Solver::getId() const
{
  return m_id;
}

SolverStatus Solver::sendString(std::string const& s)
{
  // m_debug_file << s;
  std::pair<size_t, std::error_code> bytes_written_and_error =
      m_process.write(reinterpret_cast<const uint8_t*>(s.data()), static_cast<unsigned int>(s.length()));
  if(bytes_written_and_error.second)
  {
    // Write to process can fail if solver closed itself due to timeout
    SPDLOG_LOGGER_TRACE(m_log, "Write to solver {} n°{} failed (error: {}), text not written: {}...",
                        solverToString(m_solver_smt), m_id, bytes_written_and_error.second.message(), s.substr(0, 10));
    return SolverStatus::FAIL;
  }
  SPDLOG_LOGGER_TRACE(m_log, "Text written to solver {} n°{} (written size: {}):\n{}", solverToString(m_solver_smt),
                      m_id, bytes_written_and_error.first, s);

  return SolverStatus::SUCCESS;
}

SolverStatus Solver::readString(std::string& s)
{
  uint8_t buf[1];
  SolverStatus final_status = SolverStatus::SUCCESS;
  unsigned int parenthesis_cnt = 0;
  bool add_char;

  while(true)
  {

    std::pair<size_t, std::error_code> bytes_read_and_error = m_process.read(reproc::stream::out, buf, 1);

    // Read from process can failed if the process is killed while we are reading on it
    // or if solver closed itself due to timeout
    if(bytes_read_and_error.second)
    {
      SPDLOG_LOGGER_TRACE(m_log, "Read on solver {} n°{} failed (error: {})", solverToString(m_solver_smt), m_id,
                          bytes_read_and_error.second.message());
      final_status = SolverStatus::FAIL;
      break;
    }

    if(bytes_read_and_error.first == 1)
    {
      // std::cout << "TEST" << buf[0] << "TEST (" << int(buf[0]) << ")" << std::endl;
      add_char = true;
      if(buf[0] == '\n')
      {
        add_char = false;
        if(parenthesis_cnt == 0)
        {
          break;
        }
      }
      else if(buf[0] == '(')
      {
        parenthesis_cnt++;
      }
      else if(buf[0] == ')')
      {
        parenthesis_cnt--;
      }
      if(add_char)
      {
        s.push_back((char)buf[0]);
      }
    }
    else
    {
      break;
    }
  }

  if(s.find("[btor>main] CAUGHT SIGNAL") != std::string::npos)
  {
    final_status = SolverStatus::FAIL;
  }
  /*
  else if (s == "unknown" || s == "timeout" || s.find("[btor>main] ALARM TRIGGERED: time limit") != std::string::npos)
  {
    final_status = SolverStatus::TIMEOUT;
  }
   */

  if(s.find("error") != std::string::npos)
  {
    final_status = SolverStatus::FAIL;
  }

  SPDLOG_LOGGER_TRACE(m_log, "Read from solver {}: '{}' (length: {})", solverToString(m_solver_smt), s, s.length());

  return final_status;
}

SolverStatus Solver::checkSat(SolverSatStatus& sat_status, std::atomic_int& check_sat_cnt)
{
  auto send_string_result = sendString(smtlib_utils::smt_check_sat());
  if(send_string_result != SolverStatus::SUCCESS)
  {
    return send_string_result;
  }
  std::string r;
  auto read_string_status = readString(r);
  if(read_string_status != SolverStatus::SUCCESS)
  {
    return read_string_status;
  }
  check_sat_cnt++;

  if(r == "sat")
  {
    sat_status = SolverSatStatus::SAT;
  }
  else if(r == "unsat")
  {
    sat_status = SolverSatStatus::UNSAT;
  }
  else
  {
    throw std::runtime_error("Check sat on solver " + std::string(solverToString(m_solver_smt)) +
                             " failed with answer: " + r);
  }
  return SolverStatus::SUCCESS;
}

SolverStatus Solver::getValues(std::unordered_map<std::string, uint32_t>& symb_vars_sat_value,
                               std::atomic_int& get_value_cnt)
{

  // Send get-value command to solver
  auto send_string_result = sendString(smtlib_utils::smt_get_value(symb_vars_sat_value));
  if(send_string_result != SolverStatus::SUCCESS)
  {
    return send_string_result;
  }

  // Read solver output
  std::string raw_answer;
  auto read_string_status = readString(raw_answer);
  if(read_string_status != SolverStatus::SUCCESS)
  {
    return read_string_status;
  }

  get_value_cnt++;

  // Attention lors du parsage, il peut y avoir des doubles (et peut etre meme des triples) espaces ... !

  // answer example: "((PF0 (_ bv0 1)) (eax1 (_ bv1 32)) (ebx1 (_ bv2 32)) (ecx1 (_ bv3 32)) ((bvadd eax1 ebx1) (_ bv3
  // 32)) ((bvadd (bvadd eax1 ebx1) ebx1) (_ bv5 32)))"

  // m_log->warn("Raw string: '{}'", raw_answer);

  std::string buffer;
  std::string buffer2;
  int parenthesis_cnt = 0;
  int parenthesis_cnt2 = 0;
  unsigned int current_pos = 0;
  bool first_parenthesis_seen = false;
  unsigned int i;
  for(const char& c : raw_answer)
  {

    // m_log->warn("Current char: '{}'", c);
    // On ne prend pas en compte les \r, \n et \t
    if(c == '\r' || c == '\n' || c == '\t')
    {
      continue;
    }

    if(c == ' ')
    {
      // On ne veut pas commencer par un espace
      if(buffer.empty())
      {
        continue;
      }
      // On ne veut pas ajouter de doubles esapces
      if(buffer.back() == ' ')
      {
        continue;
      }
      // On ne veut pas ajouter d'espace après un '('
      if(buffer.back() == '(')
      {
        continue;
      }
    }

    // On ne veut pas ajouter d'espace avant ')'
    if(c == ')' && !buffer.empty() && buffer.back() == ' ')
    {
      buffer.pop_back();
    }

    // On veut ignore la première parenthèse
    if(!first_parenthesis_seen && c == '(')
    {
      first_parenthesis_seen = true;
      continue;
    }

    // m_log->warn("Je l'ajoute");
    // On ajoute le char dans buffer
    buffer.push_back(c);

    if(c == '(')
    {
      parenthesis_cnt++;
    }
    else if(c == ')')
    {
      parenthesis_cnt--;
    }

    if(parenthesis_cnt == 0)
    {
      if(buffer == " ")
      {
        // We are in the middle of two symb_var_sat_values
        buffer = "";
      }
      else
      {
        // We have a complete var valuation (symb var name + sat value)
        // e.g. ((bvadd eax1 ebx1) (_ bv3 32))
        if(buffer.find("(_ bv") == std::string::npos)
        {
          // throw std::runtime_error("Unexpected buffer (" + buffer + ") with this raw_answer :'" +
          // raw_answer + "' from " + solverToString(m_solver_smt) + " solver");
          return SolverStatus::FAIL;
        }

        // std::cout << "BUFFER " << buffer << std::endl;
        // Remove first '(' and last ')'
        buffer.erase(0, 1);
        buffer.pop_back();

        buffer2 = "";

        uint32_t current_symb_var_sat_value = 0;

        parenthesis_cnt2 = 0;
        current_pos = 0;

        for(i = buffer.length() - 1; i >= 0; i--)
        {
          const char& c2 = buffer.at(i);
          current_pos++;
          buffer2 = c2 + buffer2;

          if(c2 == '(')
          {
            parenthesis_cnt2++;
          }
          else if(c2 == ')')
          {
            parenthesis_cnt2--;
          }

          if(parenthesis_cnt2 == 0 && !buffer2.empty())
          {
            // std::cout << "BUFFER2 " << buffer2 << std::endl;
            // Remove '(_ bv' and ')' from '(_ bv3 32)'
            buffer2.erase(0, 5);
            buffer2.pop_back();

            // "3 32"

            std::vector<std::string> splitted_buffer2 = utils::string_splitter(buffer2, " ");
            if(splitted_buffer2.size() != 2)
            {
              // throw std::runtime_error("Unexpected buffer2_delim size with this raw_answer :'" +
              // raw_answer + "' from " + solverToString(m_solver_smt) + " solver (buffer2: '" + buffer2 +
              // "')");
              return SolverStatus::FAIL;
            }
            current_symb_var_sat_value = strtoul(splitted_buffer2.at(0).c_str(), NULL, 10);
            break;
          }
        }
        std::string current_symb_var = buffer.substr(0, buffer.length() - current_pos - 1);
        SPDLOG_LOGGER_TRACE(m_log, "Sat value of {}: {:#x}", current_symb_var, current_symb_var_sat_value);
        if(symb_vars_sat_value.find(current_symb_var) == symb_vars_sat_value.end())
        {
          // throw std::runtime_error("Unknwon symb var name: '" + current_symb_var + "' with raw_answer :'" +
          // raw_answer + "' from " + solverToString(m_solver_smt) + " solver");
          return SolverStatus::FAIL;
        }
        symb_vars_sat_value[current_symb_var] = current_symb_var_sat_value;
        buffer = "";
      }
    }
  }

  return SolverStatus::SUCCESS;
}

SolverStatus Solver::sendAssert(std::string const& s)
{
  return sendString(smtlib_utils::smt_assert(s));
}

// MARK: - Symbolic variable valuation engine (do not check value unicity)
void Solver::getSymbVarsValuation(std::string smtlib_formula_text,
                                  std::unordered_map<std::string, uint32_t>& symb_vars_sat_value, SolverStatus& status,
                                  std::atomic_int& check_sat_cnt, std::atomic_int& get_value_cnt)
{

  // Send formula
  status = sendString(smtlib_formula_text);
  if(status == SolverStatus::SUCCESS)
  {
    // Init formula should be sat
    SolverSatStatus sat_result;
    status = checkSat(sat_result, check_sat_cnt);
    if(status == SolverStatus::SUCCESS)
    {
      if(sat_result == SolverSatStatus::UNSAT)
      {
        throw std::runtime_error("Init formula should be SAT on solver " + std::string(solverToString(m_solver_smt)) +
                                 ", formula:\n" + smtlib_formula_text);
      }

      // On demande au solver les sat values
      // En même temps on met à jour status pour vérifier les erreurs
      status = getValues(symb_vars_sat_value, get_value_cnt);
    }
  }

  return;
}

// MARK: - Symbolic variable valuation engine (check value unicity)
void Solver::getUniqueSymbVarValuation(std::string smtlib_formula_text,
                                       std::pair<std::string, std::optional<uint32_t>>& symb_var_unique_sat_value,
                                       unsigned int symb_var_size, SolverStatus& status, std::atomic_int& check_sat_cnt,
                                       std::atomic_int& get_value_cnt)
{

  std::unordered_map<std::string, uint32_t> symb_vars_sat_value{{symb_var_unique_sat_value.first, 0x0}};

  // Send formula
  status = sendString(smtlib_formula_text);
  if(status == SolverStatus::FAIL)
  {
    return;
  }

  // Init formula should be sat
  SolverSatStatus sat_result;
  status = checkSat(sat_result, check_sat_cnt);
  if(status == SolverStatus::FAIL)
  {
    return;
  }

  if(sat_result == SolverSatStatus::UNSAT)
  {
    throw std::runtime_error("Init formula should be SAT on solver " + std::string(solverToString(m_solver_smt)) +
                             ", formula:\n" + smtlib_formula_text);
  }

  // Get first sat value
  status = getValues(symb_vars_sat_value, get_value_cnt);
  if(status != SolverStatus::FAIL)
  {
    // Assert false this sat value
    const uint32_t& sat_value = symb_vars_sat_value.begin()->second;
    std::string to_assert = smtlib_utils::smt_not(
        smtlib_utils::smt_equal(symb_var_unique_sat_value.first, smtlib_utils::smt_bv_val(sat_value, symb_var_size)));

    // Send assert
    status = sendAssert(to_assert);
    if(status == SolverStatus::FAIL)
    {
      return;
    }

    // Check sat
    SolverSatStatus sat_result_2;
    status = checkSat(sat_result_2, check_sat_cnt);
    if(status == SolverStatus::FAIL)
    {
      return;
    }

    // Si on est SAT c'est qu'il existe au moins deux valeurs possible, donc non unique
    if(sat_result_2 == SolverSatStatus::SAT)
    {
      symb_var_unique_sat_value.second = std::nullopt;
    }
    else
    {
      symb_var_unique_sat_value.second = sat_value;
    }
  }
  return;
}

// MARK: - Utils
const char* Solver::solverToString(const SMTSolver& s)
{
  switch(s)
  {
  case SMTSolver::CVC4:
    return "CVC4";
  case SMTSolver::BOOLECTOR:
    return "Boolector";
  case SMTSolver::Z3:
    return "Z3";
  case SMTSolver::YICES:
    return "Yices";
  case SMTSolver::MATHSAT:
    return "MathSAT";
  }
}

} // namespace boa
