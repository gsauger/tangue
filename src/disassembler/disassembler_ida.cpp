#include "disassembler_ida.hpp"
#include <nlohmann/json.hpp>
#include <fstream>
using json = nlohmann::json;

namespace boa{
	DisassemblerIda::DisassemblerIda(std::shared_ptr<Binary> binary, Config* config){
  	m_log = spdlog::get(utils::disassembler_logger);
  	SPDLOG_LOGGER_TRACE(m_log, "DisassemblerIDA::DisassemblerIDA()\n");

  	m_binary = binary;
  	m_ida_script_folder = config->getConfigGlobal().path_to_ida_script;

  	// capstone stuff to disas the instructions
  	if (cs_open(CS_ARCH_X86, CS_MODE_64, &m_cs_handle) != CS_ERR_OK) {
    	m_log->info("Failed to open Capstone");
  	}
  	cs_option(m_cs_handle, CS_OPT_DETAIL, CS_OPT_ON);
  	m_cs_insn = cs_malloc(m_cs_handle);
  }

	DisassemblerIda::~DisassemblerIda(){
  	SPDLOG_LOGGER_TRACE(m_log, "DisassemblerIDA::~DisassemblerIDA()\n");
  	cs_free(m_cs_insn, 1);
  	cs_close(&m_cs_handle);
  }

bool DisassemblerIda::checkForBB2Merge(std::shared_ptr<AnalysisEngine> analysis_engine){
	for(auto const& fun : analysis_engine->getFunctions()){
		for(auto const& [bb_ep,bb] : fun->getBasicBlocks()){
			if(!bb->getLastInstr()->is_cflow_ins()){
				sptrBasicBlock next_bb = fun->getBasicBlock(bb->getLastInstr()->getNextAddr());
				if(next_bb == NULL){continue;}
				// we merge the next bb in this one
				for(auto const& inst : next_bb->getInstrs()){
					bb->pushBackInstr(inst);
				}
				for(auto const& out_inst : next_bb->getOutInstrs()){
					bb->addOutgoingInstruction(out_inst);
				}
				fun->removeBasicBlock(next_bb->getEntryPointAddr(), m_log);
				return true;
			}
		}
	}
	return false;
}
	/**
	 * Uses IDAPRO to disassemble the binary.
	 * IMPORTANT: we fill the m_cfg member of the analysis_engine. this will be used
	 * for printing output.
	 * This is because IDAPRO directly gives us functions to work with.
	 * For now, the only time the m_cfg member is used is when we want to print
	 * the FULL CFG of the binary.
	 * I believe this can be achieved by binding together all the functions
	 * in the analysis_engine. To this end I added to each function ptr in the
	 * analysis_engine the list of the outgoing edges of this function
	 * NOTE: or we build the cfg object and fill it along the way, and assign it to m_cfg
	 **/
	int DisassemblerIda::recursive_disasm(std::shared_ptr<CFG> cfg, std::shared_ptr<AnalysisEngine> analysis_engine){
		// launch the ida script on the binary
		std::string command = getIdaScriptFolder() + "/runScriptCommand.sh" + " " + m_binary->getFilepath() + " " + getIdaScriptFolder();
		m_log->debug("Running with std::system : \n", command);
		m_log->debug("\t{}\n", command);
		std::system(command.c_str()); // to be grabbed in the config pbly
		//m_log->debug("\techo $? : {} \n", std::system("echo $?"));
		std::string output_json_path = getIdaScriptFolder();
		output_json_path = output_json_path.substr(0, output_json_path.find_last_of("/"));
		output_json_path += "/output/" + utils::base_name(m_binary->getFilepath()) + ".json";
		SPDLOG_LOGGER_DEBUG(m_log,"Loading .json for bin {} from path {}\n",m_binary->getFilepath(), output_json_path);
		// recover the json
		std::ifstream f(output_json_path);
		auto ida_json = json::parse(f, nullptr, false);
		if(ida_json.is_discarded()){
			// check if parsing failed or not
			return EXIT_FAILURE;
		}

		// recover functions from ida_json
		sptrFunc new_function;
		uint color=0;
		m_log->debug("There are {} func\n", ida_json.at("functions").size());

		for(const auto& function : ida_json.at("functions").items()){

			new_function.reset();
			auto f_val = function.value();

			// Gather a basic data on the function
			addr_t f_ep = f_val.at("sea");
			std::string f_name = f_val.at("name");
			m_log->debug("\t {}\n", f_name);
			m_log->trace("\nf_val : {}\n", f_val.dump());

			// check when asking for specific functions pairs
  	if(!analysis_engine->getConfig()->getConfigGlobal().pairs_test_file.empty()){
  		bool is_in_pair = false;
  		for(auto const& pair_data : analysis_engine->getConfig()->getConfigGlobal().test_funcs_pairs){
  			if(analysis_engine->getBinName() == pair_data.bin1){
  				if(f_name == pair_data.fname1 || f_ep == pair_data.fva1){ is_in_pair = true; break;}
  			}
  			else if(analysis_engine->getBinName() == pair_data.bin2){
  				if(f_name == pair_data.fname2 || f_ep == pair_data.fva2) {is_in_pair = true; break;}
  			}
  		}
  		if(!is_in_pair){continue;}
  	}

			// Get the indice of the basic blocks of the fun
			std::vector<addr_t> fun_bbs_addr;
			for(auto const& bb_item : f_val.at("blocks").items()){
				addr_t bb_ep= bb_item.value().at("sea");
				fun_bbs_addr.push_back(bb_ep);
			}

			// insert the new function ptr in analysis engine
			new_function = analysis_engine->insertFunction(f_ep, color, f_name, analysis_engine->getBinName());

			// fill the function ptr
			// - Called functions

			for(const auto& call_addr: f_val.at("call").items()){
				addr_t addr = call_addr.value();
				new_function->addCalledAddr(addr);
			}

			// We need to add the basic Blocks fo the current function
			sptrBasicBlock new_bb;

			for(const auto& bb_item : f_val.at("blocks").items()){

				new_bb.reset();
				auto bb_val = bb_item.value();
				if(bb_val.at("src").size()==0){continue;}

				m_log->trace("\t\t Basic block val: {}\n", bb_val.dump());

				// basic members of the new_bb
				addr_t bb_ep = bb_val.at("sea");
				new_bb = std::make_shared<BasicBlock>(bb_ep);
				new_bb->setColor(color);
				new_bb->setFunName(f_name);
				new_bb->setEntryBlockBool( ( f_ep == bb_ep ) );

				// get basic block children addr list
				std::unordered_set<addr_t> bb_call_set;
				for(auto const& bb_call_item : bb_val.at("call").items()){
					bb_call_set.insert(fun_bbs_addr.at( (uint)bb_call_item.value() ));
					m_log->trace("\t\t\t-> added child {:x}\n", fun_bbs_addr.at( (uint)bb_call_item.value()));
				}

				// We're going to use cs_disasm to disas the current instruction and get all the relevant data

				const std::string raw_bytes_str = bb_val.at("bytes"); // raw bytes ?
  			byte_t * ptr_code = new byte_t[raw_bytes_str.size() + 1];
  			copystring2byte(ptr_code, raw_bytes_str);

				addr_t sa, ea;
				sa = bb_val.at("sea");
				ea = bb_val.at("eea");
  			size_t code_size = ea - sa;
  			addr_t addr = sa;
  			//size_t nb_inst = bb_val.at("src").size();
/*
				m_log->trace("Various arguments:\n");
				m_log->trace("Start addr / end addr : {:x} / {:x}:\n", sa, ea);
				m_log->trace("ptr_const_code[0-5]: {:x},{:x},{:x},{:x},{:x}\n", ptr_code_const[0], ptr_code_const[1], ptr_code_const[2], ptr_code_const[3], ptr_code_const[4]);
				m_log->trace("raw_bytes_str: {}\n", raw_bytes_str);
				m_log->trace("code_size: {}\n", code_size);
				m_log->trace("addr: {:x}\n", addr);
				m_log->trace("nb_inst: {}\n", nb_inst);
*/
				//uint inst_count=0;
				sptrInstr new_inst;
				// NOTE: when going throught the instructions, everytime there's a target jump
				// we remove the target addr from the "bb_call_set";
				// in the end the remaining calls will be allocated to the last instr
				// (case example: switch)
  			while (cs_disasm_iter(m_cs_handle, (const byte_t**)&ptr_code, &code_size, &addr, m_cs_insn)) {
					m_log->trace("\t\tm_cs_insn {:x} : {} {} \n",m_cs_insn->address, m_cs_insn->mnemonic, m_cs_insn->op_str);
					//inst_count++;

					// break cases
					//if(inst_count > nb_inst){break;}
        	if (m_cs_insn->id == X86_INS_INVALID || m_cs_insn->size == 0){break;}

					new_inst.reset();

					new_inst = std::make_shared<Instr>(getCapstoneInstStr(m_cs_insn), utils::bytes_to_vect(m_cs_insn->bytes, m_cs_insn->size), csToInstrType(m_cs_insn), InstrSubType::UNKNOWN, m_cs_insn->address, (int)0, m_cs_insn->size, m_cs_insn->address + m_cs_insn->size, std::nullopt, std::nullopt, ".text?");
					new_inst->setOperandCount(m_cs_insn->detail->x86.op_count);

        	for(int j=0; j<m_cs_insn->detail->x86.op_count; j++){
          	cs_x86_op op = m_cs_insn->detail->x86.operands[j];
          	if(op.type == X86_OP_REG){
            	new_inst->addOperandRegAccess((x86_reg)op.reg, opAccess2str(op.access));
          	}
        	}

					if(is_cs_cflow_ins(m_cs_insn)){
						addr_t target = get_cs_ins_immediate_target(m_cs_insn);
						if(bb_call_set.count(target)){
							bb_call_set.erase(target);
						}
						if(target){
							new_inst->addEdge({0, target}, m_log);
							new_inst->SetJmpAddr(target);
						}
						if(!is_cs_unconditional_cflow_ins(m_cs_insn) && !is_cs_call_ins(m_cs_insn)){
							// in case of a "jmp" we don't add the next addr instruction, only the target
							// if able (see above)
							new_inst->addEdge({0,new_inst->getNextAddr()} , m_log);
						}
						new_bb->addOutgoingInstruction(new_inst);
					}
					new_bb->pushBackInstr(new_inst);
				}
				// all the remaining bb_call_set targets are added to the last instruction (which should be a jmp instr)
				for(auto const& jmp_addr : bb_call_set){
					new_bb->getLastInstr()->addEdge({0,jmp_addr}, m_log);
				}
				new_bb->addOutgoingInstruction(new_bb->getLastInstr());
				// we insert the new_bb in the new_fun
				new_function->insertBasicBlock(new_bb);
  		}
				//delete[] ptr_code; // segfault if uncommented. Maybe because m_cs_handle still uses it ?
			color++;
		}

		// When a bb ends in a sequential instruction, we need
		// to stitch it to the next bb
		// Fix point algorithm
		bool deleted_a_bb = true;
		while(deleted_a_bb){
			deleted_a_bb = checkForBB2Merge(analysis_engine);
		}

		// remove all functions of less than n basic blocks

		std::unordered_set<sptrFunc> new_functions_set;
		for(auto fun : analysis_engine->getFunctions()){
			if((int)fun->getBasicBlocks().size() >= analysis_engine->getConfig()->getConfigGlobal().min_nb_bb){
				new_functions_set.insert(fun);
			}
		}
		analysis_engine->setFunctions(new_functions_set);//Dirty but don't have time to understand why .erase doesn't work

		// parse it and fill the CFG
		for( auto const& fun : analysis_engine->getFunctions()){
			cfg->addFunSymbol(fun->getEntryPointAddr(), fun->getName());
			for(auto const& [ep,fun_bb] : fun->getSortedBasicBlocks()){
				cfg->addBasicBlock(fun_bb);
			}
		}

		return EXIT_SUCCESS;
	}
}
