#ifndef DISASSEMBLER_CAPSTONE_H
#define DISASSEMBLER_CAPSTONE_H

#include <string>
#include <unordered_map>
#include <vector>


//#include <capstone/capstone.h>

//#include "disassembler.hpp"
#include "config.hpp"
#include "binary/binary.hpp"
#include "common/cfg.hpp"
#include "common/basic_block.hpp"
#include "common/instr.hpp"
#include "utils/utils.hpp"

namespace boa
{

class DisassemblerCapstone 
{

public:
  // Constructor and destructor
  DisassemblerCapstone();

  ~DisassemblerCapstone();
  
  // Disas methods

  int recursive_disasm(std::shared_ptr<Binary> binary, Config* config, std::shared_ptr<CFG> cfg);
  void print_ins(cs_insn* ins);


private:

  std::shared_ptr<spdlog::logger> m_log;
  csh m_cs_handle;
  cs_insn* m_cs_insn;

};

} // namespace boa

#endif
