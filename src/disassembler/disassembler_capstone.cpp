#include "disassembler_capstone.hpp"
#include "spdlog/fmt/bin_to_hex.h"
#include <iomanip>
#include <iostream>
#include <queue>
#include <sstream>
#include <string>
#include <vector>

namespace boa {
DisassemblerCapstone::DisassemblerCapstone() {

  m_log = spdlog::get(utils::disassembler_logger);
  SPDLOG_LOGGER_TRACE(m_log, "DisassemblerCapstone::DisassemblerCapstone()\n");

  if (cs_open(CS_ARCH_X86, CS_MODE_64, &m_cs_handle) != CS_ERR_OK) {
    m_log->info("Failed to open Capstone");
  }
  cs_option(m_cs_handle, CS_OPT_DETAIL, CS_OPT_ON);
  m_cs_insn = cs_malloc(m_cs_handle);
}

DisassemblerCapstone::~DisassemblerCapstone() {

  SPDLOG_LOGGER_TRACE(m_log, "DisassemblerCapstone::~DisassemblerCapstone()\n");
  cs_free(m_cs_insn, 1);
  cs_close(&m_cs_handle);
}

void DisassemblerCapstone::print_ins(cs_insn *ins) {
  /* pretty prints a Capstone instruction */
  std::vector<byte_t> byte_vect;
  for (size_t i = 0; i < ins->size; i++) {
    byte_vect.push_back(ins->bytes[i]);
  }

  std::string byte_space;
  for (int i = 0; i < (8 - ins->size); i++) {
    byte_space += "   ";
  }

  m_log->info("{0}: {1:Xn}{2} {3} {4}\n", (void *)ins->address,
              spdlog::to_hex(byte_vect), byte_space, ins->mnemonic,
              ins->op_str);
}

  /**
    Cleans up a CFG after disassembling a binary.
    Removes edges to void BB, redirects them if the target address is in a visited BB.
    @param cfg CFG object to clean.
  */
void cleanUp(std::shared_ptr<CFG> cfg, std::shared_ptr<spdlog::logger> log) {
  SPDLOG_LOGGER_DEBUG(log, "Entering cleanup\n");

  for (auto const& [bb_addr, bb] : cfg->getBasicBlocks()){
    if(bb->getSize()==0){
      continue;
    }

    std::vector<waddr_t> delete_list;
    SPDLOG_LOGGER_TRACE(log, "Looking at bb {:x}, size {}, {}\n", bb_addr, bb->getSize(), bb->printFullBB());
    for (auto const &[waddr, edge] :  bb->getEdges()) {
      if(edge == NULL){
        continue;
      }
      if (!cfg->isBasicBlockExists(edge.get()->getDst().second)){
        delete_list.push_back(edge.get()->getDst());
      }
    }
    //SPDLOG_LOGGER_DEBUG(log, "Delete list size : {}\n", delete_list.size());
    for (waddr_t addr_to_delete : delete_list) {

      SPDLOG_LOGGER_DEBUG(log, "Deleting {:x}\n", addr_to_delete.second);
      // remove edge
      bb->getLastInstr()->removeEdge(addr_to_delete, log);
      // add edge back to the correct BB

      if(cfg->isInstrExists(addr_to_delete.second)){ // TODO: check if this is correct

        SPDLOG_LOGGER_DEBUG(log, "Looking for BB that contains {:x}, found this BB (with getBasicBlock):\n{}\n",addr_to_delete.second, cfg->getBasicBlock(addr_to_delete.second)->printFullBB());

        //SPDLOG_LOGGER_DEBUG(log, "And adding back edge to the BB that corresponds to {:x} (ep : {:x} ): \n{}\n",addr_to_delete.second,cfg->getBasicBlock(addr_to_delete.second)->getEntryPointAddr() , cfg->getBasicBlock(addr_to_delete.second)->printFullBB());
        bb->getLastInstr()->addEdge(cfg->getBasicBlock(addr_to_delete.second)->getEntryPoint(), log);
      }
    }
  }
}

bool is_cs_cflow_ins(uptrConstInstr inst_ptr){
  if(inst_ptr->getType() == InstrType::CALL ||
  inst_ptr->getType() == InstrType::JCC ||
  inst_ptr->getType() == InstrType::JMP ||
  inst_ptr->getType() == InstrType::RET){
    return true;
  }
  return false;
}


std::string getSymbolName(addr_t addr, std::shared_ptr<Binary> binary) {
  for (auto &sym : binary->getSymbols()) {
    if (sym->m_vaddr == addr) {
      return sym->m_name;
    }
  }
  return std::string();
}

std::string getImplicitRegReadStr(cs_insn *cs_insn){
  std::string output_str("Register read: ");
  uint16_t reg;
  for(int i =0; i < cs_insn->detail->regs_read_count; i++){
    reg = cs_insn->detail->regs_read[i];
    output_str += reg2str(reg);
    output_str += " ";
  }
  return output_str;
}

std::string getImplicitRegWriteStr(cs_insn *cs_insn){
  std::string output_str("Register written: ");
  uint16_t reg;
  for(int i =0; i < cs_insn->detail->regs_write_count; i++){
    reg = cs_insn->detail->regs_write[i];
    std::cout << "REG NAME: " << reg << "\n";
    output_str += reg2str(reg);
    output_str += " ";
  }
  return output_str;
}

std::string getMnemonic(cs_insn *cs_insn){
  //cs_insn->id
  return "INVALID";
}


std::string printOperandsInfoX86(cs_insn *cs_insn){
  std::string operand_str = " / op info: ";
  /*
 operand_str += std::to_string(cs_insn->detail->x86.op_count);
 operand_str += "; ";*/
 for (int i = 0; i < cs_insn->detail->x86.op_count; i++){
   cs_x86_op op = cs_insn->detail->x86.operands[i];
   if( op.type == X86_OP_REG){
    operand_str += reg2str(op.reg);
    operand_str += ": ";
   }
   operand_str += opAccess2str(op.access);
   operand_str += "; ";
 }
 operand_str += '\n';
 return operand_str;
}



/**
 * Try to obtain main symbol by disassembling from the entry point
 * The idea is to find the first argument of libc_start_main
 * WEAK TO OBFUSCATIONS
 @return address of the main function, 0 if failed
*/
addr_t getSupposedMainSymbol(std::shared_ptr<Binary> binary, std::shared_ptr<spdlog::logger> log){

  addr_t previous_addr = 0; // rename this preivous operand addr
  bool was_previous_addr_one_imm = false;
  bool was_another_operand_imm_already = false;
  std::size_t n;
  addr_t addr, offset;
  cs_insn* cs_insn_ptr;
  csh cs_handle;
  cs_x86_op *cs_op;

  if (cs_open(CS_ARCH_X86, CS_MODE_64, &cs_handle) != CS_ERR_OK) {
    log->info("Failed to open Capstone for main EP\n");
  }

  cs_option(cs_handle, CS_OPT_DETAIL, CS_OPT_ON);
  cs_insn_ptr = cs_malloc(cs_handle);


  addr = binary->getEntryPoint(); // start at binary ep
  offset = addr - binary->getSection(addr)->getEntryPoint();
  const byte_t *ptr_code = binary->getSection(addr)->getBytesRaw() + offset;
  n = binary->getSection(addr)->getSize() - offset;

  while (cs_disasm_iter(cs_handle, &ptr_code, &n, &addr, cs_insn_ptr)) {

    was_another_operand_imm_already = false;

    if (cs_insn_ptr->id == X86_INS_INVALID || cs_insn_ptr->size == 0){
      break;
    }

    for (std::size_t i = 0; i < cs_insn_ptr->detail->groups_count; i++) {
      if(is_cs_cflow_group(cs_insn_ptr->detail->groups[i])){ // we have a jump
        if (cs_insn_ptr->detail->groups[i] == CS_GRP_CALL) { // it's a call !
          if(was_previous_addr_one_imm){ return previous_addr;}
        }
        return EXIT_FAILURE; // we had a jump but failed to recover the main_ep addr. We abort
      }
    }

    // if we reach here in the loop, we are hitting a regular instruction.
    // We have to check if it is an instr with only ONE imm operand
    // if that's the case we might be looking at main's ep

    // search the immediates operands
    for (uint j = 0; j < cs_insn_ptr->detail->x86.op_count; j++) {
      cs_op = &cs_insn_ptr->detail->x86.operands[j];
      if (cs_op->type == X86_OP_IMM && was_another_operand_imm_already) {
        previous_addr = 0;
        was_previous_addr_one_imm = false;
        break;
      }
      else if(cs_op->type == X86_OP_IMM){
        was_another_operand_imm_already = true;
        previous_addr = (addr_t)cs_op->imm;
        was_previous_addr_one_imm = true;
      }
    }

  }

  return EXIT_FAILURE; // search failed
}


  /**
    Performs recursive disassembly on a binary executable sections using CAPSTONE.
    Fills a CFG object along the way.
    Directly reduce the CFG by creating Basic Blocks and their edges.
    If no .text section is found, dumps the whole binary as the .text section.
    @param binary Binary object to be disassembled.
    @param config Config file.
    @param cfg CFG object to be filled along the way.
    @return 0 if successful.
  */
int DisassemblerCapstone::recursive_disasm(std::shared_ptr<Binary> binary,
                                           Config *config,
                                           std::shared_ptr<CFG> cfg) {

  std::size_t n;
  addr_t addr, offset, target;
  std::queue<addr_t> queue;
  std::set<addr_t> seen;
  // mimick the stack for return addresses
  std::queue<addr_t> call_queue;
  uptrConstInstr inst_ptr = NULL;
  addr_t addr_to_check1 = 0x0;
	float exec_section_inst_size=0;
	uint ntot_count=0;

	//compute exec section inst size
	for(auto const& section : binary->getExecutableSections()){
		exec_section_inst_size += (float(section->getSize())/3);
	}

  if (config->getConfigGlobal().start_addrs.empty()) {
    queue.push(binary->getEntryPoint());
  }
  else{
  		for(auto const& add : config->getConfigGlobal().start_addrs){
  			queue.push(add);}
  }
  if(config->getConfigGlobal().multithreading && binary->getExecutableSections().empty()){
    m_log->info("\tExecutable section is empty. Aborting disassembly\n");
    return EXIT_FAILURE;
  }

  // get the entry point of executable functions

  SPDLOG_LOGGER_DEBUG(m_log, "Pushing into queue:\n");
  for(Section* current_executable_section : binary->getExecutableSections()){

    addr_t ep = current_executable_section->getEntryPoint();

    queue.push(ep);
    SPDLOG_LOGGER_DEBUG(m_log, "\tEntry point: {:x} of executable section {}\n", ep, current_executable_section->getName());
  }

    // get the functions symbols entry point and push them
    // also add them to the cfg members data
    // NOTE: for sections not containing the ep, symbols are the only way to disas code from them

    for (auto &sym : binary->getSymbols()) {
      if (sym->m_type == SymbolType::FUNC && binary->getSection(sym->m_vaddr)->isSectionExecutable()){
        cfg->addFunSymbol(sym->m_vaddr, sym->m_name);
        queue.push(sym->m_vaddr);
        SPDLOG_LOGGER_DEBUG(m_log, "\tSymbol: {}, addr {:x}, section {}\n", sym->m_name, sym->m_vaddr, binary->getSection(sym->m_vaddr)->getName());
      }
    }

    // HEURISTIC: If we did not find a "main" symbol, we try to get it by
    // searching the first argument of libc_start_main
    if(!cfg->isMainInSymbols()){
      addr_t main_ep = getSupposedMainSymbol(binary, m_log);
      m_log->info("\"main\" was not found\n");
      if(main_ep){
      	m_log->info("\tSupposed main address: {:x} \n", main_ep);
        cfg->addFunSymbol(main_ep, "main_supposed");
        queue.push(main_ep);
      }
    }

    sptrBasicBlock current_BB = NULL;

    SPDLOG_LOGGER_INFO(m_log, "-> Found {} symbols\n", queue.size());
    SPDLOG_LOGGER_INFO(m_log, "--- Beginning disassembly\n");

    while (!queue.empty()){


      addr = queue.front();
      queue.pop();

      SPDLOG_LOGGER_DEBUG(m_log, "----> dequeuing {:x}. Queue size : {}\n", addr, queue.size());

      if (seen.count(addr)){
        continue;
      }

      // prepare args of cs_disasm_iter

      if(binary->getSection(addr) == NULL){
        SPDLOG_LOGGER_DEBUG(m_log, "Found no section containing {:x}\n", addr);
        continue;
      }
      offset = addr - binary->getSection(addr)->getEntryPoint();
      const byte_t *ptr_code = binary->getSection(addr)->getBytesRaw() + offset;
      n = binary->getSection(addr)->getSize() - offset;

      SPDLOG_LOGGER_TRACE(m_log, "\nArgs of cs_disam_iter : addr {:x}; sec ep {:x}; sec size {:x}; offset {:x}; n {}; *ptr_code {:x} \n",addr, binary->getSection(addr)->getEntryPoint(), binary->getSection(addr)->getSize(), offset, n, *ptr_code);


      if (config->getConfigGlobal().print) {
        m_log->info("{0} <{1}> \n", (void *)addr, getSymbolName(addr, binary));
      }

      while (cs_disasm_iter(m_cs_handle, &ptr_code, &n, &addr, m_cs_insn)) {

				ntot_count++;
      	m_log->info("--> {}/{} : {} %  $$  n_tot_inst : {} ------\r",seen.size(), exec_section_inst_size,(float( seen.size() / exec_section_inst_size  * 150)), ntot_count);
        SPDLOG_LOGGER_DEBUG(m_log, "\n---\nDisassing address {:x} \n", m_cs_insn->address);

        // If the current inst is invalid
        if (m_cs_insn->id == X86_INS_INVALID || m_cs_insn->size == 0){
          SPDLOG_LOGGER_DEBUG(m_log, "starting new BB after invalid or 0 sized inst\n");
          if(current_BB != NULL){
            current_BB->addOutgoingInstruction(inst_ptr);
            current_BB = NULL;
          }
          break;
        }

        // IMPORTANT: if the current addr (reached by cs_disasm_iter) is a function symbol
        // AND we did not start this disassembly from this function
        // We stop the disassembly
        // We should then reach that address again and start disassembly again

        // Or ... if we meet a fun symbol and the current_BB is not null
        // we NULL it (start a new BB)

        if(cfg->getFunSymbolsMap().count(m_cs_insn->address) > 0){
          if(current_BB != NULL){
            current_BB->addOutgoingInstruction(inst_ptr);
            current_BB = NULL;
          }
        }

        if(seen.count(m_cs_insn->address)){

          if(current_BB != NULL){ // Case where we need to stich together two basic blocks

            SPDLOG_LOGGER_TRACE(m_log, "Coming from {:x} (previous inst_ptr), {:x} was already seen \n",inst_ptr->getAddr(), m_cs_insn->address);

            if(!is_cs_cflow_ins(inst_ptr)){
              SPDLOG_LOGGER_TRACE(m_log, "And {:x} (previous inst_ptr) was not a jump address ! \n",inst_ptr->getAddr());
              sptrBasicBlock target_BB = cfg->getBasicBlock(m_cs_insn->address);
              if(target_BB != NULL){
                SPDLOG_LOGGER_TRACE(m_log, "Merging bb:\n{}\nAnd bb:\n{}  \n",target_BB->printFullBB(), current_BB->printFullBB());

                // merges the second one into the first one
                /*
                  Get whose edges pointed to target_BB
                */

                cfg->mergeBasicBlocksAndUpdateGraph(target_BB, current_BB, m_log);

                SPDLOG_LOGGER_TRACE(m_log, "Final BB in the cfg:\n{}  \n",target_BB->printFullBB());
              }

            }

            current_BB->addOutgoingInstruction(inst_ptr);
            current_BB = NULL;
            SPDLOG_LOGGER_TRACE(m_log, "starting new BB after already seen addr\n");
          }

          break;
        }

        seen.insert(m_cs_insn->address);

        // information is stored in m_cs_insn
        std::vector<uint8_t> bytes_vect = utils::bytes_to_vect(m_cs_insn->bytes, m_cs_insn->size);

        std::string opcode_str = getCapstoneInstStr(m_cs_insn);

        // Create the instruction object from m_cs_insn
        inst_ptr = std::make_shared<Instr>(
            opcode_str, bytes_vect, csToInstrType(m_cs_insn), InstrSubType::UNKNOWN,
            m_cs_insn->address, (int)0, m_cs_insn->size, m_cs_insn->address + m_cs_insn->size,
            std::nullopt, std::nullopt, binary->getSection(m_cs_insn->address)->getName());
        // NOTE: X86 ONLY (??)

        // fill in this instruction's operand info about its potential registers
        inst_ptr->setOperandCount(m_cs_insn->detail->x86.op_count);

        // And dataflow information
        for(int j=0; j<m_cs_insn->detail->x86.op_count; j++){
          cs_x86_op op = m_cs_insn->detail->x86.operands[j];
          if(op.type == X86_OP_REG){
            inst_ptr->addOperandRegAccess((x86_reg)op.reg, opAccess2str(op.access));
          }
        }

        if (config->getConfigGlobal().print) {
          print_ins(m_cs_insn);
        }

        if (current_BB == NULL) {
          /* If we're starting a new basic block (case at start)
              and after any jump   */
          if (cfg->isBasicBlockExists(m_cs_insn->address)){
            current_BB = cfg->getBasicBlock(m_cs_insn->address); // !
          }
          else{

            std::string opcode_str = getCapstoneInstStr(m_cs_insn);
            current_BB = cfg->createEmptyBB(m_cs_insn->address);
            SPDLOG_LOGGER_DEBUG(m_log, "Created new BB at addr {:x}\n", m_cs_insn->address);
          }
        }

        if(inst_ptr->getAddr() == addr_to_check1){
          SPDLOG_LOGGER_DEBUG(m_log, "\n ----- \n FOUND the addr : {:x}; string is: {} (inst_ptr) / {:x}(m_cs_insn)\n",
               inst_ptr->getAddr(),  inst_ptr->toStringLight(), m_cs_insn->address);
          SPDLOG_LOGGER_DEBUG(m_log, "m_inst size : {}\n", current_BB->getInstrs().size());

          SPDLOG_LOGGER_DEBUG(m_log, "opcode {} \n  BB is\n{}",
               m_cs_insn->mnemonic , current_BB->printFullBB());
          SPDLOG_LOGGER_DEBUG(m_log, "Inst bytes : {}\n", inst_ptr->getBytesString());
          SPDLOG_LOGGER_DEBUG(m_log, "\n ----- \n");
        }

				// now we look at the current disassembled instruction
        if (is_cs_cflow_ins(m_cs_insn)) {

          target = get_cs_ins_immediate_target(m_cs_insn);

          if (target){
            // if target is in any executable section, add an edge
            // and add target to queue if unseen
            for(Section* exe_sec : binary->getExecutableSections()){
              if(exe_sec->contains(target)){
                // if the target is in the current_executable_section
                if (!seen.count(target)) {
                  queue.push(target);
                  if (config->getConfigGlobal().print) {
                    m_log->info("  --> new target: {} \n", (void *)target);
                  }
                }
                // NOTED : CHECK HERE
                inst_ptr->addEdge({0, target}, m_log);
                inst_ptr->SetJmpAddr(target);
                break;
              }
            }
          }
          if (is_cs_unconditional_cflow_ins(m_cs_insn)){
            // In that case we stop the current disas loop without adding edges
            // add current instruction to current BB

            cfg->pushBackInstrInBB(current_BB, inst_ptr);
            SPDLOG_LOGGER_TRACE(m_log, "Added inst {:x} (op : {}, size {}) to BB: \n{}\n", inst_ptr->getAddr(), inst_ptr->getBytesString(), inst_ptr->getSize(),  current_BB->printFullBB() );

            // update edges set in cfg
            /*
            for(auto const& [waddr, edge] : current_BB->getEdges()){
              cfg->insertEdge(edge);
            }
            */
            // start a new BB
            current_BB->addOutgoingInstruction(inst_ptr);
            current_BB = NULL;
            SPDLOG_LOGGER_DEBUG(m_log, "starting new BB after unconditional CF inst\n");

            // stop the current recursive disas loop
            break;
          } else {
            // Else we can at least add an edge to the next address in memory
            // HEURISTIC: but only if the jump target was not imp.exit
            if(cfg->getAddrFunSymbol(target) != "imp.exit"){
              // add edge to next address
                inst_ptr->addEdge({0, inst_ptr->m_next_addr}, EdgeFoundMethod::BOA_DISAS , m_log);
            }

            // add current instruction to BB
            cfg->pushBackInstrInBB(current_BB, inst_ptr);
            SPDLOG_LOGGER_TRACE(m_log, "Added inst {:x} (op : {}, size {}) to BB: \n{}\n", inst_ptr->getAddr(), inst_ptr->getBytesString(), inst_ptr->getSize(),  current_BB->printFullBB() );

            // End BB
            current_BB->addOutgoingInstruction(inst_ptr);
            current_BB = NULL;
            SPDLOG_LOGGER_DEBUG(m_log, "starting new BB after conditional CF inst \n");
          }
        }
        // here, we know it's not a control flow instruction (not any kind of jump,
        // including CALL)
        else if (m_cs_insn->id == X86_INS_HLT) {
          SPDLOG_LOGGER_DEBUG(m_log, "starting new BB after HALT inst\n");
            cfg->pushBackInstrInBB(current_BB, inst_ptr);
            SPDLOG_LOGGER_TRACE(m_log, "Added inst {:x} (op : {}, size {}) to BB: \n{}\n", inst_ptr->getAddr(), inst_ptr->getBytesString(), inst_ptr->getSize(),  current_BB->printFullBB() );

          current_BB->addOutgoingInstruction(inst_ptr);
          current_BB = NULL;
          break;
        } else {
          // we add the instruction to the current block
          cfg->pushBackInstrInBB(current_BB, inst_ptr);
          SPDLOG_LOGGER_TRACE(m_log, "Added inst {:x} (op : {}, size {}) to BB (current_BB): \n{}\n", inst_ptr->getAddr(), inst_ptr->getBytesString(), inst_ptr->getSize(),  current_BB->printFullBB() );
        }
      }

      if (config->getConfigGlobal().print) {
        m_log->info("---------------------\n", queue.size());
      }
    }

  m_log->info("--> {}/{} : {} %  $$  n_tot_inst : {} ----- \n",seen.size(), exec_section_inst_size,(float( seen.size() / exec_section_inst_size  * 150)), ntot_count);
  SPDLOG_LOGGER_INFO(m_log, "--- Starting Clean-up \n\n");
  // clean-up of the CFG (?)
  cleanUp(cfg, m_log);

  return EXIT_SUCCESS;

} 

}// namespace boa
