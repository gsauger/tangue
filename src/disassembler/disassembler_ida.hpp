#ifndef DISASSEMBLER_IDA_H
#define DISASSEMBLER_IDA_H

#include "config.hpp"
#include "common/cfg.hpp"
#include "analysis/function.hpp"
#include "binary/binary.hpp"
#include "utils/utils.hpp"
#include "analysis/analysis.hpp"

//#include <capstone/capstone.h>
// we actually need capstone to gather all the instruction data efficiently yikes

namespace boa{

class DisassemblerIda{
	public:

		DisassemblerIda(std::shared_ptr<Binary> binary, Config* config);
		~DisassemblerIda();

		bool checkForBB2Merge(std::shared_ptr<AnalysisEngine> analysis_engine);
		int recursive_disasm(std::shared_ptr<CFG> cfg, std::shared_ptr<AnalysisEngine> analysis_engine);
		std::string getIdaScriptFolder() const {return m_ida_script_folder;}

	private:
		std::shared_ptr<Binary> m_binary;
		std::shared_ptr<spdlog::logger> m_log;
		std::string m_ida_script_folder;
		csh m_cs_handle;
		cs_insn* m_cs_insn;
};

}
#endif
