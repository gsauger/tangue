#include "config.hpp"
#include "utils/utils.hpp"
#include "utils/handle_results.hpp"

#include "binary/binary_parser_r2.hpp"

#include "disassembler/disassembler_capstone.hpp"
#include "disassembler/disassembler_ida.hpp"

#include "analysis/analysis.hpp"
#include "classification/classification.hpp"
#include "db_manager/db_manager.hpp"

#include "common/cfg_manager.hpp"

#include "../thirdparty/md5/md5.h"

//#include <Eigen/SparseCore>
#include <thread>
#include <chrono>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <signal.h>
#include <sstream>
#include <algorithm>
#include <execution>
#define GET_VARIABLE_NAME(Variable) (#Variable)

namespace boa {

// ----------------- Subcommands functions to process files

bool processBinaryFile(Config *config, std::shared_ptr<spdlog::logger> log,
    std::shared_ptr<AnalysisEngine> analysis_engine, std::shared_ptr<std::mutex> r2_lock=std::shared_ptr<std::mutex>(nullptr), uint i = 0){

  // Create a binary parser and a binary object
		uint multithreading = config->getConfigGlobal().multithreading;
    std::shared_ptr<BinaryParser> bin_parser = std::make_shared<BinaryParserRadare2>();

    SPDLOG_LOGGER_INFO(log, "\t {} Attempting to load binary {}...\n", i, analysis_engine->getBinary()->getName());

		SPDLOG_LOGGER_TRACE(log, "{} waiting for R2_LOCK...\n",i);
		r2_lock->lock();
		SPDLOG_LOGGER_TRACE(log, "{} locked R2_LOCK\n",i);
    // Load binary using binary_parser_r2
    if (bin_parser->loadBinary(analysis_engine->getBinary())) {
      SPDLOG_LOGGER_INFO(log, "\t {} Error loading library\n", i);
			r2_lock->unlock();
			SPDLOG_LOGGER_TRACE(log, "{} unlocked R2_LOCK\n",i);
      return EXIT_FAILURE;
    }

		// analysis engine setup
    analysis_engine->setSitesSize(config->getConfigGlobal().sites_size);

    // disassembly

		r2_lock->unlock();

		SPDLOG_LOGGER_TRACE(log, "{} unlocked R2_LOCK\n",i);
    std::vector<addr_t> CFG_ep_vect{config->getConfigGlobal().current_start_addr};

    // Create cfg object
    //
    std::shared_ptr<CFG> cfg = std::make_shared<CFG>(CFG_ep_vect, 0);

    // Create disassembly engine

    if(config->getConfigGlobal().disassembler == DisassemblerEngine::CAPSTONE){
    	// CAPSTONE disas
    	std::unique_ptr<DisassemblerCapstone> disas_engine = std::make_unique<DisassemblerCapstone>();
      SPDLOG_LOGGER_DEBUG(log,"\t {} Beginning CAPSTONE recursive disassembly...\n", i);
      if (disas_engine->recursive_disasm(analysis_engine->getBinary(), config, cfg)==EXIT_FAILURE) {
        log->info("Something went wrong disassembling the file. Aborting.\n");
        return EXIT_FAILURE;
      }
      // FUNCTION delimitation algo
    	analysis_engine->setCFG(cfg);

    	SPDLOG_LOGGER_INFO(log, "\t {} Beginning function boundaries detection...\n", i);

    	analysis_engine->setFunctionBoundaries();
    	//This takes time;
			if(multithreading == 1){log->info("Bin has {} functions...\n", analysis_engine->getFunctions().size());}
    }

    else if(config->getConfigGlobal().disassembler == DisassemblerEngine::IDA){
    	// IDA disas
    	std::unique_ptr<DisassemblerIda> disas_engine = std::make_unique<DisassemblerIda>(analysis_engine->getBinary(), config);

      SPDLOG_LOGGER_DEBUG(log,"\t {} Beginning IDA  disassembly...\n", i);
      if (disas_engine->recursive_disasm(cfg, analysis_engine)==EXIT_FAILURE) {
        log->info("Something went wrong disassembling the file. Aborting.\n");
        return EXIT_FAILURE;
      }
      analysis_engine->setCFG(cfg);
      //log->info("CFG info : {} \n -------- \n {}\n", cfg->getBasicBlocks().begin()->second->printFullBB(), analysis_engine->getFunctions().begin()->get()->getBasicBlocks().begin()->second->printFullBB());
			if(multithreading == 1){log->info("Bin has {} functions...\n", analysis_engine->getFunctions().size());}
    }

    else{
      log->info("\tOnly capstone or IDA disassembler are supported. Aborting.\n");
      return EXIT_FAILURE;
    }

    if(multithreading == 1){SPDLOG_LOGGER_INFO(log,"\t {} Beginning sites extraction...\n", i);}
    // This is fast
    analysis_engine->extractBinarySites();

    if(analysis_engine->getNumberOfSites() == 0){
      log->info("\t{} has 0 site\n", analysis_engine->getBinary()->getFilepath());
    }

    if(multithreading == 1){log->info("...and {} have sites\n", analysis_engine->getNbFunWithKSites(1));}

    // here we should now compute the dataflows of the Sites of the binary
    if(multithreading == 1){SPDLOG_LOGGER_INFO(log, "\t {} Beginning site dataflow extraction...\n", i);}
    analysis_engine->encodeSites();
    //analysis_engine->hashSites();
    if(!config->getConfigGlobal().sample_distance){
      analysis_engine->extractSitesDataflowGraphs();
    }

    return EXIT_SUCCESS;
}

void thread_job_learn(std::queue<std::string>& jobs, std::mutex& lock, std::unordered_set<sptrAnalysis>& analysis_engine_set , std::mutex& result_lock, std::shared_ptr<std::mutex> r2_lock, std::shared_ptr<spdlog::logger> log, Config* config, uint i){
	while(true){
		//if(!lock.try_lock()){SPDLOG_LOGGER_TRACE(log, "{} waiting for LOCK...\n",i);}
		SPDLOG_LOGGER_TRACE(log, "{} waiting for LOCK...\n",i);
		lock.lock();
		SPDLOG_LOGGER_TRACE(log, "{} locked LOCK\n", i);
		if(jobs.empty()){
			//log->info("xxxxx Thread finished jobs {}\n", i);
			lock.unlock();
			SPDLOG_LOGGER_TRACE(log, "{} unlocked LOCK\n", i);
			break;}
		std::string job = jobs.front();
		jobs.pop();
		lock.unlock();
		SPDLOG_LOGGER_TRACE(log, "{} unlocked LOCK\n", i);

	 const std::string& file_name = job;
   // Create a new analysis engine for the current binary
   sptrBinary binary = std::make_shared<Binary>(file_name);
 	 log->info(" --- Learning file : {} (thread {})\n", binary->getFilepath(), i);
   std::shared_ptr<AnalysisEngine> analysis_engine = std::make_shared<AnalysisEngine>(binary, config);
   // Process the binary like with "learn" subcommand
   if(processBinaryFile(config, log, analysis_engine, r2_lock, i)){
     return;
   }
   result_lock.lock();
   analysis_engine_set.insert(analysis_engine);
   result_lock.unlock();
	}
	return;
}

void thread_job_correlation(std::queue<std::string>& jobs, std::mutex& lock, std::set<sptrAnalysis>& analysis_engine_set , std::mutex& result_lock, std::shared_ptr<std::mutex> r2_lock, std::shared_ptr<spdlog::logger> log, Config* config, std::shared_ptr<ClassificationEngine> classification_engine, uint i){
	while(true){
		//if(!lock.try_lock()){SPDLOG_LOGGER_TRACE(log, "{} waiting for LOCK...\n",i);}
		SPDLOG_LOGGER_DEBUG(log, "{} waiting for LOCK...\n",i);
		lock.lock();
		SPDLOG_LOGGER_DEBUG(log, "{} locked LOCK\n", i);
		if(jobs.empty()){
			//log->info("xxxxx Thread finished jobs {}\n", i);
			lock.unlock();
			SPDLOG_LOGGER_DEBUG(log, "{} unlocked LOCK\n", i);
			break;}
		std::string job = jobs.front();
		jobs.pop();
		log->info("\t {} more to go !\n",jobs.size());
		lock.unlock();
		SPDLOG_LOGGER_DEBUG(log, "{} unlocked LOCK\n", i);
		const std::string& file_name = job;

   // Create a new analysis engine for the current binary
   sptrBinary binary = std::make_shared<Binary>(file_name);

   if(classification_engine->isBinInEngine(binary->getName())){ return; }

 	 log->info(" --- Learning file : {} (thread {})\n", binary->getFilepath(), i);
   std::shared_ptr<AnalysisEngine> analysis_engine = std::make_shared<AnalysisEngine>(binary, config);

   // Process the binary like with "learn" subcommand
   if(processBinaryFile(config, log, analysis_engine, r2_lock, i)){
     return;
   }

   SPDLOG_LOGGER_DEBUG(log, "Result of string parsing: version {} and opt level {}\n",analysis_engine->getBinary()->getBinVersion(), opt2str(analysis_engine->getBinOptLvl()));

		SPDLOG_LOGGER_DEBUG(log, "{} waiting for RESULT_LOCK...\n",i);
   result_lock.lock();

		SPDLOG_LOGGER_DEBUG(log, "{} locked RESULT_LOCK\n", i);
   analysis_engine_set.insert(analysis_engine);
   result_lock.unlock();
		SPDLOG_LOGGER_DEBUG(log, "{} unlocked RESULT_LOCK\n", i);
 }
	return;
}

// ----------------- Run function

bool run(Config *config, std::shared_ptr<spdlog::logger> log) {
//  const auto start_time = std::chrono::steady_clock::now();
  const bool is_static_mode = config->getConfigGlobal().disas_mode == DisassemblyMode::STATIC;

  // Test mode
  if (config->getConfigGlobal().test_mode) {
    // If you want to try something,
    // put some code here and start TANGUE with --test argument

	std::cout << "Hello TEST World" << std::endl;

    return EXIT_SUCCESS;
}

  // If not static mode
  if (!is_static_mode) {log->info("Only static mode is supported. Aborting.\n");
    return EXIT_FAILURE; }

  log->info(" ------------ Welcome to TANGUE!\n");
  config->printOptions(log);
	config->checkForCsvInput(log);
	log->info("Number of binaries to analyze: {}\n", config->getBinFolder().size());

  // Learn mode
  if (config->getConfigGlobal().is_learn_mode){
    log->info("Entering learn mode.\n");

		std::unordered_set<sptrAnalysis> analysis_engine_set;

			uint8_t max_threads = std::thread::hardware_concurrency();
			uint8_t threads = 1;

			if(config->getConfigGlobal().multithreading == 0){ threads = max_threads; }
			else if(max_threads < config->getConfigGlobal().multithreading){ threads = max_threads; }
			else{ threads = (threads > config->getConfigGlobal().multithreading ? threads : config->getConfigGlobal().multithreading );}

			log->info("Using {} cores\n",threads);
			std::vector<std::thread> threads_vect;
			std::queue<std::string> jobs_queue;
  		for(std::string file_name : config->getBinFolder()){
				jobs_queue.push(file_name);
			}
			std::mutex lock, result_lock;
			std::shared_ptr<std::mutex> r2_lock = std::make_shared<std::mutex>();

			for (uint8_t i = 0; i < threads; ++i){
				threads_vect.emplace_back(thread_job_learn, std::ref(jobs_queue), std::ref(lock), std::ref(analysis_engine_set), std::ref(result_lock), r2_lock, log, config, i);
			}

			for (auto& thread: threads_vect){ thread.join(); }
			threads_vect.clear();
			log->info("Analysis engine set SIZE: {}\n", analysis_engine_set.size());
			// we now have a filled analysis_engine_set
    // Give Results
  handleResultsLearn(config, analysis_engine_set, log);
}

  // correlation mode

  else if(config->getConfigGlobal().is_correlation_mode){

    log->info("Entering correlation mode.\n");
    // create a correlation engine, that contains an analysis engine for each binary
    // maybe classification engine is enough ?

    std::shared_ptr<ClassificationEngine> classification_engine = std::make_shared<ClassificationEngine>(config->getConfigGlobal().site_sim_method, config->getConfigGlobal().bgraph_sim_method);

    auto time_tick = std::chrono::system_clock::now();

		uint8_t max_threads = std::thread::hardware_concurrency();
		uint8_t threads = 1;

		if(config->getConfigGlobal().multithreading == 0){ threads = max_threads; }
		else if(max_threads < config->getConfigGlobal().multithreading){ threads = max_threads; }
		else{ threads = (threads > config->getConfigGlobal().multithreading ? threads : config->getConfigGlobal().multithreading );}
		log->info("Using {} core.s\n",threads);

		std::vector<std::thread> threads_vect;
		std::queue<std::string> jobs_queue;
  	for(auto const& file_name : config->getBinFolder()){
			jobs_queue.push(file_name);
		}
		std::mutex lock, result_lock;
		std::shared_ptr<std::mutex> r2_lock = std::make_shared<std::mutex>();
		std::set<sptrAnalysis> analysis_engine_set;

		for (uint8_t i = 0; i < threads; ++i){
			threads_vect.emplace_back(thread_job_correlation, std::ref(jobs_queue), std::ref(lock), std::ref(analysis_engine_set), std::ref(result_lock), r2_lock, log, config, classification_engine, i);
		}

		for (auto& thread: threads_vect){ thread.join(); }
		threads_vect.clear();
		log->info("Analysis engine set SIZE: {}\n", analysis_engine_set.size());
    uint nb_fun_in_aengine_set = 0;
    for(auto const& analysis_engine_ptr : analysis_engine_set){
      classification_engine->addBinaryFunctionsForCorrelation(analysis_engine_ptr, config);
      nb_fun_in_aengine_set += analysis_engine_ptr->getFunctions().size();
    }

    std::chrono::duration<double> elapsed_seconds = std::chrono::system_clock::now() - time_tick;

    log->info("Functions processing elapsed time (cores used : {}): {} sec\n", config->getConfigGlobal().multithreading, elapsed_seconds.count());
    log->info(" ------------ Correlation Computing\n");

    // classification engine is full now, time to compute correlations and print it
    //log->info("We have {} functions in the engine.\n", classification_engine->getCorrelFunctions().size());
    log->info("Computing function correlation matrix...\n");

    // This method is the one using the inclusions core computation
    classification_engine->computeFunctionsCorrelationScores(config);

    uint nb_pairs_engine=0;
    for(auto const& fun_data : classification_engine->getCorrelFunctions()){
    	nb_pairs_engine += fun_data.score_map.size();
    	}
		log->info("We have {} pairs in the csv file, {} functions in the analysis_engine_set, {} correl functions data, and {} unique 'pairs' in the classificatio engine\n",
							config->getConfigGlobal().test_funcs_pairs.size(), nb_fun_in_aengine_set, classification_engine->getCorrelFunctions().size(), nb_pairs_engine);
    log->info(" ------------ Results handling\n");

    // Print the results, print the graph or whatever is written in the config file
    handleResultsCorrelation(config, classification_engine, log);

  }

/*
  else if(config->getConfigGlobal().is_dbdistance_mode){

    std::shared_ptr<DBManager> db_manager = std::make_shared<DBManager>(config->getConfigGlobal().dbdistance_database);

    std::shared_ptr<ClassificationEngine> classification_engine = std::make_shared<ClassificationEngine>(config->getConfigGlobal().site_sim_method,config->getConfigGlobal().bgraph_sim_method);

    // First process data like learn and store it (using processbinaryfile I suppose)
    std::shared_ptr<AnalysisEngine> analysis_engine1 = std::make_shared<AnalysisEngine>();
    if(processBinaryFile(config, log, config->getConfigGlobal().bin_filepath, analysis_engine1)){
      log->info("Could not process binary file.\n");
      return EXIT_FAILURE;
    }
    // Then use the classification engine methods to update its members about common sites and distance

    // deprecated
    //classification_engine->compareBinToDb(config->getConfigGlobal().sample_distance, db_manager, analysis_engine1->getBinName(), config->getConfigGlobal().bin_db_name);
    classification_engine->computeDistanceLikeSigtool(analysis_engine1->getBinName(), config->getConfigGlobal().bin_db_name);

    // Handle results like a champ

    handleResultsDBCompare(config, log, classification_engine);



  }
  */

/*
  // Dbcompare mode
  else if (config->getConfigGlobal().is_dbcompare_mode){

    std::shared_ptr<DBManager> db_manager = std::make_shared<DBManager>(config->getConfigGlobal().dbcompare_database);

    std::shared_ptr<ClassificationEngine> classification_engine = std::make_shared<ClassificationEngine>(config->getConfigGlobal().site_sim_method,config->getConfigGlobal().bgraph_sim_method);

    //db_manager->getCommonSitesAddr(config->getConfigGlobal().bin1_name, config->getConfigGlobal().bin2_name);
    //classification_engine->computeBinSimilarityFromDb(config->getConfigGlobal().sample_distance, db_manager, config->getConfigGlobal().bin1_name, //config->getConfigGlobal().bin2_name);
    classification_engine->computeDistanceLikeSigtool(config->getConfigGlobal().bin1_name, config->getConfigGlobal().bin2_name);

    //handleResultsDBCompare(config, log, classification_engine);

  }
  */

  return EXIT_SUCCESS;

} // run()

} // namespace boa

// MARK: TANGUE entry point

int main(int argc, char *argv[]) {
  // Ignore SIGPIPE
  signal(SIGPIPE, SIG_IGN);

  // Parse user config (argc, argv)
  std::unique_ptr<boa::Config> config;
  try {
    config = std::make_unique<boa::Config>(argc, argv);
  } catch (std::exception const &e) {
    std::cout << "[error] " << e.what() << std::endl;
    return EXIT_FAILURE;
  } catch (int const &r) {
    return r;
  }

  // Get core logger
  auto core_log = spdlog::get(boa::utils::core_logger);

  // Start TANGUE platform
  try {
    return boa::run(config.get(), core_log);
  } catch (std::exception const &e) {
    core_log->error("{}", e.what());
    return EXIT_FAILURE;
  }
}
