#!/bin/bash
FILES2="coreutils-5.93-O1/*"
bin_path="/home/gab/TANGUE/bin/./tangue"
for f in $FILES2; do
    echo "Processing $f file ..."
    $bin_path learn -b $f -D tmp/TANGUE_DATABASE.db -o tmp/tangue_learn_log.txt
done
