#!/bin/bash
FILES1="../examples/coreutils/binaries/coreutils-5.93-O1/*"
OUTPUTFOLDER="../tmp/"

for f in $FILES1; do
   ./sigtool_tangue_sites_addr_comparison_mdec_script.sh $f $OUTPUTFOLDER
done