import argparse

parser = argparse.ArgumentParser(description='Sigtool & Tangue sites addresses comparison script.')
parser.add_argument("input", help="Input log path.")

args = parser.parse_args()

file = open(args.input)
data = file.readlines()
file.close()
#reg_data.split(',')

sigtool_data = data[0].split(",")
sigtool_data[-1] = sigtool_data[-1][:-1] #remove the last \n
tangue_data = data[1].split(",")
tangue_data[-1] = tangue_data[-1]
print("Bin is: " + args.input)
list_common_addr = []

for elt in sigtool_data:
    for elt2 in tangue_data:
        if elt == elt2:
            list_common_addr.append(elt)

output_string = ""
for line in list_common_addr:
    output_string += line
    output_string += ","
output_string=output_string[:-1]
print("Found " + str(len(list_common_addr)) + " sites with tangue AND sigtool:")
print(output_string)
print("\n")

# Sites in tangue not in sigtool

different_sites = []
for elt in tangue_data:
    if list_common_addr.count(elt) == 0:
        different_sites.append(elt)

output_string = ""
for line in different_sites:
    output_string += line
    output_string += ","
output_string=output_string[:-1]
print("Found " + str(len(different_sites)) + " sites with tangue that sigtool did not find:")
print(output_string)
print("\n")


# Sites in sigtool not in tangue

different_sites = []
for elt in sigtool_data:
    if list_common_addr.count(elt) == 0:
        different_sites.append(elt)

output_string = ""
for line in different_sites:
    output_string += line
    output_string += ","
output_string=output_string[:-1]
print("Found " + str(len(different_sites)) + " with sigtool that tangue did not find:")
print(output_string)
print("\n")