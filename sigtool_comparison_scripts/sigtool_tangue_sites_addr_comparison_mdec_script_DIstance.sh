#!/bin/bash

# PREREQUISITE: open database with mdec before, preferably of the same binary as input $1
# Input: a binary, and an output folder
# Output: differences between the sites found with tangue and sigtool

FILEPATH=$1
OUTPUTFOLDER=$2
DISTANCEFOLDER=$3
FINAL_OUPUT="${OUTPUTFOLDER}/final_comparison_log_COREUTILS_01.txt"

#echo "Processing $FILEPATH file..."
# sigtool; saved to outputfolder/sigtool_log_binaryname.txt
#sigtool -l $SIGTOOL_OUTPUT_DB $FILEPATH
# start mdec server
# - first start a new terminal
#exec konsole --noclose -e mdec -p 1234 $SIGTOOL_OUTPUT_DB

for f in ${DISTANCEFOLDER}/*; do
    FILENAME=$(basename $f)
    TANGUE_OUTPUT_PATH="${OUTPUTFOLDER}/tangue_log_${FILENAME}.txt"
    SIGTOOL_OUTPUT_DB="${OUTPUTFOLDER}/sigtool_${FILENAME}.db"
    SIGTOOL_OUTPUT_PATH="${OUTPUTFOLDER}/sigtool_log_${FILENAME}.json"
    PARSED_SIGTOOL_OUTPUT_PATH="${OUTPUTFOLDER}/sigtool_log_${FILENAME}_parsed.txt"

    # tangue learn; saved to outputfolder/tangue_log_binaryname.txt
   echo "Processing $f against $FILEPATH..."
   truncate -s 0 $TANGUE_OUTPUT_PATH
   tangue distance -b $FILEPATH $f -o $TANGUE_OUTPUT_PATH -c

    # mdec command
    ABSOLUTE_FILEPATH=$(readlink -f $f)
    echo "{\"jsonrpc\": \"2.0\",\"method\": \"fulldist\",\"params\": [\"$ABSOLUTE_FILEPATH\"],\"id\": 0}"| nc localhost 1234 > $SIGTOOL_OUTPUT_PATH
    # remove first two lines
    echo "$(tail -n +3 $SIGTOOL_OUTPUT_PATH)" > $SIGTOOL_OUTPUT_PATH

    # parse sigtool log
    python3 mdec_parsing_script_sites_addr.py $SIGTOOL_OUTPUT_PATH > $PARSED_SIGTOOL_OUTPUT_PATH

    # append tangue data to sigtool data
    cat $TANGUE_OUTPUT_PATH >> $PARSED_SIGTOOL_OUTPUT_PATH

    # compare the two
    python3 common_addresses_sigtool_tangue_script.py $PARSED_SIGTOOL_OUTPUT_PATH >> $FINAL_OUPUT
done

echo "Done withe everything; Find your data in $FINAL_OUPUT."
#killall konsole