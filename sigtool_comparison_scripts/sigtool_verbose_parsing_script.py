import argparse

parser = argparse.ArgumentParser(description='Sigtool verbose distance output parsing script.')
parser.add_argument("input", help="Input log path.")

args = parser.parse_args()

file = open(args.input)
reg_data = file.readlines()
file.close()
#reg_data.split(',')

hash_list =[]
output_list = []


for line in reg_data:
    if(line != "\n"):
        if(line.split()[-1] != "failed" and line.split().count("hash") != 0):
            if(line.split()[0] == "hash"):
                #seen_hash_list.append(line.split()[3])
                hash_list.append(line.split()[1])
                #if(seen_hash_list.count(line.split()[-1]) == 0):
                    #output_list.append(line.split()[0] + ",")
                #else:
                    #print("already seen " + str(line.split()[-1]))
            #else:
              #  output_list.append(line)

seen_hash_list = []
for line in reg_data:
    if(line != "\n"):
        #if(line.split()[-1] != "failed" and line.split().count("hash") != 0):
        current_hash = line.split()[-1]
        if(hash_list.count(current_hash)>0):
            if seen_hash_list.count(current_hash) > 0 :
                #print("Already saw hash: " + str(current_hash) + "\n")
                continue
            else:
                seen_hash_list.append(current_hash)
            
            output_list.append(line.split()[0] + ",")

                #seen_hash_list.append(line.split()[3])
                #hash_list.append(line.split()[1])
                #if(seen_hash_list.count(line.split()[-1]) == 0):
                #else:
                    #print("already seen " + str(line.split()[-1]))
            #else:
              #  output_list.append(line)            

output_string = ""
for line in output_list:
    output_string += line
print(output_string)
