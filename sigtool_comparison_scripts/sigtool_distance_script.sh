#!/bin/bash
FILES1="/*"
FILES2="/*"
for f in $FILES1; do
   echo "Processing $f file..."
   sigtool -l tmp/$f.db $f
   sigtool -d tmp/$f.db $FILES2
done