#!/bin/bash
FILEPATH=$1
FILENAME=$(basename $FILEPATH)
FILES1=$FILEPATH
OUTPUT_LOG="../tmp/tangue_distance_log_${FILENAME}.txt"
FILES2="/home/gab/TANGUE/examples/coreutils/binaries/coreutils-5.93-O1/*"
for f in $FILES1; do
  for F in $FILES2; do
   echo "Processing $f file against $F file..."
   tangue distance -b $f $F -o $OUTPUT_LOG  -q
   done
done