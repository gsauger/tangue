import argparse
import json

def selectEltFromListWithValue(dict_list, key, key_value):
    output = []
    for elt in dict_list:
        if key not in elt.keys():
            continue
        if elt[key] == key_value:
            output.append(elt)
    return output

def selectEltFromListWithKey(dict_list, key):
    output = []
    for elt in dict_list:
        if key in elt.keys():
            output.append(elt)
    return output

def selectEltFromListWithUniqueKeyValue(dict_list, key):
    output = []
    seen_key_values = []
    for elt in dict_list:
        if key in elt.keys():
            if elt[key] not in seen_key_values:
                output.append(elt)
                seen_key_values.append(elt[key])
            else:
                print("Already seen " + str(elt[key]))
    return output

parser = argparse.ArgumentParser(description='Mdec fulldist output parsing script.')
parser.add_argument("input", help="Input log path.")

args = parser.parse_args()

with open(args.input) as json_file:
    data = json.load(json_file)

res_data = data["result"]["details"]

sigtool_sites_addr_list = []

for site_dict in selectEltFromListWithKey(res_data, "hash"):
    sigtool_sites_addr_list.append(site_dict["address"])

output_string = ""
for addr in sigtool_sites_addr_list:
    output_string += str(hex(addr))
    output_string += ","
output_string = output_string[:-1]
print(output_string)