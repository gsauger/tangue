#!/bin/bash
FILEPATH=$1
FILENAME=$(basename $FILEPATH)
FILES1=$(find ../examples/coreutils/binaries -name $FILENAME -type f)
OUTPUT_LOG="../tmp/sigtool_distance_ver_opt_log_${FILENAME}.txt"
for f in $FILES1; do
  sigtool -l "../tmp/"
  for F in $FILES1; do
   echo "Processing $f file against $F file..."
   sigtool -d $f $F >> $OUTPUT_LOG
   done
done