#!/bin/bash
FILES1="coreutils-5.93-O1/*"
FILES2="coreutils-5.93-O1/*"
bin_path="/home/gab/TANGUE/bin/./tangue"
for f in $FILES1; do
  for F in $FILES2; do
   echo "Processing $f file against $F file..."
   $bin_path dbcompare -b $f $F -d tmp/TANGUE_DATABASE.db -o tmp/tangue_dbcompare_log.txt -s
   done
done