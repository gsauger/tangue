#!/bin/bash

FILEPATH=$1
OUTPUTFOLDER=$2
FILENAME=$(basename $FILEPATH)
TANGUE_OUTPUT_PATH="${OUTPUTFOLDER}/tangue_log_${FILENAME}.txt"
SIGTOOL_OUTPUT_DB="${OUTPUTFOLDER}/sigtool_${FILENAME}.db"
SIGTOOL_OUTPUT_PATH="${OUTPUTFOLDER}/sigtool_log_${FILENAME}.txt"
PARSED_SIGTOOL_OUTPUT_PATH="${OUTPUTFOLDER}/sigtool_log_${FILENAME}_parsed.txt"
FINAL_OUPUT="${OUTPUTFOLDER}/final_log_${FILENAME}.txt"



echo "Processing $FILEPATH file..."

# tangue learn; saved to outputfolder/tangue_log_binaryname.txt
tangue learn -b $FILEPATH -o $TANGUE_OUTPUT_PATH -c

# sigtool; saved to outputfolder/sigtool_log_binaryname.txt
sigtool -l $SIGTOOL_OUTPUT_DB $FILEPATH
sigtool -d $SIGTOOL_OUTPUT_DB $FILEPATH -v &> $SIGTOOL_OUTPUT_PATH

# parse sigtool log
python3 sigtool_verbose_parsing_script.py $SIGTOOL_OUTPUT_PATH > $PARSED_SIGTOOL_OUTPUT_PATH

# append tangue data to sigtool data
cat $TANGUE_OUTPUT_PATH >> $PARSED_SIGTOOL_OUTPUT_PATH

# compare the two
python3 common_addresses_sigtool_tangue_script.py $PARSED_SIGTOOL_OUTPUT_PATH > $FINAL_OUPUT

echo "Done withe everything; Find your data in $FINAL_OUPUT."