#!/bin/bash

FILEPATH=$1
OUTPUTFOLDER=$2
FILENAME=$(basename $FILEPATH)
TANGUE_OUTPUT_PATH="${OUTPUTFOLDER}/tangue_log_${FILENAME}.txt"
SIGTOOL_OUTPUT_DB="${OUTPUTFOLDER}/sigtool_${FILENAME}.db"
SIGTOOL_OUTPUT_PATH="${OUTPUTFOLDER}/sigtool_log_${FILENAME}.txt"
PARSED_SIGTOOL_OUTPUT_PATH="${OUTPUTFOLDER}/sigtool_log_${FILENAME}_parsed.txt"
FINAL_OUPUT="${OUTPUTFOLDER}/final_log_${FILENAME}.txt"



echo "Processing $FILEPATH file..."

# tangue learn; saved to outputfolder/tangue_log_binaryname.txt
tangue learn -b $FILEPATH -o $TANGUE_OUTPUT_PATH -c

# sigtool; saved to outputfolder/sigtool_log_binaryname.txt
sigtool -l $SIGTOOL_OUTPUT_DB $FILEPATH
dbmanager $SIGTOOL_OUTPUT_DB -j > $SIGTOOL_OUTPUT_PATH

# parse sigtool log
python3 dbmanager_parser_script.py $SIGTOOL_OUTPUT_PATH > $PARSED_SIGTOOL_OUTPUT_PATH

# append tangue data to sigtool data
python3 tangue_get_nb_sites_script.py $TANGUE_OUTPUT_PATH > $PARSED_TANGUE_OUTPUT_PATH
cat $PARSED_TANGUE_OUTPUT_PATH >> $PARSED_SIGTOOL_OUTPUT_PATH

# compare the two
python3 common_sites_sigtool_tangue_script.py $PARSED_SIGTOOL_OUTPUT_PATH > $FINAL_OUPUT

echo "Done with everything; Find your data in $FINAL_OUPUT."