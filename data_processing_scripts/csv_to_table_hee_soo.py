import pandas as pd
import argparse

parser = argparse.ArgumentParser(description='Turns a tangue -o .csv into some nice table for LATEX')
parser.add_argument("--input",dest="input", help="Input path (.csv file).")
parser.add_argument("--output",dest="output", help="Output path (txt/csv file)")

args = parser.parse_args()

df = pd.read_csv(args.input)

df1 = df[['bin1_name', 'bin2_name', 'bin1_common_sites']]
df1 = df1.groupby(['bin1_name', 'bin2_name']).sum().unstack()

# write it in latex form in txt
with open(args.output, "w") as file:
    file.write(df1.to_csv())
    #file.write(new_df.to_latex())