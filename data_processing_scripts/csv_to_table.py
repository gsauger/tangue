import pandas as pd

import argparse

parser = argparse.ArgumentParser(description='Turns a tangue -o .csv into some nice table for LATEX')
parser.add_argument("-i",dest="input", help="Input path (.csv file).")
parser.add_argument("-o",dest="output", help="Output path (txt file)")
parser.add_argument("-f", dest="format", help="'csv' or 'lat', format f the output")

args = parser.parse_args()

# read the csv to dataframe
df = pd.read_csv(args.input)

# variables

values_to_column = 'bin2_name'
values_to_indexes = 'bin1_name'

# group by bin2_name to get new columns
new_columns=(pd.Index(['bin1_nb_sites']))
new_columns=new_columns.append( df.groupby(values_to_column).count().index)
## add bin1_sites numbers (used later)
bin1_site_values = df.groupby('bin1_name')['bin1_sites'].max()

# group by bin1_name to get new indexes
new_indexes=(pd.Index(['bin2_nb_sites']))
new_indexes=new_indexes.append( df.groupby(values_to_indexes).count().index )
## also add bin2 nb sites as a row
bin2_site_values = df.groupby('bin2_name')['bin2_sites'].max()


# create new dataframe with correct indexes and columns
new_df = pd.DataFrame(index=new_indexes, columns=new_columns)

# fill it
for name1 in new_indexes:
    if(name1=='bin2_nb_sites'):
        continue
    for name2 in new_columns:
        if(name2=='bin1_nb_sites'):
            continue

        # here we only have an array of 1 element
        value = df.loc[(df[values_to_indexes]==name1) & (df[values_to_column]== name2)]['bin1_common_sites'].values[0]
        # assign this new value
        new_df.at[name1, name2] = value


for name2 in new_columns:
    if name2 != 'bin1_nb_sites':
        new_df.at['bin2_nb_sites', name2] = bin2_site_values[name2]


# add nbr of sites of bin1
for name1 in new_indexes:
    if name1 != 'bin2_nb_sites':
        new_df.at[name1, 'bin1_nb_sites'] = bin1_site_values[name1]

new_df.at['bin2_nb_sites', 'bin1_nb_sites'] = " "

# write output
with open(args.output, "w") as file:
    file.write(new_df.to_csv())
    if(args.format=='csv'):
        pass
    if(args.format=='lat'):
        file.write(new_df.to_latex())
    else:
        print("Unrecognized output format")

