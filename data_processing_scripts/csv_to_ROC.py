import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, auc, roc_curve
import numpy as np
import random as rd
import argparse
import math
from tqdm import tqdm

### ============= Args
parser = argparse.ArgumentParser(description='Turns a tangue -o .csv into some ROC data')
parser.add_argument("-i",dest="input", help="Input path (.csv file).")
#parser.add_argument("-o",dest="output", help="Output path (txt file)")

args = parser.parse_args()

### ============= Constants

threshold = 0.8
threshold_step = 50

### ============= Read data

data = pd.read_csv(args.input, sep=";")
data = data.drop(columns=data.columns[-1])
np_data = data.to_numpy()
print(np_data)
print(np_data.shape)
print(data.columns)

### ============= Functions

def get_funs_score_except_self(row_number, threshold):
    global data
    output_list = []
    column_index = 0
    for value in np_data[row_number,:] :
        column_index += 1
        if (value > threshold):
            if (row_number != column_index):
                output_list.append(data.columns[column_index])

    return(output_list)

def get_fun_name_from_index(index):
    global data
    return(data.columns[index])

def get_similar_fun_ground_truth(row_index):
    global data
    output_sim = []
    output_disim = []
    row_str = get_fun_name_from_index(row_index)

    for col_index in range(len(data.columns)):
        col_str = get_fun_name_from_index(col_index)
        if(are_similar(row_str, col_str)):
            output_sim .append((row_index, col_index))
        else:
            output_disim .append((row_index, col_index))
    return output_sim, output_disim

def get_random_funs_ground_truth(row_index):
    global data
    output_sim = []
    output_disim = []
    row_str = get_fun_name_from_index(row_index)

    rand_list = [i for i in range(len(data.columns))]

    while len(output_sim) == 0 or len(output_disim) == 0:
        if(len(rand_list) == 0):
            break
        col_index = rd.randint(0, len(rand_list)-1)
        col_str = get_fun_name_from_index(col_index)
        if are_similar(row_str, col_str) and len(output_sim) == 0:
            output_sim.append((row_index, col_index))
        elif ((not are_similar(row_str, col_str))) and len(output_disim) == 0:
            output_disim.append((row_index, col_index))
        rand_list.pop(col_index)
    return output_sim, output_disim

def are_similar(row_str, col_str):
    global data
    #print(row_str, col_str)

    if(row_str.split("/")[-1] != col_str.split("/")[-1]):
        # not same name
        return False
    elif(row_str.split("/")[0].split("_")[2] != col_str.split("/")[0].split("_")[2]):
        # not the same version (subject to change)
        return False
    return True

## == plotting

def plot_roc(y_true, probabilities, figure_title):
    global threshold_step
    threshold_values = np.linspace(0,1,threshold_step)       #Threshold values range from 0 to 1

    FPR_list, TPR_list, thresholds_list = roc_curve(y_true, probabilities, pos_label=1)

    plt.plot(FPR_list, TPR_list)

    return auc(FPR_list, TPR_list)

### ============= Script

fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_aspect('equal', adjustable='box')

# range
plt.xlim([-0.1,1.1])
plt.ylim([-0.1,1.1])


## === ROC curve with my test

pairs_similar = []
pairs_disimilar = []
# gather ground truth
for fun_index in range(len(data.columns)):
    # get pairs for ground truth
    pairs_similar_tmp, pairs_disimilar_tmp = get_similar_fun_ground_truth(fun_index)
    pairs_disimilar += pairs_disimilar_tmp
    pairs_similar += pairs_similar_tmp
    #print(pairs_disimilar)

y_train = []
probability = []


for dis in tqdm(pairs_disimilar):
    if(len(np_data)<=dis[0] or len(np_data)<=dis[1]):
        continue
    y_train.append(0)
    score = np_data[dis]
    probability.append(score)

for sim in tqdm(pairs_similar):
    y_train.append(1)
    score = np_data[sim]
    probability.append(score)

'''
probability = []
for i in range(len(y_train)) :
    score = rd.random()
    probability.append((1-score,score))
'''

auc_full = plot_roc(y_train, probability, "ROC curve - full ground truth")
pairs_nb_full = len(probability)

## === ROC curve with GEMINI's test

#Only one representative for each pair; so 2 pairs for every function
pairs_similar_tmp = []
pairs_disimilar_tmp = []
pairs_disimilar = []
pairs_similar = []
# gather ground truth
for fun_index in range(len(data.columns)):
    # get pairs for ground truth
    pairs_similar_tmp, pairs_disimilar_tmp = get_random_funs_ground_truth(fun_index)
    pairs_disimilar += pairs_disimilar_tmp
    pairs_similar += pairs_similar_tmp
    #print(pairs_disimilar)

y_train = []
probability = []

for dis in tqdm(pairs_disimilar):
    if(len(np_data)<=dis[0] or len(np_data)<=dis[1]):
        continue
    y_train.append(0)
    score = np_data[dis]
    probability.append(score)

for sim in tqdm(pairs_similar):
    y_train.append(1)
    score = np_data[sim]
    probability.append(score)

pairs_nb_gemini = len(probability)
auc_gemini = plot_roc(y_train, probability,"ROC curve - Randomized style ground truth")

print("Full pairs AUC: %.3f ; Randomized pairs AUC: %.3f" %(auc_full, auc_gemini))
# labels
plt.ylabel('TPR')
plt.xlabel('FPR')


# legends
legend_str_full = "Full - AUC = %.3f \n#pairs = %s" %(auc_full, f"{pairs_nb_full:,}")
legend_str_gemini = "Randomized - AUC = %.3f \n#pairs = %s" %(auc_gemini, f"{pairs_nb_gemini:,}")
plt.legend([legend_str_full, legend_str_gemini], loc='lower right')
#plt.legend(["full ground truth; AUC = " + str(round(auc_full, 2)), "GEMINI style ground truth; AUC = " + str(round(auc_gemini)], loc='lower right')

# title
title_str = "TANGUE fun correlation ROC - db '%s' - %i functions" %(args.input.split(".")[-2].split("/")[-1] ,len(data.columns))
plt.title(title_str)
plt.savefig(args.input[:-4] + "_ROC_Curve.png")
# show
plt.show()
