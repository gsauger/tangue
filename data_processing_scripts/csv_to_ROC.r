#!/usr/bin/env Rscript

# @input: input path of csv file
args <- commandArgs(trailingOnly = TRUE)

# test if there is at least one argument: if not, return an error
if (length(args) == 0) {
  stop("At least one argument must be supplied (input file).n", call. = FALSE)
} else if (length(args) == 1) {
  # default output file
  args[2] <- paste(args[1], "_out.txt", sep = "")
}

# global var
data <- read.csv(file = args[1], sep = ";", header = T)

get_funs_score_except_self <- function(row_number, threshold) {
  output_list <- c()
  column_index <- 0
  for (value in data[row_number, ]) {
      column_index <- column_index + 1
      if (value > threshold) {
          if (row_number != column_index) {
            output_list <- c(output_list, names(data)[column_index])
          }
      }
  }
  return(output_list)
}

get_fun_name_from_index <- function(index) {
    return(names(data)[index])
}

threshold <- 0.8

get_similar_fun <- function(row_index) {
    output_sim <- c()
    output_disim <- c()

    for(col_value in data[row_index, ]) {
        if(are_similar(row_index, col_value)){
            output_sim <- c(output, (row_index, col_value))
        }
        else{
            output_disim <- c(output, (row_index, col_value))
        }
    }
}

for (fun_index in seq_len(ncol(data))) {
    # get pairs for ground truth
    pairs_similar <- get_similar_fun(fun_index)
    pairs_disimilar <- get_disimilar_fun(fun_index)

}



.f <- function() {

    for (fun_index in seq_len(ncol(data))) {
        tmp_fun_match <- get_funs_score_except_self(fun_index, threshold)
        print(paste("looking at: ", get_fun_name_from_index((fun_index)), sep = ""))
        if(length(tmp_fun_match) != 0) {
            str(max(tmp_fun_match), vec.len = 6)
        }
        else {
            str(tmp_fun_match, vec.len = 6)
        }
        str("---")
    }

}