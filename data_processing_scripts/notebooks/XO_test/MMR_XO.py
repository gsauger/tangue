import pandas as pd
import argparse
from tqdm  import tqdm
# First we'll just do O0 and O1


#========================== FUNCTIONS

def getFunOpt(func_name):
    # func_name is of the shape "usage_chmod_O1_8.30/usage"
    corel_name = func_name.split("/")[0]
    opt_str = corel_name.split("_")[-2]
    if(int(opt_str[1]) not in [0,1,2,3]):
        raise ValueError('Error computing the optimization level !')
    return int(opt_str[1])

def getGroundTruth(fun_name, fun_data):
    """
    @input:
        - fun_name: string
        - fun_data: dict of scores
    @return:
        - ground_truth: list
    Builds pairs of ground truth AMONG functions appearing i the fun_data dict
    We're going to fetch from the fun_data all the functions with same name as fun_name
    returns the list of functions that matches the fun_data (ground truth)
    basically the functions that share the same name
    """
    ground_truth = []
    # a name is like "usage_chmod_O1_8.30/usage"
    fun1_label = fun_name.split("/")[-1]
    for other_fun_name, score in fun_data.items():
        fun2_label = other_fun_name.split("/")[-1]
        if fun1_label == fun2_label:
            ground_truth.append(other_fun_name)
    return ground_truth

def getFunOfGivenOptScoreDict(row, opt_lvl):
    """
    In a line of the sim matrix (i.e. a function sim score with all other functions), gathers
    scores for all functions of a given opt_lvl.
    @return: a sorted dict {"other_fun_name" : inclu_score}
    """
    # all the scores with other functions in a dict
    out_dict = {}
    for (index_name,index_score) in row.iteritems():
        if(getFunOpt(index_name)==opt_lvl):
            out_dict[index_name] = index_score
    return out_dict

def getRank(qfun_name, func_score_dict, ground_truth, output_log_dict):
    """
    @input:
        - qfun_name: queried function's name (usage_chmod_O1_8.30/usage)
        - func_score_dict: the query function (a dict)
        - ground_truth: list of functions from the list_function that matches with qfunction (list of strings)
    @return:
        - int: the rank of the best matching ground truth functions
    If the ground truth list is empty, skip the function
    Computes the rank of the query "function" in "list_function"
    The rank is the highest "correct" score (correct as in the ground truth) among the sim scores with functions in list_function
    In case of equality we take the worst score possible for function
    We also fill the output_log_dict
    """

    if(len(ground_truth)==0):
        return -1


    output_log_dict[qfun_name] = []

    # get the max score of all the functions of the ground truth
    max_gt_score = -1
    best_matching_gt = ""
    for gt_fun in ground_truth:
        #note: the qfunction objects contains only db functions of the interesting optimiaztion
        gt_score = func_score_dict[gt_fun] # score of the ground truth function
        if(gt_score > max_gt_score):
            max_gt_score = gt_score
            best_matching_gt = gt_fun

    # we now have the best matched gt_fun
    # let's compute its rank
    rank = 1
    for fun_name, fun_score in func_score_dict.items():
        if(fun_score>=max_gt_score):
            # IMPORTANT: the equality here ensures we compute the WORST case
            rank += 1
            output_log_dict[qfun_name].append((fun_name, fun_score))

    # sort the vector
    output_log_dict[qfun_name].sort(key=lambda tup : tup[1], reverse = True)
    return rank

#=========================== CORE SCRIPT

# variables

out_log_dict = {} # fun_name : [(other_fun_name, score)]

# args

parser = argparse.ArgumentParser(description='Turns a tangue -o .csv into some ROC data')
parser.add_argument("-i",dest="input", help="Input path (.csv file).")
parser.add_argument("-o",dest="output", help="output folder.")

args = parser.parse_args()

# import csv

df = pd.read_csv(args.input, delimiter=';')

# last column is empty
df = df.drop(columns=df.columns[-1])

# fun list
fun_complete_list = df.columns

# build {fun:index} dictionnary for later use
fun_index_dict = {}
for i,function in enumerate(fun_complete_list):
    # note: "function" is like "usage_chmod_O1_8.30/usage"
    fun_index_dict[i] = function

# determine optimization pair

opt_i = 0 #first opt level
opt_j = 1 #second one

# gather all fun of opt i, and then j
dict_fscorej_for_fun_i, dict_fscorei_for_fun_j = {}, {} # key: fun_name , value: dict_scores {"other_fun_name":score}

for index, row in df.iterrows():

    current_fun = fun_index_dict[row.name] # current function name (like "usage_chmod_O1_8.30/usage")

    if(getFunOpt(current_fun)==opt_i):
        dict_fscorej_for_fun_i[current_fun] = getFunOfGivenOptScoreDict(row, opt_j)
    if(getFunOpt(current_fun)==opt_j):
        dict_fscorei_for_fun_j[current_fun] = getFunOfGivenOptScoreDict(row, opt_i)

# for all function of optimization O0

MRR = 0

for fun_k_name, fun_k_score_dict in dict_fscorej_for_fun_i.items():
    #fun_i_score_dict : all the relevant functions scores for the query
    ground_truth_fun_k_data = getGroundTruth(fun_k_name, fun_k_score_dict)
    rank_k = getRank( fun_k_name, fun_k_score_dict, ground_truth_fun_k_data, out_log_dict)
    if(rank_k == -1):
        continue
    #print(str(rank_i) + "/" + str(len(dict_fscorej_for_fun_i)))
    MRR += 1./rank_k

MRR = 1./len(dict_fscorej_for_fun_i) * MRR
print("MMR O0 -> O1 : " + str(MRR))

# for all function of optimization O1

MRR = 0

for fun_k_name, fun_k_score_dict in dict_fscorei_for_fun_j.items():
    #fun_i_score_dict : all the relevant functions scores for the query
    ground_truth_fun_k_data = getGroundTruth(fun_k_name, fun_k_score_dict)
    rank_k = getRank( fun_k_name, fun_k_score_dict, ground_truth_fun_k_data, out_log_dict)
    if(rank_k == -1):
        continue
    MRR += 1./rank_k

MRR = 1./len(dict_fscorei_for_fun_j) * MRR
print("MMR O1 -> O0 : " + str(MRR))

# ====================== Output

out_str = ""
for function, tuple_dict in tqdm(out_log_dict.items()):
    out_str += str(function)
    out_str += ";"
    for other_func_name, score in tuple_dict:
        out_str += "(" + str(other_func_name) + "," + str(score) + ")"
        out_str += ";"
    out_str += "\n"

with open(args.output + "/" + args.input.split("/")[-1].split('.')[-2] + ".csv","w") as file:
    file.write(out_str)
