#!/usr/bin/env bash

# arg 1: folder path where c files to move are located

if [ "$#" -lt 1 ]; then
    echo -e "\nusage: $0 files_folder_path\n"
    exit 1
fi

FOLDER_PATH="$1"

cd "$FOLDER_PATH"

for filename in *.c; do
    mkdir "${filename%.c}"
    mv "${filename}" "${filename%.c}/${filename}"
done