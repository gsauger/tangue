#!/bin/bash
FILES1="coreutils-5.93-O0/*"
FILES2="coreutils-8.30-O0/*"
bin_path="/home/gab/TANGUE/bin/./tangue"
for f in $FILES
do
  for F in $FILES2
  do
   echo "Processing $f file against $F file..."
   $bin_path -b $f -B $F -S 10 -o > ${f/.elf/_log}
   done
done