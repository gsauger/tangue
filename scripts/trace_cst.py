#!/usr/bin/env python3

import sys

# A utiliser avec une trace au niveau 2 (extra lite)

if len(sys.argv) <= 1:
    print('Usage: python3 {} txt_trace_filepath'.format(sys.argv[0]))
    exit(-1)


TXT_TRACE_FILEPATH = sys.argv[1]

print('')
print('# Call stack tampering analyser for execution trace')
print('\t* Execution trace filepath: {}'.format(TXT_TRACE_FILEPATH))
print('')

returnsite_stack = []
rets = dict()
last_was_a_ret = False
last_ret = 0x0

cnt = -1

with open(TXT_TRACE_FILEPATH, 'r') as f:
    for line in f:
        splitted_line = line.split()
        # print(splitted_line)
        if len(splitted_line) >= 1:
            if '_0x' not in splitted_line[0]:
                # print('Ignored line: ' + line)
                continue
            addr = int(splitted_line[0].split('_')[1], 0)
            cnt += 1

            # Just pass a RET
            if last_was_a_ret:
                last_was_a_ret = False
                if returnsite_stack and addr == returnsite_stack[-1]:
                    rets[last_ret]['genuine'] += 1
                    # print('{} ret {:#x} --> Target is {:x} (genuine)'.format(cnt, last_ret, addr))
                else:
                    rets[last_ret]['violated'] += 1
                    # print('{} ret {:#x} --> Target is {:x} but expected returnsite is {:x}'.format(cnt, last_ret, addr, returnsite_stack[-1]))
                rets[last_ret]['targets'].add(addr)

            # Update retursite stack
            if returnsite_stack and addr == returnsite_stack[-1]:
                returnsite_stack.pop()

            # Detect static CALL
            if splitted_line[2] == 'CALL':
                returnsite = addr + int(splitted_line[1][1:-1])
                returnsite_stack.append(returnsite)
                # print('{}: {} --> {:#x}'.format(cnt, line.rstrip(), returnsite))

            # Detect RET
            elif splitted_line[2] == 'RET':
                if addr not in rets:
                    rets[addr] = {'genuine': 0, 'violated': 0, 'targets': set()}
                last_was_a_ret = True
                last_ret = addr


print('')
print('#RETs: {}'.format(len(rets)))
print('')

for ret_addr, ret_infos in rets.items():
    print('* RET 0x{:x}:'.format(ret_addr))
    print('\t- #Genuine: {}'.format(ret_infos['genuine']))
    print('\t- #Violated: {}'.format(ret_infos['violated']))
    print('\t- Targets ({}):'.format(len(ret_infos['targets'])))
    for target in ret_infos['targets']:
        print('\t\t* 0x{:x}'.format(target))
