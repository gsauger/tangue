#!/usr/bin/env bash

# exit when any command fails
set -e

echo -e "Start run_g5K.sh script at $(date)\n"



if [ ! -d "dependencies_and_tools_for_boa" ]; then
    mkdir dependencies_and_tools_for_boa
fi
cd dependencies_and_tools_for_boa



# Install tools and dependencies with apt-get to compile and run BOA/BinSec
sudo-g5k apt-get update
sudo-g5k apt-get install -y git build-essential autoconf make curl wget opam protobuf-compiler menhir graphviz libgmp-dev libzmq3-dev llvm-8-dev gperf



# Install Cmake 3.15.6
sudo-g5k apt-get purge -y cmake
if [ ! -f "cmake-3.15.6-Linux-x86_64.sh" ]; then
    wget https://cmake.org/files/v3.15/cmake-3.15.6-Linux-x86_64.sh
fi
chmod +x ./cmake-3.15.6-Linux-x86_64.sh
sudo-g5k ./cmake-3.15.6-Linux-x86_64.sh --skip-license --prefix=/usr



# Install opam and BinSec opam dependencies
opam init -y --compiler=4.04.2
eval $(opam env)
#opam update
#eval $(opam env)
opam install -y depext ocamlfind menhir ocamlgraph piqi zarith zmq llvm.8.0.0 yojson ounit


# Compile and instal Yices
if [ ! -d "yices2-afb9bfd5834b3ead4c9d00b5cad86cec643e73c6" ]; then
    wget https://github.com/SRI-CSL/yices2/archive/afb9bfd5834b3ead4c9d00b5cad86cec643e73c6.zip -O yices2.zip
    unzip yices2.zip
    rm yices2.zip
fi
cd yices2-afb9bfd5834b3ead4c9d00b5cad86cec643e73c6
autoconf
./configure
make
sudo-g5k make install
cd ..


# Compile and install Boolector
# if [ ! -d "boolector-master" ]; then
#     wget -O boolector.zip https://github.com/Boolector/boolector/archive/master.zip
#     unzip boolector.zip
#     rm boolector.zip
# fi
# cd boolector-master
# if [ ! -f "build/bin/boolector" ]; then
#     ./contrib/setup-lingeling.sh
#     ./contrib/setup-btor2tools.sh
#     ./configure.sh
#     cd build
#     make
#     sudo-g5k make install
# else
#     cd build
#     sudo-g5k make install
# fi
# cd ../..


# Install Z3
# if [ ! -d "z3-4.8.7-x64-ubuntu-16.04" ]; then
#     wget -O z3-4.8.7-x64-ubuntu-16.04.zip https://github.com/Z3Prover/z3/releases/download/z3-4.8.7/z3-4.8.7-x64-ubuntu-16.04.zip
#     unzip z3-4.8.7-x64-ubuntu-16.04.zip
#     rm z3-4.8.7-x64-ubuntu-16.04.zip
# fi
# cd z3-4.8.7-x64-ubuntu-16.04/bin
# sudo-g5k cp z3 /usr/local/bin/
# cd ../..


# Install Radare2
if [ ! -d "radare2-cb7d6b4390a6a38d3d9525337d6749248b206dd3" ]; then
    wget https://github.com/radareorg/radare2/archive/cb7d6b4390a6a38d3d9525337d6749248b206dd3.zip -O r2.zip
    unzip r2.zip
    rm r2.zip
fi
cd radare2-cb7d6b4390a6a38d3d9525337d6749248b206dd3
sys/install.sh --install
cd ..

# Compile and install Capstone
if [ ! -d "capstone-4.0.1" ]; then
    wget https://github.com/aquynh/capstone/archive/4.0.1.zip -O capstone_4.0.1.zip
    unzip capstone_4.0.1.zip
    rm capstone_4.0.1.zip
fi
cd capstone-4.0.1
CAPSTONE_ARCHS="x86" CAPSTONE_X86_ATT_DISABLE=yes ./make.sh
sudo-g5k ./make.sh install
cd ..


# Compile BinSec and BOA
cd ~
if [ ! -d "boa-v2" ]; then
    git clone git@gitlab.inria.fr:scecchet/boa-v2.git
fi
cd boa-v2
git fetch origin
git clean -fdx
git submodule foreach --recursive git clean -xfd
git checkout master
git reset --hard origin/master
#git submodule foreach --recursive git reset --hard
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
cd ..


# Now run what you want (here, we are in boa-v2 directory)

./scripts/run_boa.sh yes ./examples/xtunnel -d static --no-disas-call-targets --loglevel-global=3 --loglevel-boa-disas=2 --op-analysis --op-analysis-mt=$(nproc)
#../wcet-benchmark/vanilla_O0_to_O3_experience/2_run_boa_on_binaries.sh
#./scripts/run_WCET2_bench.sh
#./scripts/run_boa.sh no ./examples/WCET/bs
#./scripts/boa_test.sh
#./scripts/run_boa.sh yes ./examples/jcc/jz_cond_false/ --loglevel-global=0
#./scripts/run_boa.sh yes ./examples/WCET2/adpcm/adpcm_O3 --disas-mode boa --loop-lap 0 --solvers boolector cvc4 z3 --parallel-solvers 32 --solvers-timeout 4 --assume-rets-genuine=false --no-compute-dyn-jmps-targets=false --disable-self-modif-analysis=true --loglevel-core 2 --loglevel-bin-parser 2 --loglevel-disassembler 2 --loglevel-rec-disas 2 --loglevel-boa-disas 2 --loglevel-x86-to-smtlib 2 --loglevel-solver 2
#./scripts/run_boa.sh yes ./examples/WCET2/adpcm/adpcm_O0/ --disas-mode static-then-boa --print-csv-results --solvers yices z3 boolector cvc4 --solvers-timeout 10 --assume-rets-genuine=false --no-compute-dyn-jmps-targets=false --disable-self-modif-analysis=true --loglevel-global 2
#./scripts/run_boa.sh yes ./examples/WCET2/fir/fir_O0/ --disas-mode static-then-boa --print-csv-results --solvers yices cvc4 --solvers-timeout 10 --assume-rets-genuine=false --no-compute-dyn-jmps-targets=false --disable-self-modif-analysis=true --loglevel-global 2



echo -e "End of run_g5K.sh script at $(date)\n"



# scp ./scripts/run_g5k.sh grenoble.g5k:/home/scecchetto

# oarsub -l host=1,walltime=10:00:00 -q production -t allow_classic_ssh --notify "mail:sylvain.cecchetto@loria.fr" --stdout=./stdout.txt --stderr=./stdout.txt ./run_g5k.sh
# oarsub -l host=1,walltime=6:00:00 -q production -t allow_classic_ssh --notify "mail:sylvain.cecchetto@loria.fr" --stdout=./stdout.txt --stderr=./stdout.txt ./run_g5k.sh
# oarsub -l host=1,walltime=5:00:00 -q production -t allow_classic_ssh --notify "mail:sylvain.cecchetto@loria.fr" --stdout=./stdout.txt --stderr=./stdout.txt ./run_g5k.sh
# oarsub -l host=1,walltime=2:00:00 -q production -t allow_classic_ssh --notify "mail:sylvain.cecchetto@loria.fr" --stdout=./stdout.txt --stderr=./stdout.txt ./run_g5k.sh
# oarsub -l host=1,walltime=1:00:00 -q production -t allow_classic_ssh --notify "mail:sylvain.cecchetto@loria.fr" --stdout=./stdout.txt --stderr=./stdout.txt ./run_g5k.sh
# oarsub -l host=1,walltime=3:00:00 -t allow_classic_ssh --notify "mail:sylvain.cecchetto@loria.fr" --stdout=./stdout.txt --stderr=./stdout.txt ./run_g5k.sh
# oarsub -l host=1,walltime=2:00:00 -t allow_classic_ssh --notify "mail:sylvain.cecchetto@loria.fr" --stdout=./stdout.txt --stderr=./stdout.txt ./run_g5k.sh
# oarsub -l host=1,walltime=1:00:00 -t allow_classic_ssh --notify "mail:sylvain.cecchetto@loria.fr" --stdout=./stdout.txt --stderr=./stdout.txt ./run_g5k.sh




