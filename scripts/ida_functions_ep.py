import idautils
import idaapi
import idc



# Wait for auto-analysis to finish before running script
idaapi.autoWait()

f = open('/tmp/funcs.txt', 'w')

for func_ep in idautils.Functions():
    f.write('0x%x\n' %(func_ep))

idc.qexit(0)
f.close()


# /Applications/IDA\ Pro\ 7.0/ida.app/Contents/MacOS/idat64 -c -A -Sscripts/ida_functions_ep.py ./examples/xtunnel/xtunnel