#!/usr/bin/env bash

echo "Compile all ASM files from this folder in 32 bit binaries"

find . -name '.DS_Store' -type f -delete
find . -name '._*' -type f -delete

find . -type f -name "*.asm" | while read file_asm; do
    # echo "Compile file: $file_asm"
    file_no_ext=${file_asm%.*}
    command="nasm -f elf32 ${file_no_ext}.asm && gcc -m32 ${file_no_ext}.o -o ${file_no_ext}"
	echo "$command"
    eval $command
done
