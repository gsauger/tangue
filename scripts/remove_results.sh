#!/usr/bin/env bash

# arg 1: Path of the folder where we need to delete BOA results

if [ "$#" -lt 1 ]; then
    echo -e "\nusage: ./script/remove_results.sh folder_path\n"
    echo -e "(e.g. ./scripts/remove_results.sh ./examples/my_binary)"
    exit 1
fi

FOLDER=${1%/}
cd "${FOLDER}"


rm *.dot
rm *.pdf
rm stdout*.txt
rm stdout*.log
rm boa.log
rm boa.log.old