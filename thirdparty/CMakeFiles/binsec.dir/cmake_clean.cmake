file(REMOVE_RECURSE
  "CMakeFiles/binsec"
  "CMakeFiles/binsec-complete"
  "binsec-prefix/src/binsec-stamp/binsec-build"
  "binsec-prefix/src/binsec-stamp/binsec-configure"
  "binsec-prefix/src/binsec-stamp/binsec-download"
  "binsec-prefix/src/binsec-stamp/binsec-install"
  "binsec-prefix/src/binsec-stamp/binsec-mkdir"
  "binsec-prefix/src/binsec-stamp/binsec-patch"
  "binsec-prefix/src/binsec-stamp/binsec-update"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/binsec.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
