/*
 * This is an OpenSSL-compatible implementation of the RSA Data Security,
 * Inc. MD5 Message-Digest Algorithm (RFC 1321).
 *
 * Written by Solar Designer <solar at openwall.com> in 2001, and placed
 * in the public domain.  There's absolutely no warranty.
 *
 * See md5.c for more information.
 */

#ifndef __MD5_H
#define __MD5_H

#include <string.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C"
{
#endif

/* Any 32-bit or wider unsigned integer data type will do */
typedef unsigned int MD5_u32plus;

typedef unsigned char md5_byte_t; /* 8-bit byte */
typedef unsigned int md5_word_t; /* 32-bit word */


/* Define the state of the MD5 Algorithm. */
typedef struct md5_state_s {
    md5_word_t count[2];	/* message length in bits, lsw first */
    md5_word_t abcd[4];		/* digest buffer */
    md5_byte_t buf[64];		/* accumulate block */
} md5_state_t;


typedef struct {
	MD5_u32plus lo, hi;
	MD5_u32plus a, b, c, d;
	unsigned char buffer[64];
	MD5_u32plus block[16];
} md5_ctx;

extern void md5_init(md5_ctx *ctx);
extern void md5_update(md5_ctx *ctx, const void *data, unsigned long size);
extern void md5_final(unsigned char *result, md5_ctx *ctx);

#define MD5_HASH_SIZE 16

#ifdef __cplusplus
}
#endif

#endif
