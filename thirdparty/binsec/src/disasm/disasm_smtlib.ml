open Disasm_options
open Dba

module SS = Set.Make(String)


(* ******************************* *)
(*              Utils              *)
(* ******************************* *)


let remove_line_breaks s =
  let remove_newlines = Str.global_replace (Str.regexp "\n") " " in
  let remove_newlines2 = Str.global_replace (Str.regexp "\r") " " in
  let remove_backslash = Str.global_replace (Str.regexp "\\") "" in
  let s = remove_newlines s in      
  let s = remove_newlines2 s in
  let s = remove_backslash s in
  s

let rec list_elts_to_string l final_s =
  match l with
  | [] -> final_s
  | elt::tl -> 
    let elt = "\"" ^ elt ^ "\"" in
    let final_s = if final_s = "" then elt else final_s ^ ", " ^ elt in
    list_elts_to_string tl final_s

let rec mem_reads_to_json_s l final_s =
  match l with
  | [] -> final_s
  | elt::tl ->
    let size_bytes, addr_expr = elt in
    let size_bytes_s = Printf.sprintf "\"bytes_size\" : %i" size_bytes in
    let addr_expr_s = Printf.sprintf "\"read_location_expr\" : \"%s\"" (remove_line_breaks addr_expr) in
    let block_s = Printf.sprintf "{ %s , %s }" size_bytes_s addr_expr_s in
    let final_s = if final_s = "" then block_s else final_s ^ ", " ^ block_s in
    mem_reads_to_json_s tl final_s


(* ******************************* *)
(*  Disassembly instruction utils  *)
(* ******************************* *)

let check_hex_string s =
  let open String_utils in
  match lfindi s (fun c -> not (is_hex_char c)) with
  | Some i ->
    begin
      let _ = Logger.fatal "Invalid hexadecimal character '%c' in opcode %s" s.[i] s in
      exit 1
    end
  | None -> ()


let inst_of_raw ?base raw =
  check_hex_string raw;
  Binstream.of_nibbles raw
  |> Disasm_core.decode_binstream ?base
  |> fst

let get_instr raw base =
  let i = inst_of_raw ~base raw in
  i


(* ******************************* *)
(*      DBA to SMTLIB utils        *)
(* ******************************* *)

let unary op e =
  match op with
  | Unary_op.Not    -> Formula.mk_bv_not
  | Unary_op.UMinus -> Formula.mk_bv_neg
  | Unary_op.Sext n -> Formula.mk_bv_sign_extend (n - Dba.Expr.size_of e)
  | Unary_op.Uext n -> Formula.mk_bv_zero_extend (n - Dba.Expr.size_of e)
  | Unary_op.Restrict interval -> Formula.mk_bv_extract interval

let as_bv bop e1 e2 =
  Formula.(mk_bv_ite (bop e1 e2) (mk_bv_one) (mk_bv_zero))

let rotate_right_const n = Formula.mk_bv_rotate_right n
let rotate_left_const n = Formula.mk_bv_rotate_left n

let rotate shift_func rev_shift_func const_rot_func value shift =
  let open Formula in
  match shift.bv_term_desc with
  | BvCst x ->
    let op = Bitvector.value_of x |> Bigint.int_of_big_int |> const_rot_func in
    op value
  | _ ->
    let part1 = shift_func value shift
    and shift_size = Formula_utils.bv_size shift
    and value_size = Formula_utils.bv_size value |> Bigint.big_int_of_int in
    let value_size = Bitvector.create value_size shift_size |> mk_bv_cst in
    let offset = mk_bv_sub value_size shift in
    let part2 = rev_shift_func value offset in
    mk_bv_or part1 part2

let rotate_right = rotate Formula.mk_bv_lshr Formula.mk_bv_shl rotate_right_const
let rotate_left = rotate Formula.mk_bv_shl Formula.mk_bv_lshr rotate_left_const

let binary op =
  let open Binary_op in
  match op with
  | Plus   -> Formula.mk_bv_add
  | Minus  -> Formula.mk_bv_sub
  | Mult   -> Formula.mk_bv_mul
  | DivU   -> Formula.mk_bv_udiv
  | DivS   -> Formula.mk_bv_sdiv
  | ModU   -> Formula.mk_bv_urem
  | ModS   -> Formula.mk_bv_srem
  | Eq     -> as_bv (Formula.mk_bv_equal)
  | Diff   -> as_bv (Formula.mk_bv_distinct)
  | LeqU   -> as_bv (Formula.mk_bv_ule)
  | LtU    -> as_bv (Formula.mk_bv_ult)
  | GeqU   -> as_bv (Formula.mk_bv_uge)
  | GtU    -> as_bv (Formula.mk_bv_ugt)
  | LeqS   -> as_bv (Formula.mk_bv_sle)
  | LtS    -> as_bv (Formula.mk_bv_slt)
  | GeqS   -> as_bv (Formula.mk_bv_sge)
  | GtS    -> as_bv (Formula.mk_bv_sgt)
  | Xor    -> Formula.mk_bv_xor
  | And    -> Formula.mk_bv_and
  | Or     -> Formula.mk_bv_or
  | Concat -> Formula.mk_bv_concat
  | LShift -> Formula.mk_bv_shl
  | RShiftU -> Formula.mk_bv_lshr
  | RShiftS -> Formula.mk_bv_ashr
  | LeftRotate -> rotate_left
  | RightRotate -> rotate_right


(* 

* Params:
- dba_expr (Dba.Expr.t): entry expression
- prefix (string): prefix to add on each used variable names
- used_var (SS.t): List of used variable names (with prefix)
- mem_reads List(int * string): List of mem reads (sread size in bytes and smtlib string of the read location)

* Return:
- used_var (SS.t): List of used variable names (with prefix)
- bv_term_expr (Formula.bv_term): entry expression in bv_term type
- mem_reads List(int * string): List of mem reads (sread size in bytes and smtlib string of the read location)
 *)

let rec dba_expr_to_bv_term dba_expr prefix used_var mem_reads =
  let smt_unary = unary and smt_binary = binary in
  let open Dba.Expr in
  match dba_expr with
  
  | Var {name; size; _} ->
    let name = (name ^ prefix) in
    let used_var = SS.add name used_var in
    used_var, (Formula.mk_bv_var (Formula.bv_var name (Size.Bit.to_int (Size.Bit.create size)))), mem_reads
  
  | Cst (_, bv) ->
    used_var, (Formula.mk_bv_cst bv), mem_reads
  
  | Load (bytes_size, _endianness, addr_dba_expr) ->
    let used_var, addr_bv_term, mem_reads = dba_expr_to_bv_term addr_dba_expr prefix used_var mem_reads in
    let addr_term = Formula_to_smtlib.bv_term addr_bv_term in
    let addr_term_s = Format.asprintf "%a" Smtlib_pp.pp_term addr_term in
    let mem_reads = mem_reads@[bytes_size, addr_term_s] in
    let mem_name = ("memory" ^ prefix) in
    let used_var = SS.add mem_name used_var in
    let mem = Formula.(mk_ax_var (ax_var mem_name 32 8)) in
    used_var, (Formula.mk_select bytes_size mem addr_bv_term), mem_reads
  
  | Binary (bop, l_dba_expr, r_dba_expr) ->
    (* Logger.debug ~level:6 "Translating binary %a" Dba_printer.Ascii.pp_bl_term e; *)
    let used_var, l_bv_term, mem_reads = dba_expr_to_bv_term l_dba_expr prefix used_var mem_reads in
    let used_var, r_bv_term, mem_reads = dba_expr_to_bv_term r_dba_expr prefix used_var mem_reads in
    used_var, (smt_binary bop l_bv_term r_bv_term), mem_reads
  
  | Unary (uop, e_dba_expr) ->
    let used_var, e_bv_term, mem_reads = dba_expr_to_bv_term e_dba_expr prefix used_var mem_reads in
    used_var, (smt_unary uop e_dba_expr e_bv_term), mem_reads
  
  | Ite (c_dba_expr, then_dba_expr, else_dba_expr) ->
    let used_var, c_bv_term, mem_reads = dba_expr_to_bv_term c_dba_expr prefix used_var mem_reads in
    let used_var, then_bv_term, mem_reads = dba_expr_to_bv_term then_dba_expr prefix used_var mem_reads in
    let used_var, else_bv_term, mem_reads = dba_expr_to_bv_term else_dba_expr prefix used_var mem_reads in
    used_var, Formula.(mk_bv_ite (mk_bv_equal c_bv_term (mk_bv_one)) then_bv_term else_bv_term), mem_reads


let lvalue_to_def_fun lvalue rvalue_bv_term l_prefix r_prefix used_var_r mem_reads =
  begin match lvalue with
  
  | LValue.Var {name; size; _} ->
    let name = (name ^ l_prefix) in
    let lvalue_bv_var = Formula.bv_var name (Size.Bit.to_int (Size.Bit.create size)) in
    "", 0 , "var_write", name, used_var_r, mem_reads, Formula.mk_bv_def lvalue_bv_var [] rvalue_bv_term;

  | LValue.Restrict ({name; size = bitsize; _}, {Interval.lo; Interval.hi}) ->
    (* Left *)
    let name_l = (name ^ l_prefix) in
    let lvalue_bv_var = Formula.bv_var name_l (Size.Bit.to_int (Size.Bit.create bitsize)) in
    
    (* Right *)
    let name_r = (name ^ r_prefix) in
    let used_var_r = SS.add name_r used_var_r in
    let rvalue_bv_var = Formula.bv_var name_r (Size.Bit.to_int (Size.Bit.create bitsize)) in
    let rvalue_bv_term_temp = Formula.mk_bv_var rvalue_bv_var in
    let concat_lo = lo - 1 and concat_hi = hi + 1 in
    let max_bit = bitsize - 1 in
    let rvalue_bv_term =
      let open Formula in
      match concat_lo < 0, concat_hi > max_bit with
      | false, false ->
        mk_bv_concat
          (mk_bv_extract {Interval.lo=concat_hi; Interval.hi=max_bit} rvalue_bv_term_temp)
          (mk_bv_concat rvalue_bv_term
             (mk_bv_extract {Interval.lo=0; Interval.hi=concat_lo} rvalue_bv_term_temp))
      | true, false ->
        mk_bv_concat
          (mk_bv_extract {Interval.lo=concat_hi; Interval.hi=max_bit} rvalue_bv_term_temp)
          rvalue_bv_term
      | false, true ->
        mk_bv_concat
          rvalue_bv_term
          (mk_bv_extract {Interval.lo=0; Interval.hi=concat_lo} rvalue_bv_term_temp)
      | true, true -> rvalue_bv_term
    in
    "", 0 , "var_write", name_l, used_var_r, mem_reads, Formula.mk_bv_def lvalue_bv_var [] rvalue_bv_term;
  
  | LValue.Store (sz, _, e) ->
    let mem_name_r = "memory" ^ r_prefix in
    let used_var_r = SS.add mem_name_r used_var_r in
    let mem = Formula.mk_ax_var (Formula.ax_var mem_name_r 32 8) in
    let used_var_r, store_addr_bv_term, mem_reads = dba_expr_to_bv_term e r_prefix used_var_r mem_reads in
    let rvalue_ax_term = Formula.mk_store sz mem store_addr_bv_term rvalue_bv_term in

    let store_addr_term = Formula_to_smtlib.bv_term store_addr_bv_term in
    let store_addr_term_s = Format.asprintf "%a" Smtlib_pp.pp_term store_addr_term in
    
    let mem_name_l = "memory" ^ l_prefix in
    let lvalue_ax_var = Formula.ax_var mem_name_l 32 8 in
    store_addr_term_s, sz, "mem_write", mem_name_l, used_var_r, mem_reads, Formula.mk_ax_def lvalue_ax_var [] rvalue_ax_term;
  end


(* ******************************* *)
(*         Main fucntions          *)
(* ******************************* *)

let treat_assign lvalue rvalue json_s =
  let r_prefix = "_r" in
  let l_prefix = "_l" in

  let used_var_r, rvalue_bv_term, mem_reads = dba_expr_to_bv_term rvalue r_prefix SS.empty [] in
  let store_addr_term_s, store_bytes_size, type_s, used_var_l, used_var_r, mem_reads, assign_def = lvalue_to_def_fun lvalue rvalue_bv_term l_prefix r_prefix used_var_r mem_reads in
  
  let used_var_r_string = list_elts_to_string (SS.elements used_var_r) "" in

  Logger.debug "- Used var left: %s" used_var_l;
  Logger.debug "- Used vars right: %s" used_var_r_string;
  
  (* let assigned_value_expr = Formula_to_smtlib.bv_term rvalue_bv_term in *)
  (* Logger.debug "- Assigned value smtlib expr: %a" Smtlib_pp.pp_term assigned_value_expr; *)

  let assign_entry = Formula.mk_define assign_def in
  let smtlib_cmd = Formula_to_smtlib.entry assign_entry in
  Logger.debug "- Smtlib expr: %a" Smtlib_pp.pp_command smtlib_cmd;
  
  (* Json *)
  let json_s = Printf.sprintf "%s, \"type\" : \"%s\"" json_s type_s in
  let json_s = Printf.sprintf "%s, \"left_var\" : \"%s\"" json_s used_var_l in
  let json_s = Printf.sprintf "%s, \"right_vars\" : [%s]" json_s used_var_r_string in
  let json_s = Printf.sprintf "%s, \"mem_readings\" : [%s]" json_s (mem_reads_to_json_s mem_reads "") in
  let json_s =
    if type_s = "mem_write" then
      let size_bytes_s = Printf.sprintf "\"bytes_size\" : %i" store_bytes_size in
      let addr_expr_s = Printf.sprintf "\"write_location_expr\" : \"%s\"" (remove_line_breaks store_addr_term_s) in
      let block_s = Printf.sprintf "{ %s , %s }" size_bytes_s addr_expr_s in
      let json_s = Printf.sprintf "%s, \"mem_writing\" : %s" json_s block_s in
      json_s
    else json_s
  in

  (* let assigned_value_expr_s = Format.asprintf "%a" Smtlib_pp.pp_term assigned_value_expr in *)
  (* let json_s = Printf.sprintf "%s, \"assigned_value_expr\" : \"%s\"" json_s (remove_line_breaks assigned_value_expr_s)  in *)
  let smtlitb_expr_s = Format.asprintf "%a" Smtlib_pp.pp_command smtlib_cmd in
  let json_s = Printf.sprintf "%s, \"smtlib_expr\" : \"%s\"" json_s (remove_line_breaks smtlitb_expr_s)  in
  json_s

let treat_djump jump_dba_expr json_s =
  let type_s = "dyn_jmp" in

  let r_prefix = "_r" in

  let used_var_r, rvalue_bv_term, mem_reads = dba_expr_to_bv_term jump_dba_expr r_prefix SS.empty [] in  
  let used_var_r_string = list_elts_to_string (SS.elements used_var_r) "" in

  let used_var_l = "jumplocationexpr_l" in

  Logger.debug "- Used var left: %s" used_var_l;
  Logger.debug "- Used vars right: %s" used_var_r_string;

  let lvalue_bv_var = Formula.bv_var used_var_l (Size.Bit.to_int (Size.Bit.create 32)) in
  let assign_def = Formula.mk_bv_def lvalue_bv_var [] rvalue_bv_term in
  
  let assign_entry = Formula.mk_define assign_def in
  let smtlib_cmd = Formula_to_smtlib.entry assign_entry in
  Logger.debug "- Smtlib expr: %a" Smtlib_pp.pp_command smtlib_cmd;
  
  (* Json *)
  let json_s = Printf.sprintf "%s, \"type\" : \"%s\"" json_s type_s in
  let json_s = Printf.sprintf "%s, \"left_var\" : \"%s\"" json_s used_var_l in
  let json_s = Printf.sprintf "%s, \"right_vars\" : [%s]" json_s used_var_r_string in
  let json_s = Printf.sprintf "%s, \"mem_readings\" : [%s]" json_s (mem_reads_to_json_s mem_reads "") in

  let smtlitb_expr_s = Format.asprintf "%a" Smtlib_pp.pp_command smtlib_cmd in
  let json_s = Printf.sprintf "%s, \"smtlib_expr\" : \"%s\"" json_s (remove_line_breaks smtlitb_expr_s)  in
  json_s

let treat_cond cond_dba_expr json_s =
  let type_s = "cond" in

  let r_prefix = "_r" in

  let used_var_r, rvalue_bv_term, mem_reads = dba_expr_to_bv_term cond_dba_expr r_prefix SS.empty [] in  
  let used_var_r_string = list_elts_to_string (SS.elements used_var_r) "" in

  let used_var_l = "condexpr_l" in

  Logger.debug "- Used var left: %s" used_var_l;
  Logger.debug "- Used vars right: %s" used_var_r_string;

  let lvalue_bv_var = Formula.bv_var used_var_l (Size.Bit.to_int (Size.Bit.create 1)) in
  let assign_def = Formula.mk_bv_def lvalue_bv_var [] rvalue_bv_term in
  
  let assign_entry = Formula.mk_define assign_def in
  let smtlib_cmd = Formula_to_smtlib.entry assign_entry in
  Logger.debug "- Smtlib expr: %a" Smtlib_pp.pp_command smtlib_cmd;
  
  (* Json *)
  let json_s = Printf.sprintf "%s, \"type\" : \"%s\"" json_s type_s in
  let json_s = Printf.sprintf "%s, \"left_var\" : \"%s\"" json_s used_var_l in
  let json_s = Printf.sprintf "%s, \"right_vars\" : [%s]" json_s used_var_r_string in
  let json_s = Printf.sprintf "%s, \"mem_readings\" : [%s]" json_s (mem_reads_to_json_s mem_reads "") in

  let smtlitb_expr_s = Format.asprintf "%a" Smtlib_pp.pp_command smtlib_cmd in
  let json_s = Printf.sprintf "%s, \"smtlib_expr\" : \"%s\"" json_s (remove_line_breaks smtlitb_expr_s)  in
  json_s


let treat_undef lvalue json_s =
  let type_s = "undef" in

  let l_prefix = "_l" in

  begin match lvalue with
  
  | LValue.Var {name; size; _} ->
    let name = (name ^ l_prefix) in
    Logger.debug "- Used var left: %s" name;
    let lvalue_bv_var = Formula.bv_var name (Size.Bit.to_int (Size.Bit.create size)) in
    let assign_decl = Formula.mk_bv_decl lvalue_bv_var [] in
    let assign_entry = Formula.mk_declare assign_decl in
    let smtlib_cmd = Formula_to_smtlib.entry assign_entry in
    Logger.debug "- Smtlib expr: %a" Smtlib_pp.pp_command smtlib_cmd;
    let json_s = Printf.sprintf "%s, \"type\" : \"%s\"" json_s type_s in
    let json_s = Printf.sprintf "%s, \"left_var\" : \"%s\"" json_s name in

    let smtlitb_expr_s = Format.asprintf "%a" Smtlib_pp.pp_command smtlib_cmd in
    let json_s = Printf.sprintf "%s, \"smtlib_expr\" : \"%s\"" json_s (remove_line_breaks smtlitb_expr_s)  in
    json_s

  | LValue.Restrict (_, _) ->
    exit 1
  
  | LValue.Store (_, _, _) ->
    exit 1
  end



let treat_stop json_s =
  let type_s = "stop" in
  let json_s = Printf.sprintf "%s, \"type\" : \"%s\"" json_s type_s in
  json_s


let treat_assert assert_dba_expr json_s =
  let type_s = "assert" in

  let r_prefix = "_r" in

  let used_var_r, rvalue_bv_term, mem_reads = dba_expr_to_bv_term assert_dba_expr r_prefix SS.empty [] in  
  let used_var_r_string = list_elts_to_string (SS.elements used_var_r) "" in

  let used_var_l = "assertexpr_l" in

  Logger.debug "- Used var left: %s" used_var_l;
  Logger.debug "- Used vars right: %s" used_var_r_string;

  let lvalue_bv_var = Formula.bv_var used_var_l (Size.Bit.to_int (Size.Bit.create 1)) in
  let assign_def = Formula.mk_bv_def lvalue_bv_var [] rvalue_bv_term in
  
  let assign_entry = Formula.mk_define assign_def in
  let smtlib_cmd = Formula_to_smtlib.entry assign_entry in
  Logger.debug "- Smtlib expr: %a" Smtlib_pp.pp_command smtlib_cmd;
  
  (* Json *)
  let json_s = Printf.sprintf "%s, \"type\" : \"%s\"" json_s type_s in
  let json_s = Printf.sprintf "%s, \"left_var\" : \"%s\"" json_s used_var_l in
  let json_s = Printf.sprintf "%s, \"right_vars\" : [%s]" json_s used_var_r_string in
  let json_s = Printf.sprintf "%s, \"mem_readings\" : [%s]" json_s (mem_reads_to_json_s mem_reads "") in

  let smtlitb_expr_s = Format.asprintf "%a" Smtlib_pp.pp_command smtlib_cmd in
  let json_s = Printf.sprintf "%s, \"smtlib_expr\" : \"%s\"" json_s (remove_line_breaks smtlitb_expr_s)  in
  json_s


let treat_sjump json_s =
  let type_s = "static_jmp" in
  let json_s = Printf.sprintf "%s, \"type\" : \"%s\"" json_s type_s in
  json_s



let treat_dba_instr dba_instr json_s =
  match dba_instr with
  | Dba.Instr.Assign (lvalue, rvalue, _) ->
  (* | Dba.Instr.Assign (lvalue, rvalue, idx) -> *)
    Logger.debug "- Type: Assign";
    let json_s = treat_assign lvalue rvalue json_s in
    json_s

  | Dba.Instr.Nondet(_, _, _) ->
  (* | Dba.Instr.Nondet(lvalue, region, idx) -> *)

    Logger.debug "- Type: Nondet";
    (* json_s *)
    exit 1

  | Dba.Instr.SJump (_, _) ->
  (* | Dba.Instr.SJump (jump_target, _) -> *)
    Logger.debug "- Type: SJump";
    let json_s = treat_sjump json_s in
    json_s


  | Dba.Instr.If (condition, _, _) ->
  (* | Dba.Instr.If (condition, jump_target, local_target) -> *)
    Logger.debug "- Type: If";
    let json_s = treat_cond condition json_s in
    json_s

  | Dba.Instr.DJump (je, _) ->
    Logger.debug "- Type: DJump";
    let json_s = treat_djump je json_s in
    json_s

  | Dba.Instr.Undef(lvalue, _) ->
    (* Si un flag est undef alors il prend la valeur 0 ou 1 (on ne sait pas) *)
    Logger.debug "- Type: Undef";
    let json_s = treat_undef lvalue json_s in
    json_s

  | Dba.Instr.Stop _ ->
    Logger.debug "- Type: Stop";
    (* Par exemple dans un DIV s'il y a un débordement *)
    (* Dans le DBA précédente on doit pouvoir trouver le cond afin de détecter une exception *)
    let json_s = treat_stop json_s in
    json_s

  | Dba.Instr.Assert (rvalue, _) ->
    Logger.debug "- Type: Assert";
    let json_s = treat_assert rvalue json_s in
    json_s

  | Dba.Instr.Assume _ ->
    Logger.debug "- Type: Assume";
    (* json_s *)
    exit 1

  | Dba.Instr.NondetAssume _ ->
    Logger.debug "- Type: NondetAssume";
    (* json_s *)
    exit 1

  | Dba.Instr.Malloc _ ->
    Logger.debug "- Type: Malloc";
    (* json_s *)
    exit 1

  | Dba.Instr.Free _ ->
    Logger.debug "- Type: Free";
    (* json_s *)
    exit 1

  | Dba.Instr.Print _ ->
    Logger.debug "- Type: Print";
    (* json_s *)
    exit 1
  


let treat_dhunk dhunk json_s =
  let dhunk_list = Dhunk.to_list dhunk in
  let rec treat_each_dba_instr dhunk_list index json_s =
    match dhunk_list with
    | [] -> json_s
    | dba_instr::tl ->
      Logger.debug "";
      Logger.debug "* DBA n°%i: %a" index Dba_printer.Ascii.pp_instruction dba_instr;
      let json_s = json_s ^ "{" in
      let json_s = Printf.sprintf "%s \"key\" : %i" json_s index in
      let mnemonic_s = Format.asprintf "%a" Dba_printer.Ascii.pp_instruction dba_instr in
      let json_s = Printf.sprintf "%s ,\"mnemonic\" : \"%s\"" json_s (remove_line_breaks mnemonic_s) in
      let json_s = treat_dba_instr dba_instr json_s in
      let json_s = json_s ^ "}," in
      treat_each_dba_instr tl (index + 1) json_s
  in
  let json_s = json_s ^ "\"dba_instrs\" : [" in
  let json_s = treat_each_dba_instr dhunk_list 0 json_s in
  let json_s = String.sub json_s 0 ((String.length json_s) - 1) in
  let json_s = json_s ^ "]" in
  json_s
 


let decode_instr opcode base json_s index =
  try
    (* First thing to do: disassemble raw hex string into the instrction *)
    let json_s =  json_s ^ "{" in (* open current instr *)
    let instr = get_instr opcode base in
    Logger.result "%a" Instruction.pp instr;
    let json_s = Printf.sprintf "%s \"address\": \"0x%Lx\"" json_s (Virtual_address.to_int64 (Instruction.address instr)) in
    let json_s = Printf.sprintf "%s, \"key\" : %i" json_s index in
    let json_s = json_s ^ ", \"opcode\" : \"" ^ opcode ^ "\"" in
    let mnemonic_s = Format.asprintf "%a" Mnemonic.pp (Instruction.mnemonic instr) in
    let json_s = json_s ^ ", \"mnemonic\" : \"" ^ (remove_line_breaks mnemonic_s) ^ "\"," in
    (* Logger.result "Opcode: %s" raw; *)
    (* Logger.result "Mnemonic: %a" Mnemonic.pp (Instruction.mnemonic instr); *)

    (* Now we can iterate over each Dba instr of the instruction *)
    let json_s = treat_dhunk (Instruction.hunk instr) json_s in
    let json_s = json_s ^ "}," in (* close current instr } *)
    json_s
  with
  | X86toDba.InstructionUnhandled s ->
    Logger.warning "Not decoded %s" s;
    exit 1


let decode raw =
  (* Function entry point in case of -disasm-decode-smtlib CLI option *)
  let base = Disasm_at.get () in
  let raw_l = Str.split (Str.regexp "-") raw in
  let json_s = "{ \"instrs\" : [" in (* open root { and instrs tab *)

  let rec decode_each_instr raw_l base json_s cnt =
    match raw_l with
    | [] -> let json_s = String.sub json_s 0 ((String.length json_s) - 1) in json_s
    | elt::tl ->
      let json_s = decode_instr elt base json_s cnt in
      let elt_length = (String.length elt)/2 in
      decode_each_instr tl (base + elt_length) json_s (cnt + 1)
  in

  let json_s = decode_each_instr raw_l base json_s 0 in  
  let json_s = json_s ^ "] }" in (* close instrs tab and root } *)
  
  Logger.result "JSON: %s\n" json_s;
  let json_o = Yojson.Basic.from_string json_s in
  let json_s = Yojson.Basic.pretty_to_string json_o in
  Logger.result "Pretty JSON: \n%s\n" json_s;
  Logger.result "Exit with success!";

  
  ()
