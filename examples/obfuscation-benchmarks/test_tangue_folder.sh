
#!/bin/bash
FILES="basic-algorithms/*.elf"
bin_path="/home/gab/TANGUE/bin/./tangue"
for f in $FILES
do
  echo "Processing $f file..."
  $bin_path -b $f > ${f/.elf/_log}
done