
#!/bin/bash
FILES="basic-algorithms/*.c"
suffix=".elf"
for f in $FILES
do
  echo "Processing $f file..."
  gcc -O0 $f -o ${f/.c/$suffix} -m32
  # take action on each file. $f store current file name
done