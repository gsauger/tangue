BITS 32
EXTERN _exit
GLOBAL _start
SECTION .text

A:
	nop
	nop
	nop
	nop
	ret

B:
	nop
	ret

_start:
	nop
	call A
	nop
	call B
	nop
	call _exit

