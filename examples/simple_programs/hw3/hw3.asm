bits 32
global _start

section .text
       
_start:

A:
        mov eax, 0x90000000
        call C

        add eax, ecx
        jmp eax

C:     
        mov ecx, 0x01234567
        call D
        ret

D:    
        add ecx, 0x01000000
        cmp ecx, [ecx + 0x1]
        jge E
back_from_E:
        ret

E:
        call F
        jmp back_from_E

F:
        mov ecx, 0x02000000
        ret