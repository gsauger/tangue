	.file	"sev-ret.cpp"
	.text
	.globl	_Z2f1i
	.type	_Z2f1i, @function
_Z2f1i:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, -4(%rbp)
	cmpl	$1, -4(%rbp)
	jne	.L2
	movl	$2, %eax
	jmp	.L3
.L2:
	cmpl	$8, -4(%rbp)
	jne	.L4
	movl	$7, %eax
	jmp	.L3
.L4:
	cmpl	$4, -4(%rbp)
	jne	.L5
	movl	$-1, %eax
	jmp	.L3
.L5:
	movl	$0, %eax
.L3:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	_Z2f1i, .-_Z2f1i
	.globl	main
	.type	main, @function
main:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %eax
	movl	%eax, %edi
	call	_Z2f1i
	movl	-4(%rbp), %edx
	addl	%edx, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	main, .-main
	.ident	"GCC: (GNU) 11.1.0"
	.section	.note.GNU-stack,"",@progbits
