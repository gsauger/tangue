bits 32
global main


section .data
    message db 'Hello'

section   .text
    global _start
_start:

main:

A       mov eax, 0x90000000
        mov ebx, eax
        jmp B

B       add eax, ecx
        cmp eax, 0x90000000
        je D

C       mov ecx, 0x01234567
        jmp E

D       jmp 0x45

E       jmp 0x45