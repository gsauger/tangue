#include <iostream>

#define	EXIT_FAILURE	1	/* Failing exit status.  */
#define	EXIT_SUCCESS	0	/* Successful exit status.  */

float f1(float arg);
float f3(float arg);

int main(int argc, char *argv[]) {
    //gather args
    float f1_result=0, f3_result=0, final_result=0;
    if(argc < 2){
        return EXIT_FAILURE;
    }
    f1_result = f1((float)*argv[1]);
    f3_result = f3((float)*argv[2]);

    final_result = f1_result / f3_result;
    std::cout << "Final result is: " << final_result;

    return EXIT_SUCCESS;
}

float f1(float arg){
    return arg*arg;
}

float f3(float arg){
    return std::abs(arg);
}