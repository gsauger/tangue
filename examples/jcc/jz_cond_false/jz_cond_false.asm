bits 32
global main



        section   .text

main:
        mov eax, 0x1
        test eax, eax
        jz label
   		mov ebx, 0x99999999
   		jmp end

label:	mov ebx, 0x88888888
end:	jmp ebx


; To compile: nasm -f elf32 xxxxx.asm && gcc -m32 xxxxx.o -o xxxxx


