bits 32
global main



        section   .text

main:
        test edx, edx
        jz label

        mov eax, 0x2
        mov ebx, 0x1
        jmp end

label:  mov eax, 0x1
        mov ebx, 0x2
        jmp end

end:    xor ecx, ecx
        add ecx, eax
        add ecx, ebx
        add ecx, 0x99999990
        jmp ecx


; To compile: nasm -f elf32 xxxxx.asm && gcc -m32 xxxxx.o -o xxxxx


