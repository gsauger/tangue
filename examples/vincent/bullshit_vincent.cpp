#include <stdio.h>

int fun (int a, int val)
{
	if (a == 0)
		return val;
	else
		return fun (a - 1, val + 1);
}

int main (void)
{   int a;
    scanf("%i", &a);
	printf("fun(%i, 0) = %i\n",a, fun(a, 0));
	return 0;
}
