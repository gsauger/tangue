; Petit programme pour tester la divisions
; Celui-ci doit lever une exception de type overflow sur le quotient

; To assemble (RAW binary) --> nasm -f bin xxxx.asm


; DIV r/m32    Unsigned divide EDX:EAX by r/m32

bits 32
global main

        section   .text

main:

        mov eax, 0xffffffff
        mov edx, 0xfffffff
        mov ecx, 0x1
        div ecx
        ret



