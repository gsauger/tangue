#include <stdlib.h>



#ifdef DEBUG
#include <stdio.h>
#endif

typedef unsigned char uint8_t;


int check_char_0(char chr) {
    register uint8_t ch = (uint8_t) chr;
    ch ^= 97; // 97 = 0x61

    if(ch != 92) {
        exit(1);
    }
    else{
        return 0;
    }
}

int check_char_1(char chr) {
    register uint8_t ch = (uint8_t) chr;
    ch ^= 107;
    ch += 67;

    if(ch != 105) {
        exit(1);
    }
    else{
        return 0;
    }
}

int check_char_2(char chr) {
    register uint8_t ch = (uint8_t) chr;
    ch += 61;
    ch *= 2;

    if(ch != 252) {
        exit(1);
    }
    else{
        return 0;
    }
}

int check_char_3(char chr) {
    register uint8_t ch = (uint8_t) chr;
    ch ^= 149;

    if(ch != 219) {
        exit(1);
    }
    else{
        return 0;
    }
}

int check_char_4(char chr) {
    register uint8_t ch = (uint8_t) chr;
    ch ^= 19;
    ch *= 2;

    if(ch != 142) {
        exit(1);
    }
    else{
        return 0;
    }
}

int check_char_5(char chr) {
    register uint8_t ch = (uint8_t) chr;
    ch ^= 5;
    ch *= 3;

    if(ch != 228) {
        exit(1);
    }
    else{
        return 0;
    }
}

int check_char_6(char chr) {
    register uint8_t ch = (uint8_t) chr;
    ch += 71;

    if(ch != 138) {
        exit(1);
    }
    else{
        return 0;
    }
}

int check_char_7(char chr) {
    register uint8_t ch = (uint8_t) chr;
    ch ^= 41;

    if(ch != 102) {
        exit(1);
    }
    else{
        return 0;
    }
}

int check_char_8(char chr) {
    register uint8_t ch = (uint8_t) chr;
    ch += 41;
    ch += 53;

    if(ch != 176) {
        exit(1);
    }
    else{
        return 0;
    }
}

int check_char_9(char chr) {
    register uint8_t ch = (uint8_t) chr;
    ch ^= 61;
    ch += 41;
    ch += 11;

    if(ch != 172) {
        exit(1);
    }
    else{
        return 0;
    }
}

int check_char_10(char chr) {
    register uint8_t ch = (uint8_t) chr;
    ch ^= 47;
    ch += 29;
    ch += 67;

    if(ch != 114) {
        exit(1);
    }
    else{
        return 0;
    }
}


int check(char *buf) {
    int r = 0;
    r = r + check_char_0(buf[0]);
    r = r + check_char_1(buf[1]);
    r = r + check_char_2(buf[2]);
    r = r + check_char_3(buf[3]);
    r = r + check_char_4(buf[4]);
    r = r + check_char_5(buf[5]);
    r = r + check_char_6(buf[6]);
    r = r + check_char_7(buf[7]);
    r = r + check_char_8(buf[8]);
    r = r + check_char_9(buf[9]);
    r = r + check_char_10(buf[10]);

    return r;
}

int main(int argc, char *argv[]) {
    //char buf[12] = "=MANTICORE=";
    char buf[12];

    // 3d   4d  41  4e  54  49  43  4f  52  45  3d
    // =    M   A   N   T   I   C   O   R   E   =

#ifdef DEBUG
    puts("Enter code:\n");
    fgets(buf, 12, stdin);
#endif


    int r = check(buf);

    if(r == 0){

#ifdef DEBUG
    puts("Success!\n");
#endif

        return 0;
    }
    else
    {
        return 1;
    }
}


