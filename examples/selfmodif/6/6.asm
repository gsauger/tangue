; Petit programme pour tester la détection d'auto-modification dans BOA

; On modifie l'adresse d'une instruction au milieu d'un BB qui est en cours d'exécution et qui a déjà été exécuté une permière fois se self-modifier
; Au moment de la deuxième exécution de la ligne ligne C, on va exécuter un NOP et pas un RET

; To assemble (RAW binary) --> nasm -f bin xxxx.asm


bits 32
global main

        section   .text

main:

A       mov eax, 0x12345678
        xor ebx, ebx
        push B
        jmp B

B       inc ebx
        mov byte[eax], 0x90
        mov eax, C
        dec ebx
C       ret
