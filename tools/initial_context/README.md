# Results of dumped initial contexts

## Windows

### Windows 7

```
                          Host A                  Host B

ENTRYPOINT              0x00401000              0x00401000
EDI                     0x00000000              0x00000000
ESI                     0x00000000              0x00000000
EBP                     0x0028ff94              0x0022ff94
ESP                     0x0028ff88              0x0022ff88
EBX                     0x7efde000              0x7ffd4000
EDX                     0x00401000              0x00401000
ECX                     0x00000000              0x00000000
EAX                     0x76e63398              0x7729ef7a
EFLAGS                  0x00000246              0x00000246
```

### Windows 10 (Jul. 2020)

```
                           Host

ENTRYPOINT              0x00401000
EDI                     0x00401000
ESI                     0x00401000
EBP                     0x0060ff80
ESP                     0x0060ff70
EBX                     0x003a6000
EDX                     0x00401000
ECX                     0x00401000
EAX                     0x0060ffcc
EFLAGS                  0x00000246
```
