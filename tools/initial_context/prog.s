; Dumps the initial context of a binary
;
; Compilation
;       Windows
;               $ nasm -f win32 -o prog.o prog.s
;               $ i686-w64-mingw32-gcc -m32 -nostartfiles -o prog.exe prog.o

bits 32

extern _printf
extern _exit

                        section .text

                        global entry

entry:
                        pushfd          ; EFLAGS
                        pushad          ; General-Purpose

                        push print_fmt  ; Leak stack
                        call _printf

                        push 0
                        call _exit

                        section .data

print_fmt               db "EDI: %x ESI: %x EBP: %x ESP: %x EBX: %x EDX: %x ECX: %x EAX: %x EFLAGS: %x", 10, 0
