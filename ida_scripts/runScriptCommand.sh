#!/bin/bash
# first arg: path to the bin to be analyzed
# second arg: path to the ida script folder

BIN_NAME=$(basename $1)
IDA_FOLDER=$2

idat64 -A -S"$IDA_FOLDER/ExtractBinaryViaIDA2.py $IDA_FOLDER/output/" -L$IDA_FOLDER/output/$BIN_NAME\.log -c -o"$IDA_FOLDER/output" $1
