# FROM ocaml/opam:ubuntu-18.04-ocaml-4.07
FROM ubuntu:latest
ARG DEBIAN_FRONTEND=noninteractive
USER root
# Install dependencies
RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y cmake autoconf wget curl graphviz gperf libgmp-dev pkg-config python2.7 libzmq3-dev llvm-11-dev protobuf-compiler python3-pip
RUN apt-get install -y gcc unzip zip
#RUN apt-get install -y opam
#RUN opam init -y
#RUN opam update && opam install -y ocamlfind menhir ocamlgraph piqi zarith zmq llvm.11.0.0 yojson ounit qcheck seq
# Update cmake
RUN apt remove -y cmake && pip3 install cmake --upgrade

# Install Radare2
RUN wget https://github.com/radareorg/radare2/archive/7ed581d2dee432c685420524f1b1eaa723db273d.zip -O r2.zip && \
unzip r2.zip && \
cd radare2-7ed581d2dee432c685420524f1b1eaa723db273d && \
sys/install.sh --install && \
cd .. && \
rm -rf r2.zip radare2-7ed581d2dee432c685420524f1b1eaa723db273d

# Install Capstone
RUN wget https://github.com/aquynh/capstone/archive/4.0.1.zip -O capstone_4.0.1.zip && \
unzip capstone_4.0.1.zip && \
cd capstone-4.0.1 && \
CAPSTONE_ARCHS="x86" CAPSTONE_X86_ATT_DISABLE=yes ./make.sh && \
./make.sh install && \
cd .. && \
rm -rf capstone_4.0.1.zip capstone-4.0.1

WORKDIR /app

RUN adduser normaluser

RUN chown -R normaluser /app

USER normaluser

COPY --chown=normaluser . /app

USER root
# Compile BinSec and TANGUE (or just TANGUE ?)
RUN gcc --version
RUN mkdir build && cd build && \
cmake -DCMAKE_BUILD_TYPE=Release .. &&\
make -j 4 tangue
ENV PATH "$PATH:/app/bin"
