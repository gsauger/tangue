# TANGUE (boa-bis)

## HOW-TO get TANGUE source code

```bash
# Use SSH if you don't want to fill in your Inria credentials
git clone --recurse-submodules git@gitlab.inria.fr:gsauger/tangue.git

# OR use HTTPS
git clone --recurse-submodules https://gitlab.inria.fr/gsauger/tangue.git
```

## HOW-TO get latest changes from GitLab

```bash
cd tangue
git pull --recurse-submodules
```

## HOW-TO compile TANGUE

### Local machine

#### DOCKER FILE

```bash
git clone https://gitlab.inria.fr/gsauger/tangue.git
cd tangue/

# build the image
docker build -t tangue .

#run the container
docker run -ti tangue

# If you need to access data from your host machine use this command
docker run -ti -v /path/to/folder/to/share:/app/share boa

# From what I understand, when you exit the docker all data is lost. Make sure to share it before exiting the docker !
```

#### Install compilation and runtime dependencies (macOS case - Directly taken form BOA)

```bash
# If you do not have it yet, install Brew (THE macOS package manager)
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Install needed tools to compile TANGUE and BinSec
brew install cmake autoconf make pkg-config wget gmp zmq graphviz llvm@11 opam protobuf menhir

# Install Radare2 framework if you don't have it
cd /tmp && \
wget https://github.com/radareorg/radare2/archive/7ed581d2dee432c685420524f1b1eaa723db273d.zip -O r2.zip && \
unzip r2.zip && \
cd radare2-7ed581d2dee432c685420524f1b1eaa723db273d && \
sys/install.sh --install

# Install Capstone disassembler if you don't have it
cd /tmp && \
wget https://github.com/aquynh/capstone/archive/4.0.1.zip -O capstone_4.0.1.zip && \
unzip capstone_4.0.1.zip && \
rm capstone_4.0.1.zip && \
cd capstone-4.0.1 && \
CAPSTONE_ARCHS="x86" CAPSTONE_X86_ATT_DISABLE=yes ./make.sh && \
sudo ./make.sh install

```

#### Install compilation and runtime dependencies (Linux case)

```bash
# Install needed tools to compile TANGUE and BinSec
apt install git build-essential autoconf make curl wget opam protobuf-compiler menhir graphviz libgmp-dev libzmq3-dev llvm-11-dev gperf graphviz cmake perl pkg-config

# Install Radare2 framework if you don't have it
cd /tmp && \
wget https://github.com/radareorg/radare2/archive/cb7d6b4390a6a38d3d9525337d6749248b206dd3.zip -O r2.zip && \
unzip r2.zip && \
cd radare2-cb7d6b4390a6a38d3d9525337d6749248b206dd3 && \
sys/install.sh --install

# Install Capstone disassembler if you don't have it
cd /tmp && \
wget https://github.com/aquynh/capstone/archive/4.0.1.zip -O capstone_4.0.1.zip && \
unzip capstone_4.0.1.zip && \
rm capstone_4.0.1.zip && \
cd capstone-4.0.1 && \
CAPSTONE_ARCHS="x86" CAPSTONE_X86_ATT_DISABLE=yes ./make.sh && \
sudo ./make.sh install

```


#### Compile TANGUE

```bash
cd tangue
mkdir build && cd build

# Option 1: Use make generator
cmake ..
make tangue # Will compile TANGUE and move executable in 'bin' directory. Add -j option if you want to go fast

# Option 2: Use Xcode generator (macOS - taken from BOA)
cmake .. -G Xcode # Will generate Xcode project file
open TANGUE_v2.xcodeproj # Will open TANGUE project in Xcode
# Notice the 'targets' selector at the top left of the window
# If you need BinSec (needed for TANGUE disassembly mode), select 'binsec' target and hit CMB-B to build BinSec
# To compile TANGUE, select 'tangue' target and hit CMD-B to build TANGUE
```

## HOW-TO run TANGUE

```bash
# Add bin folder to the PATH (so that boa finds tangue binary)
export PATH=/path/to/bin/folder/:$PATH

# List all available options with
tangue -h
```

### The basics

tangue *options* _subcommand_  *subcommand_options*

Interesting options and flags:

### Before choosing subcommand:

Options:

`-S 3` to specify the size of the sites of the functions (here sites of size 3).
`-M` to enable multithreading
`-m` and `-g` to specify diverse graph comparison techniques (useful for benchmarks)

### subcommand

Two of them are useful:

`learn` to process a single binary (see `tangue learn -h` for output options)

`correlation` to extract signatures from binary functions and compute similarity scores between them

### After subcommand:

Common options:

`-b` to specify the binary/folder path. Bash-like globbing is supported.
`-o` to specify output file;

## First examples

I currently use coreutils binaries (found in `examples/coreutils_good/`) to test `tangue`.

"Learn" a binary and output its CFG:
`tangue -S 4 basename/coreutils-5.93-O0-basename -o /tmp/log_basename.txt -p /tmp/` to analyse basename 5.93 O0. it outputs an analysis log (.txt file) and a CFG (.pdf).

"Compare" two binarie's functions:
`tangue -S 4 -M correlation -b basename/* -o /tmp/all_basename` to compare all functions from the different compiled versions of basename that I have. Uses multi-threading, and outputs the .csv file containing the similarity scores in `/tmp/all_basename.csv`

## Data processing

I have a script: `data_processing_scripts/csv_to_ROC.py` that computes the AUC of the binary classification task that is recognizing the name of anonymous analyzed functions based on a signature database (see *Gemini* paper).

Just launch `python3 csv_to_ROC.py -i path/to/tangue_correlation_output.csv`. A ROC curve is saved at the `.csv` location. The score is also printed in the console.

## Advanced config options

You can configure TANGUE with the multitude of arguments given by `./bin/tangue -h`. Or you can create a config file `config.ini` and put TANGUE arguments in this file, then you only need to give this file to TANGUE with `./bin/tangue -b my_binary -c config.ini`.

You can print a detailed description and default value of each setting with `./bin/tangue -h`.

