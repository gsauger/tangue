\beamer@sectionintoc {1}{Introduction}{7}{0}{1}
\beamer@sectionintoc {3}{Main algorithm - TANGUE}{10}{0}{2}
\beamer@subsectionintoc {3}{1}{Framework presentation}{10}{0}{2}
\beamer@subsectionintoc {3}{2}{Binary loading - RADARE2}{11}{0}{2}
\beamer@subsectionintoc {3}{3}{Disassembly - CAPSTONE}{12}{0}{2}
\beamer@subsubsectionintoc {3}{3}{1}{Dissasembly algorithm - main function}{13}{0}{2}
\beamer@sectionintoc {4}{Control Flow Graph (CFG)}{15}{0}{3}
\beamer@subsectionintoc {4}{1}{Definition}{15}{0}{3}
\beamer@subsectionintoc {4}{2}{Reduced CFG (rCFG)}{16}{0}{3}
\beamer@sectionintoc {5}{Functions}{17}{0}{4}
\beamer@subsectionintoc {5}{1}{Definition}{17}{0}{4}
\beamer@subsectionintoc {5}{2}{Function boundaries delimitation}{18}{0}{4}
\beamer@sectionintoc {6}{Dataflow of a CFG}{22}{0}{5}
\beamer@subsectionintoc {6}{1}{Dataflow: registers taint propagation}{22}{0}{5}
\beamer@subsubsectionintoc {6}{1}{1}{Registers categories}{25}{0}{5}
\beamer@subsectionintoc {6}{2}{Definition - Dataflow tensor (DF Tensor)}{27}{0}{5}
\beamer@subsectionintoc {6}{3}{DF Tensor computation procedure}{28}{0}{5}
\beamer@subsubsectionintoc {6}{3}{1}{Strongly Connected Components Graph (SCCG)}{29}{0}{5}
\beamer@subsubsectionintoc {6}{3}{2}{SCC dataflow computation algorithm}{30}{0}{5}
\beamer@subsubsectionintoc {6}{3}{3}{SCCG dataflow tensor computation algorithm}{31}{0}{5}
\beamer@sectionintoc {7}{SuperSites}{33}{0}{6}
\beamer@subsectionintoc {7}{1}{Sites}{33}{0}{6}
\beamer@subsubsectionintoc {7}{1}{1}{Definition}{33}{0}{6}
\beamer@subsubsectionintoc {7}{1}{2}{Site perimeter}{34}{0}{6}
\beamer@subsubsectionintoc {7}{1}{3}{Site cutting algorithm}{36}{0}{6}
\beamer@subsectionintoc {7}{2}{Augmented Site: SuperSite}{38}{0}{6}
\beamer@subsectionintoc {7}{3}{SuperSites isomorphisms}{39}{0}{6}
\beamer@sectionintoc {8}{Applications}{40}{0}{7}
\beamer@subsectionintoc {8}{1}{What to do with a SuperSite ?}{40}{0}{7}
\beamer@sectionintoc {9}{Performance evaluation}{41}{0}{8}
\beamer@subsectionintoc {9}{1}{Sigtool comparison}{41}{0}{8}
\beamer@subsectionintoc {9}{2}{Version / Optimization matrix benchmark}{42}{0}{8}
\beamer@subsubsectionintoc {9}{2}{1}{False positives evaluation}{43}{0}{8}
\beamer@subsectionintoc {9}{3}{Function correlation benchmark}{44}{0}{8}
\beamer@subsubsectionintoc {9}{3}{1}{Protocol description}{44}{0}{8}
\beamer@sectionintoc {10}{Future work}{47}{0}{9}
\beamer@subsectionintoc {10}{1}{Obfuscations}{47}{0}{9}
\beamer@sectionintoc {11}{Annex}{48}{0}{10}
\beamer@subsubsectionintoc {11}{0}{1}{Site encoding}{50}{0}{10}
\beamer@sectionintoc {12}{Bibliography}{53}{0}{11}
